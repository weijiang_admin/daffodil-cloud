package com.daffodil.tenant.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.daffodil.core.jdbc.DaffodilDataSourceConstant;
import com.daffodil.core.jdbc.DaffodilDataSourceContextHolder;
import com.daffodil.framework.context.TenantContextHolder;

/**
 * 切换租户（数据源）切面
 * @author yweijian
 * @date 2024年5月9日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Aspect
@Order(-2)
@Component
@ConditionalOnProperty(name = DaffodilDataSourceConstant.DATASOURCE_MULTIPLE_PREFIX_KEY, havingValue = "true")
public class DaffodilDataSourceTenantAspect {

    @Pointcut("@within(org.springframework.stereotype.Service) || @within(org.springframework.stereotype.Repository)")
    public void daffodilDataSourceTenantPointCut() {

    }

    @Before("daffodilDataSourceTenantPointCut()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        String tenantId = TenantContextHolder.getTenantHolder();
        String dataSourceLookupKey = DaffodilDataSourceContextHolder.getDataSourceLookupKey(tenantId);
        if(DaffodilDataSourceContextHolder.containsDataSource(dataSourceLookupKey)) {
            DaffodilDataSourceContextHolder.setDataSourceLookupKey(dataSourceLookupKey);
        }else {
            DaffodilDataSourceContextHolder.setDataSourceLookupKey("primary");
        }
    }

    @After("daffodilDataSourceTenantPointCut()")
    public void doAfter(JoinPoint joinPoint) {
        DaffodilDataSourceContextHolder.clearDataSourceLookupKey();
    }
}
