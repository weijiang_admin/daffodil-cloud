## Daffodil-cloud（达佛迪尔）基础管理平台微服务版`免费开源`
使用Java语言编程的代码简洁，通俗易读的微服务框架的基础项目脚手架。使用Maven搭建的SpringCloud工程项目，支持多数据源注解式动态切换，前端使用vue3渐进式框架搭建的漂亮大方的管理平台界面。 本人出生地水仙花之都【[漳州](http://www.zhangzhou.gov.cn/)】Daffodil英文翻译意思是水仙花，本人也比较佛系且Daffodil音含 [ 达佛 ]。

### 1. 介绍
达佛迪尔基础管理平台（英文名：Daffodil-cloud）乃一款基于SpringCloud微服务架构体系精心构建的旗舰级管理系统，其显著特色在于代码设计的精炼雅致、阅读理解上的轻松易懂，以及界面呈现的美学质感。该平台深蕴技术底蕴，支持多数据源动态切换的灵活机制，兼容ShardingSphere主从读写分离配置等高级特性，从而实现了数据处理效能与系统稳定性的卓越平衡。

在技术栈层面，达佛迪尔基础管理平台采用了业界领先的技术框架和工具组件，包括但不限于Java后端领域的SpringCloud(2021.0.1+版本)，以其强大而敏捷的SpingBoot(2.7.4+版本)为开发引擎；倚仗大名鼎鼎的Shardingsphere(5.1.1+版本)对数据库层进行智能分片与治理；采用阿里巴巴开源的Nacos(2.0.4+版本)作为服务注册与发现的核心中枢，以实现服务治理的智能化和自动化；同时，它还融合了Redis(6.2+版本)这一高性能缓存系统，以优化数据访问速度，提升系统响应效率；而在数据库支撑方面，无论是稳健可靠的MySQL(5.7+版本)还是与时俱进的MySQL(8.0+版本)，均能与其无缝对接。前端采用了Vue3框架，集成了ElementUI-Plus+2.2.9的UI库。

### 2. 演示地址
- [达佛迪尔基础管理平台演示示例（demo/123456）](http://www.daffodilcloud.com.cn)

 **系统基础** 

- 基础管理：系统首页、个人中心、主题皮肤
- 系统管理：用户管理、角色管理(权限管理)、部门管理、岗位管理、职级管理、群组管理、通知管理
- 数据管理：菜单管理、字典管理、标签管理、区划管理、缓存管理、敏感词管理、应用管理、文件管理、日期管理、资源管理
- 审计管理：操作日志、登录日志、在线用户
- 系统设置：登录设置
- 监控管理：缓存监控、服务监控
- 开发管理：元数据管理、库表管理（代码生成）

**基础功能** 
*   **系统首页**：作为用户进入系统的首界面，系统首页集成了关键业务指标、最新公告通知、快捷操作入口等多种信息展示模块，旨在提供直观、便捷的一站式工作台体验。
*   **个人中心**：用户可在个人中心完成个人信息维护、修改密码、查看权限分配、个性化设置（如头像、昵称、主题皮肤等）以及消息提醒等功能，确保每位用户可以根据自身需求定制专属的工作环境。   
*   **主题皮肤**：支持多种风格的主题切换，满足不同用户的视觉喜好，使用户在长时间使用系统的过程中能够保持愉悦的心情和高效的工作状态。 
  
**系统管理**   
*   **用户管理**：涵盖了用户账号的增删改查、角色分配、账户锁定解锁及密码重置等一系列操作，实现对系统内所有用户的有效管理和控制。   
*   **角色管理（权限管理）**：通过定义不同的角色，并为每个角色分配相应的菜单权限和操作权限，实现了基于角色的访问控制（RBAC），保证了系统的安全性与合规性。  
*   **部门管理、岗位管理、职级管理**：构建组织架构体系，明确各层级之间的隶属关系和上下级结构，方便进行人员配置、职责划分以及绩效考核等工作。    
*   **群组管理**：支持创建和管理多个业务相关的用户群体，便于实现团队协作、权限分组、消息通知等功能。    
*   **通知管理**：发布系统内部重要通知、公告或消息推送，以确保信息传达及时准确，提高工作效率。    

**数据管理**    
*   **菜单管理**：用于搭建系统菜单结构，包括菜单的新增、编辑、删除以及排序调整，直接影响到用户界面的功能布局和导航路径。    
*   **字典管理**：维护系统中各类枚举类型、固定选项的数据字典，为前端页面提供下拉选择、标签分类等服务，提升数据录入规范性和查询效率。    
*   **标签管理**：对特定对象进行分类标记，实现内容的快速检索和归类，例如文章标签、客户标签等。    
*   **区划管理**：对行政区域、地理位置等信息进行管理，适用于需要按地域划分业务场景的应用。    
*   **日期管理**：针对特殊日期设定，比如节假日安排、活动期限等进行统一管理，便于系统自动执行相关业务规则。
    
**审计管理**    
*   **操作日志**：记录系统内的每一次关键操作行为，包括操作人、操作时间、操作对象、操作类型等信息，以便于追溯问题根源和审计追踪。    
*   **登录日志**：记录用户的登录、登出事件及其相关信息，对于异常登录情况可迅速发现并采取应对措施。    
*   **在线用户**：实时监控当前系统中的在线用户数量及状态，方便管理员了解系统负载情况和用户活跃度。 
   
**系统设置**    
*   **登录设置**：包括但不限于验证码开关、登录失败策略、会话超时时间等登录相关的参数配置，以优化用户体验和保障系统安全。   
*   **邮件设置**：提供邮件服务器配置、邮件模板管理、邮件发送测试等功能，支持系统通过邮件方式向用户发送验证码、通知、报告等各种信息。

### 3. 基础模块
- [daffodil-cloud-utils](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-utils)：作为项目的基础工具库，封装了一系列通用、便捷的工具类和方法，包括但不限于数据处理、日期时间操作、文件读写、加密解密等实用功能，旨在提升开发效率并保证代码的一致性与规范性。

- [daffodil-cloud-code](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-code)：作为底层核心代码库，基于强大的Spring Data JPA技术栈进行了深度封装和优化，实现数据库操作的CRUD（创建、读取、更新和删除）、复杂查询以及事务管理等，包含了项目的核心业务逻辑和关键算法实现，是最基础且重要的业务组件，为其他微服务提供强有力的技术支撑，确保系统的稳定性和高效运行。

- [daffodil-cloud-tenant](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-code)：作为多租户架构设计的基础组件模块，目标在于简化微服务架构中的多租户应用开发，通过自动处理租户上下文的切换，感知租户的数据库访问，能够根据当前请求的租户ID自动路由到对应的数据库或者在单个数据库中实施租户数据隔离策略。

- [daffodil-cloud-common](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-common)：作为基础平台服务公共代码库，提供应用系统基础用户体系服务内容，通过统一管理和复用这些公共资源，降低模块间的耦合度，提高项目的可维护性和扩展性。

- [daffodil-cloud-framework](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-framework)：作为整个项目的基础设施框架代码库，集成了SpringCloud微服务架构相关的基础配置和服务治理功能。它定义了微服务的基本结构和启动模板，并抽象出一套适用于本平台的标准开发框架，简化后续微服务的开发流程。

- [daffodil-cloud-system](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-system)：作为基础服务平台微服务，承载了系统管理、权限分配、资源配置等一系列基础服务功能，是整个达佛迪尔基础管理平台的核心组成部分，确保平台日常运维及管理工作的顺利进行。

- [daffodil-cloud-auth](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-auth)：作为统一授权认证微服务，实现了用户的身份验证、权限校验以及基于角色的访问控制（RBAC）等功能，保障了平台内部各微服务间的安全通信与资源访问控制。

- [daffodil-cloud-gateway](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-gateway)：作为网关路由微服务，扮演着系统对外统一入口的角色，负责请求路由、负载均衡、API鉴权、限流熔断等多种策略实施。通过该微服务，能够有效实现对所有微服务集群的集中化管理和安全防护，优化系统性能，提升用户体验。

### 4. 前端模块（分离单独仓库）
- [daffodil-cloud-ui](https://gitee.com/weijiang_admin/daffodil-cloud-ui)：达佛迪尔基础平台前端UI1.0.0工程（该前端使用的element-plus版本是^1.1.0-beta.9已停止更新维护）
- [daffodil-cloud-ui2](https://gitee.com/weijiang_admin/daffodil-cloud-ui2)：达佛迪尔基础平台前端UI2.0.0工程（该前端使用的element-plus版本是^2.2.9持续更新）

### 5. 其它模块（分离单独仓库）
- [daffodil-cloud-devtool](https://gitee.com/weijiang_admin/daffodil-cloud-devtool)：辅助开发微服务
- [daffodil-cloud-flowable](https://gitee.com/weijiang_admin/daffodil-cloud-flowable)：流程引擎微服务
- [daffodil-cloud-sensitive](https://gitee.com/weijiang_admin/daffodil-cloud-sensitive)：敏感词词语微服务（不文明用语）
- [daffodil-cloud-monitor](https://gitee.com/weijiang_admin/daffodil-cloud-monitor)：健康监控微服务
- [daffodil-cloud-cms](https://gitee.com/weijiang_admin/daffodil-cloud-cms)：简易内容管理微服务
- [daffodil-cloud-demo](https://gitee.com/weijiang_admin/daffodil-cloud-demo)：示例应用微服务
- [daffodil-cloud-medium](https://gitee.com/weijiang_admin/daffodil-cloud-medium)：讯息发送微服务
- [daffodil-cloud-storage](https://gitee.com/weijiang_admin/daffodil-cloud-storage)：对象存储微服务

### 6. 环境要求

|  **序号**  |  **组件名称**  |  **版本要求**  |  **必要**  |  **描述**  |
| --- | --- | --- | --- | --- |
|  1  |  JDK  |  1.8+  |  是  |  平台构建在Java语言之基石上，因此对JDK版本的要求不低于1.8+，以为系统提供坚实的语言环境支撑。  |
|  2  |  MySql  |  5.7+或8.0+  |  是  |  平台所采用的数据库方案兼容MySQL 5.7和8.0两个主流版本，无论是单机部署还是主从集群模式，均可依据实际业务规模与需求进行灵活配置与高效运行。  |
|  3  |  Redis  |  3.2+或6.2+  |  是  |  平台选用Redis作为高速缓存层，推荐使用性能更优的6.2+版本，同时兼顾向下兼容性，支持3.2+以前版本。无论是在单机环境或是集群架构中，Redis都能发挥其卓越的数据缓存与快速响应能力。  |
|  4  |  Nacos  |  2.0.0+  |  是  |  高可用的服务治理体系核心组件Nacos，要求版本在2.0.0以上，不论是单一节点部署还是集群化部署，均能确保服务的动态注册、发现以及配置管理等功能高效稳定运作。  |
|  5  |  Nginx  |  1.20+  |  否  |  Nginx作为一款享誉业界的高性能HTTP和反向代理服务器，在生产环境中常用于优化流量分发与负载均衡。但在开发阶段，其部署并非绝对必需，可视具体需求而定。  |

### 7. 框架说明

#### 7.1. 数据源

兼容了多种主流的关系型数据库，如MySql、Oracle与SqlServer等，并默认采用了广受青睐的开源MySql数据库作为数据源。当面临切换至其他数据库的实际需求时，只需对相应应用工程的Maven依赖项进行精细化调整，即更改对应的数据源驱动配置。以下内容同样以极具人气的MySql数据库为例展开说明，尽管语境如此，但其普适性适用于任何所支持的数据库类型，体现了项目的灵活性和扩展性。

 **单例数据源配置** 

    spring:
    datasource: 
      url: jdbc:mysql://127.0.0.1:3306/daffodil-cloud?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2B8&useSSL=false&useInformationSchema=true&nullCatalogMeansCurrent=true
      username: ENC(sXbr2rQZNY4dI0N1Zn6Muw==)
      password: ENC(VyPdE3xFgic+7ObADKITEw==)
      driver-class-name: com.mysql.cj.jdbc.Driver
    jpa: 
      # 数据库方言
      database-platform: org.hibernate.dialect.MySQL5InnoDBDialect
      # 是否显示sql语句
      show-sql: true
      hibernate: 
        # 自动更新数据库表
        ddl-auto: update
      
    # MySql主从读写分离配置
    shardingsphere: 
      enabled: false

 **MySql主从读写分离配置** 

    spring: 
    #MySql主从读写分离配置
    shardingsphere: 
      enabled: true
      props: 
        # 是否打印sql语句
        sql-show: true
      # 数据源配置
      datasource: 
        names: master,slave0
        # 主库（又称逻辑库）自定义名称
        master: 
          type: com.zaxxer.hikari.HikariDataSource
          driver-class-name: com.mysql.cj.jdbc.Driver
          jdbc-url: jdbc:mysql://127.0.0.1:3306/daffodil-cloud?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2B8&useSSL=false&useInformationSchema=true&nullCatalogMeansCurrent=true
          username: root
          password: 123456
        # 从库（又称查询库）自定义名称
        slave0: 
          type: com.zaxxer.hikari.HikariDataSource
          driver-class-name: com.mysql.cj.jdbc.Driver
          jdbc-url: jdbc:mysql://127.0.0.1:3307/daffodil-cloud?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2B8&useSSL=false&useInformationSchema=true&nullCatalogMeansCurrent=true
          username: root
          password: 123456
      schema: 
        name: daffodil-cloud
      rules: 
        # 读写分离规则配置
        readwrite-splitting: 
          load-balancers: 
            # 负载均衡算法名称自定义
            datasource-balancer: 
              # 负载均衡算法类型 ROUND_ROBIN=轮询算法 RANDOM=随机访问算法 WEIGHT=权重访问算法
              type: ROUND_ROBIN
          data-sources: 
            # 主库（又称逻辑库）自定义名称
            master: 
              # STATIC=静态读写分离算法 DYNAMIC=动态读写分离算法
              type: STATIC
              props: 
                write-data-source-name: master
                read-data-source-names: master,slave0
              # 负载均衡算法名称
              load-balancer-name: datasource-balancer

 **数据加密配置** 

为了确保配置中心中敏感数据的安全性，防范潜在的信息泄露风险，可以采用spring-boot-jasypt这一先进的加密工具进行深度的数据保护措施。具体来说，对于数据库登录凭证这类高敏感信息，我们可以通过jasypt的加密机制对其进行艺术性的封装，以"ENC(密文内容)"的形态进行优雅而隐秘的存储。这样，既确保了关键数据在静默中得到了坚若磐石般的防护，又兼顾了系统的流畅运行与逻辑合理性，彰显出技术与安全并重，实用与美学共生的语言运用风格。

    #配置数据加解密
    jasypt: 
      encryptor:
        password: daffodil-cloud
        algorithm: PBEWithMD5AndDES
        key-obtention-iterations: 1000
        pool-size: 1
        provider-name: SunJCE
        salt-generator-classname: org.jasypt.salt.RandomSaltGenerator
        iv-generator-classname: org.jasypt.iv.NoIvGenerator
        string-output-type: base64

为了开发者方便本项目也提供JUI操作界面，下载插件工具

[https://gitee.com/weijiang\_admin/daffodil-cloud/tree/master/daffodil-cloud-doc/src/jasypt/daffodil-jasypt-boot.jar](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-doc/src/jasypt/daffodil-jasypt-boot.jar)

在window的环境下打开CMD命令,输入以下命令Enter即可展示JUI界面。

    java -jar daffodil-jasypt-boot.jar


#### 7.2. Redis缓存

    #redis缓存配置 
    spring: 
      redis:
        ###  redis单机配置  ###
        # Redis数据库索引（默认为0）
        database: 0
        # Redis服务器地址
        host: 192.168.1.26
        # Redis服务器连接端口
        port: 6379
        ###  redis集群配置  ###
        #cluster:
        #  nodes: 192.168.20.26:7001,192.168.20.27:7001,192.168.20.28:7001,192.168.20.26:7002,192.168.20.27:7002,192.168.20.28:7002
        # Redis服务器连接密码（默认为空）
        password: 123456
        # 连接超时时间（毫秒）
        timeout: 10000ms
        lettuce:
          pool:
            # 连接池最大连接数
            max-active: 200
            # 连接池最大阻塞等待时间（使用负值表示没有限制）
            max-wait: -1ms
            # 连接池中的最大空闲连接
            max-idle: 10
            # 连接池中的最小空闲连接
            min-idle: 0

#### 7.3. 应用安全

应用安全配置相对内容比较多，但其实也很简单，包括操作日志、链路追踪、演示模式、登录验证码（图片验证/动态验证码）、扫码登录等，详见示例代码所述：

##### 7.3.1. 操作日志

    daffodil: 
      operlog: 
        # 是否开启记录操作日志
        enable: true

##### 7.3.2. 租户模式

    daffodil: 
      tenant: 
        # 是否开启租户模式
        enable: true

##### 7.3.3. 演示模式

    daffodil: 
      demo: 
        # 是否演示模式
        enable: false
        # 演示模式下，不允许的操作，用逗号分割 
        # OTHER=其他 INSERT=新增 UPDATE=修改 DELETE=删除 GRANT=授权 EXPORT=导出 IMPORT=导入 FORCE=强退 GENCODE=生成代码 CLEAN=清空 TRASH=作废 UPLOAD=上传 DOWNLOAD=下载
        not-allowed: OTHER,DELETE,IMPORT,CLEAN,TRASH,UPLOAD

##### 7.3.4. 请求源安全

    daffodil:
      security: 
        referer: 
          #允许访问来源的Host，*为任何访问来源
          allowed-origin: >
            *
          #忽略访问来源拦截的URL
          ignore-urls: >
            /api-test/**

##### 7.3.5. 授权令牌安全

    daffodil:
      security:
        access-token: 
          #是否开启登录IP过滤，校验请求地址和授权令牌的登录地址是否一致
          enable-filter-login-ip: false
          #忽略Token认证拦截的URL
          ignore-urls: >
            /api-auth/token/sign,
            /api-auth/token/refresh,
            /api-test/**

##### 7.3.6. 数据签名安全

    daffodil:
      security:
        #参数签名
        signature: 
          #是否开启
          enable: false
          #签名有效时间 默认5秒 单位秒
          duration: 60
          #签名算法
          method: SM3
          #忽略签名验证的Url
          ignore-urls: >
            /api-auth/oauth/token,
            /api-auth/oauth/refresh,
            /api-auth/oauth/user,
            /api-test/**

##### 7.3.7. 跨域请求安全

    daffodil:
      security:
        cros: 
          allowed-credentials: false
          #线上最好写真实域名地址，逗号分隔
          allowed-origin: >
            *
          allowed-method: >
            GET,
            POST,
            OPTIONS
          allowed-header: >
            *
          allowed-urls: >
            /api-test/**

##### 7.3.8. WEB跨站漏洞

    daffodil:
      security:
        #xss漏洞渗透攻击
        xss-attack: 
          #是否开启xss渗透拦截
          enable: true
          #定义需要渗透匹配验证的正则表达式
          regexs: >
            <(.*?)(/)?>,
            <(no)?script.*?(>)?.*?</(no)?script>,
            (expression|eval|execute)\((.*?)\),
            javascript:|typescript:|vbscript:|view-source:,
            \s*(oncontrolselect|oncopy|oncut|ondataavailable|ondatasetchanged|ondatasetcomplete|ondblclick|ondeactivate|ondrag|ondragend|ondragenter|ondragleave|ondragover|ondragstart|ondrop|onerror=|onerroupdate|onfilterchange|onfinish|onfocus|onfocusin|onfocusout|onhelp|onkeydown|onkeypress|onkeyup|onlayoutcomplete|onload|onlosecapture|onmousedown|onmouseenter|onmouseleave|onmousemove|onmousout|onmouseover|onmouseup|onmousewheel|onmove|onmoveend|onmovestart|onabort|onactivate|onafterprint|onafterupdate|onbefore|onbeforeactivate|onbeforecopy|onbeforecut|onbeforedeactivate|onbeforeeditocus|onbeforepaste|onbeforeprint|onbeforeunload|onbeforeupdate|onblur|onbounce|oncellchange|onchange|onclick|oncontextmenu|onpaste|onpropertychange|onreadystatechange|onreset|onresize|onresizend|onresizestart|onrowenter|onrowexit|onrowsdelete|onrowsinserted|onscroll|onselect|onselectionchange|onselectstart|onstart|onstop|onsubmit|onunload)+\s*=+,
            window\.location|window\.|\.location|document\.cookie|document\.|alert(\s?)\(|window\.open(\s?)\(
          #忽略xss渗透验证的url
          ignore-urls: >
            /api-test/**

##### 7.3.9. SQL注入漏洞

    daffodil:
      security:
        #SQL注入攻击
        sql-attack: 
          #是否开启sql注入攻击拦截
          enable: true
          #定义不允许sql注入的正则表达式
          regexs: >
            select(\s)+(.*?)(\s)+from,
            insert(\s)+into,
            update(\s)+(.*?)(\s)+set,
            delete(\s)+from,
            truncate(\s)+table,
            create(\s)+(database|event|function|index|logfile|procedure|server|table|tablespace|trigger|user|view),
            drop(\s)+(database|event|function|index|procedure|server|table|tablespace|trigger|user|view),
            alter(\s)+(database|event|function|instance|logfile|procedure|server|table|tablespace|user|view),
            show(\s)+(database|event|function|index|logfile|procedure|server|table|tablespace|trigger|user|view|binlog|character|collation|columns|open|privileges),
            grant(\s)+(select|insert|update|delete|index|create|show|execute|alter|all|for)(\s)+on(.*?)(\s)+to
          #忽略sql注入攻击验证的url
          ignore-urls: >
            /api-test/**

#### 7.4. 网关路由

网关路由断言配置其实就是API请求的路由规则，如果你的业务需求还想额外更多的配置详见官网的Gateway路由断言章节。基础管理平台本着一切从简的思路原则，解放开发者让开发者更简单容易上手，路由配置简单化，新增的项目微服务API以此类推配置，网关路由配置参考示例：

    #系统路由配置
    spring: 
      cloud: 
        gateway: 
          routes: 
          #授权认证微服务所有API
          - id: daffodil-cloud-auth
            uri: lb://daffodil-cloud-auth
            predicates: 
            - Path=/api-auth/**
          #基础服务微服务所有API
          - id: daffodil-cloud-system
            uri: lb://daffodil-cloud-system
            predicates: 
            - Path=/api-system/**

#### 7.5. 程序注解

程序注解主要介绍基础管理平台自定义的注解使用与其注解的作用。包括数据权限、QL语义注解、多数据源、路由权限、操作日志等相关注解。

##### 7.5.1. 数据权限 @DataScope

数据权限 @DataScope 其作用于Service层方法，是基于系统的部门组织结构的数据范围权限注解，如果要依赖于它那边目标的数据实体字段必须包含deptId字段或及userId字段。参见SysUser的实现。@DataScope(deptAlias = "d.deptId", userAlias = "u.userId") deptAlias=部门ID的别名，userAlias=用户ID别名。

    @Override
    @DataScope(deptAlias = "d.deptId", userAlias = "d.userId")
    public List<CmsDocument> selectUserList(Query<CmsDocument> query, @Nullable SysUser user) {
        StringBuffer hql = ew StringBuffer("from CmsDocument d where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        ...(略)...
        HqlUtils.createHql(hql, paras, query, "c");
        return jpaDao.search(hql.toString(), paras, CmsDocument.class, query.getPage());
    }

##### 7.5.2. HQL语义注解 @Hql

HQL语义注解 @Hql 主要帮助解放开发者在写HQL语句时简化各种实体表业务查询的条件判断处理，配合HqlUtils.createHql(...)实现。

    /**
    * -模糊匹配 col like '%char%'
    */
    LIKE("like"), 
    /**
    * -模糊匹配 col like 'char%'
    */
    LLIKE("like"), 
    /**
    * -模糊匹配 col like '%char'
    */
    RLIKE("like"), 
    /**
    * -相等匹配 col = 'char'
    */
    EQ("="), 
    /**
    * -不等匹配 col != 'char'
    */
    NEQ("!="), 
    /**
    * -小于等于匹配 col <= 123
    */
    LEQ("<="), 
    /**
    * -小于匹配 col < 123
    */
    LT("<"), 
    /**
    * -大于等于匹配 col >= 123
    */
    REQ(">="), 
    /**
    * -大于匹配 col > 123
    */
    RT(">"), 
    /**
    * -包含匹配 col in('char1','char2')
    */
    IN("in"), 
    /**
    * -空字符（无匹配意义）
    */
    NONE("");

    @Hql(type = Logical.LIKE)
    private String rankName;
    
    @Hql(type = Logical.EQ)
    private String status;
    -------------------------------------------------------------------------------------
    @Override
    public List<SysRank> selectRankList(Query<SysRank> query) {
        StringBuffer hql = new StringBuffer("from SysRank where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        //等价于hql = from SysRank where 1=1 and rankName like ? and status = ?
        return jpaDao.search(hql.toString(), paras, SysRank.class, query.getPage());
    }
    

##### 7.5.3. 多数据源 @TargetDataSource

多数据源 @TargetDataSource 其作于用Service层方法，告知本方法内的操作使用哪个数据源，默认是primary数据库数据源。同MyBatils-plus的@DS注解用法一致。

    @Override
    @TargetDataSource("slave")
    public List<SysGroup> selectGroupList(Query<SysGroup> query) {
        StringBuffer hql = new StringBuffer("from SysGroup where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        return jpaDao.search(hql.toString(), paras, SysGroup.class, query.getPage());
    }

##### 7.5.4. 路由权限 @AuthPermission

路由权限 @AuthPermission 继承于@Permission 其标注在@RestController层的方法名上，主要标识该请求方法需要用户授权才能请求访问。@AuthPermission("system:user:add") value = system:area:add 权限标识码，这个权限标识码将对应系统的菜单管理-权限标识一致。权限码定义规则一般是module1:module2:operate。假如模块超过2层以上则取最后两层即可让代码更简洁直观。

    @ApiOperation("新增用户")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:user:add")
    @OperLog(title = "用户管理", type = Business.INSERT)
    @PostMapping("/user/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysUser user, @ApiIgnore ServerHttpRequest request) {
        userService.insertUser(user);
        return Mono.just(JsonResult.success());
    }

##### 7.5.5. 开放权限 @OpenPermission

开放权限 @OpenPermission 继承于@Permission，其标注在@RestController层的方法名上，主要标识该请求方法是开放授权资源，类似于资源开放平台开放的API，一般用于应用管理的授权接口使用。同@AuthPermission相似。

##### 7.5.6. 操作日志 @OperLog

操作日志 @OperLog 其标注在@RestController层的方法名上，主要标识该请求操作方法是否需求记录操作日志。@OperLog (title = "标签管理", type = Business.UPDATE)

##### 7.5.7. 操作业务 @OperBusiness

操作业务 @OperBusiness 其标注在 @RestController层的方法名上的@OperLog注解中，主要标识该请求操作业务名称以及业务标识。@OperLog (title = "用户管理", business = @OperBusiness(name = "重置", label = "RESET", remark = "用户密码重置"))

##### 7.5.8. 操作客户端 @OperClient

操作业务 @OperClient 其标注在 @RestController层的方法名上的@OperLog注解中，主要标识该请求操作客户端名称以及客户端标识。@OperLog (title = "用户管理", type = Business.UPDATE, client = @OperClient(name = "桌面端", label = "DESKTOP"))

##### 7.5.9. 数据脱敏 @MaskFormat

数据脱敏 @MaskFormat 其标注于实体对象参数Field字段上，主要标识该字段需要脱敏处理。@MaskFormat (type = MaskType.MIDDLE, count = 3, begin = 3, end = 6，chart = '\*') 表示脱敏从第3位到第6位，脱敏3位字符为'\*'字符， 其中type = MaskType.ALL 全部， MaskType.HEAD 头部，MaskType.MIDDLE 中间， MaskType.TAIL 尾部，支持手机号、身份证号、银行卡号、邮箱、姓名、地址、密码等任意字符串脱敏。

#### 7.6. 持久层框架

##### 7.6.1. SpringDataJPA

本框架在遵循Spring Data JPA规范并依托Hibernate作为具体实现技术的基础上，进一步定制封装了一系列高效实用的持久层组件，即JpaDao和JdbcDao。
JpaDao通过深度整合EntityManager机制，实现了对实体对象的全面管理，并严格遵循HQL（Hibernate Query Language）的语法规则进行查询操作。
JdbcDao同样充分利用了EntityManager的强大功能，但其主要面向原生SQL查询场景，旨在提供更灵活、底层的数据访问能力，完全符合SQL的标准语法规范。
通过这两种持久层Dao的设计与实现，本框架有效提升了开发效率及数据访问性能，同时兼顾了领域驱动设计原则和实际业务需求的高度契合。

    @Autowired
    private JpaDao<String> jpaDao;
    
    String hql = "from SysUser where loginName = ? ";
    ...(略)...
    SysUser sysUser = jpaDao.find(hql, paras, SysUser.class);

    @Autowired
    private JdbcDao jdbcDao;
    
    String sql = "select browser as name, count(*) as value from sys_login_info where login_name=? group by browser";
    ...(略)...
    List<Map<String, String>> list = jdbcDao.search(sql, paras);

框架也集成了底层的基础数据操作功能，包括但不限于创建、读取、更新及删除等核心CRUD方法的实现。

    public interface IAccountService extends IBaseEntityService<Long, Account> {
    
    }
    
    @Service
    public class AccountServiceImpl extends BaseEntityServiceImpl<Long, Account> implements IAccountService {
    
    }

    public interface IBaseEntityService<ID, T> {
        /**
         * -分页查询实体信息集合
         * @param query
         * @return
         */
        public List<T> selectEntityList(Query<T> query);
        /**
         * -通过实体ID查询职级信息
         * @param id
         * @return
         */
        public T selectEntityById(ID id);
        /**
         * -批量删除实体信息
         * @param ids
         */
        public void deleteEntityByIds(ID[] ids);
        /**
         * -新增保存实体信息
         * @param entity
         */
        public void insertEntity(T entity);
        /**
         * -修改保存实体信息
         * @param entity
         */
        public void updateEntity(T entity);
    }

##### 7.6.2. Mybatis

本框架在设计与实现过程中，充分考虑了业界广泛采用的持久层框架——Mybatis的兼容性与整合能力。    

Mybatis-plus作为Mybatis的优秀扩展，通过引入强大的CRUD操作、条件构造器、分页插件等功能，极大地提升了数据库操作的便利性和可维护性。在此框架中，用户不仅可以继续沿用Mybatis-plus的这些高效工具，而且还能结合框架本身的特色功能，实现业务逻辑的深度定制与灵活扩展。因此，特别优化了框架内核，确保能够无缝对接并充分利用Mybatis-plus的各项先进特性。

在实际开发过程中，若项目中同时采用了Spring Data JPA与Mybatis这两种持久层框架，值得注意的是，实体类对象的注解映射存在一定的差异性。具体来说，由于两种框架遵循不同的ORM规范，因此其各自的注解特性有所不同。

然而，有趣的一点是，实体类可以兼容并蓄地使用来自两个框架的注解进行联合标注，这意味着开发者可以在同一份代码基础上，依据业务需求无缝切换或融合运用这两种持久化解决方案，实现对数据访问层的高效、灵活管理与操作。

    Spring Data JPA 框架的实体对象注解使用的是：
    @Entity、@Table(name = "tb_account")、@Id、@Column(name = "id")
    Mybatis-plus框架的实体对象注解使用的是：
    @TableName(value = "tb_account")、@TableId(value = "id", type = IdType.AUTO)、@TableField("user_id")
    

    @Data
    @EqualsAndHashCode(callSuper = true)
    @Entity
    @Table(name = "tb_account")
    @TableName(value = "tb_account")
    public class Account extends BaseEntity<Long> {
    
        /** 用户ID */
        @Id
        @JsonSerialize(using = ToStringSerializer.class)
        @GeneratedValue(generator = "snowflake")
        @GenericGenerator(name = "snowflake", strategy = "com.xxxxx.SnowflakeIdentifierGenerator")
        @TableId(value = "id", type = IdType.AUTO)
        @Column(name = "id")
        private Long id;
    
        /** 用户ID 绑定系统用户ID */
        @TableField("user_id")
        @Column(name = "user_id", length = 32)
        @Hql(type = Logical.EQ)
        private String userId;
        ...(略)...
    }

#### 7.7. WEB框架模型

本框架同时兼容WebFlux与Servlet这两种迥异的Web应用程序模型。在决定采用WebFlux架构模型时，建议在控制器层(Controller层)的设计中，遵循Reactive编程范式，令Controller类继承自ReactiveBaseController，以实现非阻塞、异步和响应式处理能力。

反之，若选择传统的Servlet架构模型构建Web应用，那么在控制器层的实现上，应遵循Servlet规范，确保Controller类继承自ServletBaseController，从而充分利用基于请求/响应生命周期管理的同步编程模式，以及Servlet容器提供的丰富功能集。

    @Api(value = "日期管理", tags = "日期管理")
    @RestController
    @RequestMapping(SystemConstant.API_CONTENT_PATH)
    public class SysDaysController extends ReactiveBaseController {

    @Api(value = "摊户管理", tags = "摊户管理")
    @RestController
    @RequestMapping(DjxxConstant.API_CONTENT_PATH + "/account")
    public class AccountController extends ServletBaseController {

### 8. 开发规约

在基于本框架进行软件开发的过程中，遵循以下编码规范至关重要：

1、为确保数据持久化的一致性和完整性，实体类的命名必须继承自基础实体类BaseEntity，并且每个对应数据库表的实体均需设定一个主键ID属性。

2、在设计领域接口层（interface）时，应采用“IXxxxxService”形式的命名规范，其中“Xxxxx”代表与之对应的业务逻辑实体。

3、对于实体服务实现层的命名，应当遵循“XxxxxServiceImpl”的命名约定，以明确表示该类是其对应接口的具体实现。

4、在实体类参数验证环节，建议采用注解方式进行校验处理，如利用Java Validation API中的@Validated注解进行约束声明和执行。

5、在控制器层的设计中，@RestController类宜扩展自ReactiveBaseController或ServletBaseController，以便更好地封装和复用基础控制逻辑。

6、针对Webflux响应式编程模型下的Controller类，请求方法(Method)应保留ServerHttpRequest参数，以适应异步非阻塞处理的需求。

7、为了便于API文档的自动化生成与管理，在控制器层务必运用Swagger相关注解进行详尽的接口说明。同时，遵循RESTful原则，查询类操作应采用HTTP GET方法，而增加、删除、修改等操作则使用POST方法。

### 9. 相关下载
达佛迪尔基础平台相关数据库脚本以及相关配置文件下载地址：[https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-doc](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-doc)

### 10. 微信公众号
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/%E8%BE%BE%E4%BD%9BDFD%E5%BE%AE%E4%BF%A1%E5%85%AC%E4%BC%97%E5%8F%B7%E4%BA%8C%E7%BB%B4%E7%A0%81.jpg)  
【达佛迪尔】微信公众号

### 11. 质量评分
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/QQ%E6%88%AA%E5%9B%BE20220512144248.png)

### 18. 系统风采（基于前端UI2.0.0工程截图）
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/1.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/2.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/3.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/4.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/5.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/6.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/7.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/8.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/9.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/10.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/11.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/12.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/13.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/14.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/15.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/16.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/17.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/18.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/19.png)

### 12. 特别感谢
特别感谢：@lyt-top前端框架支持[https://gitee.com/lyt-top/vue-next-admin](https://gitee.com/lyt-top/vue-next-admin)

### 23. 码云特技

1.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
2.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
3.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
4.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
5.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
