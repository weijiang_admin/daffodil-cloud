package com.daffodil.system.controller.model;

import java.io.Serializable;

import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2022年9月30日
 * @version 2.0.0
 * @description
 */
@Data
public class RedisCacheData implements Serializable {

    private static final long serialVersionUID = -8580433751886130888L;

    private String type = "none";

    private long expire = -1;

    private String key = "";

    private String hashKey = "";

    private Object value = "";

    private String remark = "";

    public void setKey(String key) {
        for(RedisCacheKeyEnum cacheKey : RedisCacheKeyEnum.values()) {
            if(key != null && key.indexOf(cacheKey.getCode()) >= 0) {
                this.remark = cacheKey.getDesc();
                break;
            }
        }
        this.key = key;
    }

    public enum RedisCacheKeyEnum {

        /** 应用动态路由 */
        DYNAMIC_ROUTER("system:router:dynamic", "应用动态路由"),
        /** 应用静态路由 */
        STATIC_ROUTER("system:router:static", "应用静态路由"),
        /** 开放动态路由 */
        OPEN_DYNAMIC_ROUTER("system:open:dynamic:router", "开放动态路由"),
        /** 开放静态路由 */
        OPEN_STATIC_ROUTER("system:open:static:router", "开放静态路由"),
        /** IP真实地理位置 */
        IP_ADDRESS("system:ip:address", "IP真实地理位置"),
        /** 敏感词矩阵 */
        SENSITIVE_WORD("system:sensitive:word", "敏感词矩阵"),
        /** 行政区划数据 */
        AREA_CACHE("system:area:cache", "行政区划数据"),
        /** 邮件服务设置 */
        CONFIG_EMAIL("system:config:email", "邮件服务设置"),
        /** 登录服务设置 */
        CONFIG_LOGIN("system:config:login", "登录服务设置"),
        /** 应用字典数据 */
        DICTIONARY_CACHE("system:dictionary:cache", "应用字典数据"),
        /** 日期假期数据 */
        DAYS_DATA("system:days:data", "日期假期数据"),
        /** 用户授权令牌 */
        ACCESS_TOKEN("system:token:access:", "用户授权令牌"),
        /** 用户刷新令牌 */
        REFRESH_TOKEN("system:token:refresh:", "用户刷新令牌"),
        /** 授权令牌类别 */
        LEVEL_TOKEN("system:token:level:", "授权令牌类别"),
        /** 用户授权权限 */
        PERMISSION_TOKEN("system:token:permission", "用户授权权限"),
        /** 国密SM2加解密私钥 */
        SM2_PRIVATE_KEY("system:sm2:private-key", "国密SM2加解密私钥"),
        /** 国密SM2加解密公钥 */
        SM2_PUBLIC_KEY("system:sm2:public-key", "国密SM2加解密公钥"),
        /** 用户登录图片验证码 */
        USER_LOGIN_KAPTCHA("user:login:kaptcha:", "用户登录图片验证码"),
        /** 用户验证码登录验证码 */
        USER_LOGIN_VERIFYCODE("user:login:verify-code:", "用户验证码登录验证码"),
        /** 用户登录计数 */
        USER_LOGIN_COUNT("user:login:count:", "用户登录计数"),
        /** 用户登录重试计数 */
        USER_RETRY_COUNT("user:retry:count:", "用户登录重试计数"),
        /** 用户二次验证令牌 */
        USER_SECOND_VERIFY_TOKEN("user:token:verify:", "用户二次验证令牌"),
        /** 应用授权码 */
        THIRDAPP_OAUTH_CODE("thirdapp:oauth:code:", "应用授权码"),
        /** 应用授权令牌 */
        THIRDAPP_OAUTH_TOKEN("thirdapp:oauth:token:", "应用授权令牌");

        private final String code;

        private final String desc;

        private RedisCacheKeyEnum(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public String getCode() {
            return code;
        }

        public String getDesc() {
            return desc;
        }
    }
}
