package com.daffodil.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.annotation.OpenPermission;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.util.IpUtils;
import com.daffodil.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2022年12月21日
 * @version 2.0.0
 * @description
 */
@Api(value = "IP位置管理", tags = "IP位置管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysIPLocationController {

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    @ApiOperation("根据IP查询真实地理位置")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @OpenPermission("system:ip:location")
    @GetMapping("/ip/location")
    public Mono<JsonResult> location(@ApiParam(value="IP地址") String ip, @ApiIgnore ServerHttpRequest request) {
        String address = (String) redisTemplate.opsForHash().get(CommonConstant.REALIP_ADDRESS_CACHEKEY, ip);
        if(StringUtils.isEmpty(address)) {
            address = IpUtils.getRealIpAddressName(ip, true);
            if(address.indexOf("未知IP") < 0) {
                redisTemplate.opsForHash().put(CommonConstant.REALIP_ADDRESS_CACHEKEY, ip, address);
            }
        }
        return Mono.just(JsonResult.success(JsonResult.SUCCESS_MSG, address));
    }
}
