package com.daffodil.system.controller.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2022年6月21日
 * @version 2.0.0
 * @description
 */
@ApiModel(value = "用户群组参数")
@Data
public class UserGroupParam implements Serializable {

    private static final long serialVersionUID = -3751423343371553222L;

    /** 群组ID */
    @ApiModelProperty(name = "groupId", value = "群组ID")
    private String groupId;
    
    /** 用户组ID */
    @ApiModelProperty(name = "userIds", value = "用户组ID")
    private String[] userIds;
    
}
