package com.daffodil.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperBusiness;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysConfig;
import com.daffodil.system.service.ISysConfigService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * -应用设置管理
 * @author yweijian
 * @date 2022年9月8日
 * @version 2.0.0
 * @description
 */
@Api(value = "应用设置管理", tags = "应用设置管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysConfigController extends ReactiveBaseController {

    @Autowired
    private ISysConfigService configService;

    @ApiOperation("获取应用设置详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:config:info")
    @GetMapping("/config/info")
    public Mono<JsonResult> info(@ApiIgnore ServerHttpRequest request) {
        SysConfig config = configService.selectConfig();
        return Mono.just(JsonResult.success(config));
    }

    @ApiOperation("修改应用设置")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:config:edit")
    @OperLog(title = "应用设置管理", business = @OperBusiness(name = "设置", label = "SETTING"))
    @PostMapping("/config/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysConfig config, @ApiIgnore ServerHttpRequest request) {
        configService.saveConfig(config);
        return Mono.just(JsonResult.success(configService.selectConfig()));
    }
}
