package com.daffodil.system.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysLoginInfo;
import com.daffodil.system.service.ISysLoginInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2021年10月9日
 * @version 1.0
 * @description
 */
@Api(value = "登录日志管理", tags = "登录日志管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysLoginInfoController extends ReactiveBaseController {

    @Autowired
    private ISysLoginInfoService loginInfoService;

    @ApiOperation("分页查询日志列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:logininfo:list")
    @GetMapping("/logininfo/list")
    public Mono<TableResult> list(SysLoginInfo loginInfo, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(loginInfo, page, request);
        List<SysLoginInfo> list = loginInfoService.selectLoginInfoList(query);
        return Mono.just(TableResult.success(list, query));
    }
    
    @ApiOperation("删除日志")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:logininfo:remove")
    @OperLog(title = "登录日志管理", type = Business.DELETE)
    @PostMapping("/logininfo/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        loginInfoService.deleteLoginInfoByIds(ids);
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("清空日志")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:logininfo:clean")
    @OperLog(title = "登录日志管理", type = Business.CLEAN)
    @PostMapping("/logininfo/clean")
    public Mono<JsonResult> clean(@ApiIgnore ServerHttpRequest request) {
        loginInfoService.cleanLoginInfo();
        return Mono.just(JsonResult.success());
    }
    
}
