package com.daffodil.system.controller.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2022年6月22日
 * @version 2.0.0
 * @description
 */
@ApiModel(value = "通知消息参数")
@Data
public class SysNoticeParam implements Serializable {

    private static final long serialVersionUID = 6292340257907082877L;

    /** 通知消息ID */
    @ApiModelProperty(name = "noticeId", value = "通知消息ID")
    private String noticeId;
    
    /** 全体用户 */
    @ApiModelProperty(name = "allIn", value = "是否全体用户")
    private Boolean allIn;
    
    /** 用户组ID */
    @ApiModelProperty(name = "userIds", value = "用户组ID")
    private String[] userIds;
    
    /** 部门组ID */
    @ApiModelProperty(name = "deptIds", value = "部门组ID")
    private String[] deptIds;
    
    /** 角色组ID */
    @ApiModelProperty(name = "roleIds", value = "角色组ID")
    private String[] roleIds;
    
    /** 岗位组ID */
    @ApiModelProperty(name = "postIds", value = "岗位组ID")
    private String[] postIds;
    
    /** 职级组ID */
    @ApiModelProperty(name = "rankIds", value = "职级组ID")
    private String[] rankIds;

    /** 群组组ID */
    @ApiModelProperty(name = "groupIds", value = "群组组ID")
    private String[] groupIds;
}
