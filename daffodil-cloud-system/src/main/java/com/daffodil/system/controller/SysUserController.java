package com.daffodil.system.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.service.ISysUserService;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperBusiness;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.framework.model.LoginUser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 系统用户管理
 * @author yweijian
 * @date 2021年9月24日
 * @version 1.0
 * @description
 */
@Api(value = "用户管理", tags = "用户管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysUserController extends ReactiveBaseController {

    @Autowired
    private ISysUserService userService;

    @ApiOperation("分页查询用户列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:user:list")
    @GetMapping("/user/list")
    public Mono<TableResult> list(SysUser user, Page page, @ApiIgnore LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        if(null == loginUser) {
            return Mono.just(TableResult.success(Collections.emptyList()));
        }
        initQuery(user, page, request);
        SysUser sysUser = userService.selectUserById(loginUser.getId());
        List<SysUser> list = userService.selectUserList(query, sysUser);
        return Mono.just(TableResult.success(list, query));
    }

    @ApiOperation("获取用户详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:user:info")
    @GetMapping("/user/info")
    public Mono<JsonResult> info(@ApiParam(value = "用户ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysUser user = userService.selectUserById(id);
        return Mono.just(JsonResult.success(user));
    }

    @ApiOperation("新增用户")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:user:add")
    @OperLog(title = "用户管理", type = Business.INSERT)
    @PostMapping("/user/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysUser user, @ApiIgnore ServerHttpRequest request) {
        userService.insertUser(user);
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("修改用户")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:user:edit")
    @OperLog(title = "用户管理", type = Business.UPDATE)
    @PostMapping("/user/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysUser user, @ApiIgnore ServerHttpRequest request) {
        userService.updateUser(user);
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("删除用户")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:user:remove")
    @OperLog(title = "用户管理", type = Business.DELETE)
    @PostMapping("/user/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        userService.deleteUserByIds(ids);
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("用户密码重置")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:user:resetPwd")
    @OperLog(title = "用户管理", business = @OperBusiness(name = "重置", label = "RESET", remark = "用户密码重置"), isSaveRequestData = false)
    @PostMapping("/user/resetPwd")
    public Mono<JsonResult> resetPwd(@RequestBody SysUser user, @ApiIgnore ServerHttpRequest request) {
        userService.resetUserPwd(user);
        return Mono.just(JsonResult.success());
    }

}
