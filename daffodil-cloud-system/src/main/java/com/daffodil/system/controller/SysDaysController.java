package com.daffodil.system.controller;

import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OpenPermission;
import com.daffodil.framework.model.HolidaysStats;
import com.daffodil.framework.util.HolidaysUtils;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.service.ISysDaysService;
import com.daffodil.util.sm.CodeUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * -日期管理控制层
 * 
 * @author EP
 * @date 2023年1月16日
 * @version 1.0.0
 * @description
 */
@Api(value = "日期管理", tags = "日期管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysDaysController extends ReactiveBaseController {

    @Autowired
    private ISysDaysService daysService;

    @ApiOperation("根据指定年份查询日期配置")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:days:get")
    @GetMapping("/days/get")
    public Mono<JsonResult> singleget(@ApiParam(value = "查询年份") @RequestParam Integer year,
            @ApiParam(value = "是否返回二进制格式，默认十六进制格式") Boolean binary,
            @ApiIgnore ServerHttpRequest request) {
        String content = daysService.selectDaysHexData(year);
        if (null != binary && binary) {
            return Mono.just(JsonResult.success(JsonResult.SUCCESS_MSG, CodeUtils.hexStringToBinary(content)));
        } else {
            return Mono.just(JsonResult.success(JsonResult.SUCCESS_MSG, content));
        }
    }

    @ApiOperation("根据开始年份和结束年份查询日期配置")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:days:multiget")
    @GetMapping("/days/multiget")
    public Mono<JsonResult> multiget(@ApiParam(value = "开始年份") @RequestParam Integer startYear,
            @ApiParam(value = "结束年份") @RequestParam Integer endYear,
            @ApiParam(value = "是否返回二进制格式，默认十六进制格式") Boolean binary,
            @ApiIgnore ServerHttpRequest request) {
        List<String> data = daysService.multiselectDaysHexData(startYear, endYear);
        if (null != binary && binary) {
            List<String> list = data.stream().map(value -> CodeUtils.hexStringToBinary(value))
                    .collect(Collectors.toList());
            return Mono.just(JsonResult.success(list));
        } else {
            return Mono.just(JsonResult.success(data));
        }

    }

    @ApiOperation("根据开始日期和结束日期统计假期")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @OpenPermission("system:days:count")
    @GetMapping("/days/count")
    public Mono<JsonResult> count(@ApiParam(value = "开始日期，格式：yyyy-MM-dd") @RequestParam String start,
            @ApiParam(value = "结束日期，格式：yyyy-MM-dd") @RequestParam String end, @ApiIgnore ServerHttpRequest request) {
        HolidaysStats stats = HolidaysUtils.getDaysStats(start, end);
        return Mono.just(JsonResult.success(stats));
    }
}
