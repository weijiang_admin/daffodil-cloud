package com.daffodil.system.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysTag;
import com.daffodil.system.service.ISysTagService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * -标签信息控制层
 * @author yweijian
 * @date 2022年6月13日
 * @version 2.0.0
 * @description
 */
@Api(value = "标签管理", tags = "标签管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysTagController extends ReactiveBaseController {

    @Autowired
    private ISysTagService tagService;

    @ApiOperation("查询标签列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:tag:list")
    @GetMapping("/tag/list")
    public Mono<TableResult> list(SysTag tag, @ApiIgnore ServerHttpRequest request) {
        initQuery(tag, request);
        List<SysTag> tagList = tagService.selectTagList(query);
        return Mono.just(TableResult.success(tagList));
    }

    @ApiOperation("获取标签详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:tag:info")
    @GetMapping("/tag/info")
    public Mono<JsonResult> info(@ApiParam(value = "标签ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysTag tag = tagService.selectTagById(id);
        return Mono.just(JsonResult.success(tag));
    }
    
    @ApiOperation("新增标签")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:tag:add")
    @OperLog(title = "标签管理", type = Business.INSERT)
    @PostMapping("/tag/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysTag tag, @ApiIgnore ServerHttpRequest request) {
        tagService.insertTag(tag);
        return Mono.just(JsonResult.success(tag));
    }

    @ApiOperation("修改标签")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:tag:edit")
    @OperLog(title = "标签管理", type = Business.UPDATE)
    @PostMapping("/tag/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysTag tag, @ApiIgnore ServerHttpRequest request) {
        tagService.updateTag(tag);
        return Mono.just(JsonResult.success(tag));
    }

    @ApiOperation("删除标签")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:tag:remove")
    @OperLog(title = "标签管理", type = Business.DELETE)
    @PostMapping("/tag/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        tagService.deleteTagByIds(ids);
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("根据标签键值获取标签列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/tag/label")
    public Mono<JsonResult> label(@ApiParam(value = "标签键值", defaultValue = "user_tags") String tagLabel, @ApiIgnore ServerHttpRequest request) {
         List<SysTag> tags = tagService.selectTagByLabel(tagLabel);
        return Mono.just(JsonResult.success(tags));
    }
}
