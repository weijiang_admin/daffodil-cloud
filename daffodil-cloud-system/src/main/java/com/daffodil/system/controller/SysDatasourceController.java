package com.daffodil.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysDatasource;
import com.daffodil.system.service.ISysDatasourceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2024年12月12日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Api(value = "数据源管理", tags = "数据源管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysDatasourceController extends ReactiveBaseController{

    @Autowired
    private ISysDatasourceService datasourceService;

    @ApiOperation("分页查询数据源列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:datasource:list")
    @GetMapping("/datasource/list")
    public Mono<TableResult> list(SysDatasource datasource, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(datasource, page, request);
        List<SysDatasource> list = datasourceService.selectDatasourceList(query);
        return Mono.just(TableResult.success(list, query));
    }

    @ApiOperation("获取数据源详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:datasource:info")
    @GetMapping("/datasource/info")
    public Mono<JsonResult> info(@ApiParam(value = "数据源ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysDatasource datasource = datasourceService.selectDatasourceById(id);
        return Mono.just(JsonResult.success(datasource));
    }

    @ApiOperation("新增数据源")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:datasource:add")
    @OperLog(title = "数据源管理", type = Business.INSERT)
    @PostMapping("/datasource/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysDatasource datasource, @ApiIgnore ServerHttpRequest request) {
        datasourceService.insertDatasource(datasource);
        return Mono.just(JsonResult.success(datasource));
    }

    @ApiOperation("修改数据源")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:datasource:edit")
    @OperLog(title = "数据源管理", type = Business.UPDATE)
    @PostMapping("/datasource/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysDatasource datasource, @ApiIgnore ServerHttpRequest request) {
        datasourceService.updateDatasource(datasource);
        return Mono.just(JsonResult.success(datasource));
    }

    @ApiOperation("删除数据源")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:datasource:remove")
    @OperLog(title = "数据源管理", type = Business.DELETE)
    @PostMapping("/datasource/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        datasourceService.deleteDatasourceByIds(ids);
        return Mono.just(JsonResult.success());
    }
}
