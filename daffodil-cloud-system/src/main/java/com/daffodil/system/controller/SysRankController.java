package com.daffodil.system.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysRank;
import com.daffodil.system.service.ISysRankService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2022年6月13日
 * @version 2.0.0
 * @description
 */
@Api(value = "职级管理", tags = "职级管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysRankController extends ReactiveBaseController {

    @Autowired
    private ISysRankService rankService;

    @ApiOperation("分页查询职级列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:rank:list")
    @GetMapping("/rank/list")
    public Mono<TableResult> list(SysRank rank, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(rank, page, request);
        List<SysRank> list = rankService.selectRankList(query);
        return Mono.just(TableResult.success(list, query));
    }
    
    @ApiOperation("获取职级详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:rank:info")
    @GetMapping("/rank/info")
    public Mono<JsonResult> info(@ApiParam(value = "职级ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysRank rank = rankService.selectRankById(id);
        return Mono.just(JsonResult.success(rank));
    }
    
    @ApiOperation("新增职级")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:rank:add")
    @OperLog(title = "职级管理", type = Business.INSERT)
    @PostMapping("/rank/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysRank rank, @ApiIgnore ServerHttpRequest request) {
        rankService.insertRank(rank);
        return Mono.just(JsonResult.success(rank));
    }

    @ApiOperation("修改职级")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:rank:edit")
    @OperLog(title = "职级管理", type = Business.UPDATE)
    @PostMapping("/rank/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysRank rank, @ApiIgnore ServerHttpRequest request) {
        rankService.updateRank(rank);
        return Mono.just(JsonResult.success(rank));
    }

    @ApiOperation("删除职级")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:rank:remove")
    @OperLog(title = "职级管理", type = Business.DELETE)
    @PostMapping("/rank/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        rankService.deleteRankByIds(ids);
        return Mono.just(JsonResult.success());
    }
    
}
