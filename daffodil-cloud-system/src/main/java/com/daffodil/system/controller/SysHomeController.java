package com.daffodil.system.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysLoginInfo;
import com.daffodil.system.entity.SysOperLog;
import com.daffodil.system.entity.SysThirdApp;
import com.daffodil.system.service.ISysHomeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 首页
 * @author yweijian
 * @date 2022年2月17日
 * @version 1.0
 * @description
 */
@Api(value = "首页管理", tags = "首页管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysHomeController extends ReactiveBaseController{

    @Autowired
    private ISysHomeService homeService;
    
    @ApiOperation("用户登录详情总数")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/home/login/count")
    public Mono<JsonResult> countLoginInfo(@ApiParam(value = "登录状态 0=成功 1=失败") String status, @ApiIgnore LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        SysLoginInfo loginInfo = new SysLoginInfo();
        loginInfo.setLoginName(loginUser.getLoginName());
        loginInfo.setStatus(status);
        int count = homeService.countLoginInfo(loginInfo);
        return Mono.just(JsonResult.success(count));
    }
    
    @ApiOperation("用户使用浏览器分布详情总数")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/home/login/browser")
    public Mono<JsonResult> browserLoginInfo(@ApiIgnore LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        List<Map<String, String>> result = homeService.selectBrowserLoginInfo(loginUser.getLoginName());
        return Mono.just(JsonResult.success(result));
    }
    
    @ApiOperation("用户月登录详情总数")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/home/login/count/month")
    public Mono<JsonResult> countLoginInfoPerMonth(@ApiParam(value = "登录状态  0=成功 1=失败") String status, @ApiIgnore LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        SysLoginInfo loginInfo = new SysLoginInfo();
        loginInfo.setLoginName(loginUser.getLoginName());
        loginInfo.setStatus(status);
        List<Integer> counts = homeService.countLoginInfoPerMonth(loginInfo);
        return Mono.just(JsonResult.success(counts));
    }
    
    @ApiOperation("用户操作详情总数")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/home/operlog/count")
    public Mono<JsonResult> countOperLog(@ApiParam(value = "操作状态  0=成功 1=失败") String status, @ApiIgnore LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        SysOperLog operLog = new SysOperLog();
        operLog.setOperName(loginUser.getLoginName());
        operLog.setStatus(status);
        int count = homeService.countOperLog(operLog);
        return Mono.just(JsonResult.success(count));
    }
    
    @ApiOperation("用户月操作详情总数")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/home/operlog/count/month")
    public Mono<JsonResult> countOperLogPerMonth(@ApiParam(value = "操作状态  0=成功 1=失败") String status, @ApiIgnore LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        SysOperLog operLog = new SysOperLog();
        operLog.setOperName(loginUser.getLoginName());
        operLog.setStatus(status);
        List<Integer> counts = homeService.countOperLogPerMonth(operLog);
        return Mono.just(JsonResult.success(counts));
    }
    
    @ApiOperation("用户操作类型分布详情总数")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/home/operlog/business")
    public Mono<JsonResult> businessLabelOperLog(@ApiIgnore LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        List<Map<String, String>> result = homeService.selectBusinessLabelOperLog(loginUser.getLoginName());
        return Mono.just(JsonResult.success(result));
    }
    
    @ApiOperation("用户操作类型分布详情总数")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/home/user/thirdapp")
    public Mono<JsonResult> userThirdApp(@ApiIgnore LoginUser loginUser, @ApiIgnore ServerHttpRequest request){
        List<SysThirdApp> list = homeService.selectUserThirdAppList(loginUser);
        return Mono.just(JsonResult.success(list));
    }
}
