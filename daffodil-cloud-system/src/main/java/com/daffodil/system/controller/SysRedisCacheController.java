package com.daffodil.system.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.SerializationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.controller.model.RedisCacheData;
import com.daffodil.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * -Redis缓存管理
 * @author yweijian
 * @date 2022年9月30日
 * @version 2.0.0
 * @description
 */
@Slf4j
@Api(value = "缓存管理", tags = "缓存管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysRedisCacheController extends ReactiveBaseController {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    
    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;
    
    @ApiOperation("查询Redis缓存键值列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:cache:keys")
    @GetMapping("/rediscache/keys")
    public Mono<JsonResult> keys(@ApiParam(value = "缓存键值")String key, @ApiParam(value = "是否哈希键值")Boolean isHashkey, @ApiIgnore ServerHttpRequest request){
        if(isHashkey != null && isHashkey) {
            return Mono.just(JsonResult.success(redisTemplate.opsForHash().keys(key)));
        }
        Set<String> keys = tenantRedisTemplate.keys(key);
        if(!keys.isEmpty()) {
            List<String> list = keys.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
            List<RedisCacheData> datas = new ArrayList<RedisCacheData>(list.size());
            for(int i = 0; i< list.size(); i++) {
                RedisCacheData data = new RedisCacheData();
                DataType type = redisTemplate.type(list.get(i));
                data.setKey(list.get(i));
                data.setType(type.code());
                datas.add(data);
            }
            return Mono.just(JsonResult.success(datas));
        }
        return Mono.just(JsonResult.success(keys));
    }
    
    @ApiOperation("根据Redis缓存键值获取数据")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:cache:values")
    @GetMapping("/rediscache/values")
    public Mono<JsonResult> values(@ApiParam(value = "缓存键值")String key, @ApiParam(value = "哈希键值")String hashKey, @ApiIgnore ServerHttpRequest request){
        RedisCacheData data = new RedisCacheData();
        DataType type = redisTemplate.type(key);
        data.setKey(key);
        data.setHashKey(hashKey);
        data.setType(type.code());
        data.setExpire(redisTemplate.opsForValue().getOperations().getExpire(key));
        try {
            if(DataType.STRING.equals(type)) {
                data.setValue(redisTemplate.opsForValue().get(key));
            }else if(DataType.HASH.equals(type) && StringUtils.isNotEmpty(hashKey)) {
                data.setValue(redisTemplate.opsForHash().get(key, hashKey));
            }else if(DataType.LIST.equals(type)) {
                data.setValue(redisTemplate.opsForList().range(key, 0, -1));
            }else if(DataType.SET.equals(type)) {
                data.setValue(redisTemplate.opsForSet().members(key));
            }else if(DataType.ZSET.equals(type)) {
                data.setValue(redisTemplate.opsForZSet().range(key, 0, -1));
            }else if(DataType.STREAM.equals(type)) {
                data.setValue("[Stream]");
            }else {
                data.setValue("[Object]");
            }
        }catch (SerializationException e) {
            log.error(e.getMessage());
            data.setValue("[Object]");
        }
        return Mono.just(JsonResult.success(data));
    }
    
}
