package com.daffodil.system.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysPost;
import com.daffodil.system.service.ISysPostService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2021年10月9日
 * @version 1.0
 * @description
 */
@Api(value = "岗位管理", tags = "岗位管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysPostController extends ReactiveBaseController {

    @Autowired
    private ISysPostService postService;

    @ApiOperation("分页查询岗位列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:post:list")
    @GetMapping("/post/list")
    public Mono<TableResult> list(SysPost post, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(post, page, request);
        List<SysPost> list = postService.selectPostList(query);
        return Mono.just(TableResult.success(list, query));
    }
    
    @ApiOperation("获取岗位详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:post:info")
    @GetMapping("/post/info")
    public Mono<JsonResult> info(@ApiParam(value = "岗位ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysPost post = postService.selectPostById(id);
        return Mono.just(JsonResult.success(post));
    }
    
    @ApiOperation("新增岗位")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:post:add")
    @OperLog(title = "岗位管理", type = Business.INSERT)
    @PostMapping("/post/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysPost post, @ApiIgnore ServerHttpRequest request) {
        postService.insertPost(post);
        return Mono.just(JsonResult.success(post));
    }

    @ApiOperation("修改岗位")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:post:edit")
    @OperLog(title = "岗位管理", type = Business.UPDATE)
    @PostMapping("/post/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysPost post, @ApiIgnore ServerHttpRequest request) {
        postService.updatePost(post);
        return Mono.just(JsonResult.success(post));
    }

    @ApiOperation("删除岗位")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:post:remove")
    @OperLog(title = "岗位管理", type = Business.DELETE)
    @PostMapping("/post/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        postService.deletePostByIds(ids);
        return Mono.just(JsonResult.success());
    }
    
}
