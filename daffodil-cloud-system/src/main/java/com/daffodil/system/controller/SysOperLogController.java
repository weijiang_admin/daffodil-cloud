package com.daffodil.system.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysOperLog;
import com.daffodil.system.service.ISysOperLogService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2021年10月9日
 * @version 1.0
 * @description
 */
@Api(value = "操作日志管理", tags = "操作日志管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysOperLogController extends ReactiveBaseController {

    @Autowired
    private ISysOperLogService operLogService;

    @ApiOperation("分页查询日志列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:operlog:list")
    @GetMapping("/operlog/list")
    public Mono<TableResult> list(SysOperLog operLog, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(operLog, page, request);
        List<SysOperLog> list = operLogService.selectOperLogList(query);
        return Mono.just(TableResult.success(list, query));
    }
    
    @ApiOperation("获取日志详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:operlog:info")
    @GetMapping("/operlog/info")
    public Mono<JsonResult> info(@ApiParam(value = "日志ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysOperLog operLog = operLogService.selectOperLogById(id);
        return Mono.just(JsonResult.success(operLog));
    }
    
    @ApiOperation("新增日志")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:operlog:add")
    @PostMapping("/operlog/add")
    public Mono<JsonResult> add(@RequestBody SysOperLog operLog, @ApiIgnore ServerHttpRequest request) {
        operLogService.insertOperlog(operLog);
        return Mono.just(JsonResult.success());
    }
    
    @ApiOperation("删除日志")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:operlog:remove")
    @OperLog(title = "操作日志管理", type = Business.DELETE)
    @PostMapping("/operlog/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        operLogService.deleteOperLogByIds(ids);
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("清空日志")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:operlog:clean")
    @OperLog(title = "操作日志管理", type = Business.CLEAN)
    @PostMapping("/operlog/clean")
    public Mono<JsonResult> clean(@ApiIgnore ServerHttpRequest request) {
        operLogService.cleanOperLog();
        return Mono.just(JsonResult.success());
    }
    
}
