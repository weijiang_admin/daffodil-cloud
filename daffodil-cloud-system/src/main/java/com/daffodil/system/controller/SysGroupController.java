package com.daffodil.system.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.controller.model.UserGroupParam;
import com.daffodil.system.entity.SysGroup;
import com.daffodil.system.entity.user.SysUserGroup;
import com.daffodil.system.service.ISysGroupService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2022年6月21日
 * @version 2.0.0
 * @description
 */
@Api(value = "群组管理", tags = "群组管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysGroupController extends ReactiveBaseController {

    @Autowired
    private ISysGroupService groupService;

    @ApiOperation("分页查询群组列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:group:list")
    @GetMapping("/group/list")
    public Mono<TableResult> list(SysGroup group, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(group, page, request);
        List<SysGroup> groupList = groupService.selectGroupList(query);
        return Mono.just(TableResult.success(groupList, query));
    }
    
    @ApiOperation("获取群组详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:group:info")
    @GetMapping("/group/info")
    public Mono<JsonResult> info(@ApiParam(value = "群组ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysGroup group = groupService.selectGroupById(id);
        return Mono.just(JsonResult.success(group));
    }
    
    @ApiOperation("新增群组")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:group:add")
    @OperLog(title = "群组管理", type = Business.INSERT)
    @PostMapping("/group/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysGroup group, @ApiIgnore ServerHttpRequest request) {
        groupService.insertGroup(group);
        return Mono.just(JsonResult.success(group));
    }

    @ApiOperation("修改群组")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:group:edit")
    @OperLog(title = "群组管理", type = Business.UPDATE)
    @PostMapping("/group/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysGroup group, @ApiIgnore ServerHttpRequest request) {
        groupService.updateGroup(group);
        return Mono.just(JsonResult.success(group));
    }

    @ApiOperation("删除群组")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:group:remove")
    @OperLog(title = "群组管理", type = Business.DELETE)
    @PostMapping("/group/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        groupService.deleteGroupByIds(ids);
        return Mono.just(JsonResult.success());
    }
    
    @ApiOperation("分页查询群组用户列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("group:user:list")
    @GetMapping("/group/user/list")
    public Mono<TableResult> userList(SysUserGroup group, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(group, page, request);
        List<SysUserGroup> list = groupService.selectUserGroupList(query);
        return Mono.just(TableResult.success(list, query));
    }
    
    @ApiOperation("添加群组用户")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("group:user:add")
    @OperLog(title = "群组管理", type = Business.INSERT)
    @PostMapping("/group/user/add")
    public Mono<JsonResult> useradd(@Validated @RequestBody UserGroupParam param, @ApiIgnore ServerHttpRequest request) {
        groupService.insertUserGroup(param.getGroupId(), param.getUserIds());
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("修改群组用户")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("group:user:edit")
    @OperLog(title = "群组管理", type = Business.UPDATE)
    @PostMapping("/group/user/edit")
    public Mono<JsonResult> useredit(@Validated @RequestBody SysUserGroup group, @ApiIgnore ServerHttpRequest request) {
        groupService.updateUserGroup(group);
        return Mono.just(JsonResult.success(group));
    }

    @ApiOperation("删除群组用户")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("group:user:remove")
    @OperLog(title = "群组管理", type = Business.DELETE)
    @PostMapping("/group/user/remove")
    public Mono<JsonResult> userremove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        groupService.deleteUserGroupByIds(ids);
        return Mono.just(JsonResult.success());
    }
}
