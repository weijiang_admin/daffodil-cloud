package com.daffodil.system.controller.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2022年6月23日
 * @version 2.0.0
 * @description
 */
@ApiModel(value = "查询通知消息参数")
@Data
public class QueryNoticeParam implements Serializable {

    private static final long serialVersionUID = 5020318044372892575L;

    /** 公告标题 */
    @ApiModelProperty(name = "noticeTitle", value = "公告标题")
    private String noticeTitle;
    
    /** 公告类型 */
    @ApiModelProperty(name = "noticeType", value = "公告类型")
    private String noticeType;
    
    /** 用户ID */
    @ApiModelProperty(name = "userId", value = "用户ID")
    private String userId;

    /** 通知ID */
    @ApiModelProperty(name = "noticeId", value = "通知消息ID")
    private String noticeId;
    
    /** 公告状态 0=未读 1=已读 */
    @ApiModelProperty(name = "readStatus", value = "公告状态 0=未读 1=已读")
    private String readStatus;
    
    @ApiModelProperty(name = "startTime", value = "开始时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    
    @ApiModelProperty(name = "endTime", value = "结束时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date endTime;
}
