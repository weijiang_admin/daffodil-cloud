package com.daffodil.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.controller.model.QueryNoticeParam;
import com.daffodil.system.entity.SysNotice;
import com.daffodil.system.entity.SysSocialUser;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.service.ISysNoticeService;
import com.daffodil.system.service.ISysSocialUserService;
import com.daffodil.system.service.ISysUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * -用户用户中心
 * @author yweijian
 * @date 2022年6月23日
 * @version 2.0.0
 * @description
 */
@Api(value = "用户中心", tags = "用户中心")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysPersonalController extends ReactiveBaseController {

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysSocialUserService socialUserService;

    @Autowired
    private ISysNoticeService noticeService;

    @ApiOperation("用户中心基本信息修改和密码修改")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @OperLog(title = "用户中心", type = Business.UPDATE)
    @PostMapping("/user/info/edit")
    public Mono<JsonResult> editUserInfo(@Validated @RequestBody SysUser user, LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        if(user == null || loginUser == null || !user.getId().equals(loginUser.getId())) {
            throw new BaseException(37111);
        }
        userService.updateUserInfo(user);
        return Mono.just(JsonResult.success());
    } 

    @ApiOperation("触发读取用户消息数据")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @PostMapping("/user/notice/target")
    public Mono<JsonResult> target(LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        if(null != loginUser) {
            noticeService.triggerNoticeTargetByUserId(loginUser.getId());
        }
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("分页查询用户消息列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/user/notice/list")
    public Mono<TableResult> notice(QueryNoticeParam param, Page page, LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        initQuery(page, request);
        List<SysNotice> list = null;
        if(null != loginUser) {
            param.setUserId(loginUser.getId());
            list = noticeService.selectNoticeList(param, page);
        }
        return Mono.just(TableResult.success(list, query));
    }

    @ApiOperation("通知消息标为已读")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @OperLog(title = "用户中心", type = Business.UPDATE)
    @PostMapping("/user/notice/read")
    public Mono<JsonResult> read(@RequestBody String[] ids, LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        if(null != loginUser) {
            noticeService.updateNoticeReadStatusByIds(ids, loginUser.getId());
        }
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("分页查询用户绑定第三方账号列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @GetMapping("/user/social/list")
    public Mono<TableResult> list(SysSocialUser user, Page page, LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        List<SysSocialUser> list = null;
        if(null != loginUser) {
            user.setUserId(loginUser.getId());
            initQuery(user, page, request);
            list = socialUserService.selectSocialUserList(query);
        }
        return Mono.just(TableResult.success(list, query));
    }

    @ApiOperation("用户解绑第三方账号")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @OperLog(title = "第三方账号管理", business = @OperBusiness(name = "解绑", label = "FREE", remark = "用户解绑第三方账号"))
    @PostMapping("/user/social/free")
    public Mono<JsonResult> free(@Validated @RequestBody SysSocialUser user, LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        if(null != loginUser) {
            user.setUserId(loginUser.getId());
            socialUserService.freeSocialUser(user);
        }
        return Mono.just(JsonResult.success());
    }
}
