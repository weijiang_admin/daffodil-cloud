package com.daffodil.system.controller.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2022年12月19日
 * @version 2.0.0
 * @description
 */
@ApiModel(value = "路由注册参数")
@Data
public class RouterParam {

    /** 路由类型 */
    @ApiModelProperty(name = "key", value = "路由类型")
    private String key;

    /** 路由名称 */
    @ApiModelProperty(name = "path", value = "路由名称")
    private String path;

    /** 权限标识 */
    @ApiModelProperty(name = "perm", value = "权限标识")
    private String perm;

    /** 权限名称 */
    @ApiModelProperty(name = "name", value = "权限名称")
    private String name;

    /** 权限归属 */
    @ApiModelProperty(name = "belong", value = "权限归属")
    private String belong;

    /** 权限模块 */
    @ApiModelProperty(name = "module", value = "权限模块")
    private String module;

    /** 权限备注 */
    @ApiModelProperty(name = "remark", value = "权限备注")
    private String remark;
}
