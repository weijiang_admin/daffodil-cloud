package com.daffodil.system.controller;

import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperBusiness;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysHoliday;
import com.daffodil.system.service.ISysHolidayService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * -假期管理控制层
 * @author EP
 * @date 2023年1月16日
 * @version 1.0.0
 * @description
 */
@Api(value = "假期管理", tags = "假期管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysHolidayController extends ReactiveBaseController {

    @Autowired
    private ISysHolidayService holidayService;

    @ApiOperation("查询假期列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:holiday:list")
    @GetMapping("/holiday/list")
    public Mono<TableResult> list(SysHoliday holiday, @ApiIgnore ServerHttpRequest request) {
        initQuery(holiday, request);
        List<SysHoliday> holidayList = holidayService.selectHolidayList(query);
        return Mono.just(TableResult.success(holidayList));
    }

    @ApiOperation("设置假期")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:holiday:set")
    @OperLog(title = "设置假期", business = @OperBusiness(name = "设置", label = "SETTING"))
    @PostMapping("/holiday/set")
    public Mono<JsonResult> set(@Validated @RequestBody SysHoliday holiday, @ApiIgnore ServerHttpRequest request) {
        holidayService.setHoliday(holiday);
        return Mono.just(JsonResult.success(holiday));
    }
}
