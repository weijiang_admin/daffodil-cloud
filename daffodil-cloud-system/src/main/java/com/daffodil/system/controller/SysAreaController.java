package com.daffodil.system.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysArea;
import com.daffodil.system.service.ISysAreaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * -区划信息控制层
 * @author yweijian
 * @date 2022年6月14日
 * @version 2.0.0
 * @description
 */
@Api(value = "区划管理", tags = "区划管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysAreaController extends ReactiveBaseController {

    @Autowired
    private ISysAreaService areaService;

    @ApiOperation("查询区划列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:area:list")
    @GetMapping("/area/list")
    public Mono<TableResult> list(SysArea area, @ApiIgnore ServerHttpRequest request) {
        initQuery(area, request);
        List<SysArea> list = areaService.selectAreaList(query);
        return Mono.just(TableResult.success(list));
    }

    @ApiOperation("获取区划详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/area/info")
    public Mono<JsonResult> info(@ApiParam(value = "区划ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysArea area = areaService.selectAreaById(id);
        return Mono.just(JsonResult.success(area));
    }

    @ApiOperation("新增区划")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:area:add")
    @OperLog(title = "区划管理", type = Business.INSERT)
    @PostMapping("/area/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysArea area, @ApiIgnore ServerHttpRequest request) {
        areaService.insertArea(area);
        return Mono.just(JsonResult.success(area));
    }

    @ApiOperation("修改区划")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:area:edit")
    @OperLog(title = "区划管理", type = Business.UPDATE)
    @PostMapping("/area/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysArea area, @ApiIgnore ServerHttpRequest request) {
        areaService.updateArea(area);
        return Mono.just(JsonResult.success(area));
    }

    @ApiOperation("删除区划")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:area:remove")
    @OperLog(title = "区划管理", type = Business.DELETE)
    @PostMapping("/area/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        areaService.deleteAreaByIds(ids);
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("根据父级编码查询子级区划列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/area/child")
    public Mono<TableResult> child(@ApiParam(value = "父级编号") String parentId, @ApiIgnore ServerHttpRequest request) {
        List<SysArea> list = areaService.selectAreaListByParentId(parentId);
        return Mono.just(TableResult.success(list));
    }

}
