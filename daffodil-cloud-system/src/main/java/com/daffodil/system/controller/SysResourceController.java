package com.daffodil.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysResource;
import com.daffodil.system.service.ISysResourceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;
import reactor.core.publisher.Mono;

/**
 * -开放资源信息控制层
 * @author yweijian
 * @date 2023年4月27日
 * @version 2.0.0
 * @description
 */
@Api(value = "开放资源管理", tags = "开放资源管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysResourceController extends ReactiveBaseController {

    @Autowired
    private ISysResourceService resourceService;

    @ApiOperation("查询开放资源信息列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:resource:list")
    @GetMapping("/resource/list")
    public Mono<TableResult> list(SysResource resource, @ApiIgnore ServerHttpRequest request) {
        initQuery(resource, request);
        List<SysResource> list = resourceService.selectResourceList(query);
        return Mono.just(TableResult.success(list));
    }

    @ApiOperation("获取开放资源信息详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:resource:info")
    @GetMapping("/resource/info")
    public Mono<JsonResult> info(@ApiParam(value = "开放资源信息ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysResource resource = resourceService.selectResourceById(id);
        return Mono.just(JsonResult.success(resource));
    }

    @ApiOperation("新增开放资源信息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:resource:add")
    @OperLog(title = "开放资源管理", type = Business.INSERT)
    @PostMapping("/resource/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysResource resource, @ApiIgnore ServerHttpRequest request) {
        resourceService.insertResource(resource);
        return Mono.just(JsonResult.success(resource));
    }

    @ApiOperation("修改开放资源信息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:resource:edit")
    @OperLog(title = "开放资源管理", type = Business.UPDATE)
    @PostMapping("/resource/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysResource resource, @ApiIgnore ServerHttpRequest request) {
        resourceService.updateResource(resource);
        return Mono.just(JsonResult.success(resource));
    }

    @ApiOperation("删除开放资源信息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:resource:remove")
    @OperLog(title = "开放资源管理", type = Business.DELETE)
    @PostMapping("/resource/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        resourceService.deleteResourceByIds(ids);
        return Mono.just(JsonResult.success());
    }

}
