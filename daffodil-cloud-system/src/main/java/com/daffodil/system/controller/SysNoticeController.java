package com.daffodil.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.controller.model.SysNoticeParam;
import com.daffodil.system.entity.SysNotice;
import com.daffodil.system.service.ISysNoticeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 公告 信息控制层
 * @author yweijian
 * @date 2021年10月21日
 * @version 1.0
 * @description
 */
@Api(value = "消息管理", tags = "消息管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysNoticeController extends ReactiveBaseController {

    @Autowired
    private ISysNoticeService noticeService;

    @ApiOperation("分页查询消息列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:notice:list")
    @GetMapping("/notice/list")
    public Mono<TableResult> list(SysNotice notice, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(notice, page, request);
        List<SysNotice> list = noticeService.selectNoticeList(query);
        return Mono.just(TableResult.success(list, query));
    }
    
    @ApiOperation("获取消息详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:notice:info")
    @GetMapping("/notice/info")
    public Mono<JsonResult> info(@ApiParam(value = "消息ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysNotice notice = noticeService.selectNoticeById(id);
        return Mono.just(JsonResult.success(notice));
    }

    @ApiOperation("新增消息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:notice:add")
    @OperLog(title = "通知公告", type = Business.INSERT)
    @PostMapping("/notice/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysNotice notice, @ApiIgnore ServerHttpRequest request) {
        noticeService.insertNotice(notice);
        return Mono.just(JsonResult.success(notice));
    }

    @ApiOperation("修改消息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:notice:edit")
    @OperLog(title = "通知公告", type = Business.UPDATE)
    @PostMapping("/notice/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysNotice notice, @ApiIgnore ServerHttpRequest request) {
        noticeService.updateNotice(notice);
        return Mono.just(JsonResult.success(notice));
    }

    @ApiOperation("删除消息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:notice:remove")
    @OperLog(title = "通知公告", type = Business.DELETE)
    @PostMapping("/notice/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        noticeService.deleteNoticeByIds(ids);
        return Mono.just(JsonResult.success());
    }
    
    @ApiOperation("发布消息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:notice:pub")
    @OperLog(title = "通知公告", business = @OperBusiness(name = "发布", label = "PUBLISH"))
    @PostMapping("/notice/pub")
    public Mono<JsonResult> pub(@Validated @RequestBody SysNoticeParam param, @ApiIgnore ServerHttpRequest request) {
        noticeService.publishNotice(param);
        return Mono.just(JsonResult.success());
    }
}
