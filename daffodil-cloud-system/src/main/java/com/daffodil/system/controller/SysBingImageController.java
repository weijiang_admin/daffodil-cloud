package com.daffodil.system.controller;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.util.BingImageUtils;
import com.daffodil.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpHeaders;
import reactor.core.publisher.Mono;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author yweijian
 * @date 2025年3月12日
 * @version 2.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Api(value = "必应图片管理", tags = "必应图片管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysBingImageController extends ReactiveBaseController {

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    @ApiOperation("获取必应图片列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @RequestMapping("/bing/image/list")
    public Mono<JsonResult> getListImageUrl(@RequestParam(value = "dix", required = false, defaultValue = "0") Integer dix,
            @RequestParam(value = "num", required = false, defaultValue = "1") Integer num,
            @RequestParam(value = "size", required = false, defaultValue = "1920x1080") String size,
            @ApiIgnore ServerHttpRequest request) {
        List<String> imageUrls = BingImageUtils.getImageUrls(dix, num, BingImageUtils.HPSIZE.test(size));
        return Mono.just(JsonResult.success(JsonResult.SUCCESS_MSG, imageUrls)); 
    }

    @ApiOperation("获取一张随机图片")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @RequestMapping("/bing/image/random")
    public Mono<JsonResult> getRandomImageUrl(@RequestParam(value = "size", required = false, defaultValue = "1920x1080") String size, 
            @ApiIgnore ServerHttpRequest request) {
        String imageUrl = BingImageUtils.getRandomImageUrl(BingImageUtils.HPSIZE.test(size));
        return Mono.just(JsonResult.success(JsonResult.SUCCESS_MSG, imageUrl));
    }

    @ApiOperation("获取一张今日图片")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @RequestMapping("/bing/image/today")
    public Mono<JsonResult> getTodayImageUrl(@RequestParam(value = "size", required = false, defaultValue = "1920x1080") String size, 
            @ApiIgnore ServerHttpRequest request) {
        String cacheKey = "bing:image:today:" + BingImageUtils.HPSIZE.test(size);
        Object cacheUrl = tenantRedisTemplate.opsForValue().get(cacheKey);
        if (cacheUrl != null) {
            return Mono.just(JsonResult.success(cacheUrl));
        }
        String imageUrl = BingImageUtils.getTodayImageUrl(BingImageUtils.HPSIZE.test(size));
        if(StringUtils.isNotBlank(imageUrl)) {
            tenantRedisTemplate.opsForValue().set(cacheKey, imageUrl, 24, TimeUnit.HOURS);
        }
        return Mono.just(JsonResult.success(JsonResult.SUCCESS_MSG, imageUrl));
    }

    @ApiOperation("随机预览一张图片")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @RequestMapping("/bing/image/preview/random")
    public Mono<Void> previewRandomImage(@RequestParam(value = "size", required = false, defaultValue = "1920x1080") String size,
            @ApiIgnore ServerHttpRequest request, @ApiIgnore ServerHttpResponse response) {
        return Mono.fromCallable(() -> BingImageUtils.getRandomImageUrl(BingImageUtils.HPSIZE.test(size)))
                .flatMap(url -> Mono.fromCallable(() -> {
                    try (InputStream in = new URL(url).openStream()) {
                        return IOUtils.toByteArray(in);
                    }
                }))
                .flatMap(imageBytes -> {
                    response.getHeaders().setContentType(MediaType.IMAGE_JPEG);
                    return response.writeWith(Mono.just(response.bufferFactory().wrap(imageBytes)));
                })
                .onErrorResume(e -> {
                    e.printStackTrace();
                    return response.setComplete();
                });
    }

    @ApiOperation("预览今日一张图片")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @RequestMapping("/bing/image/preview/today")
    public Mono<Void> previewTodayImage(@RequestParam(value = "size", required = false, defaultValue = "1920x1080") String size,
            @ApiIgnore ServerHttpRequest request, @ApiIgnore ServerHttpResponse response) {
        String cacheKey = "bing:image:preview:today:" + BingImageUtils.HPSIZE.test(size);
        Object cacheBytes = tenantRedisTemplate.opsForValue().get(cacheKey);
        if(cacheBytes != null) {
            response.getHeaders().setContentType(MediaType.IMAGE_JPEG);
            return response.writeWith(Mono.just(response.bufferFactory().wrap((byte[]) cacheBytes)));
        }

        return Mono.fromCallable(() -> BingImageUtils.getTodayImageUrl(BingImageUtils.HPSIZE.test(size)))
                .flatMap(url -> Mono.fromCallable(() -> {
                    try (InputStream in = new URL(url).openStream()) {
                        return IOUtils.toByteArray(in);
                    }
                }))
                .flatMap(imageBytes -> {
                    tenantRedisTemplate.opsForValue().set(cacheKey, imageBytes, 24, TimeUnit.HOURS);
                    response.getHeaders().setContentType(MediaType.IMAGE_JPEG);
                    return response.writeWith(Mono.just(response.bufferFactory().wrap(imageBytes)));
                })
                .onErrorResume(e -> {
                    e.printStackTrace();
                    return response.setComplete();
                });
    }

}
