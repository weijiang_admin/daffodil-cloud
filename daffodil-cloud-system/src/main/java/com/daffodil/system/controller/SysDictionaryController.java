package com.daffodil.system.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.controller.ReactiveBaseController;

import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysDictionary;
import com.daffodil.system.service.ISysDictionaryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 字典信息控制层
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
@Api(value = "字典管理", tags = "字典管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysDictionaryController extends ReactiveBaseController {

    @Autowired
    private ISysDictionaryService dictionaryService;

    @ApiOperation("查询字典列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:dictionary:list")
    @GetMapping("/dictionary/list")
    public Mono<TableResult> list(SysDictionary dictionary, @ApiIgnore ServerHttpRequest request) {
        initQuery(dictionary, request);
        List<SysDictionary> dictionaryList = dictionaryService.selectDictionaryList(query);
        return Mono.just(TableResult.success(dictionaryList));
    }

    @ApiOperation("获取字典详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:dictionary:info")
    @GetMapping("/dictionary/info")
    public Mono<JsonResult> info(@ApiParam(value = "字典ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysDictionary dictionary = dictionaryService.selectDictionaryById(id);
        return Mono.just(JsonResult.success(dictionary));
    }
    
    @ApiOperation("新增字典")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:dictionary:add")
    @OperLog(title = "字典管理", type = Business.INSERT)
    @PostMapping("/dictionary/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysDictionary dictionary, @ApiIgnore ServerHttpRequest request) {
        dictionaryService.insertDictionary(dictionary);
        return Mono.just(JsonResult.success(dictionary));
    }

    @ApiOperation("修改字典")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:dictionary:edit")
    @OperLog(title = "字典管理", type = Business.UPDATE)
    @PostMapping("/dictionary/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysDictionary dictionary, @ApiIgnore ServerHttpRequest request) {
        dictionaryService.updateDictionary(dictionary);
        return Mono.just(JsonResult.success(dictionary));
    }

    @ApiOperation("删除字典")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:dictionary:remove")
    @OperLog(title = "字典管理", type = Business.DELETE)
    @PostMapping("/dictionary/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        dictionaryService.deleteDictionaryByIds(ids);
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("根据目录键值获取字典列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/dictionary/label")
    public Mono<JsonResult> label(@ApiParam(value = "目录键值", defaultValue = "sys_data_status") String dictLabel, @ApiIgnore ServerHttpRequest request) {
         List<SysDictionary> dictionarys = dictionaryService.selectDictionaryByLabel(dictLabel);
        return Mono.just(JsonResult.success(dictionarys));
    }
}
