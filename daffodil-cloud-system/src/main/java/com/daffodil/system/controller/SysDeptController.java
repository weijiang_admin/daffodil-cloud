package com.daffodil.system.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.controller.ReactiveBaseController;

import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysDept;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.service.ISysDeptService;
import com.daffodil.system.service.ISysUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 部门信息控制层
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
@Api(value = "部门管理", tags = "部门管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysDeptController extends ReactiveBaseController {

    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private ISysUserService userService;
    
    @ApiOperation("查询部门列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:dept:list")
    @GetMapping("/dept/list")
    public Mono<TableResult> list(SysDept dept, LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        initQuery(dept, request);
        SysUser user = userService.selectUserById(loginUser.getId());
        List<SysDept> deptList = deptService.selectDeptList(query, user);
        return Mono.just(TableResult.success(deptList));
    }

    @ApiOperation("获取部门详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:dept:info")
    @GetMapping("/dept/info")
    public Mono<JsonResult> info(@ApiParam(value = "部门ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysDept dept = deptService.selectDeptById(id);
        return Mono.just(JsonResult.success(dept));
    }
    
    @ApiOperation("新增部门")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:dept:add")
    @OperLog(title = "部门管理", type = Business.INSERT)
    @PostMapping("/dept/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysDept dept, @ApiIgnore ServerHttpRequest request) {
        deptService.insertDept(dept);
        return Mono.just(JsonResult.success(dept));
    }

    @ApiOperation("修改部门")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:dept:edit")
    @OperLog(title = "部门管理", type = Business.UPDATE)
    @PostMapping("/dept/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysDept dept, @ApiIgnore ServerHttpRequest request) {
        deptService.updateDept(dept);
        return Mono.just(JsonResult.success(dept));
    }

    @ApiOperation("删除部门")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:dept:remove")
    @OperLog(title = "部门管理", type = Business.DELETE)
    @PostMapping("/dept/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        deptService.deleteDeptByIds(ids);
        return Mono.just(JsonResult.success());
    }

}
