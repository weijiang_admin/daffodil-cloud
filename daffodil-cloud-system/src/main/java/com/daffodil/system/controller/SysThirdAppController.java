package com.daffodil.system.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysThirdApp;
import com.daffodil.system.service.ISysThirdAppService;

import cn.hutool.crypto.digest.DigestAlgorithm;
import cn.hutool.crypto.digest.Digester;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2023年1月3日
 * @version 2.0.0
 * @description
 */
@Api(value = "第三方应用管理", tags = "第三方应用管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysThirdAppController extends ReactiveBaseController {

    @Autowired
    private ISysThirdAppService thirdAppService;

    @ApiOperation("分页查询第三方应用列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:thirdapp:list")
    @GetMapping("/thirdapp/list")
    public Mono<TableResult> list(SysThirdApp app, Page page, LoginUser loginUser, @ApiIgnore ServerHttpRequest request) {
        initQuery(app, page, request);
        List<SysThirdApp> list = thirdAppService.selectThirdAppList(query, loginUser);
        return Mono.just(TableResult.success(list, query));
    }
    
    @ApiOperation("获取第三方应用详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:thirdapp:info")
    @GetMapping("/thirdapp/info")
    public Mono<JsonResult> info(@ApiParam(value = "第三方应用ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysThirdApp app = thirdAppService.selectThirdAppById(id);
        return Mono.just(JsonResult.success(app));
    }
    
    @ApiOperation("新增第三方应用")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:thirdapp:add")
    @OperLog(title = "第三方应用管理", type = Business.INSERT)
    @PostMapping("/thirdapp/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysThirdApp app, @ApiIgnore ServerHttpRequest request) {
        thirdAppService.insertThirdApp(app);
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("修改第三方应用")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:thirdapp:edit")
    @OperLog(title = "第三方应用管理", type = Business.UPDATE)
    @PostMapping("/thirdapp/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysThirdApp app, @ApiIgnore ServerHttpRequest request) {
        thirdAppService.updateThirdApp(app);
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("删除第三方应用")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:thirdapp:remove")
    @OperLog(title = "第三方应用管理", type = Business.DELETE)
    @PostMapping("/thirdapp/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        thirdAppService.deleteThirdAppByIds(ids);
        return Mono.just(JsonResult.success());
    }
    
    @ApiOperation("重置第三方应用秘钥")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:thirdapp:reset")
    @OperLog(title = "第三方应用管理", business = @OperBusiness(name = "重置", label = "RESET", remark = "重置第三方应用秘钥"))
    @PostMapping("/thirdapp/reset")
    public Mono<JsonResult> reset(@Validated @RequestBody SysThirdApp app, @ApiIgnore ServerHttpRequest request) {
        String source = UUID.randomUUID().toString();
        String secret = new Digester(DigestAlgorithm.SHA512).digestHex(source);
        app.setAppSecret(secret);
        thirdAppService.resetThirdApp(app);
        return Mono.just(JsonResult.success());
    }
    
}
