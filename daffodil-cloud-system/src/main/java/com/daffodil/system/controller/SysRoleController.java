package com.daffodil.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysRole;
import com.daffodil.system.service.ISysRoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2021年10月9日
 * @version 1.0
 * @description
 */
@Api(value = "角色管理", tags = "角色管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysRoleController extends ReactiveBaseController {
    
    @Autowired
    private ISysRoleService roleService;

    @ApiOperation("分页查询角色列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:role:list")
    @GetMapping("/role/list")
    public Mono<TableResult> list(SysRole role, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(role, page, request);
        List<SysRole> list = roleService.selectRoleList(query);
        return Mono.just(TableResult.success(list, query));
    }

    @ApiOperation("获取角色详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:role:info")
    @GetMapping("/role/info")
    public Mono<JsonResult> info(@ApiParam(value = "角色ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysRole role = roleService.selectRoleById(id);
        return Mono.just(JsonResult.success(role));
    }
    
    @ApiOperation("新增角色")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:role:add")
    @OperLog(title = "角色管理", type = Business.INSERT)
    @PostMapping("/role/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysRole role, @ApiIgnore ServerHttpRequest request) {
        roleService.insertRole(role);
        return Mono.just(JsonResult.success(role));
    }

    @ApiOperation("修改角色")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:role:edit")
    @OperLog(title = "角色管理", type = Business.UPDATE)
    @PostMapping("/role/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysRole role, @ApiIgnore ServerHttpRequest request) {
        roleService.updateRole(role);
        return Mono.just(JsonResult.success(role));
    }

    @ApiOperation("删除角色")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:role:remove")
    @OperLog(title = "角色管理", type = Business.DELETE)
    @PostMapping("/role/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        roleService.deleteRoleByIds(ids);
        return Mono.just(JsonResult.success());
    }

}