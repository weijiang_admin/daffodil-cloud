package com.daffodil.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysMenu;
import com.daffodil.system.entity.SysTenant;
import com.daffodil.system.enums.DataStatusEnum;
import com.daffodil.system.service.ISysTenantService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2024年12月12日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Api(value = "租户管理", tags = "租户管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysTenantController extends ReactiveBaseController{

    @Autowired
    private ISysTenantService tenantService;

    @ApiOperation("分页查询租户列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("system:tenant:list")
    @GetMapping("/tenant/list")
    public Mono<TableResult> list(SysTenant tenant, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(tenant, page, request);
        List<SysTenant> list = tenantService.selectTenantList(query);
        return Mono.just(TableResult.success(list, query));
    }

    @ApiOperation("获取租户详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:tenant:info")
    @GetMapping("/tenant/info")
    public Mono<JsonResult> info(@ApiParam(value = "租户ID") String id, @ApiIgnore ServerHttpRequest request) {
        SysTenant tenant = tenantService.selectTenantById(id);
        return Mono.just(JsonResult.success(tenant));
    }

    @ApiOperation("新增租户")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:tenant:add")
    @OperLog(title = "租户管理", type = Business.INSERT)
    @PostMapping("/tenant/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysTenant tenant, @ApiIgnore ServerHttpRequest request) {
        tenantService.insertTenant(tenant);
        if(DataStatusEnum.NORMAL.code().equals(tenant.getStatus())) {
            List<SysMenu> menus = tenantService.selectMenuListByTenantId(tenant.getId());
            TenantContextHolder.apply(tenant.getId(), () -> tenantService.batchSaveOrUpdateMenus(menus));
        }
        return Mono.just(JsonResult.success(tenant));
    }

    @ApiOperation("修改租户")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:tenant:edit")
    @OperLog(title = "租户管理", type = Business.UPDATE)
    @PostMapping("/tenant/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysTenant tenant, @ApiIgnore ServerHttpRequest request) {
        tenantService.updateTenant(tenant);
        if(DataStatusEnum.NORMAL.code().equals(tenant.getStatus())) {
            List<SysMenu> menus = tenantService.selectMenuListByTenantId(tenant.getId());
            TenantContextHolder.apply(tenant.getId(), () -> tenantService.batchSaveOrUpdateMenus(menus));
        }
        return Mono.just(JsonResult.success(tenant));
    }

    @ApiOperation("删除租户")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:tenant:remove")
    @OperLog(title = "租户管理", type = Business.DELETE)
    @PostMapping("/tenant/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        tenantService.deleteTenantByIds(ids);
        return Mono.just(JsonResult.success());
    }
}
