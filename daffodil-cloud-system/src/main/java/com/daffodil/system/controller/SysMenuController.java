package com.daffodil.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.system.constant.SystemConstant;
import com.daffodil.system.entity.SysMenu;
import com.daffodil.system.service.ISysMenuService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 系统菜单管理
 * @author yweijian
 * @date 2021年9月28日
 * @version 1.0
 * @description
 */
@Api(value = "菜单管理", tags = "菜单管理")
@RestController
@RequestMapping(SystemConstant.API_CONTENT_PATH)
public class SysMenuController extends ReactiveBaseController {

    @Autowired
    private ISysMenuService menuService;
    
    @ApiOperation("查询菜单列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:menu:list")
    @GetMapping("/menu/list")
    public Mono<TableResult> list(SysMenu menu, @ApiIgnore ServerHttpRequest request) {
        List<SysMenu> menuList = menuService.selectMenuList(menu);
        return Mono.just(TableResult.success(menuList));
    }
    
    @ApiOperation("获取菜单详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:menu:info")
    @GetMapping("/menu/info")
    public Mono<JsonResult> info(@ApiParam(value = "菜单ID")String id, @ApiIgnore ServerHttpRequest request) {
        SysMenu menu = menuService.selectMenuById(id);
        return Mono.just(JsonResult.success(menu));
    }
    
    @ApiOperation("新增菜单")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:menu:add")
    @OperLog(title = "菜单管理", type = Business.INSERT)
    @PostMapping("/menu/add")
    public Mono<JsonResult> add(@Validated @RequestBody SysMenu menu, @ApiIgnore ServerHttpRequest request) {
        menuService.insertMenu(menu);
        return Mono.just(JsonResult.success(menu));
    }
    
    @ApiOperation("修改菜单")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:menu:edit")
    @OperLog(title = "菜单管理", type = Business.UPDATE)
    @PostMapping("/menu/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody SysMenu menu, @ApiIgnore ServerHttpRequest request) {
        menuService.updateMenu(menu);
        return Mono.just(JsonResult.success(menu));
    }
    
    @ApiOperation("删除菜单")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:menu:remove")
    @OperLog(title = "菜单管理", type = Business.DELETE)
    @PostMapping("/menu/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        menuService.deleteMenuByIds(ids);
        return Mono.just(JsonResult.success());
    }
}
