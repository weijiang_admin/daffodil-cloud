package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.core.util.HqlUtils;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysMenu;
import com.daffodil.system.entity.SysMenu2;
import com.daffodil.system.entity.SysTenant;
import com.daffodil.system.entity.SysTenantMenu;
import com.daffodil.system.service.ISysTenantService;
import com.daffodil.util.StringUtils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;

/**
 * 
 * @author yweijian
 * @date 2024年12月12日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Service
public class SysTenantServiceImpl implements ISysTenantService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Override
    public List<SysTenant> selectTenantList(Query<SysTenant> query) {
        StringBuffer hql = new StringBuffer("from SysTenant where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        return jpaDao.search(hql.toString(), paras, SysTenant.class, query.getPage());
    }

    @Override
    public SysTenant selectTenantById(String tenantId) {
        SysTenant tenant = jpaDao.find(SysTenant.class, tenantId);
        if(tenant != null) {
            List<SysMenu> menus = this.selectMenuListByTenantId(tenantId);
            List<String> menuIds = menus.stream().map(SysMenu::getId).collect(Collectors.toList());
            tenant.setMenuIds(menuIds.toArray(new String[menuIds.size()]));
        }
        return tenant;
    }

    @Override
    @Transactional
    public void deleteTenantByIds(String[] ids) {
        jpaDao.delete(SysTenant.class, ids);
    }

    @Override
    @Transactional
    public void insertTenant(SysTenant tenant) {
        SysTenant entity = jpaDao.find(SysTenant.class, tenant.getId());
        if(entity != null) {
            throw new BaseException(37941, new Object[] { tenant.getId() });
        }
        jpaDao.save(tenant);

        if(StringUtils.isNotEmpty(tenant.getMenuIds())){
            for (String menuId : tenant.getMenuIds()) {
                SysTenantMenu tenantMenu = new SysTenantMenu();
                tenantMenu.setTenantId(tenant.getId());
                tenantMenu.setMenuId(menuId);
                jpaDao.save(tenantMenu);
            }
        }
    }

    @Override
    @Transactional
    public void updateTenant(SysTenant tenant) {
        jpaDao.update(tenant);
        if(StringUtils.isNotNull(tenant.getMenuIds())) {
            List<String> mids = Arrays.asList(tenant.getMenuIds());
            List<String> nids = jpaDao.search("select menuId from SysTenantMenu where tenantId = ? ", tenant.getId(), String.class);
            // 需要新增的菜单id
            List<String> mnids = mids.stream().filter(item -> !nids.contains(item)).collect(Collectors.toList());
            for (String menuId : mnids) {
                SysTenantMenu tenantMenu = new SysTenantMenu();
                tenantMenu.setTenantId(tenant.getId());
                tenantMenu.setMenuId(menuId);
                jpaDao.save(tenantMenu);
            }

            // 需要删除的菜单id
            List<String> nmids = nids.stream().filter(item -> !mids.contains(item)).collect(Collectors.toList());
            if(StringUtils.isNotEmpty(nmids)) {
                List<Object> paras = new LinkedList<Object>();
                paras.add(tenant.getId());
                String in = HqlUtils.createHql(paras, nmids.toArray());
                jpaDao.delete(StringUtils.format("delete from SysTenantMenu where tenantId = ? and menuId in {}", in), paras);
            }
        }
    }

    @Override
    public List<SysMenu> selectMenuListByTenantId(String tenantId) {
        return jpaDao.search("select m from SysMenu m,SysTenantMenu t where m.id = t.menuId and t.tenantId = ?", tenantId, SysMenu.class);
    }

    @Override
    @Transactional
    public void batchSaveOrUpdateMenus(List<SysMenu> menus) {
        if(CollectionUtil.isNotEmpty(menus)) {
            menus.forEach(menu -> {
                SysMenu entity = this.jpaDao.find(SysMenu.class, menu.getId());
                if(entity == null) {
                    this.jpaDao.save(BeanUtil.copyProperties(menu, SysMenu2.class));
                }else {
                    entity.setName(menu.getName());
                    entity.setPerms(menu.getPerms());
                    entity.setComponent(menu.getComponent());
                    entity.setIsLink(menu.getIsLink());
                    entity.setLinkUrl(menu.getLinkUrl());
                    entity.setUpdateTime(new Date());
                    this.jpaDao.update(entity);
                }
            });

            List<SysMenu> list = this.jpaDao.search("from SysMenu", SysMenu.class);
            List<String> mids = menus.stream().map(SysMenu::getId).collect(Collectors.toList());
            List<String> nids = CollectionUtil.isNotEmpty(list) ? list.stream().map(SysMenu::getId).collect(Collectors.toList()) : Collections.emptyList();
            List<String> nmids = nids.stream().filter(item -> !mids.contains(item)).collect(Collectors.toList());
            if(StringUtils.isNotEmpty(nmids)) {
                List<Object> paras = new LinkedList<Object>();
                String in = HqlUtils.createHql(paras, nmids.toArray());
                jpaDao.delete(StringUtils.format("delete from SysMenu where id in {}", in), paras);
            }
        }
    }
}
