package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.util.HqlUtils;
import com.daffodil.core.dao.JdbcDao;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.entity.SysLoginInfo;
import com.daffodil.system.entity.SysOperLog;
import com.daffodil.system.entity.SysRole;
import com.daffodil.system.entity.SysThirdApp;
import com.daffodil.system.service.ISysHomeService;
import com.daffodil.util.StringUtils;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

/**
 * 首页业务实现
 * @author yweijian
 * @date 2022年2月17日
 * @version 1.0
 * @description
 */
@Service
public class SysHomeServiceImpl implements ISysHomeService{

    @Autowired
    private JpaDao<String> jpaDao;
    
    @Autowired
    private JdbcDao jdbcDao;
    
    @Override
    public int countLoginInfo(SysLoginInfo loginInfo) {
        StringBuffer hql = new StringBuffer("from SysLoginInfo where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, loginInfo, null);
        return jpaDao.count(hql.toString(), paras);
    }

    @Override
    public int countOperLog(SysOperLog operLog) {
        StringBuffer hql = new StringBuffer("from SysOperLog where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, operLog, null);
        return jpaDao.count(hql.toString(), paras);
    }

    @Override
    public List<Integer> countLoginInfoPerMonth(SysLoginInfo loginInfo) {
        List<Integer> counts = new ArrayList<Integer>();
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        for(int month = 1; month <= 12; month++ ) {
            counts.add(this.countLoginInfoPerMonth(loginInfo, year, month));
        }
        return counts;
    }
    
    private int countLoginInfoPerMonth(SysLoginInfo loginInfo, int year, int month) {
        StringBuffer hql = new StringBuffer("from SysLoginInfo where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, loginInfo, null);
        hql.append("and createTime >= ? and createTime <= ? ");
        DateTime dateTime = DateUtil.parse(StringUtils.format("{}-{}-{}", year, month, "1"));
        DateTime firstDateTime = DateUtil.beginOfMonth(dateTime);
        DateTime lastDateTime = DateUtil.endOfMonth(dateTime);
        paras.add(firstDateTime);
        paras.add(lastDateTime);
        return jpaDao.count(hql.toString(), paras);
    }

    @Override
    public List<Integer> countOperLogPerMonth(SysOperLog operLog) {
        List<Integer> counts = new ArrayList<Integer>();
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        for(int month = 1; month <= 12; month++ ) {
            counts.add(this.countOperLogPerMonth(operLog, year, month));
        }
        return counts;
    }

    private Integer countOperLogPerMonth(SysOperLog operLog, int year, int month) {
        StringBuffer hql = new StringBuffer("from SysOperLog where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, operLog, null);
        hql.append("and createTime >= ? and createTime <= ? ");
        DateTime dateTime = DateUtil.parse(StringUtils.format("{}-{}-{}", year, month, "1"));
        DateTime firstDateTime = DateUtil.beginOfMonth(dateTime);
        DateTime lastDateTime = DateUtil.endOfMonth(dateTime);
        paras.add(firstDateTime);
        paras.add(lastDateTime);
        return jpaDao.count(hql.toString(), paras);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Map<String, String>> selectBrowserLoginInfo(String loginName) {
        List<Object> paras = new ArrayList<Object>();
        paras.add(loginName);
        return jdbcDao.search("select browser as name, count(*) as value from sys_login_info where login_name=? group by browser", paras);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Map<String, String>> selectBusinessLabelOperLog(String loginName) {
        List<Object> paras = new ArrayList<Object>();
        paras.add(loginName);
        return jdbcDao.search("select business_label as name, count(*) as value from sys_oper_log where oper_name=? group by business_label", paras);
    }

    @Override
    public List<SysThirdApp> selectUserThirdAppList(LoginUser loginUser) {
        StringBuffer hql = new StringBuffer("select a from SysThirdApp a where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        if(!loginUser.getIsAdminRole()) {
            List<SysRole> roles = jpaDao.search("select r from SysRole r,SysUserRole ur where r.id = ur.roleId and r.status = '0' and ur.userId = ?", loginUser.getId(), SysRole.class);
            List<String> ids = roles.stream().map(SysRole::getId).collect(Collectors.toList());
            if(StringUtils.isNotEmpty(ids)) {
                String roleIds = HqlUtils.createHql(paras, ids.toArray(new String[ids.size()]));
                hql.append(StringUtils.format("and (a.id in (select distinct appId from SysThirdAppRole where roleId in {}) or a.createBy = ?) ", roleIds));
                paras.add(loginUser.getLoginName());
            }else {
                hql.append("and a.createBy = ? ");
                paras.add(loginUser.getLoginName());
            }
        }
        return jpaDao.search(hql.toString(), paras, SysThirdApp.class);
    }

}
