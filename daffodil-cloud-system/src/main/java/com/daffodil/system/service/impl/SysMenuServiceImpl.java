package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.util.HqlUtils;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysMenu;
import com.daffodil.system.service.ISysMenuService;
import com.daffodil.util.StringUtils;

/**
 * 菜单 业务层服务实现
 * @author yweijian
 * @date 2019年12月19日
 * @version 1.0
 */
@Service
public class SysMenuServiceImpl implements ISysMenuService {
    
    @Autowired
    private JpaDao<String> jpaDao;

    @Override
    public List<SysMenu> selectMenuList(SysMenu menu) {
        StringBuffer hql = new StringBuffer("select m from SysMenu m where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, menu, "m");
        hql.append("order by m.orderNum asc, m.ancestors asc");
        return jpaDao.search(hql.toString(), paras, SysMenu.class);
    }
    
    @Override
    @Transactional
    public void deleteMenuByIds(String[] ids) {
        for(String menuId : ids) {
            if (this.selectCountMenuByParentId(menuId) > 0) {
                throw new BaseException(37606);
            }
            if (this.selectCountRoleMenuByMenuId(menuId) > 0) {
                throw new BaseException(37607);
            }
            jpaDao.delete(SysMenu.class, menuId);
        }
    }
    
    private int selectCountMenuByParentId(String parentId) {
        return jpaDao.count("from SysMenu where parentId = ?", parentId);
    }

    private int selectCountRoleMenuByMenuId(String menuId) {
        return jpaDao.count("from SysRoleMenu where menuId = ?", menuId);
    }

    @Override
    public SysMenu selectMenuById(String menuId) {
        if(CommonConstant.ROOT.equals(menuId)){
            SysMenu menu = new SysMenu();
            menu.setId(CommonConstant.ROOT);
            menu.setMenuName("目录菜单");
            menu.setAncestors("");
            return menu;
        }
        return jpaDao.find(SysMenu.class, menuId);
    }

    @Override
    @Transactional
    public void insertMenu(SysMenu menu) {
        if (this.checkMenuNameUnique(menu)) {
            throw new BaseException(37601, new Object[] { menu.getMenuName() });
        }
        if(this.checkRouterNameUnique(menu)) {
            throw new BaseException(37602, new Object[] { menu.getMenuName() });
        }
        SysMenu parent = this.selectMenuById(menu.getParentId());
        if (StringUtils.isNotNull(parent)){
            String ancestors = parent.getAncestors() + "," + parent.getId();
            menu.setAncestors(ancestors);
        }
        jpaDao.save(menu);
    }

    @Override
    @Transactional
    public void updateMenu(SysMenu menu) {
        if (this.checkMenuNameUnique(menu)) {
            throw new BaseException(37603, new Object[] { menu.getMenuName() });
        }
        if(this.checkRouterNameUnique(menu)) {
            throw new BaseException(37604, new Object[] { menu.getMenuName() });
        }
        if(this.checkMenuIsSelfOrChildren(menu)){
            throw new BaseException(37605, new Object[] { menu.getMenuName() });
        }
        SysMenu sysMenu = this.selectMenuById(menu.getId());
        SysMenu parent = this.selectMenuById(menu.getParentId());
        if (StringUtils.isNotNull(parent) && StringUtils.isNotNull(sysMenu)) {
            String newAncestors = parent.getAncestors() + "," + parent.getId();
            String oldAncestors = sysMenu.getAncestors();
            menu.setAncestors(newAncestors);
            this.updateChildrenMenu(menu, newAncestors, oldAncestors);
        }
        jpaDao.update(menu);
    }

    /***
     * 检查是否是自己或自己的子目录
     * @param menu
     * @return
     */
    private boolean checkMenuIsSelfOrChildren(SysMenu menu){
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysMenu where id = ? or ancestors like ?";
        paras.add(menu.getId());
        paras.add("%" + menu.getId() + "%");
        List<SysMenu> menus = jpaDao.search(hql, paras, SysMenu.class);
        for(SysMenu sysMenu : menus){
            if(menu.getParentId().equals(sysMenu.getId())){
                return true;
            }
        }
        return false;
    }
    
    @Transactional
    public void updateChildrenMenu(SysMenu menu, String newAncestors, String oldAncestors){
        List<Object> paras = new ArrayList<Object>();
        paras.add(oldAncestors);
        paras.add(newAncestors);
        paras.add("%" + menu.getId() + "%");
        jpaDao.update("update SysMenu set ancestors = replace(ancestors, ?, ?) where ancestors like ?", paras);
    }
    
    private boolean checkMenuNameUnique(SysMenu menu) {
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysMenu where menuName = ? and parentId = ? ";
        paras.add(menu.getMenuName());
        paras.add(menu.getParentId());
        if(StringUtils.isNotEmpty(menu.getId())){
            hql += "and id != ?";
            paras.add(menu.getId());
        }
        SysMenu sysMenu = jpaDao.find(hql, paras, SysMenu.class);
        return null != sysMenu;
    }
    
    private boolean checkRouterNameUnique(SysMenu menu) {
        if(StringUtils.isEmpty(menu.getName())) {
            return false;
        }
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysMenu where name = ?";
        paras.add(menu.getName());
        if(StringUtils.isNotEmpty(menu.getId())){
            hql += "and id != ?";
            paras.add(menu.getId());
        }
        SysMenu sysMenu = jpaDao.find(hql, paras, SysMenu.class);
        return null != sysMenu;
    }

}
