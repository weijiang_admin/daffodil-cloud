package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.core.util.HqlUtils;
import com.daffodil.system.entity.SysSocialUser;
import com.daffodil.system.enums.DataStatusEnum;
import com.daffodil.system.service.ISysSocialUserService;

/**
 * 
 * @author yweijian
 * @date 2023年1月11日
 * @version 2.0.0
 * @description
 */
@Service
public class SysSocialUserServiceImpl implements ISysSocialUserService {

    @Autowired
    private JpaDao<String> jpaDao;
    
    @Override
    public List<SysSocialUser> selectSocialUserList(Query<SysSocialUser> query) {
        StringBuffer hql = new StringBuffer("from SysSocialUser where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        return jpaDao.search(hql.toString(), paras, SysSocialUser.class, query.getPage());
    }

    @Override
    @Transactional
    public void freeSocialUser(SysSocialUser user) {
        user.setStatus(DataStatusEnum.UNUSED.code());
        user.setFreeTime(new Date());
        jpaDao.update(user);
    }

}
