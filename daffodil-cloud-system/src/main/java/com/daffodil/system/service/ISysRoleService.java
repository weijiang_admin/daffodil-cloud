package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysRole;

/**
 * 角色业务层服务
 * @author yweijian
 * @date 2019年12月18日
 * @version 1.0
 */
public interface ISysRoleService {
    
    /**
     * 根据条件分页查询角色数据
     * @param query
     * @return
     */
    public List<SysRole> selectRoleList(Query<SysRole> query);

    /**
     * 通过角色ID查询角色
     * @param roleId
     * @return
     */
    public SysRole selectRoleById(String roleId);

    /**
     * 批量删除角色用户信息
     * @param ids
     */
    public void deleteRoleByIds(String[] ids);

    /**
     * 新增保存角色信息
     * @param role
     */
    public void insertRole(SysRole role);

    /**
     * 修改保存角色信息
     * @param role
     */
    public void updateRole(SysRole role);
}
