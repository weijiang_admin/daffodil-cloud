package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.core.util.HqlUtils;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysResource;
import com.daffodil.system.service.ISysResourceService;
import com.daffodil.util.StringUtils;

/**
 * -开放资源信息Service接口业务实现层
 * @author yweijian
 * @date 2023-04-27
 * @version 1.0
 * @description
 */
@Service
public class SysResourceServiceImpl implements ISysResourceService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Override
    public List<SysResource> selectResourceList(Query<SysResource> query) {
        StringBuffer hql = new StringBuffer("from SysResource where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        hql.append("order by orderNum asc");
        return jpaDao.search(hql.toString(), paras, SysResource.class);
    }

    @Override
    public SysResource selectResourceById(String id) {
        if(CommonConstant.ROOT.equals(id)){
            SysResource resource = new SysResource();
            resource.setId(CommonConstant.ROOT);
            resource.setName("");
            resource.setPath("");
            resource.setPerms("");
            resource.setAncestors("");
            return resource;
        }
        return jpaDao.find(SysResource.class, id);
    }

    @Override
    @Transactional
    public void insertResource(SysResource resource) {
        if(CommonConstant.ROOT.equals(resource.getParentId())){
            resource.setAncestors(CommonConstant.ROOT);
        }else{
            SysResource parent = jpaDao.find(SysResource.class, resource.getParentId());
            if(parent != null){
                resource.setAncestors(parent.getAncestors() + "," + parent.getId());
            }
        }
        jpaDao.save(resource);
    }

    @Override
    @Transactional
    public void updateResource(SysResource resource) {
        if(this.checkResourceIsSelfOrChildren(resource)){
            throw new BaseException(37912, new Object[] { resource.getName() });
        }
        SysResource sysResource = this.selectResourceById(resource.getId());
        SysResource parent = this.selectResourceById(resource.getParentId());
        if (StringUtils.isNotNull(parent) && StringUtils.isNotNull(sysResource)) {
            String newAncestors = parent.getAncestors() + "," + parent.getId();
            String oldAncestors = sysResource.getAncestors();
            resource.setAncestors(newAncestors);
            this.updateChildrenResource(resource, newAncestors, oldAncestors);
        }
        jpaDao.update(resource);
    }
    
    private boolean checkResourceIsSelfOrChildren(SysResource resource){
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysResource where id = ? or ancestors like ? ";
        paras.add(resource.getId());
        paras.add("%" + resource.getId() + "%");
        List<SysResource> list = jpaDao.search(hql, paras, SysResource.class);
        for(SysResource sysResource : list){
            if(resource.getParentId().equals(sysResource.getId())){
                return true;
            }
        }
        return false;
    }
    
    @Transactional
    public void updateChildrenResource(SysResource resource, String newAncestors, String oldAncestors){
        List<Object> paras = new ArrayList<Object>();
        paras.add(oldAncestors);
        paras.add(newAncestors);
        paras.add(resource.getStatus());
        paras.add("%" + resource.getId() + "%");
        jpaDao.update("update SysResource set ancestors = replace(ancestors, ?, ?), status = ? where ancestors like ?", paras);
    }

    @Override
    @Transactional
    public void deleteResourceByIds(String[] ids) {
        if(StringUtils.isNotEmpty(ids)) {
            for (int i = 0; i < ids.length; i++) {
                String id = ids[i];
                int count = jpaDao.count("from SysResource where parentId = ?", id);
                if(count > 0) {
                    throw new BaseException(37911);
                }
            }
            jpaDao.delete(SysResource.class, ids);
        }
    }

    @Override
    @Transactional
    public void batchSaveResource(List<SysResource> resources) {
        if(StringUtils.isEmpty(resources)) {
            return;
        }
        for(SysResource resource : resources) {
            List<Object> paras = new ArrayList<Object>();
            paras.add(resource.getPath());
            paras.add(resource.getBelong());
            int count = jpaDao.count("from SysResource where path = ? and belong = ?", paras);
            if(count <= 0) {
                jpaDao.save(resource);
            }
        }
    }

}
