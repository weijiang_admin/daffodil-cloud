package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.util.HqlUtils;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysPost;
import com.daffodil.system.service.ISysPostService;
import com.daffodil.util.StringUtils;

/**
 * 岗位信息 服务实现
 * @author yweijian
 * @date 2019年12月18日
 * @version 1.0
 */
@Service
public class SysPostServiceImpl implements ISysPostService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Override
    public List<SysPost> selectPostList(Query<SysPost> query) {
        StringBuffer hql = new StringBuffer("from SysPost where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        return jpaDao.search(hql.toString(), paras, SysPost.class, query.getPage());
    }

    @Override
    public SysPost selectPostById(String postId) {
        return jpaDao.find(SysPost.class, postId);
    }

    @Override
    @Transactional
    public void deletePostByIds(String[] ids) {
        for (String postId : ids) {
            int count = jpaDao.count("from SysUserPost where postId = ? ", postId);
            if (count > 0) {
                throw new BaseException(37303);
            }
        }
        jpaDao.delete(SysPost.class, ids);
    }

    @Override
    @Transactional
    public void insertPost(SysPost post) {
        if (this.checkPostCodeUnique(post)) {
            throw new BaseException(37301, new Object[] { post.getPostName() });
        }
        jpaDao.save(post);
    }

    @Override
    @Transactional
    public void updatePost(SysPost post) {
        if (this.checkPostCodeUnique(post)) {
            throw new BaseException(37302, new Object[] { post.getPostName() });
        }
        jpaDao.update(post);
    }

    private boolean checkPostCodeUnique(SysPost post){
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysPost where postCode = ? ";
        paras.add(post.getPostCode());
        if(StringUtils.isNotEmpty(post.getId())){
            hql += "and id != ? ";
            paras.add(post.getId());
        }
        SysPost sysPost = jpaDao.find(hql, paras, SysPost.class);
        return null != sysPost;
    }
}
