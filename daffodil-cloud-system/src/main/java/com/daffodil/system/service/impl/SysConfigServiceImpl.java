package com.daffodil.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.system.entity.SysConfig;
import com.daffodil.system.entity.config.SysConfigLoginAccount;
import com.daffodil.system.entity.config.SysConfigLoginEmail;
import com.daffodil.system.entity.config.SysConfigLoginMobile;
import com.daffodil.system.entity.config.SysConfigLoginScan;
import com.daffodil.system.model.ConfigLogin;
import com.daffodil.system.service.ISysConfigService;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2022年9月8日
 * @version 2.0.0
 * @description
 */
@Service
public class SysConfigServiceImpl implements ISysConfigService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public SysConfig selectConfig() {
        SysConfig config = new SysConfig();
        ConfigLogin login = this.selectConfigLogin();
        config.setLogin(login);
        return config;
    }

    @Override
    public ConfigLogin selectConfigLogin() {
        ConfigLogin login = new ConfigLogin();
        login.setAccount(this.selectConfigLoginAccount());
        login.setMobile(this.selectConfigLoginMobile());
        login.setEmail(this.selectConfigLoginEmail());
        login.setScan(this.selectConfigLoginScan());
        return login;
    }

    @Override
    public SysConfigLoginAccount selectConfigLoginAccount() {
        return jpaDao.find("from SysConfigLoginAccount", SysConfigLoginAccount.class);
    }

    @Override
    public SysConfigLoginMobile selectConfigLoginMobile() {
        return jpaDao.find("from SysConfigLoginMobile", SysConfigLoginMobile.class);
    }

    @Override
    public SysConfigLoginEmail selectConfigLoginEmail() {
        return jpaDao.find("from SysConfigLoginEmail", SysConfigLoginEmail.class);
    }

    @Override
    public SysConfigLoginScan selectConfigLoginScan() {
        return jpaDao.find("from SysConfigLoginScan", SysConfigLoginScan.class);
    }

    @Override
    @Transactional
    public void saveConfig(SysConfig config) {
        this.saveConfigLogin(config.getLogin());
    }

    @Override
    @Transactional
    public void saveConfigLogin(ConfigLogin login) {
        if(StringUtils.isNull(login)) {
            return;
        }
        this.saveConfigLoginAccount(login.getAccount());
        this.saveConfigLoginMobile(login.getMobile());
        this.saveConfigLoginEmail(login.getEmail());
        this.saveConfigLoginScan(login.getScan());
        //更新缓存设置
        redisTemplate.opsForValue().set(CommonConstant.CONFIG_LOGIN_CACHEKEY, this.selectConfigLogin());
    }

    @Override
    @Transactional
    public void saveConfigLoginAccount(SysConfigLoginAccount account) {
        if(StringUtils.isNull(account)) {
            return;
        }
        SysConfigLoginAccount config = this.selectConfigLoginAccount();
        if(StringUtils.isNull(config)) {
            jpaDao.save(account);
        }else {
            account.setId(config.getId());
            jpaDao.update(account);
        }
    }

    @Override
    @Transactional
    public void saveConfigLoginMobile(SysConfigLoginMobile mobile) {
        if(StringUtils.isNull(mobile)) {
            return;
        }
        SysConfigLoginMobile config = this.selectConfigLoginMobile();
        if(StringUtils.isNull(config)) {
            jpaDao.save(mobile);
        }else {
            mobile.setId(config.getId());
            jpaDao.update(mobile);
        }
    }

    @Override
    @Transactional
    public void saveConfigLoginEmail(SysConfigLoginEmail email) {
        if(StringUtils.isNull(email)) {
            return;
        }
        SysConfigLoginEmail config = this.selectConfigLoginEmail();
        if(StringUtils.isNull(config)) {
            jpaDao.save(email);
        }else {
            email.setId(config.getId());
            jpaDao.update(email);
        }
    }

    @Override
    @Transactional
    public void saveConfigLoginScan(SysConfigLoginScan scan) {
        if(StringUtils.isNull(scan)) {
            return;
        }
        SysConfigLoginScan config = this.selectConfigLoginScan();
        if(StringUtils.isNull(config)) {
            jpaDao.save(scan);
        }else {
            scan.setId(config.getId());
            jpaDao.update(scan);
        }
    }

}
