package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysRank;

/**
 * -职级信息
 * @author yweijian
 * @date 2019年12月18日
 * @version 1.0
 */
public interface ISysRankService {
    /**
     * -分页查询职级信息集合
     * @param query
     * @return
     */
    public List<SysRank> selectRankList(Query<SysRank> query);

    /**
     * -通过职级ID查询职级信息
     * @param rankId
     * @return
     */
    public SysRank selectRankById(String rankId);

    /**
     * -批量删除职级信息
     * @param ids
     */
    public void deleteRankByIds(String[] ids);

    /**
     * -新增保存职级信息
     * @param rank
     */
    public void insertRank(SysRank rank);

    /**
     * -修改保存职级信息
     * @param rank
     */
    public void updateRank(SysRank rank);

}
