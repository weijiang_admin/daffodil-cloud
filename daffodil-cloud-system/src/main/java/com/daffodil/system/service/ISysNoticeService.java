package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.Query;
import com.daffodil.system.controller.model.QueryNoticeParam;
import com.daffodil.system.controller.model.SysNoticeParam;
import com.daffodil.system.entity.SysNotice;

/**
 * -通知消息 服务
 * @author yweijian
 * @date 2019年12月18日
 * @version 1.0
 */
public interface ISysNoticeService {

    /**
     * -查询通知消息列表
     * @param query
     * @return
     */
    public List<SysNotice> selectNoticeList(Query<SysNotice> query);

    /**
     * -查询通知消息信息
     * @param noticeId
     * @return
     */
    public SysNotice selectNoticeById(String noticeId);

    /**
     * -新增通知消息
     * @param notice
     */
    public void insertNotice(SysNotice notice);

    /**
     * -修改通知消息
     * @param notice
     */
    public void updateNotice(SysNotice notice);

    /**
     * -删除通知消息信息
     * @param ids
     */
    public void deleteNoticeByIds(String[] ids);

    /**
     * -发布通知消息信息
     * @param ids
     */
    public void publishNotice(SysNoticeParam param);
    
    /**
     * -分页查询用户消息列表
     * @param param
     * @param page
     * @return
     */
    public List<SysNotice> selectNoticeList(QueryNoticeParam param, Page page);

    /**
     * -触发获取用户通知消息
     * @param userId
     */
    public void triggerNoticeTargetByUserId(String userId);

    /**
     * -通知消息标为已读
     * @param ids
     * @param userId
     */
    public void updateNoticeReadStatusByIds(String[] ids, String userId);
}
