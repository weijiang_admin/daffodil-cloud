package com.daffodil.system.service;

import com.daffodil.system.entity.SysConfig;
import com.daffodil.system.entity.config.SysConfigLoginAccount;
import com.daffodil.system.entity.config.SysConfigLoginEmail;
import com.daffodil.system.entity.config.SysConfigLoginMobile;
import com.daffodil.system.entity.config.SysConfigLoginScan;
import com.daffodil.system.model.ConfigLogin;

/**
 * 
 * @author yweijian
 * @date 2022年9月8日
 * @version 2.0.0
 * @description
 */
public interface ISysConfigService {

    /**
     * -查询系统应用设置
     * @return
     */
    public SysConfig selectConfig();

    /**
     * -查询登录服务设置
     * @return
     */
    public ConfigLogin selectConfigLogin();
    
    /**
     * -查询账号密码登录设置
     * @return
     */
    public SysConfigLoginAccount selectConfigLoginAccount();
    
    /**
     * -查询手机号码登录设置
     * @return
     */
    public SysConfigLoginMobile selectConfigLoginMobile();
    
    /**
     * -查询邮箱账号登录设置
     * @return
     */
    public SysConfigLoginEmail selectConfigLoginEmail();
    
    /**
     * -查询扫码登录设置
     * @return
     */
    public SysConfigLoginScan selectConfigLoginScan();

    /**
     * -保存系统应用设置
     * @param config
     */
    public void saveConfig(SysConfig config);
    
    /**
     * -保存登录服务设置
     * @param login
     */
    public void saveConfigLogin(ConfigLogin login);

    /**
     * -保存账号密码登录设置
     * @param account
     */
    public void saveConfigLoginAccount(SysConfigLoginAccount account);

    /**
     * -保存手机号码登录设置
     * @param mobile
     */
    public void saveConfigLoginMobile(SysConfigLoginMobile mobile);

    /**
     * -保存邮箱账号登录设置
     * @param email
     */
    public void saveConfigLoginEmail(SysConfigLoginEmail email);
    
    /**
     * -保存扫码登录设置
     * @param scan
     */
    public void saveConfigLoginScan(SysConfigLoginScan scan);

}
