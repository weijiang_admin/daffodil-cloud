package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.util.HqlUtils;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysRank;
import com.daffodil.system.service.ISysRankService;
import com.daffodil.util.StringUtils;

/**
 * -职级信息 服务实现
 * @author yweijian
 * @date 2022年6月13日
 * @version 2.0.0
 * @description
 */
@Service
public class SysRankServiceImpl implements ISysRankService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Override
    public List<SysRank> selectRankList(Query<SysRank> query) {
        StringBuffer hql = new StringBuffer("from SysRank where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        return jpaDao.search(hql.toString(), paras, SysRank.class, query.getPage());
    }

    @Override
    public SysRank selectRankById(String rankId) {
        return jpaDao.find(SysRank.class, rankId);
    }

    @Override
    @Transactional
    public void deleteRankByIds(String[] ids) {
        for (String rankId : ids) {
            int count = jpaDao.count("from SysUserRank where rankId = ? ", rankId);
            if (count > 0) {
                throw new BaseException(37403);
            }
        }
        jpaDao.delete(SysRank.class, ids);
    }

    @Override
    @Transactional
    public void insertRank(SysRank rank) {
        if (this.checkRankCodeUnique(rank)) {
            throw new BaseException(37401, new Object[] { rank.getRankName() });
        }
        jpaDao.save(rank);
    }

    @Override
    @Transactional
    public void updateRank(SysRank rank) {
        if (this.checkRankCodeUnique(rank)) {
            throw new BaseException(37402, new Object[] { rank.getRankName() });
        }
        jpaDao.update(rank);
    }

    private boolean checkRankCodeUnique(SysRank rank){
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysRank where rankCode = ? ";
        paras.add(rank.getRankCode());
        if(StringUtils.isNotEmpty(rank.getId())){
            hql += "and id != ? ";
            paras.add(rank.getId());
        }
        SysRank sysRank = jpaDao.find(hql, paras, SysRank.class);
        return null != sysRank;
    }
}
