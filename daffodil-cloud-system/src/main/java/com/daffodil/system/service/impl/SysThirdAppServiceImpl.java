package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.util.HqlUtils;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysRole;
import com.daffodil.system.entity.SysThirdApp;
import com.daffodil.system.entity.SysThirdAppResource;
import com.daffodil.system.entity.SysThirdAppRole;
import com.daffodil.system.entity.SysThirdAppScope;
import com.daffodil.system.event.SysThirdAppEvent;
import com.daffodil.system.event.publisher.SysThirdAppPublisherService;
import com.daffodil.system.service.ISysThirdAppService;
import com.daffodil.util.StringUtils;

import cn.hutool.crypto.digest.DigestAlgorithm;
import cn.hutool.crypto.digest.Digester;

/**
 * 第三方应用信息管理
 * @author yweijian
 * @date 2023年1月3日
 * @version 2.0.0
 * @description
 */
@Service
public class SysThirdAppServiceImpl implements ISysThirdAppService {

    @Autowired
    private JpaDao<String> jpaDao;
    
    @Autowired
    private SysThirdAppPublisherService thirdAppPublisherService;

    @Override
    public List<SysThirdApp> selectThirdAppList(Query<SysThirdApp> query, LoginUser loginUser) {
        StringBuffer hql = new StringBuffer("select a from SysThirdApp a where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        if(!loginUser.getIsAdminRole()) {
            List<SysRole> roles = jpaDao.search("select r from SysRole r,SysUserRole ur where r.id = ur.roleId and r.status = '0' and ur.userId = ?", loginUser.getId(), SysRole.class);
            List<String> ids = roles.stream().map(SysRole::getId).collect(Collectors.toList());
            if(StringUtils.isNotEmpty(ids)) {
                String roleIds = HqlUtils.createHql(paras, ids.toArray(new String[ids.size()]));
                hql.append(StringUtils.format("and (a.id in (select distinct appId from SysThirdAppRole where roleId in {}) or a.createBy = ?) ", roleIds));
                paras.add(loginUser.getLoginName());
            }else {
                hql.append("and a.createBy = ? ");
                paras.add(loginUser.getLoginName());
            }
        }
        
        HqlUtils.createHql(hql, paras, query, "a");
        
        return jpaDao.search(hql.toString(), paras, SysThirdApp.class, query.getPage());
    }

    @Override
    public SysThirdApp selectThirdAppById(String appId) {
        SysThirdApp app = jpaDao.find(SysThirdApp.class, appId);
        
        List<SysThirdAppScope> appScopes = jpaDao.search("from SysThirdAppScope where appId = ?", appId, SysThirdAppScope.class);
        List<String> scopes = appScopes.stream().map(SysThirdAppScope::getScope).collect(Collectors.toList());
        app.setScopes(scopes.toArray(new String[scopes.size()]));
        
        List<SysThirdAppResource> appResources = jpaDao.search("from SysThirdAppResource where appId = ?", appId, SysThirdAppResource.class);
        List<String> resourceIds = appResources.stream().map(SysThirdAppResource::getResourceId).collect(Collectors.toList());
        app.setResourceIds(resourceIds.toArray(new String[resourceIds.size()]));
        
        List<SysThirdAppRole> appRoles = jpaDao.search("from SysThirdAppRole where appId = ?", appId, SysThirdAppRole.class);
        List<String> roleIds = appRoles.stream().map(SysThirdAppRole::getRoleId).collect(Collectors.toList());
        app.setRoleIds(roleIds.toArray(new String[roleIds.size()]));
        
        return app;
    }

    @Override
    @Transactional
    public void deleteThirdAppByIds(String[] ids) {
        if(StringUtils.isNotEmpty(ids)) {
            String tenantId = TenantContextHolder.getTenantHolder();
            for(String id : ids) {
                SysThirdApp app = jpaDao.find(SysThirdApp.class, id);
                jpaDao.delete(app);
                thirdAppPublisherService.publish(app, tenantId, SysThirdAppEvent.Operation.DELETE.name());
            }
        }
    }

    @Override
    @Transactional
    public void insertThirdApp(SysThirdApp app) {
        String source = UUID.randomUUID().toString();
        String secret = new Digester(DigestAlgorithm.SHA512).digestHex(source);
        app.setAppSecret(secret);
        jpaDao.save(app);
        thirdAppPublisherService.publish(app, TenantContextHolder.getTenantHolder(), SysThirdAppEvent.Operation.INSERT.name());
    }

    @Override
    @Transactional
    public void updateThirdApp(SysThirdApp app) {
        jpaDao.update(app);
        thirdAppPublisherService.publish(app, TenantContextHolder.getTenantHolder(), SysThirdAppEvent.Operation.UPDATE.name());
    }

    @Override
    @Transactional
    public void resetThirdApp(SysThirdApp app) {
        jpaDao.update(app);
    }

}
