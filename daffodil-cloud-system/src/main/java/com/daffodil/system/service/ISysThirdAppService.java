package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.entity.SysThirdApp;

/**
 * -第三方应用信息
 * @author yweijian
 * @date 2023年1月3日
 * @version 2.0.0
 * @description
 */
public interface ISysThirdAppService {

    /**
     * -分页查询第三方应用信息集合
     * @param query
     * @return
     */
    public List<SysThirdApp> selectThirdAppList(Query<SysThirdApp> query, LoginUser loginUser);

    /**
     * -通过第三方应用ID查询第三方应用信息
     * @param appId
     * @return
     */
    public SysThirdApp selectThirdAppById(String appId);

    /**
     * -批量删除第三方应用信息
     * @param ids
     */
    public void deleteThirdAppByIds(String[] ids);

    /**
     * -新增保存第三方应用信息
     * @param app
     */
    public void insertThirdApp(SysThirdApp app);

    /**
     * -修改保存第三方应用信息
     * @param app
     */
    public void updateThirdApp(SysThirdApp app);

    /**
     * -重置第三方应用信息秘钥
     * @param app
     */
    public void resetThirdApp(SysThirdApp app);

}
