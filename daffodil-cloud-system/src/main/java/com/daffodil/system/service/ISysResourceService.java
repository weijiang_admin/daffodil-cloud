package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysResource;

/**
 * -开放资源信息Service接口
 * @author yweijian
 * @date 2023年4月27日
 * @version 2.0.0
 * @description
 */
public interface ISysResourceService {

    /**
     * -分页查询开放资源信息列表
     * @param query 开放资源信息
     * @return 开放资源信息
     */
    public List<SysResource> selectResourceList(Query<SysResource> query);

    /**
     * -查询开放资源信息
     * @param id 开放资源信息ID
     * @return 开放资源信息
     */
    public SysResource selectResourceById(String id);

    /**
     * -新增开放资源信息
     * @param resource
     */
    public void insertResource(SysResource resource);

    /**
     * -修改开放资源信息
     * @param resource
     */
    public void updateResource(SysResource resource);

    /**
     * -删除开放资源信息
     * @param ids
     */
    public void deleteResourceByIds(String[] ids);
    

    /**
     * -批量保存开放资源信息
     * @param resources
     */
    public void batchSaveResource(List<SysResource> resources);

}
