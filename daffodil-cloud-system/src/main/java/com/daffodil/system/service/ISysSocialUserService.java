package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysSocialUser;

/**
 * -第三方账号信息
 * @author yweijian
 * @date 2019年12月18日
 * @version 1.0
 */
public interface ISysSocialUserService {

    /**
     * -分页查询第三方账号信息集合
     * @param query
     * @return
     */
    public List<SysSocialUser> selectSocialUserList(Query<SysSocialUser> query);

    /**
     * -解绑第三方账号信息
     * @param user
     */
    public void freeSocialUser(SysSocialUser user);

}
