package com.daffodil.system.service;

import java.util.List;

import org.springframework.lang.Nullable;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysUser;

/**
 * -用户 业务服务
 * @author yweijian
 * @date 2021年10月9日
 * @version 1.0
 * @description
 */
public interface ISysUserService {
    /**
     * -根据查询条件分页查询用户列表
     * @param query
     * @param user
     * @return
     */
    public List<SysUser> selectUserList(Query<SysUser> query, @Nullable SysUser user);
    /**
     * -通过用户ID查询用户
     * @param userId
     * @return
     */
    public SysUser selectUserById(String userId);

    /**
     * -批量删除用户信息
     * @param ids
     */
    public void deleteUserByIds(String[] ids);

    /**
     * -保存用户信息
     * @param user
     */
    public void insertUser(SysUser user);

    /**
     * -保存用户信息
     * @param user
     */
    public void updateUser(SysUser user);

    /**
     *- 修改用户详细信息
     * @param user
     * @return
     */
    public void updateUserInfo(SysUser user);

    /**
     * -修改用户密码信息
     * @param user
     * @return
     */
    public void resetUserPwd(SysUser user);

}
