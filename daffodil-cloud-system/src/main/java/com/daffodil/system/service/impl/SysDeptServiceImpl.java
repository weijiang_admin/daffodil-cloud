package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.common.annotation.DataScope;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.util.HqlUtils;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysDept;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.service.ISysDeptService;
import com.daffodil.util.StringUtils;

/**
 * 部门管理
 * @author yweijian
 * @date 2019年12月18日
 * @version 1.0
 */
@Service
public class SysDeptServiceImpl implements ISysDeptService {
    
    @Autowired
    private JpaDao<String> jpaDao;

    @Override
    @DataScope(business = SysDept.class, deptAlias = "d.id")
    public List<SysDept> selectDeptList(Query<SysDept> query, SysUser user) {
        List<Object> paras = new ArrayList<Object>();
        StringBuffer hql = new StringBuffer("from SysDept d where 1=1 ");
        HqlUtils.createHql(hql, paras, query, "d");
        hql.append("order by d.orderNum asc,d.ancestors asc");
        return jpaDao.search(hql.toString(), paras, SysDept.class);
    }

    @Override
    @Transactional
    public void deleteDeptByIds(String[] ids) {
        for (String deptId : ids) {
            int count = jpaDao.count("from SysDept where parentId = ?", deptId);
            if(count > 0){
                throw new BaseException(37001);
            }
            int count2 = jpaDao.count("from SysUser where deptId = ?", deptId);
            if(count2 > 0){
                throw new BaseException(37002);
            }
            jpaDao.delete(SysDept.class, deptId);
        }
    }

    @Override
    @Transactional
    public void insertDept(SysDept dept) {
        if (this.checkDeptNameUnique(dept)) {
            throw new BaseException(37003, new Object[]{ dept.getDeptName() });
        }
        SysDept parentDept = this.selectDeptById(dept.getParentId());
        if(StringUtils.isNotEmpty(parentDept.getAncestors())){
            dept.setAncestors(parentDept.getAncestors() + "," + dept.getParentId());
        }else{
            dept.setAncestors(dept.getParentId());
        }
        jpaDao.save(dept);
    }

    @Override
    @Transactional
    public void updateDept(SysDept dept) {
        if (this.checkDeptNameUnique(dept)) {
            throw new BaseException(37004, new Object[]{ dept.getDeptName() });
        }
        if (checkDeptIsSelfOrChildren(dept)) {
            throw new BaseException(37005, new Object[]{ dept.getDeptName() });
        }
        SysDept sysDept = this.selectDeptById(dept.getId());
        SysDept parent= this.selectDeptById(dept.getParentId());
        if (StringUtils.isNotNull(parent) && StringUtils.isNotNull(sysDept)) {
            String newAncestors = parent.getAncestors() + "," + parent.getId();
            String oldAncestors = sysDept.getAncestors();
            dept.setAncestors(newAncestors);
            this.updateChildrenDept(dept, newAncestors, oldAncestors);
        }
        jpaDao.update(dept);
    }
    
    private boolean checkDeptIsSelfOrChildren(SysDept dept){
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysDept where id = ? or ancestors like ?";
        paras.add(dept.getId());
        paras.add("%" + dept.getId() + "%");
        List<SysDept> depts = jpaDao.search(hql, paras, SysDept.class);
        for(SysDept sysDept : depts){
            if(dept.getParentId().equals(sysDept.getId())){
                return true;
            }
        }
        return false;
    }

    @Transactional
    public void updateChildrenDept(SysDept dept, String newAncestors, String oldAncestors) {
        List<Object> paras = new ArrayList<Object>();
        paras.add(oldAncestors);
        paras.add(newAncestors);
        paras.add(dept.getStatus());
        paras.add("%" + dept.getId() + "%");
        jpaDao.update("update SysDept set ancestors = replace(ancestors, ?, ?), status = ? where ancestors like ?", paras);
    }

    @Override
    public SysDept selectDeptById(String deptId) {
        if(CommonConstant.ROOT.equals(deptId)){
            SysDept dept = new SysDept();
            dept.setId(CommonConstant.ROOT);
            dept.setDeptName("组织机构");
            dept.setAncestors("");
            return dept;
        }
        return jpaDao.find(SysDept.class, deptId);
    }

    private boolean checkDeptNameUnique(SysDept dept) {
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysDept where deptName=? and parentId=? ";
        paras.add(dept.getDeptName());
        paras.add(dept.getParentId());
        if(StringUtils.isNotEmpty(dept.getId())){
            hql += "and id != ?";
            paras.add(dept.getId());
        }
        SysDept sysDept = jpaDao.find(hql, paras, SysDept.class);
        return null != sysDept;
    }
}
