package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysDatasource;

/**
 * 
 * @author yweijian
 * @date 2024年12月12日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public interface ISysDatasourceService {

    /**
     * 分页查询数据源信息集合
     * @param query
     * @return
     */
    public List<SysDatasource> selectDatasourceList(Query<SysDatasource> query);

    /**
     * 通过数据源ID查询数据源信息
     * @param datasourceId
     * @return
     */
    public SysDatasource selectDatasourceById(String datasourceId);

    /**
     * 批量删除数据源信息
     * @param ids
     */
    public void deleteDatasourceByIds(String[] ids);

    /**
     * 新增保存数据源信息
     * @param datasource
     */
    public void insertDatasource(SysDatasource datasource);

    /**
     * 修改保存数据源信息
     * @param datasource
     */
    public void updateDatasource(SysDatasource datasource);

}
