package com.daffodil.system.service;

import com.daffodil.system.entity.SysDays;

import java.util.Date;
import java.util.List;

/**
 * -日期管理
 * @author EP
 * @date 2023年1月29日
 * @version 1.0.0
 * @description
 */
public interface ISysDaysService {

    /**
     * 初始化日期数据
     * */
    void initSysDaysData();

    /**
     * -从redis缓存获取指定年份的日期数据
     * @param year
     * @return 前四位为年份，第五位为闰年标识（0：平年，1：闰年），第六位到结尾为当年1月1日到12月31日的每日标识（0：工作日 1：节假日） 例：2023 0 0111110...11100
     */
    String selectDaysHexData(Integer year);

    /**
     * -根据开始和结束时间从redis缓存批量获取指定年份的日期数据
     * @param startYear 开始年份(包含)
     * @param endYear 结束年份(包含)
     * @return 返回日期数据16进制字符串列表
     */
    List<String> multiselectDaysHexData(Integer startYear, Integer endYear);

    /**
     * -根据日期查询日期信息
     * @param year
     * @return
     */
    SysDays selectDaysByYear(Integer year);

    /**
     * -新增日期配置信息
     * @param days
     */
    void insertDays(SysDays days);

    /**
     * -更新日期配置信息
     * @param days
     */
    void updateDays(SysDays days);

    /**
     * -配置更新指定日期的工作日标识
     * @param date
     * @param isWorkDay
     */
    void updateDaysByDate(Date date, boolean isWorkDay);

    /**
     * -删除日期管理信息
     * @param ids
     */
    void deleteDaysByIds(String[] ids);

}
