package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysDept;
import com.daffodil.system.entity.SysUser;

/**
 * 部门管理
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
public interface ISysDeptService {
    
    /**
     * 根据查询条件查询部门
     * @param query
     * @return
     */
    public List<SysDept> selectDeptList(Query<SysDept> query, SysUser user);

    /**
     * 新增保存部门信息
     * @param dept
     */
    public void insertDept(SysDept dept);

    /**
     * 修改保存部门信息
     * @param dept
     */
    public void updateDept(SysDept dept);

    /**
     * 删除部门管理信息
     * @param ids
     */
    public void deleteDeptByIds(String[] ids);
    
    /**
     * 根据部门ID查询信息
     * @param deptId
     * @return
     */
    public SysDept selectDeptById(String deptId);

}
