package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.util.HqlUtils;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysArea;
import com.daffodil.system.service.ISysAreaService;
import com.daffodil.util.StringUtils;

import cn.hutool.core.collection.CollectionUtil;

/**
 * -区划管理
 * @author yweijian
 * @date 2022年6月14日
 * @version 2.0.0
 * @description
 */
@Service
public class SysAreaServiceImpl implements ISysAreaService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    /** 中国行政区划顶级代码 */
    public static final String ROOT = "100000000000";

    @Override
    public List<SysArea> selectAreaList(Query<SysArea> query) {
        List<Object> paras = new ArrayList<Object>();
        StringBuffer hql = new StringBuffer("from SysArea a where 1=1 ");
        HqlUtils.createHql(hql, paras, query, "a");
        hql.append("order by a.orderNum asc,a.ancestors asc");
        return jpaDao.search(hql.toString(), paras, SysArea.class);
    }

    @Override
    @Transactional
    public void deleteAreaByIds(String[] ids) {
        for (String areaId : ids) {
            int count = jpaDao.count("from SysArea where parentId = ?", areaId);
            if(count > 0){
                throw new BaseException(37801);
            }
            jpaDao.delete(SysArea.class, areaId);
        }
    }

    @Override
    @Transactional
    public void insertArea(SysArea area) {
        int count = jpaDao.count("from SysArea where id = ?", area.getId());
        if(count > 0) {
            throw new BaseException(37803, new Object[]{ area.getId() });
        }
        SysArea parentArea = this.selectAreaById(area.getParentId());
        if(StringUtils.isNotEmpty(parentArea.getAncestors())){
            area.setAncestors(parentArea.getAncestors() + "," + area.getParentId());
        }else{
            area.setAncestors(area.getParentId());
        }
        jpaDao.save(area);
        String cacheKey = StringUtils.format(CommonConstant.AREA_DATA_CACHEKEY, area.getParentId());
        this.tenantRedisTemplate.delete(cacheKey);
    }

    @Override
    @Transactional
    public void updateArea(SysArea area) {
        if (checkAreaIsSelfOrChildren(area)) {
            throw new BaseException(37802, new Object[]{ area.getAreaName() });
        }
        SysArea sysArea = this.selectAreaById(area.getId());
        SysArea parent= this.selectAreaById(area.getParentId());
        if (StringUtils.isNotNull(parent) && StringUtils.isNotNull(sysArea)) {
            String newAncestors = parent.getAncestors() + "," + parent.getId();
            String oldAncestors = sysArea.getAncestors();
            area.setAncestors(newAncestors);
            this.updateChildrenArea(area, newAncestors, oldAncestors);
        }
        jpaDao.update(area);
        String cacheKey = StringUtils.format(CommonConstant.AREA_DATA_CACHEKEY, area.getId());
        this.tenantRedisTemplate.delete(cacheKey);
    }

    private boolean checkAreaIsSelfOrChildren(SysArea area){
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysArea where id = ? or ancestors like ?";
        paras.add(area.getId());
        paras.add("%" + area.getId() + "%");
        List<SysArea> areas = jpaDao.search(hql, paras, SysArea.class);
        for(SysArea sysArea : areas){
            if(area.getParentId().equals(sysArea.getId())){
                return true;
            }
        }
        return false;
    }

    @Transactional
    public void updateChildrenArea(SysArea area, String newAncestors, String oldAncestors) {
        List<Object> paras = new ArrayList<Object>();
        paras.add(oldAncestors);
        paras.add(newAncestors);
        paras.add(area.getStatus());
        paras.add("%" + area.getId() + "%");
        jpaDao.update("update SysArea set ancestors = replace(ancestors, ?, ?), status = ? where ancestors like ?", paras);
        String cacheKey = StringUtils.format(CommonConstant.AREA_DATA_CACHEKEY, area.getId());
        this.tenantRedisTemplate.delete(cacheKey);
    }

    @Override
    public SysArea selectAreaById(String areaId) {
        if(ROOT.equals(areaId) || StringUtils.isEmpty(areaId)){
            SysArea area = new SysArea();
            area.setId(ROOT);
            area.setAreaName("中华人民共和国行政区划");
            area.setAncestors("");
            return area;
        }
        return jpaDao.find(SysArea.class, areaId);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<SysArea> selectAreaListByParentId(String parentId) {
        //10 00 00 000 000
        if(StringUtils.isEmpty(parentId)) {
            return Collections.emptyList();
        }
        // 补全parentId至12位
        parentId = String.format("%-12s", parentId).replace(' ', '0');
        try {
            Long.parseLong(parentId);
        }catch (NumberFormatException e) {
            return Collections.emptyList();
        }
        String cacheKey = StringUtils.format(CommonConstant.AREA_DATA_CACHEKEY, parentId);
        Object object = this.tenantRedisTemplate.opsForValue().get(cacheKey);
        if(null != object) {
            return (List<SysArea>) object;
        }
        String hql = "from SysArea a where a.parentId = ? order by a.orderNum asc,a.ancestors asc";
        List<SysArea> list = jpaDao.search(hql, parentId, SysArea.class);
        if(CollectionUtil.isNotEmpty(list)) {
            this.tenantRedisTemplate.opsForValue().set(cacheKey, list);
        }
        return list;
    }
}
