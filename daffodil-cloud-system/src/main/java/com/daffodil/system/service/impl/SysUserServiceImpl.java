package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.common.annotation.DataScope;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.util.HqlUtils;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysBusinessScope;
import com.daffodil.system.entity.SysDept;
import com.daffodil.system.entity.SysGroup;
import com.daffodil.system.entity.SysPost;
import com.daffodil.system.entity.SysRank;
import com.daffodil.system.entity.SysRole;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.enums.PasswordReasonEnum;
import com.daffodil.system.event.SysUserEvent;
import com.daffodil.system.event.publisher.SysUserPublisherService;
import com.daffodil.system.service.ISysUserService;
import com.daffodil.util.PasswordUtils;
import com.daffodil.util.StringUtils;
import com.daffodil.util.sm.SM2Utils;

/**
 * -用户管理逻辑实现
 * @author yweijian
 * @date 2020年1月7日
 * @version 1.0
 */
@Service
public class SysUserServiceImpl implements ISysUserService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Autowired
    private SysUserPublisherService userPublisherService;

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    @DataScope(business = SysUser.class, deptAlias = "d.id", userAlias = "u.id")
    public List<SysUser> selectUserList(Query<SysUser> query, @Nullable SysUser user) {

        StringBuffer hql = new StringBuffer();
        List<Object> paras = new ArrayList<Object>();
        SysUser sysUser = query.getEntity();

        hql.append("select u from SysUser u left join SysDept d on u.deptId=d.id where 1=1 ");

        //部门用户
        if(StringUtils.isNotEmpty(sysUser.getDeptId())){
            hql.append("and (u.deptId = ? or u.deptId in (select t.id from SysDept t where ancestors like ?)) ");
            paras.add(sysUser.getDeptId());
            paras.add("%" + sysUser.getDeptId() + "%");
        }

        //角色用户
        if(StringUtils.isNotEmpty(sysUser.getRoleIds())) {
            hql.append(CommonConstant.YES.equals(sysUser.getAllocated()) ? "and u.id in " : "and u.id not in ");
            String roleIds = HqlUtils.createHql(paras, sysUser.getRoleIds());
            hql.append(StringUtils.format("(select distinct userId from SysUserRole where roleId in {})", roleIds));
        }

        //岗位用户
        if(StringUtils.isNotEmpty(sysUser.getPostIds())) {
            hql.append(CommonConstant.YES.equals(sysUser.getAllocated()) ? "and u.id in " : "and u.id not in ");
            String postIds = HqlUtils.createHql(paras, sysUser.getPostIds());
            hql.append(StringUtils.format("(select distinct userId from SysUserPost where postId in {})", postIds));
        }

        //职级用户
        if(StringUtils.isNotEmpty(sysUser.getRankIds())) {
            hql.append(CommonConstant.YES.equals(sysUser.getAllocated()) ? "and u.id in " : "and u.id not in ");
            String rankIds = HqlUtils.createHql(paras, sysUser.getRankIds());
            hql.append(StringUtils.format("(select distinct userId from SysUserRank where rankId in {})", rankIds));
        }

        //群组用户
        if(StringUtils.isNotEmpty(sysUser.getGroupIds())) {
            hql.append(CommonConstant.YES.equals(sysUser.getAllocated()) ? "and u.id in " : "and u.id not in ");
            String groupIds = HqlUtils.createHql(paras, sysUser.getGroupIds());
            hql.append(StringUtils.format("(select distinct userId from SysUserGroup where status = '0' and groupId in {})", groupIds));
        }

        HqlUtils.createHql(hql, paras, query, "u");

        return jpaDao.search(hql.toString(), paras, SysUser.class, query.getPage());
    }

    @Override
    public SysUser selectUserById(String userId) {
        SysUser user = jpaDao.find("from SysUser u where u.id = ?", userId, SysUser.class);

        // 部门
        SysDept dept = jpaDao.find(SysDept.class, user.getDeptId());
        user.setDept(null == dept ? new SysDept() : dept);

        // 角色
        List<SysRole> roles = jpaDao.search("select r from SysRole r,SysUserRole ur where r.id = ur.roleId and r.status = '0' and ur.userId = ?", userId, SysRole.class);
        roles.stream().forEach(role -> {
            List<SysBusinessScope> businessScopes = jpaDao.search("select b from SysBusinessScope b where b.roleId = ? ", role.getId(), SysBusinessScope.class);
            role.setBusinessScopes(businessScopes);
        });
        user.setRoles(roles);
        List<String> roleIds = roles.stream().map(SysRole::getId).collect(Collectors.toList());
        user.setRoleIds(roleIds.toArray(new String[roleIds.size()]));

        // 岗位
        List<SysPost> posts = jpaDao.search("select p from SysPost p,SysUserPost up where p.id = up.postId and p.status = '0' and up.userId = ?", userId, SysPost.class);
        user.setPosts(posts);
        List<String> postIds = posts.stream().map(SysPost::getId).collect(Collectors.toList());
        user.setPostIds(postIds.toArray(new String[postIds.size()]));

        // 职级
        List<SysRank> ranks = jpaDao.search("select r from SysRank r,SysUserRank ur where r.id = ur.rankId and r.status = '0' and ur.userId = ?", userId, SysRank.class);
        user.setRanks(ranks);
        List<String> rankIds = ranks.stream().map(SysRank::getId).collect(Collectors.toList());
        user.setRankIds(rankIds.toArray(new String[rankIds.size()]));

        // 群组
        List<SysGroup> groups = jpaDao.search("select g from SysGroup g, SysUserGroup ug where g.id = ug.groupId and g.status = '0' and ug.status = '0' and ug.userId = ?", userId, SysGroup.class);
        user.setGroups(groups);
        List<String> groupIds = groups.stream().map(SysGroup::getId).collect(Collectors.toList());
        user.setGroupIds(groupIds.toArray(new String[groupIds.size()]));

        return user;
    }

    @Override
    @Transactional
    public void deleteUserByIds(String[] ids) {
        if(StringUtils.isNotEmpty(ids)){
            String tenantId = TenantContextHolder.getTenantHolder();
            for(String userId : ids){
                SysUser user = jpaDao.find(SysUser.class, userId);
                if (StringUtils.isNotNull(user) && CommonConstant.YES.equals(user.getIsAdmin())) {
                    throw new BaseException(37108, new Object[] { user.getLoginName() });
                }
                // 删除用户
                jpaDao.delete(user);
                userPublisherService.publish(user, tenantId, SysUserEvent.Operation.DELETE.name());
            }
        }
    }

    @Override
    @Transactional
    public void insertUser(SysUser user) {
        if (this.checkLoginNameUnique(user)) {
            throw new BaseException(37101, new Object[] { user.getLoginName() });
        } else if (this.checkPhoneUnique(user)) {
            throw new BaseException(37102, new Object[] { user.getLoginName() });
        } else if (this.checkEmailUnique(user)) {
            throw new BaseException(37103, new Object[] { user.getLoginName() });
        }
        // 用户密码校验
        String encryptPassword = user.getPassword();
        if(StringUtils.isEmpty(encryptPassword)) {
            throw new BaseException(37104, new Object[] { user.getLoginName() });
        }
        String privateKey = (String) redisTemplate.opsForValue().get(CommonConstant.SM2_PRIVATE_KEY_CACHEKEY);
        String decryptPassword = SM2Utils.decryptData(privateKey, encryptPassword);
        if(!PasswordUtils.verifyPassword(decryptPassword)) {
            throw new BaseException(37104, new Object[] { user.getLoginName() });
        }
        String salt = PasswordUtils.randomSalt();
        String password = PasswordUtils.encryptPassword(user.getLoginName(), decryptPassword, salt);
        user.setSalt(salt);
        user.setPassword(password);
        // 新增用户的密码有效期1个月
        user.setExpireTime(DateUtils.addMonths(new Date(), 1));
        user.setChangeReason(PasswordReasonEnum.REASON_PASSWORD_NEW.code());

        jpaDao.save(user);
        userPublisherService.publish(user, TenantContextHolder.getTenantHolder(), SysUserEvent.Operation.INSERT.name());
    }

    @Override
    @Transactional
    public void updateUser(SysUser user) {
        if (this.checkPhoneUnique(user)) {
            throw new BaseException(37106, new Object[] { user.getLoginName() });
        } else if (this.checkEmailUnique(user)) {
            throw new BaseException(37107, new Object[] { user.getLoginName() });
        }
        jpaDao.update(user);
        userPublisherService.publish(user, TenantContextHolder.getTenantHolder(), SysUserEvent.Operation.UPDATE.name());
    }

    @Override
    @Transactional
    public void updateUserInfo(SysUser user) {
        if (this.checkPhoneUnique(user)) {
            throw new BaseException(37106, new Object[] { user.getLoginName() });
        } else if (this.checkEmailUnique(user)) {
            throw new BaseException(37107, new Object[] { user.getLoginName() });
        }
        String encryptPassword = user.getPassword();
        if(StringUtils.isNotEmpty(encryptPassword)) {
            //用户密码修改后有效期为6个月
            this.updateUserPwd(user, 6, PasswordReasonEnum.REASON_PASSWORD_NOMAL.code());
        }else {
            jpaDao.update(user);
        }
    }

    @Override
    @Transactional
    public void resetUserPwd(SysUser user) {
        //密码被系统管理员重置后有效期为1个月
        this.updateUserPwd(user, 1, PasswordReasonEnum.REASON_PASSWORD_RESET.code());
    }

    @Transactional
    public void updateUserPwd(SysUser user, Integer month, String reason) {
        SysUser sysUser  = jpaDao.find(SysUser.class, user.getId());
        if(StringUtils.isNull(sysUser)) {
            throw new BaseException(37109);
        }
        String encryptPassword = user.getPassword();
        if(StringUtils.isEmpty(encryptPassword)) {
            throw new BaseException(37110, new Object[] { sysUser.getLoginName() });
        }
        String privateKey = (String) redisTemplate.opsForValue().get(CommonConstant.SM2_PRIVATE_KEY_CACHEKEY);
        String decryptPassword = SM2Utils.decryptData(privateKey, encryptPassword);
        if(!PasswordUtils.verifyPassword(decryptPassword)) {
            throw new BaseException(37110, new Object[] { sysUser.getLoginName() });
        }
        String salt = PasswordUtils.randomSalt();
        String password = PasswordUtils.encryptPassword(sysUser.getLoginName(), decryptPassword, salt);
        sysUser.setSalt(salt);
        sysUser.setPassword(password);
        sysUser.setExpireTime(DateUtils.addMonths(new Date(), month));
        sysUser.setChangeReason(reason);
        sysUser.setUpdateBy(user.getUpdateBy());
        sysUser.setUpdateTime(new Date());

        jpaDao.update(sysUser);
    }

    private boolean checkLoginNameUnique(SysUser user) {
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysUser where loginName = ? ";
        paras.add(user.getLoginName());
        if(StringUtils.isNotEmpty(user.getId())){
            hql += "and id != ? ";
            paras.add(user.getId());
        }
        SysUser sysUser = jpaDao.find(hql, paras, SysUser.class);
        return null != sysUser;
    }

    private boolean checkPhoneUnique(SysUser user) {
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysUser where phone = ? ";
        paras.add(user.getPhone());
        if(StringUtils.isNotEmpty(user.getId())){
            hql += "and id != ? ";
            paras.add(user.getId());
        }
        SysUser sysUser = jpaDao.find(hql, paras, SysUser.class);
        return null != sysUser;
    }

    private boolean checkEmailUnique(SysUser user) {
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysUser where email = ? ";
        paras.add(user.getEmail());
        if(StringUtils.isNotEmpty(user.getId())){
            hql += "and id != ? ";
            paras.add(user.getId());
        }
        SysUser sysUser = jpaDao.find(hql, paras, SysUser.class);
        return null != sysUser;
    }

}
