package com.daffodil.system.service.impl;

import cn.hutool.core.date.DateUtil;

import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.system.entity.SysDays;
import com.daffodil.system.entity.SysHoliday;
import com.daffodil.system.service.ISysDaysService;
import com.daffodil.util.StringUtils;
import com.daffodil.util.sm.CodeUtils;
import com.daffodil.util.text.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * -日期管理
 * @author EP
 * @date 2023年1月29日
 * @version 1.0.0
 * @description
 */
@Service
public class SysDaysServiceImpl implements ISysDaysService {
    
    @Autowired
    private JpaDao<String> jpaDao;

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    @Value("${daffodil.days.start-year:2000}")
    private Integer startYear;

    @Value("${daffodil.days.end-year:2100}")
    private Integer endYear;

    @Override
    @Transactional
    public void initSysDaysData() {
        //获取所有后台配置的节假日
        List<SysHoliday> holidayList = jpaDao.search("from SysHoliday where 1 = 1", SysHoliday.class);
        Map<String, SysHoliday> holidayMap = holidayList.stream()
                        .collect(Collectors.toMap(holiday -> DateUtil.format(holiday.getDate(), "yyyy-MM-dd"), holiday -> holiday));

        //初始化2000~2100年的日期数据到数据库和Redis缓存
        for (int year = startYear; year <= endYear; year++) {
            SysDays sysDays = this.selectDaysByYear(year);
            if(sysDays == null) {
                sysDays = new SysDays(year, holidayMap);
                this.insertDays(sysDays);
            }
            this.tenantRedisTemplate.opsForHash().put(CommonConstant.DAYS_DATA_CACHEKEY, Convert.toStr(sysDays.getYear()), sysDays.getData());
        }
    }

    @Override
    public String selectDaysHexData(Integer year) {
        Object object = this.tenantRedisTemplate.opsForHash().get(CommonConstant.DAYS_DATA_CACHEKEY, Convert.toStr(year));
        if(null != object) {
            return CodeUtils.getHexString((byte[]) object);
        }
        SysDays sysDays = this.selectDaysByYear(year);
        byte[] data = {};
        if(null != sysDays) {
            data = sysDays.getData();
        }
        this.tenantRedisTemplate.opsForHash().put(CommonConstant.DAYS_DATA_CACHEKEY, Convert.toStr(year), data);
        return CodeUtils.getHexString(data);
    }

    @Override
    public List<String> multiselectDaysHexData(Integer start, Integer end) {
        start = Math.max(start, startYear);
        end = Math.min(end, endYear);
        List<Object> keys = new ArrayList<Object>();
        for (int y = start; y <= end; y++) {
            keys.add(Convert.toStr(y));
        }
        return this.tenantRedisTemplate.opsForHash()
                .multiGet(CommonConstant.DAYS_DATA_CACHEKEY, keys)
                .stream()
                .map(data -> CodeUtils.getHexString((byte[])data))
                .collect(Collectors.toList());
    }

    @Override
    public SysDays selectDaysByYear(Integer year) {
        return jpaDao.find("from SysDays where year = ?", year, SysDays.class);
    }

    @Override
    @Transactional
    public void insertDays(SysDays sysDays) {
        SysDays result = this.selectDaysByYear(sysDays.getYear());
        if(StringUtils.isNull(result)) {
            jpaDao.save(sysDays);
            this.tenantRedisTemplate.opsForHash().put(CommonConstant.DAYS_DATA_CACHEKEY, Convert.toStr(sysDays.getYear()), sysDays.getData());
        }
    }

    @Override
    @Transactional
    public void updateDays(SysDays sysDays) {
        SysDays result = this.selectDaysByYear(sysDays.getYear());
        if(StringUtils.isNotNull(result)) {
            sysDays.setId(result.getId());
            jpaDao.update(sysDays);
            this.tenantRedisTemplate.opsForHash().put(CommonConstant.DAYS_DATA_CACHEKEY, Convert.toStr(sysDays.getYear()), sysDays.getData());
        }
    }

    @Override
    @Transactional
    public void updateDaysByDate(Date date, boolean isWorkDay) {
        if(null == date) {
            return;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int index = calendar.get(Calendar.DAY_OF_MONTH) - 1;
        
        SysDays sysDays = this.selectDaysByYear(year);
        if(null == sysDays) {
            return;
        }
        //取出当月字节数组
        byte[] monthDataBytes = new byte[4];
        System.arraycopy(sysDays.getData(), month * 4, monthDataBytes, 0, 4);
        int monthData = CodeUtils.byteToIntBigEndian(monthDataBytes);
        //修改当日节假日状态
        int holidayFlag = 1 << index;
        if (isWorkDay) {
            monthData &= ~holidayFlag; //设置为工作日0
        } else {
            monthData |= holidayFlag; //设置为节假日1
        }
        //替换结果
        byte[] bytes = CodeUtils.intToBytesBigEndian(monthData);
        System.arraycopy(bytes, 0, sysDays.getData(), month * 4, 4);
        
        sysDays.setContent(CodeUtils.bytes2BinaryString(sysDays.getData()));
        jpaDao.update(sysDays);

        this.tenantRedisTemplate.opsForHash().put(CommonConstant.DAYS_DATA_CACHEKEY, Convert.toStr(sysDays.getYear()), sysDays.getData());
    }

    @Override
    @Transactional
    public void deleteDaysByIds(String[] ids) {
        jpaDao.delete(SysDays.class, ids);
    }
}
