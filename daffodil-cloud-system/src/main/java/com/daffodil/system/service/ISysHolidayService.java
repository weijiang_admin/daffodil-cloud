package com.daffodil.system.service;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysHoliday;

import java.util.List;

/**
 * -节假日管理
 * @author EP
 * @date 2023年1月16日
 * @version 2.0.0
 * @description
 */
public interface ISysHolidayService {
    
    /**
     * -根据查询条件查询节假日配置
     * @param query
     * @return
     */
    List<SysHoliday> selectHolidayList(Query<SysHoliday> query);

    /**
     * -设置节假日配置信息
     * @param holiday
     */
    void setHoliday(SysHoliday holiday);

    /**
     * -删除节假日管理信息
     * @param ids
     */
    void deleteHolidayByIds(String[] ids);

}
