package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysMenu;
import com.daffodil.system.entity.SysTenant;

/**
 * 
 * @author yweijian
 * @date 2024年12月12日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public interface ISysTenantService {

    /**
     * 分页查询租户信息集合
     * @param query
     * @return
     */
    public List<SysTenant> selectTenantList(Query<SysTenant> query);

    /**
     * 通过租户ID查询租户信息
     * @param tenantId
     * @return
     */
    public SysTenant selectTenantById(String tenantId);

    /**
     * 批量删除租户信息
     * @param ids
     */
    public void deleteTenantByIds(String[] ids);

    /**
     * 新增保存租户信息
     * @param tenant
     */
    public void insertTenant(SysTenant tenant);

    /**
     * 修改保存租户信息
     * @param tenant
     */
    public void updateTenant(SysTenant tenant);

    /**
     * 获取租户所授权的菜单集合
     * @param id
     * @return
     */
    public List<SysMenu> selectMenuListByTenantId(String tenantId);

    /**
     * 批量保存更新租户菜单数据
     * @param menus
     */
    public void batchSaveOrUpdateMenus(List<SysMenu> menus);

}
