package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysTag;

/**
 * -标签 业务层服务
 * @author yweijian
 * @date 2019年12月18日
 * @version 1.0
 */
public interface ISysTagService{
    
    /**
     * -根据查询条件查询标签列表
     * @param query
     * @return
     */
    public List<SysTag> selectTagList(Query<SysTag> query);
    
    /**
     * -根据标签ID查询信息
     * @param tagId
     * @return
     */
    public SysTag selectTagById(String tagId);
    
    /**
     * -新增保存标签信息
     * @param tag
     */
    public void insertTag(SysTag tag);
    
    /**
     * -删除标签管理信息
     * @param ids
     */
    public void deleteTagByIds(String[] ids);

    /**
     * -修改保存标签信息
     * @param tag
     */
    public void updateTag(SysTag tag);

    /**
     * -根据标签类型查询信息
     * @param tagLabel
     * @return
     */
    public List<SysTag> selectTagByLabel(String tagLabel);

}
