package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.core.util.HqlUtils;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysDatasource;
import com.daffodil.system.service.ISysDatasourceService;

/**
 * 
 * @author yweijian
 * @date 2024年12月12日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Service
public class SysDatasourceServiceImpl implements ISysDatasourceService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Override
    public List<SysDatasource> selectDatasourceList(Query<SysDatasource> query) {
        StringBuffer hql = new StringBuffer("from SysDatasource where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        return jpaDao.search(hql.toString(), paras, SysDatasource.class, query.getPage());
    }

    @Override
    public SysDatasource selectDatasourceById(String datasourceId) {
        return jpaDao.find(SysDatasource.class, datasourceId);
    }

    @Override
    @Transactional
    public void deleteDatasourceByIds(String[] ids) {
        for (String datasourceId : ids) {
            int count = jpaDao.count("from SysTenant where datasourceId = ? ", datasourceId);
            if (count > 0) {
                throw new BaseException(37931);
            }
        }
        jpaDao.delete(SysDatasource.class, ids);
    }

    @Override
    @Transactional
    public void insertDatasource(SysDatasource datasource) {
        jpaDao.save(datasource);
    }

    @Override
    @Transactional
    public void updateDatasource(SysDatasource datasource) {
        jpaDao.update(datasource);
    }
}
