package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.util.HqlUtils;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.Query;
import com.daffodil.system.controller.model.QueryNoticeParam;
import com.daffodil.system.controller.model.SysNoticeParam;
import com.daffodil.system.entity.SysNotice;
import com.daffodil.system.entity.SysNoticeOffset;
import com.daffodil.system.entity.SysNoticeTarget;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.entity.user.SysUserNotice;
import com.daffodil.system.event.publisher.SysNoticePublisherService;
import com.daffodil.system.service.ISysNoticeService;
import com.daffodil.util.StringUtils;

/**
 * 公告 服务层实现
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Service
public class SysNoticeServiceImpl implements ISysNoticeService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Autowired
    private SysNoticePublisherService noticePublisherService;

    @Resource(name = "systemEventExecutor")
    private ThreadPoolTaskExecutor executor;

    @Override
    public List<SysNotice> selectNoticeList(Query<SysNotice> query) {
        StringBuffer hql = new StringBuffer("from SysNotice where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        return jpaDao.search(hql.toString(), paras, SysNotice.class, query.getPage());
    }

    @Override
    public SysNotice selectNoticeById(String noticeId) {
        return jpaDao.find(SysNotice.class, noticeId);
    }

    @Override
    @Transactional
    public void insertNotice(SysNotice notice) {
        jpaDao.save(notice);
    }

    @Override
    @Transactional
    public void updateNotice(SysNotice notice) {
        jpaDao.update(notice);
    }

    @Override
    @Transactional
    public void deleteNoticeByIds(String[] ids) {
        jpaDao.delete(SysNotice.class, ids);
    }

    @Override
    @Transactional
    public void publishNotice(SysNoticeParam param) {
        SysNotice notice = jpaDao.find(SysNotice.class, param.getNoticeId());
        if(null == notice) {
            return;
        }
        notice.setStatus("1");
        jpaDao.update(notice);
        noticePublisherService.publish(param, TenantContextHolder.getTenantHolder());
    }

    @Override
    public List<SysNotice> selectNoticeList(QueryNoticeParam param, Page page) {
        List<Object> paras = new ArrayList<Object>();
        StringBuffer hql = new StringBuffer("select new SysNotice(n.id, n.noticeTitle, n.noticeType, n.noticeContent, n.status, n.remark, n.createTime, un.status) ");
        hql.append("from SysNotice n left join SysUserNotice un on un.noticeId = n.id where 1=1 ");
        if(StringUtils.isNotEmpty(param.getUserId())) {
            hql.append("and un.userId = ? ");
            paras.add(param.getUserId());
        }
        if(StringUtils.isNotEmpty(param.getNoticeId())) {
            hql.append("and n.id = ? ");
            paras.add(param.getNoticeId());
        }
        if(StringUtils.isNotEmpty(param.getNoticeTitle())) {
            hql.append("and n.noticeTitle like ? ");
            paras.add("%" + param.getNoticeTitle() + "%");
        }
        if(StringUtils.isNotEmpty(param.getNoticeType())) {
            hql.append("and n.noticeType = ? ");
            paras.add(param.getNoticeType());
        }
        if(StringUtils.isNotEmpty(param.getReadStatus())) {
            hql.append("and un.status = ? ");
            paras.add(param.getReadStatus());
        }
        if(StringUtils.isNotNull(param.getStartTime())) {
            hql.append("and n.createTime >= ? ");
            paras.add(param.getStartTime());
        }
        if(StringUtils.isNotNull(param.getEndTime())) {
            hql.append("and n.createTime <= ? ");
            paras.add(param.getEndTime());
        }
        hql.append("order by n.createTime desc ");
        return jpaDao.search(hql.toString(), paras, SysNotice.class, page);
    }

    @Override
    @Transactional
    public void triggerNoticeTargetByUserId(String userId) {
        SysNoticeOffset offset = jpaDao.find("from SysNoticeOffset where userId = ?", userId, SysNoticeOffset.class);
        Date startTime= (null == offset) ? null : offset.getOffsetTime(), endTime = new Date();
        List<String> noticeIds = this.selectNoticeIdsByUserId(userId, startTime, endTime);

        //写入用户和通知消息关系
        if(StringUtils.isNotEmpty(noticeIds)) {
            for(String noticeId : noticeIds) {
                List<Object> paras = new ArrayList<Object>();
                paras.add(userId);
                paras.add(noticeId);
                int count = jpaDao.count("from SysUserNotice where userId = ? and noticeId = ?", paras);
                if(count <= 0) {
                    SysUserNotice userNotice = new SysUserNotice();
                    userNotice.setStatus("0");
                    userNotice.setUserId(userId);
                    userNotice.setNoticeId(noticeId);
                    jpaDao.save(userNotice);
                }
            }
        }

        offset = (null != offset) ? offset : new SysNoticeOffset();
        offset.setUserId(userId);
        offset.setOffsetTime(endTime);
        this.saveOrUpdateNoticeOffset(offset);
    }

    /**
     * -获取消息通知ID
     * @param userId
     * @param startTime
     * @param endTime
     * @return
     */
    private List<String> selectNoticeIdsByUserId(String userId, Date startTime, Date endTime){
        String tenantId = TenantContextHolder.getTenantHolder();
        List<CompletableFuture<List<String>>> futures = new ArrayList<CompletableFuture<List<String>>>();
        //全体用户
        futures.add(CompletableFuture.supplyAsync(() -> {
            return TenantContextHolder.supply(tenantId, () -> this.selectSysNoticeTarget(SysNoticeTarget.Target.ALL.ordinal(), "", startTime, endTime));
        }, executor));
        //指定用户
        futures.add(CompletableFuture.supplyAsync(() -> {
            return TenantContextHolder.supply(tenantId, () -> this.selectSysNoticeTarget(SysNoticeTarget.Target.USER.ordinal(), userId, startTime, endTime));
        }, executor));
        //指定部门
        futures.add(CompletableFuture.supplyAsync(() -> {
            return TenantContextHolder.supply(tenantId, () -> {
                SysUser user = jpaDao.find(SysUser.class, userId);
                return this.selectSysNoticeTarget(SysNoticeTarget.Target.DEPT.ordinal(), user.getDeptId(), startTime, endTime);
            });
        }));
        //指定角色
        futures.add(CompletableFuture.supplyAsync(() -> {
            return TenantContextHolder.supply(tenantId, () -> {
                List<String> roleIds = jpaDao.search("select r.id from SysRole r,SysUserRole ur where r.id = ur.roleId and r.status = '0' and ur.userId = ?", userId, String.class);
                return this.selectSysNoticeTarget(SysNoticeTarget.Target.ROLE.ordinal(), roleIds, startTime, endTime);
            });
        }, executor));
        //指定岗位
        futures.add(CompletableFuture.supplyAsync(() -> {
            return TenantContextHolder.supply(tenantId, () -> {
                List<String> postIds = jpaDao.search("select p.id from SysPost p,SysUserPost up where p.id = up.postId and p.status = '0' and up.userId = ?", userId, String.class);
                return this.selectSysNoticeTarget(SysNoticeTarget.Target.POST.ordinal(), postIds, startTime, endTime);
            });
        }, executor));
        //指定职级
        futures.add(CompletableFuture.supplyAsync(() -> {
            return TenantContextHolder.supply(tenantId, () -> {
                List<String> rankIds = jpaDao.search("select r.id from SysRank r,SysUserRank ur where r.id = ur.rankId and r.status = '0' and ur.userId = ?", userId, String.class);
                return this.selectSysNoticeTarget(SysNoticeTarget.Target.RANK.ordinal(), rankIds, startTime, endTime);
            });
        }, executor));
        //指定群组
        futures.add(CompletableFuture.supplyAsync(() -> {
            List<String> groupIds = jpaDao.search("select g.id from SysGroup g,SysUserGroup ug where g.id = ug.groupId and g.status = '0' and ug.userId = ?", userId, String.class);
            return this.selectSysNoticeTarget(SysNoticeTarget.Target.GROUP.ordinal(), groupIds, startTime, endTime);
        }, executor));

        final List<String> list = new ArrayList<String>();
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()])).whenComplete((ac, ex) -> {
            futures.forEach(future -> list.addAll(future.getNow(new ArrayList<String>())));
        }).join();

        return list.stream().distinct().collect(Collectors.toList());
    }

    /**
     * -保存或更新
     * @param offset
     */
    @Transactional
    public void saveOrUpdateNoticeOffset(SysNoticeOffset offset) {
        if(StringUtils.isEmpty(offset.getId())) {
            jpaDao.save(offset);
        }else {
            jpaDao.update(offset);
        }
    }

    /**
     * -获取通知消息ID
     * @param target
     * @param targetId
     * @param startTime
     * @param endTime
     * @return
     */
    private List<String> selectSysNoticeTarget(int target, String targetId, Date startTime, Date endTime){
        List<Object> paras = new ArrayList<Object>();
        StringBuffer hql = new StringBuffer("select t.noticeId from SysNoticeTarget t where t.target = ? ");
        paras.add(target);
        if(StringUtils.isNotEmpty(targetId)) {
            hql.append("and t.targetId = ? ");
            paras.add(targetId);
        }
        if(StringUtils.isNotNull(startTime)) {
            hql.append("and t.createTime > ? ");
            paras.add(startTime);
        }
        if(StringUtils.isNotNull(endTime)) {
            hql.append("and t.createTime <= ? ");
            paras.add(endTime);
        }
        List<String> list = jpaDao.search(hql.toString(), paras, String.class);
        return StringUtils.isNotEmpty(list) ? list : Collections.emptyList();
    }

    /**
     * -获取通知消息ID
     * @param target
     * @param targetIds
     * @param startTime
     * @param endTime
     * @return
     */
    private List<String> selectSysNoticeTarget(int target, List<String> targetIds, Date startTime, Date endTime){
        List<Object> paras = new ArrayList<Object>();
        StringBuffer hql = new StringBuffer("select t.noticeId from SysNoticeTarget t where t.target = ? ");
        paras.add(target);
        if(StringUtils.isNotEmpty(targetIds)) {
            String ids = HqlUtils.createHql(paras, targetIds.toArray());
            hql.append(StringUtils.format("and t.targetId in {} ", ids));
        }
        if(StringUtils.isNotNull(startTime)) {
            hql.append("and t.createTime > ? ");
            paras.add(startTime);
        }
        if(StringUtils.isNotNull(endTime)) {
            hql.append("and t.createTime <= ? ");
            paras.add(endTime);
        }
        List<String> list = jpaDao.search(hql.toString(), paras, String.class);
        return StringUtils.isNotEmpty(list) ? list : Collections.emptyList();
    }

    @Override
    @Transactional
    public void updateNoticeReadStatusByIds(String[] ids, String userId) {
        List<Object> paras = new ArrayList<Object>();
        paras.add(userId);
        String noticeIds = HqlUtils.createHql(paras, ids);
        String hql = StringUtils.format("update SysUserNotice set status = '1' where userId = ? and noticeId in {} ", noticeIds);
        jpaDao.update(hql, paras);
    }
}
