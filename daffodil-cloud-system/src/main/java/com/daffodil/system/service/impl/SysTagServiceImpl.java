package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.util.HqlUtils;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysTag;
import com.daffodil.system.service.ISysTagService;
import com.daffodil.util.StringUtils;

import cn.hutool.core.collection.CollectionUtil;

/**
 * 
 * @author yweijian
 * @date 2022年6月13日
 * @version 2.0.0
 * @description
 */
@Service
public class SysTagServiceImpl implements ISysTagService{

    @Autowired
    private JpaDao<String> jpaDao;

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    @Override
    public List<SysTag> selectTagList(Query<SysTag> query) {
        StringBuffer hql = new StringBuffer();
        List<Object> paras = new ArrayList<Object>();
        hql.append("from SysTag d where 1=1 ");
        HqlUtils.createHql(hql, paras, query, "d");
        hql.append("order by d.orderNum asc, d.ancestors asc");
        return jpaDao.search(hql.toString(), paras, SysTag.class);
    }

    @Override
    @Transactional
    public void deleteTagByIds(String[] ids) {
        for(String tagId : ids) {
            List<SysTag> list = jpaDao.search("from SysTag where parentId = ?", tagId, SysTag.class);
            if (list != null && list.size() > 0) {
                throw new BaseException(37704);
            }
            jpaDao.delete(SysTag.class, tagId);
        }
    }

    @Override
    public SysTag selectTagById(String tagId) {
        if(CommonConstant.ROOT.equals(tagId)){
            SysTag tag = new SysTag();
            tag.setId(CommonConstant.ROOT);
            tag.setTagName("");
            tag.setAncestors("");
            return tag;
        }

        return jpaDao.find(SysTag.class, tagId);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<SysTag> selectTagByLabel(String tagLabel) {
        String cacheKey = StringUtils.format(CommonConstant.TAG_DATA_CACHEKEY, tagLabel);
        Object object = this.tenantRedisTemplate.opsForValue().get(cacheKey);
        if(null != object) {
            return (List<SysTag>) object;
        }
        String hql = "from SysTag where parentId in (select id from SysTag where tagLabel = ?) order by orderNum asc";
        List<SysTag> list = jpaDao.search(hql, tagLabel, SysTag.class);
        if(CollectionUtil.isNotEmpty(list)) {
            this.tenantRedisTemplate.opsForValue().set(cacheKey, list);
        }
        return list;
    }

    @Override
    @Transactional
    public void insertTag(SysTag tag) {
        if (StringUtils.isNotEmpty(tag.getTagLabel()) && this.checkTagLabelUnique(tag)) {
            throw new BaseException(37701, new Object[] { tag.getTagName(), tag.getTagLabel()});
        }
        if(CommonConstant.ROOT.equals(tag.getParentId())){
            tag.setAncestors(CommonConstant.ROOT);
        }else{
            SysTag parent = jpaDao.find(SysTag.class, tag.getParentId());
            if(parent != null){
                tag.setAncestors(parent.getAncestors() + "," + parent.getId());
            }
        }
        jpaDao.save(tag);
        String cacheKey = StringUtils.format(CommonConstant.TAG_DATA_CACHEKEY, tag.getTagLabel());
        this.tenantRedisTemplate.delete(cacheKey);
    }

    @Override
    @Transactional
    public void updateTag(SysTag tag) {
        if (StringUtils.isNotEmpty(tag.getTagLabel()) && this.checkTagLabelUnique(tag)) {
            throw new BaseException(37702, new Object[] { tag.getTagName(), tag.getTagLabel()});
        }
        if(this.checkTagIsSelfOrChildren(tag)){
            throw new BaseException(37703, new Object[] { tag.getTagName()});
        }
        SysTag sysTag = this.selectTagById(tag.getId());
        SysTag parent = this.selectTagById(tag.getParentId());
        if (StringUtils.isNotNull(parent) && StringUtils.isNotNull(sysTag)) {
            String newAncestors = parent.getAncestors() + "," + parent.getId();
            String oldAncestors = sysTag.getAncestors();
            tag.setAncestors(newAncestors);
            this.updateChildrenTag(tag, newAncestors, oldAncestors);
        }
        jpaDao.update(tag);
        String cacheKey = StringUtils.format(CommonConstant.TAG_DATA_CACHEKEY, tag.getTagLabel());
        this.tenantRedisTemplate.delete(cacheKey);
    }

    private boolean checkTagLabelUnique(SysTag tag) {
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysTag where tagLabel = ? and parentId = ? ";
        paras.add(tag.getTagLabel());
        paras.add(tag.getParentId());
        if(StringUtils.isNotEmpty(tag.getId())){
            hql += "and id != ?";
            paras.add(tag.getId());
        }
        SysTag sysTag = jpaDao.find(hql, paras, SysTag.class);
        return null != sysTag;
    }

    private boolean checkTagIsSelfOrChildren(SysTag tag){
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysTag where id = ? or ancestors like ?";
        paras.add(tag.getId());
        paras.add("%" + tag.getId() + "%");
        List<SysTag> list = jpaDao.search(hql, paras, SysTag.class);
        for(SysTag sysTag : list){
            if(tag.getParentId().equals(sysTag.getId())){
                return true;
            }
        }
        return false;
    }

    @Transactional
    public void updateChildrenTag(SysTag tag, String newAncestors, String oldAncestors){
        List<Object> paras = new ArrayList<Object>();
        paras.add(oldAncestors);
        paras.add(newAncestors);
        paras.add(tag.getStatus());
        paras.add("%" + tag.getId() + "%");
        jpaDao.update("update SysTag set ancestors = replace(ancestors, ?, ?), status = ? where ancestors like ?", paras);
        String cacheKey = StringUtils.format(CommonConstant.TAG_DATA_CACHEKEY, tag.getTagLabel());
        this.tenantRedisTemplate.delete(cacheKey);
    }

}
