package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.util.HqlUtils;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysGroup;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.entity.user.SysUserGroup;
import com.daffodil.system.enums.DataStatusEnum;
import com.daffodil.system.service.ISysGroupService;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2022年6月21日
 * @version 2.0.0
 * @description
 */
@Service
public class SysGroupServiceImpl implements ISysGroupService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Override
    public List<SysGroup> selectGroupList(Query<SysGroup> query) {
        StringBuffer hql = new StringBuffer("from SysGroup where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        return jpaDao.search(hql.toString(), paras, SysGroup.class, query.getPage());
    }

    @Override
    public SysGroup selectGroupById(String groupId) {
        return jpaDao.find(SysGroup.class, groupId);
    }

    @Override
    @Transactional
    public void deleteGroupByIds(String[] ids) {
        jpaDao.delete(SysGroup.class, ids);
        List<Object> paras = new ArrayList<Object>();
        String groupIds = HqlUtils.createHql(paras, ids);
        jpaDao.delete(StringUtils.format("delete from SysUserGroup where groupId in {}", groupIds), paras);
    }

    @Override
    @Transactional
    public void insertGroup(SysGroup group) {
        if (this.checkGroupCodeUnique(group)) {
            throw new BaseException(37901, new Object[] { group.getGroupName() });
        }
        jpaDao.save(group);
        this.insertUserGroupManager(group.getId(), group.getCreateBy());
    }
    
    /**
     * -添加群主
     * @param group
     */
    @Transactional
    public void insertUserGroupManager(String groupId, String loginName) {
        SysUser user = jpaDao.find("from SysUser where loginName = ?", loginName, SysUser.class);
        if(user != null) {
            SysUserGroup userGroup = new SysUserGroup();
            userGroup.setGroupId(groupId);
            userGroup.setUserId(user.getId());
            userGroup.setLoginName(user.getLoginName());
            userGroup.setUserName(user.getUserName());
            userGroup.setStatus(DataStatusEnum.NORMAL.code());
            userGroup.setUserPerm(1);
            userGroup.setCreateTime(new Date());
            jpaDao.save(userGroup);
        }
    }

    @Override
    @Transactional
    public void updateGroup(SysGroup group) {
        if (this.checkGroupCodeUnique(group)) {
            throw new BaseException(37902, new Object[] { group.getGroupName() });
        }
        jpaDao.update(group);
    }

    private boolean checkGroupCodeUnique(SysGroup group){
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysGroup where groupCode = ? ";
        paras.add(group.getGroupCode());
        if(StringUtils.isNotEmpty(group.getId())){
            hql += "and id != ? ";
            paras.add(group.getId());
        }
        SysGroup sysGroup = jpaDao.find(hql, paras, SysGroup.class);
        return null != sysGroup;
    }

    @Override
    public List<SysUserGroup> selectUserGroupList(Query<SysUserGroup> query) {
        StringBuffer hql = new StringBuffer("from SysUserGroup where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        return jpaDao.search(hql.toString(), paras, SysUserGroup.class, query.getPage());
    }

    @Override
    @Transactional
    public void insertUserGroup(String groupId, String[] userIds) {
        if(StringUtils.isEmpty(groupId) || StringUtils.isEmpty(userIds)) {
            throw new BaseException(37903);
        }
        for(String userId : userIds) {
            List<Object> paras = new ArrayList<Object>();
            paras.add(groupId);
            paras.add(userId);
            SysUserGroup userGroup = jpaDao.find("from SysUserGroup where groupId = ? and userId = ?", paras, SysUserGroup.class);
            if(userGroup != null) {
                userGroup.setStatus(DataStatusEnum.NORMAL.code());
                userGroup.setCreateTime(new Date());
                jpaDao.update(userGroup);
            }else {
                SysUser user = jpaDao.find(SysUser.class, userId);
                if(null != user) {
                    SysUserGroup group = new SysUserGroup();
                    group.setGroupId(groupId);
                    group.setUserId(user.getId());
                    group.setLoginName(user.getLoginName());
                    group.setUserName(user.getUserName());
                    group.setStatus(DataStatusEnum.NORMAL.code());
                    group.setUserPerm(3);
                    group.setCreateTime(new Date());
                    jpaDao.save(group);
                }
            }
        }
    }

    @Override
    @Transactional
    public void updateUserGroup(SysUserGroup group) {
        jpaDao.update(group);
    }

    @Override
    @Transactional
    public void deleteUserGroupByIds(String[] ids) {
        List<Object> paras = new ArrayList<Object>();
        String idsx = HqlUtils.createHql(paras, ids);
        jpaDao.update(StringUtils.format("update SysUserGroup set status = 1 where id in {}", idsx), paras);
    }
}
