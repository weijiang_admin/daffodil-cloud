package com.daffodil.system.service.impl;

import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.core.util.HqlUtils;
import com.daffodil.system.entity.SysHoliday;
import com.daffodil.system.entity.SysHoliday.HolidayType;
import com.daffodil.system.service.ISysDaysService;
import com.daffodil.system.service.ISysHolidayService;
import com.daffodil.util.StringUtils;

import cn.hutool.core.date.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * -节假日管理
 * @author EP
 * @date 2023年1月16日
 * @version 1.0.0
 * @description
 */
@Service
public class SysHolidayServiceImpl implements ISysHolidayService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Autowired
    private ISysDaysService daysService;

    @Override
    public List<SysHoliday> selectHolidayList(Query<SysHoliday> query) {
        List<Object> paras = new ArrayList<Object>();
        StringBuffer hql = new StringBuffer("from SysHoliday a where 1=1 ");
        HqlUtils.createHql(hql, paras, query, "a");
        hql.append("order by a.date asc");
        List<SysHoliday> list = jpaDao.search(hql.toString(), paras, SysHoliday.class, query.getPage());
        return this.builderHolidayDataList(query.getEntity(), list);
    }

    @Override
    @Transactional
    public void setHoliday(SysHoliday holiday) {
        this.builder(holiday);
        SysHoliday curDay = jpaDao.find("from SysHoliday where date = ?", holiday.getDate(), SysHoliday.class);
        if(null == curDay) {
            jpaDao.save(holiday);
        }else {
            holiday.setId(curDay.getId());
            jpaDao.update(holiday);
        }
        daysService.updateDaysByDate(holiday.getDate(), holiday.getWorkDay());

    }

    @Override
    @Transactional
    public void deleteHolidayByIds(String[] ids) {
        for (String id : ids) {
            jpaDao.delete(SysHoliday.class, id);
        }
    }

    /**
     * 保存数据前，初始化实体基础字段
     * @param holiday
     */
    private void builder(SysHoliday holiday) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(holiday.getDate());
        holiday.setYear(calendar.get(Calendar.YEAR));
        holiday.setMonth(calendar.get(Calendar.MONTH) + 1);
        holiday.setWeek(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY ? 7 : calendar.get(Calendar.DAY_OF_WEEK) - 1);
        if(StringUtils.isNull(holiday.getHolidayType())) {
            holiday.setHolidayType(HolidayType.NORMAL.value());
        }
        //判断是否是周末
        List<Integer> weekEnd = Arrays.asList(6, 7);
        holiday.setWeekend(weekEnd.contains(holiday.getWeek()));
        //判断是否是工作日
        holiday.setWorkDay(!weekEnd.contains(holiday.getWeek()));
        if(SysHoliday.HolidayType.OVERTIME.value().equals(holiday.getHolidayType())) {
            holiday.setWorkDay(true);
        }else if(SysHoliday.HolidayType.HOLIDAY.value().equals(holiday.getHolidayType())) {
            holiday.setWorkDay(false);
        }
    }

    /**
     * 组装包括非数据库配置日期在内的日期列表数据
     * */
    private List<SysHoliday> builderHolidayDataList(SysHoliday holiday, List<SysHoliday> data) {
        List<SysHoliday> resultList = new ArrayList<>();
        if (StringUtils.isNotEmpty(data)) {
            resultList.addAll(data);
        }
        //非配置日期也返回
        if (StringUtils.isNotNull(holiday.getDate())) {
            if (!StringUtils.isNotEmpty(data)) {
                this.builder(holiday);
                resultList.add(holiday);
            }
        }
        if(StringUtils.isNotNull(holiday.getYear()) && StringUtils.isNotNull(holiday.getMonth())) {
            this.filterDays(holiday, resultList);
        }
        return resultList;
    }

    /**
     * 根据年份和月份遍历组装数据，并去重、过滤、排序
     * @param holiday
     * @param resultList
     * @return
     */
    private List<SysHoliday> filterDays(SysHoliday holiday, List<SysHoliday> resultList) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, holiday.getYear());
        c.set(Calendar.MONTH, holiday.getMonth() - 1);
        c.set(Calendar.DATE, 1);
        c.roll(Calendar.DATE, -1);
        int dayNumOfMonth = c.get(Calendar.DATE);
        c.set(Calendar.DAY_OF_MONTH, 1);

        List<String> dateList = resultList.stream().map(SysHoliday::getDate)
                .map(date -> DateUtil.format(date, "yyyy-MM-dd"))
                .collect(Collectors.toList());

        for (int i = 0; i < dayNumOfMonth; i++, c.add(Calendar.DATE, 1)) {
            //数据库已有配置，去重
            if(dateList.contains(DateUtil.format(c.getTime(), "yyyy-MM-dd"))){
                continue;
            }

            //筛选星期
            Integer week = c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY ? 7 : c.get(Calendar.DAY_OF_WEEK) - 1;
            if(StringUtils.isNotNull(holiday.getWeek()) && !week.equals(holiday.getWeek())) {
                continue;
            }
            //筛选工作日
            List<Integer> weekEnd = Arrays.asList(6, 7);
            if(StringUtils.isNotNull(holiday.getWorkDay())){
                if((holiday.getWorkDay() && weekEnd.contains(week))
                        || (!holiday.getWorkDay() && !weekEnd.contains(week))) {
                    continue;
                }
            }
            //筛选周末
            if(StringUtils.isNotNull(holiday.getWeekend())){
                if((!holiday.getWeekend() && weekEnd.contains(week))
                        || (holiday.getWeekend() && !weekEnd.contains(week))) {
                    continue;
                }
            }

            SysHoliday sysHoliday = new SysHoliday();
            sysHoliday.setDate(c.getTime());
            this.builder(sysHoliday);
            resultList.add(sysHoliday);
        }
        return resultList.stream().sorted(Comparator.comparing(SysHoliday::getDate)).collect(Collectors.toList());
    }
}
