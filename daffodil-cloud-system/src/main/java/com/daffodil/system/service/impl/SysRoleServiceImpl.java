package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.core.util.HqlUtils;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysBusinessScope;
import com.daffodil.system.entity.SysDept;
import com.daffodil.system.entity.SysMenu;
import com.daffodil.system.entity.SysRole;
import com.daffodil.system.event.SysRoleEvent;
import com.daffodil.system.event.publisher.SysRolePublisherService;
import com.daffodil.system.service.ISysRoleService;
import com.daffodil.util.StringUtils;

/**
 * 角色 业务层处理
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Service
public class SysRoleServiceImpl implements ISysRoleService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Autowired
    private SysRolePublisherService rolePublisherService;

    @Override
    public List<SysRole> selectRoleList(Query<SysRole> query) {
        StringBuffer hql = new StringBuffer("from SysRole where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        return jpaDao.search(hql.toString(), paras, SysRole.class, query.getPage());
    }

    @Override
    public SysRole selectRoleById(String roleId) {
        SysRole role = jpaDao.find(SysRole.class, roleId);

        List<SysMenu> menus = jpaDao.search("select m from SysMenu m,SysRoleMenu rm where m.id = rm.menuId and rm.roleId = ?", roleId, SysMenu.class);
        List<String> menuIds = menus.stream().map(SysMenu::getId).collect(Collectors.toList());
        role.setMenuIds(menuIds.toArray(new String[menuIds.size()]));

        List<SysDept> depts = jpaDao.search("select d from SysDept d,SysRoleDept rd where d.id = rd.deptId and rd.roleId = ?", roleId, SysDept.class);
        List<String> deptIds = depts.stream().map(SysDept::getId).collect(Collectors.toList());
        role.setDeptIds(deptIds.toArray(new String[deptIds.size()]));

        List<SysBusinessScope> businessScopes = jpaDao.search("select b from SysBusinessScope b where b.roleId = ? ", role.getId(), SysBusinessScope.class);
        businessScopes.stream().forEach(businessScope -> {
            List<Object> paras = new ArrayList<Object>();
            paras.add(businessScope.getBusiness());
            paras.add(businessScope.getRoleId());
            List<String> scopeDeptIds = jpaDao.search("select d.deptId from SysBusinessScopeDept d where d.business = ? and d.roleId = ? ", paras, String.class);
            businessScope.setDeptIds(scopeDeptIds.toArray(new String[scopeDeptIds.size()]));
        });
        role.setBusinessScopes(businessScopes);

        return role;
    }

    @Override
    @Transactional
    public void deleteRoleByIds(String[] ids) {
        String tenantId = TenantContextHolder.getTenantHolder();
        for (String roleId : ids) {
            int count = jpaDao.count("from SysUserRole where roleId = ? ", roleId);
            if (count > 0) {
                throw new BaseException(37205);
            }
            SysRole role = jpaDao.find(SysRole.class, roleId);
            if(role != null) {
                jpaDao.delete(role);
                rolePublisherService.publish(role, tenantId, SysRoleEvent.Operation.DELETE.name());
            }
        }
    }

    @Override
    @Transactional
    public void insertRole(SysRole role) {
        if (this.checkRoleKeyUnique(role)) {
            throw new BaseException(37202, new Object[] { role.getRoleName() });
        }
        jpaDao.save(role);
        rolePublisherService.publish(role, TenantContextHolder.getTenantHolder(), SysRoleEvent.Operation.INSERT.name());
    }

    @Override
    @Transactional
    public void updateRole(SysRole role) {
        if (this.checkRoleKeyUnique(role)) {
            throw new BaseException(37204, new Object[] { role.getRoleName() });
        }
        jpaDao.update(role);
        rolePublisherService.publish(role, TenantContextHolder.getTenantHolder(), SysRoleEvent.Operation.UPDATE.name());
    }

    /**
     * -检查角色编码是否唯一
     * @param role
     * @return
     */
    private boolean checkRoleKeyUnique(SysRole role) {
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysRole where roleKey = ? ";
        paras.add(role.getRoleKey());
        if(StringUtils.isNotEmpty(role.getId())){
            hql += "and id != ? ";
            paras.add(role.getId());
        }
        SysRole sysRole = jpaDao.find(hql, paras, SysRole.class);
        return null != sysRole;
    }

}
