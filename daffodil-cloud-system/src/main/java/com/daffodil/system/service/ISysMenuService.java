package com.daffodil.system.service;

import java.util.List;

import com.daffodil.system.entity.SysMenu;

/**
 * 菜单 业务层服务
 * @author yweijian
 * @date 2021年9月24日
 * @version 1.0
 * @description
 */
public interface ISysMenuService {

    /**
     * 查询系统菜单所有信息列表
     * @param menu
     * @param user
     * @return
     */
    public List<SysMenu> selectMenuList(SysMenu menu);
    

    /**
     * 新增保存菜单信息
     * @param menu
     */
    public void insertMenu(SysMenu menu);

    /**
     * 修改保存菜单信息
     * @param menu
     */
    public void updateMenu(SysMenu menu);
    
    /**
     * 删除菜单管理信息
     * @param ids
     */
    public void deleteMenuByIds(String[] ids);

    /**
     * 根据菜单ID查询信息
     * @param menuId
     * @return
     */
    public SysMenu selectMenuById(String menuId);

}
