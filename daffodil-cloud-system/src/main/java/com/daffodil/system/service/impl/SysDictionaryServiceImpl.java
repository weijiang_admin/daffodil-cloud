package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.util.HqlUtils;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysDictionary;
import com.daffodil.system.service.ISysDictionaryService;
import com.daffodil.util.StringUtils;

import cn.hutool.core.collection.CollectionUtil;

/**
 * 
 * @author yweijian
 * @date 2021年6月1日
 * @version 1.0
 * @description
 */
@Service
public class SysDictionaryServiceImpl implements ISysDictionaryService{

    @Autowired
    private JpaDao<String> jpaDao;

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    @Override
    public List<SysDictionary> selectDictionaryList(Query<SysDictionary> query) {
        StringBuffer hql = new StringBuffer();
        List<Object> paras = new ArrayList<Object>();
        hql.append("from SysDictionary d where 1=1 ");
        HqlUtils.createHql(hql, paras, query, "d");
        hql.append("order by d.orderNum asc, d.ancestors asc");
        return jpaDao.search(hql.toString(), paras, SysDictionary.class);
    }

    @Override
    @Transactional
    public void deleteDictionaryByIds(String[] ids) {
        for(String dictId : ids) {
            List<SysDictionary> list = jpaDao.search("from SysDictionary where parentId = ?", dictId, SysDictionary.class);
            if (list != null && list.size() > 0) {
                throw new BaseException(37504);
            }
            jpaDao.delete(SysDictionary.class, dictId);
        }
    }

    @Override
    public SysDictionary selectDictionaryById(String dictId) {
        if(CommonConstant.ROOT.equals(dictId)){
            SysDictionary dictionary = new SysDictionary();
            dictionary.setId(CommonConstant.ROOT);
            dictionary.setDictName("");
            dictionary.setAncestors("");
            return dictionary;
        }

        return jpaDao.find(SysDictionary.class, dictId);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<SysDictionary> selectDictionaryByLabel(String dictLabel) {
        String cacheKey = StringUtils.format(CommonConstant.DICTIONARY_DATA_CACHEKEY, dictLabel);
        Object object = this.tenantRedisTemplate.opsForValue().get(cacheKey);
        if(null != object) {
            return (List<SysDictionary>) object;
        }
        String hql = "from SysDictionary where parentId in (select id from SysDictionary where dictLabel = ?) order by orderNum asc";
        List<SysDictionary> list = jpaDao.search(hql, dictLabel, SysDictionary.class);
        if(CollectionUtil.isNotEmpty(list)) {
            this.tenantRedisTemplate.opsForValue().set(cacheKey, list);
        }
        return list;
    }

    @Override
    @Transactional
    public void insertDictionary(SysDictionary dictionary) {
        if (StringUtils.isNotEmpty(dictionary.getDictLabel()) && this.checkDictionaryLabelUnique(dictionary)) {
            throw new BaseException(37501, new Object[] { dictionary.getDictName(), dictionary.getDictLabel()});
        }
        if(CommonConstant.ROOT.equals(dictionary.getParentId())){
            dictionary.setAncestors(CommonConstant.ROOT);
        }else{
            SysDictionary parent = jpaDao.find(SysDictionary.class, dictionary.getParentId());
            if(parent != null){
                dictionary.setAncestors(parent.getAncestors() + "," + parent.getId());
            }
        }
        jpaDao.save(dictionary);
        String cacheKey = StringUtils.format(CommonConstant.DICTIONARY_DATA_CACHEKEY, dictionary.getDictLabel());
        this.tenantRedisTemplate.delete(cacheKey);
    }

    @Override
    @Transactional
    public void updateDictionary(SysDictionary dictionary) {
        if (StringUtils.isNotEmpty(dictionary.getDictLabel()) && this.checkDictionaryLabelUnique(dictionary)) {
            throw new BaseException(37502, new Object[] { dictionary.getDictName(), dictionary.getDictLabel()});
        }
        if(this.checkDictionaryIsSelfOrChildren(dictionary)){
            throw new BaseException(37503, new Object[] { dictionary.getDictName()});
        }
        SysDictionary sysDictionary = this.selectDictionaryById(dictionary.getId());
        SysDictionary parent = this.selectDictionaryById(dictionary.getParentId());
        if (StringUtils.isNotNull(parent) && StringUtils.isNotNull(sysDictionary)) {
            String newAncestors = parent.getAncestors() + "," + parent.getId();
            String oldAncestors = sysDictionary.getAncestors();
            dictionary.setAncestors(newAncestors);
            this.updateChildrenDictionary(dictionary, newAncestors, oldAncestors);
        }
        jpaDao.update(dictionary);
        String cacheKey = StringUtils.format(CommonConstant.DICTIONARY_DATA_CACHEKEY, dictionary.getDictLabel());
        this.tenantRedisTemplate.delete(cacheKey);
    }

    private boolean checkDictionaryLabelUnique(SysDictionary dictionary) {
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysDictionary where dictLabel = ? and parentId = ? ";
        paras.add(dictionary.getDictLabel());
        paras.add(dictionary.getParentId());
        if(StringUtils.isNotEmpty(dictionary.getId())){
            hql += "and id != ?";
            paras.add(dictionary.getId());
        }
        SysDictionary sysDictionary = jpaDao.find(hql, paras, SysDictionary.class);
        return null != sysDictionary;
    }

    private boolean checkDictionaryIsSelfOrChildren(SysDictionary dictionary){
        List<Object> paras = new ArrayList<Object>();
        String hql = "from SysDictionary where id = ? or ancestors like ?";
        paras.add(dictionary.getId());
        paras.add("%" + dictionary.getId() + "%");
        List<SysDictionary> dictionaries = jpaDao.search(hql, paras, SysDictionary.class);
        for(SysDictionary sysDictionary : dictionaries){
            if(dictionary.getParentId().equals(sysDictionary.getId())){
                return true;
            }
        }
        return false;
    }

    @Transactional
    public void updateChildrenDictionary(SysDictionary dictionary, String newAncestors, String oldAncestors){
        List<Object> paras = new ArrayList<Object>();
        paras.add(oldAncestors);
        paras.add(newAncestors);
        paras.add(dictionary.getStatus());
        paras.add("%" + dictionary.getId() + "%");
        jpaDao.update("update SysDictionary set ancestors = replace(ancestors, ?, ?), status = ? where ancestors like ?", paras);
        String cacheKey = StringUtils.format(CommonConstant.DICTIONARY_DATA_CACHEKEY, dictionary.getDictLabel());
        this.tenantRedisTemplate.delete(cacheKey);
    }

}
