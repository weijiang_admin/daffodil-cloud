package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysGroup;
import com.daffodil.system.entity.user.SysUserGroup;

/**
 * -群组信息
 * @author yweijian
 * @date 2022年6月21日
 * @version 2.0.0
 * @description
 */
public interface ISysGroupService {
    /**
     * -分页查询群组信息集合
     * @param query
     * @return
     */
    public List<SysGroup> selectGroupList(Query<SysGroup> query);

    /**
     * -通过群组ID查询群组信息
     * @param groupId
     * @return
     */
    public SysGroup selectGroupById(String groupId);

    /**
     * -批量删除群组信息
     * @param ids
     */
    public void deleteGroupByIds(String[] ids);

    /**
     * -新增保存群组信息
     * @param group
     */
    public void insertGroup(SysGroup group);

    /**
     * -修改保存群组信息
     * @param group
     */
    public void updateGroup(SysGroup group);

    /**
     * -分页查询群组用户信息集合
     * @param query
     * @return
     */
    public List<SysUserGroup> selectUserGroupList(Query<SysUserGroup> query);

    /**
     * -新增保存群组用户信息
     * @param group
     */
    public void insertUserGroup(String groupId, String[] userIds);

    /**
     * -修改保存群组用户信息
     * @param group
     */
    public void updateUserGroup(SysUserGroup group);

    /**
     * -批量删除群组用户信息
     * @param ids
     */
    public void deleteUserGroupByIds(String[] ids);

}
