package com.daffodil.system.service;

import java.util.List;
import java.util.Map;

import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.entity.SysLoginInfo;
import com.daffodil.system.entity.SysOperLog;
import com.daffodil.system.entity.SysThirdApp;

/**
 * 首页业务
 * @author yweijian
 * @date 2022年2月17日
 * @version 1.0
 * @description
 */
public interface ISysHomeService {

    /**
     * 根据登录用户查询登录总数
     * @param loginInfo
     * @return
     */
    public int countLoginInfo(SysLoginInfo loginInfo);
    
    /**
     * 根据登录用户查询操作总数
     * @param operLog
     * @return
     */
    public int countOperLog(SysOperLog operLog);
    
    /**
     * 根据登录用户查询每月登录总数
     * @param loginInfo
     * @return
     */
    public List<Integer> countLoginInfoPerMonth(SysLoginInfo loginInfo);
    
    /**
     * 根据登录用户查询每月操作总数
     * @param loginInfo
     * @return
     */
    public List<Integer> countOperLogPerMonth(SysOperLog operLog);
    
    /**
     * 查询所有用户使用浏览器的分布情况
     * @return
     */
    public List<Map<String, String>> selectBrowserLoginInfo(String loginName);
    
    /**
     * 查询登录用户使用操作的分布情况
     * @return
     */
    public List<Map<String, String>> selectBusinessLabelOperLog(String loginName);

    /**
     * 获取登录用户的第三方应用
     * @param loginName
     * @return
     */
    public List<SysThirdApp> selectUserThirdAppList(LoginUser loginUser);
}
