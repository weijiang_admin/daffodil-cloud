package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysArea;

/**
 * -中国行政区划管理
 * @author yweijian
 * @date 2022年6月14日
 * @version 2.0.0
 * @description
 */
public interface ISysAreaService {
    
    /**
     * -根据查询条件查询区划
     * @param query
     * @return
     */
    public List<SysArea> selectAreaList(Query<SysArea> query);

    /**
     * -新增保存区划信息
     * @param area
     */
    public void insertArea(SysArea area);

    /**
     * -修改保存区划信息
     * @param area
     */
    public void updateArea(SysArea area);

    /**
     * -删除区划管理信息
     * @param ids
     */
    public void deleteAreaByIds(String[] ids);
    
    /**
     * -根据区划ID查询信息
     * @param areaId
     * @return
     */
    public SysArea selectAreaById(String areaId);
    
    /**
     * -根据父级ID查询条件查询区划
     * @param parentId
     * @return
     */
    public List<SysArea> selectAreaListByParentId(String parentId);

}
