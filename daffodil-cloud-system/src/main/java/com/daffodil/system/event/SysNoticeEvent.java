package com.daffodil.system.event;

import org.springframework.context.ApplicationEvent;

/**
 * -系统消息通知事件
 * @author yweijian
 * @date 2022年6月21日
 * @version 2.0.0
 * @description
 */
public class SysNoticeEvent extends ApplicationEvent {

    private static final long serialVersionUID = 6201171555188642164L;

    /** 租户ID */
    private String tenantId;

    public SysNoticeEvent(Object source, String tenantId) {
        super(source);
        this.tenantId = tenantId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

}
