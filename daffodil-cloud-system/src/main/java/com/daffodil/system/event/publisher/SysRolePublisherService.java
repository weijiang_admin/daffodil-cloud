package com.daffodil.system.event.publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.daffodil.system.entity.SysRole;
import com.daffodil.system.event.SysRoleEvent;

/**
 * 
 * @author yweijian
 * @date 2022年6月23日
 * @version 2.0.0
 * @description
 */
@Component
public class SysRolePublisherService {

    @Autowired
    ApplicationEventPublisher publisher;
    
    public void publish(SysRole role, String tenantId, String operation) {
        SysRoleEvent event = new SysRoleEvent(role, tenantId, operation);
        publisher.publishEvent(event);
    }
}
