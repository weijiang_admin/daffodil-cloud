package com.daffodil.system.event.publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.daffodil.system.entity.SysThirdApp;
import com.daffodil.system.event.SysThirdAppEvent;

/**
 * 
 * @author yweijian
 * @date 2023年1月3日
 * @version 2.0.0
 * @description
 */
@Component
public class SysThirdAppPublisherService {

    @Autowired
    ApplicationEventPublisher publisher;
    
    public void publish(SysThirdApp app, String tenantId, String operation) {
        SysThirdAppEvent event = new SysThirdAppEvent(app, tenantId, operation);
        publisher.publishEvent(event);
    }
}
