package com.daffodil.system.event.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import com.daffodil.core.dao.JpaDao;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.system.entity.SysThirdApp;
import com.daffodil.system.entity.SysThirdAppResource;
import com.daffodil.system.entity.SysThirdAppRole;
import com.daffodil.system.entity.SysThirdAppScope;
import com.daffodil.system.entity.SysThirdAppScope.ThirdAppScope;
import com.daffodil.system.event.SysThirdAppEvent;
import com.daffodil.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author yweijian
 * @date 2023年1月3日
 * @version 2.0.0
 * @description
 */
@Slf4j
@Service
public class SysThirdAppListener {

    @Autowired
    private JpaDao<String> jpaDao;

    @Async("systemEventExecutor")
    @Transactional
    @TransactionalEventListener(classes = SysThirdAppEvent.class, phase = TransactionPhase.AFTER_COMMIT, condition = "#event.operation == 'INSERT'")
    public void insertThirdAppListener(SysThirdAppEvent event) {
        TenantContextHolder.apply(event.getTenantId(), () -> {
            SysThirdApp app = (SysThirdApp) event.getSource();
            //绑定第三方应用授权信息范围
            this.insertThirdAppScope(app);
            //绑定第三方应用授权接口管理
            this.insertThirdAppResource(app);
            //绑定第三方应用授权角色管理
            this.insertThirdAppRole(app);
        });
    }

    @Async("systemEventExecutor")
    @Transactional
    @TransactionalEventListener(classes = SysThirdAppEvent.class, phase = TransactionPhase.AFTER_COMMIT, condition = "#event.operation == 'UPDATE'")
    public void updateThirdAppListener(SysThirdAppEvent event) {
        TenantContextHolder.apply(event.getTenantId(), () -> {
            SysThirdApp app = (SysThirdApp) event.getSource();
            //解绑第三方应用授权权限范围
            this.deleteThirdAppScope(app);
            //绑定第三方应用授权权限范围
            this.insertThirdAppScope(app);

            //解绑第三方应用授权接口管理
            this.deleteThirdAppResource(app);
            //绑定第三方应用授权接口管理
            this.insertThirdAppResource(app);

            //解绑第三方应用授权角色管理
            this.deleteThirdAppRole(app);
            //绑定第三方应用授权角色管理
            this.insertThirdAppRole(app);
        });
    }

    @Async("systemEventExecutor")
    @Transactional
    @TransactionalEventListener(classes = SysThirdAppEvent.class, phase = TransactionPhase.AFTER_COMMIT, condition = "#event.operation == 'DELETE'")
    public void deleteThirdAppListener(SysThirdAppEvent event) {
        TenantContextHolder.apply(event.getTenantId(), () -> {
            SysThirdApp app = (SysThirdApp) event.getSource();
            //解绑第三方应用授权权限范围
            this.deleteThirdAppScope(app);
            //解绑第三方应用授权接口管理
            this.deleteThirdAppResource(app);
            //解绑第三方应用授权角色管理
            this.deleteThirdAppRole(app);
        });
    }

    @Transactional
    public void deleteThirdAppScope(SysThirdApp app) {
        jpaDao.delete("delete from SysThirdAppScope where appId = ? ", app.getId());
    }

    @Transactional
    public void insertThirdAppScope(SysThirdApp app) {
        String[] scopes = app.getScopes();
        if(StringUtils.isNotEmpty(scopes)) {
            for (String scope : scopes) {
                ThirdAppScope value = this.checkThirdAppScope(scope);
                if(null != value) {
                    SysThirdAppScope appScope = new SysThirdAppScope();
                    appScope.setAppId(app.getId());
                    appScope.setScope(value.name());
                    jpaDao.save(appScope);
                }
            }
        }else {//默认授权权限范围用户基本信息
            SysThirdAppScope scope = new SysThirdAppScope();
            scope.setAppId(app.getId());
            scope.setScope(ThirdAppScope.USER.name());
            jpaDao.save(scope);
        }
    }

    @Transactional
    public void deleteThirdAppResource(SysThirdApp app) {
        jpaDao.delete("delete from SysThirdAppResource where appId = ? ", app.getId());
    }

    @Transactional
    public void insertThirdAppResource(SysThirdApp app) {
        String[] resourceIds = app.getResourceIds();
        if(StringUtils.isNotEmpty(resourceIds)) {
            for (String resourceId : resourceIds) {
                SysThirdAppResource appResource = new SysThirdAppResource();
                appResource.setAppId(app.getId());
                appResource.setResourceId(resourceId);
                jpaDao.save(appResource);
            }
        }
    }

    @Transactional
    public void insertThirdAppRole(SysThirdApp app) {
        String[] roleIds = app.getRoleIds();
        if(StringUtils.isNotEmpty(roleIds)) {
            for (String roleId : roleIds) {
                SysThirdAppRole appRole = new SysThirdAppRole();
                appRole.setAppId(app.getId());
                appRole.setRoleId(roleId);
                jpaDao.save(appRole);
            }
        }
    }

    @Transactional
    public void deleteThirdAppRole(SysThirdApp app) {
        jpaDao.delete("delete from SysThirdAppRole where appId = ? ", app.getId());
    }

    /**
     * -检查授权权限标识码是否正确
     * @param scope
     * @return
     */
    private ThirdAppScope checkThirdAppScope(String scope) {
        if(StringUtils.isEmpty(scope)) {
            return null;
        }
        ThirdAppScope value = null;
        try {
            value = ThirdAppScope.valueOf(scope.toUpperCase());
        }catch (Exception e) {
            if(log.isWarnEnabled()) {
                log.warn(e.getMessage());
            }
        }
        return value;
    }
}
