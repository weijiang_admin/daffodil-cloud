package com.daffodil.system.event.listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import com.daffodil.core.util.HqlUtils;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.system.entity.SysBusinessScope;
import com.daffodil.system.entity.SysBusinessScopeDept;
import com.daffodil.system.entity.SysRole;
import com.daffodil.system.entity.SysRoleDept;
import com.daffodil.system.entity.SysRoleMenu;
import com.daffodil.system.enums.DataScopeEnum;
import com.daffodil.system.event.SysRoleEvent;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2022年6月21日
 * @version 2.0.0
 * @description
 */
@Service
public class SysRoleListener {

    @Autowired
    private JpaDao<String> jpaDao;

    @Async("systemEventExecutor")
    @Transactional
    @TransactionalEventListener(classes = SysRoleEvent.class, phase = TransactionPhase.AFTER_COMMIT, condition = "#event.operation == 'INSERT'")
    public void insertRoleListener(SysRoleEvent event) {
        TenantContextHolder.apply(event.getTenantId(), () -> {
            SysRole role = (SysRole) event.getSource();

            if(StringUtils.isNotEmpty(role.getMenuIds())){
                // 新增角色与菜单管理
                for (String menuId : role.getMenuIds()) {
                    SysRoleMenu sysRoleMenu = new SysRoleMenu();
                    sysRoleMenu.setRoleId(role.getId());
                    sysRoleMenu.setMenuId(menuId);
                    jpaDao.save(sysRoleMenu);
                }
            }

            if(StringUtils.isNotEmpty(role.getDeptIds())) {
                // 新增角色和部门信息 数据权限
                for (String deptId : role.getDeptIds()) {
                    SysRoleDept sysRoleDept = new SysRoleDept();
                    sysRoleDept.setRoleId(role.getId());
                    sysRoleDept.setDeptId(deptId);
                    jpaDao.save(sysRoleDept);
                }
            }

            if(StringUtils.isNotEmpty(role.getBusinessScopes())) {
                // 新增角色和业务数据权限
                for (SysBusinessScope scope : role.getBusinessScopes()) {
                    scope.setRoleId(role.getId());
                    jpaDao.save(scope);

                    if(StringUtils.isNotEmpty(scope.getDeptIds())) {
                        // 新增业务和部门信息  数据权限
                        for (String deptId : scope.getDeptIds()) {
                            SysBusinessScopeDept scopeDept = new SysBusinessScopeDept();
                            scopeDept.setDeptId(deptId);
                            scopeDept.setRoleId(role.getId());
                            scopeDept.setBusiness(scope.getBusiness());
                            jpaDao.save(scopeDept);
                        }
                    }
                }
            }
        });
    }

    @Async("systemEventExecutor")
    @Transactional
    @TransactionalEventListener(classes = SysRoleEvent.class, phase = TransactionPhase.AFTER_COMMIT, condition = "#event.operation == 'UPDATE'")
    public void updateRoleListener(SysRoleEvent event) {
        TenantContextHolder.apply(event.getTenantId(), () -> {
            SysRole role = (SysRole) event.getSource();
            this.updateRoleMenu(role);
            this.updateRoleDept(role);
            this.updateBusinessScope(role);
        });
    }

    @Async("systemEventExecutor")
    @Transactional
    @TransactionalEventListener(classes = SysRoleEvent.class, phase = TransactionPhase.AFTER_COMMIT, condition = "#event.operation == 'DELETE'")
    public void deleteRoleListener(SysRoleEvent event) {
        TenantContextHolder.apply(event.getTenantId(), () -> {
            SysRole role = (SysRole) event.getSource();
            // 删除角色与部门关联
            jpaDao.delete("delete from SysRoleDept where roleId = ?", role.getId());
            // 删除角色与菜单关联
            jpaDao.delete("delete from SysRoleMenu where roleId = ?", role.getId());
            // 删除角色与业务关联
            jpaDao.delete("delete from SysBusinessScope where roleId = ?", role.getId());
            // 删除角色业务与部门关联
            jpaDao.delete("delete from SysBusinessScopeDept where roleId = ?", role.getId());
        });
    }

    /**
     * -更新角色和菜单关联关系
     * @param role
     */
    @Transactional
    public void updateRoleMenu(SysRole role) {
        if(StringUtils.isNotNull(role.getMenuIds())) {
            List<String> mids = Arrays.asList(role.getMenuIds());
            List<String> nids = this.selectRoleMenusByRoleId(role.getId());
            // 需要新增的菜单id
            List<String> mnids = mids.stream().filter(item -> !nids.contains(item)).collect(Collectors.toList());
            for (String menuId : mnids) {
                SysRoleMenu sysRoleMenu = new SysRoleMenu();
                sysRoleMenu.setRoleId(role.getId());
                sysRoleMenu.setMenuId(menuId);
                jpaDao.save(sysRoleMenu);
            }

            // 需要删除的菜单id
            List<String> nmids = nids.stream().filter(item -> !mids.contains(item)).collect(Collectors.toList());
            if(StringUtils.isNotEmpty(nmids)) {
                List<Object> paras = new LinkedList<Object>();
                paras.add(role.getId());
                String in = HqlUtils.createHql(paras, nmids.toArray());
                jpaDao.delete(StringUtils.format("delete from SysRoleMenu where roleId = ? and menuId in {}", in), paras);
            }
        }
    }

    /**
     * -更新角色和部门关联关系
     * @param role
     */
    @Transactional
    public void updateRoleDept(SysRole role) {
        //非自定义数据权限
        if(!DataScopeEnum.DATA_SCOPE_CUSTOM.code().equals(role.getDataScope())) {
            jpaDao.delete("delete from SysRoleDept where roleId = ?", role.getId());
        }else {
            List<String> mids = StringUtils.isNotEmpty(role.getDeptIds()) ? Arrays.asList(role.getDeptIds()) : new ArrayList<String>();
            List<String> nids = this.selectRoleDeptsByRoleId(role.getId());
            // 需要新增的部门id
            List<String> mnids = mids.stream().filter(item -> !nids.contains(item)).collect(Collectors.toList());
            for (String deptId : mnids) {
                SysRoleDept sysRoleDept = new SysRoleDept();
                sysRoleDept.setRoleId(role.getId());
                sysRoleDept.setDeptId(deptId);
                jpaDao.save(sysRoleDept);
            }

            // 需要删除的部门id
            List<String> nmids = nids.stream().filter(item -> !mids.contains(item)).collect(Collectors.toList());
            if(StringUtils.isNotEmpty(nmids)) {
                List<Object> paras = new ArrayList<Object>();
                paras.add(role.getId());
                String in = HqlUtils.createHql(paras, nmids.toArray());
                jpaDao.delete(StringUtils.format("delete from SysRoleDept where roleId = ? and deptId in {}", in), paras);
            }
        }
    }

    @Transactional
    public void updateBusinessScope(SysRole role) {
        List<SysBusinessScope> scopes = role.getBusinessScopes();
        if(StringUtils.isEmpty(scopes)) {
            // 删除角色与业务关联
            jpaDao.delete("delete from SysBusinessScope where roleId = ?", role.getId());
            // 删除角色业务与部门关联
            jpaDao.delete("delete from SysBusinessScopeDept where roleId = ?", role.getId());
            return;
        }
        List<String> mbusis = scopes.stream().map(SysBusinessScope::getBusiness).collect(Collectors.toList());
        List<String> nbusis = this.selectBusinessScopeByRoleId(role.getId());


        for (SysBusinessScope scope : scopes) {
            List<Object> paras = new ArrayList<Object>();
            paras.add(role.getId());
            paras.add(scope.getBusiness());
            SysBusinessScope entity = jpaDao.find("from SysBusinessScope where roleId = ? and business = ?", paras, SysBusinessScope.class);
            if(entity == null) {
                entity = new SysBusinessScope();
                entity.setRoleId(role.getId());
                entity.setBusiness(scope.getBusiness());
                entity.setDataScope(scope.getDataScope());
                jpaDao.save(entity);
            }else {
                entity.setDataScope(scope.getDataScope());
                jpaDao.update(entity);
            }
            this.updateBusinessScopeDept(scope);
        }

        // 需要删除的业务类型
        List<String> nmbusis = nbusis.stream().filter(item -> !mbusis.contains(item)).collect(Collectors.toList());
        if(StringUtils.isNotEmpty(nmbusis)) {
            List<Object> paras = new ArrayList<Object>();
            paras.add(role.getId());
            String in = HqlUtils.createHql(paras, nmbusis.toArray());
            jpaDao.delete(StringUtils.format("delete from SysBusinessScope where roleId = ? and business in {}", in), paras);
            jpaDao.delete(StringUtils.format("delete from SysBusinessScopeDept where roleId = ? and business in {}", in), paras);
        }
    }

    @Transactional
    public void updateBusinessScopeDept(SysBusinessScope scope) {
        if(!DataScopeEnum.DATA_SCOPE_CUSTOM.code().equals(scope.getDataScope())) {
            List<Object> paras = new ArrayList<Object>();
            paras.add(scope.getRoleId());
            paras.add(scope.getBusiness());
            jpaDao.delete("delete from SysBusinessScopeDept where roleId = ? and business = ?", paras);
        }else {
            List<String> mids = StringUtils.isNotEmpty(scope.getDeptIds()) ? Arrays.asList(scope.getDeptIds()) : new ArrayList<String>();
            List<String> nids = this.selectBusinessScopeDeptsByRoleId(scope.getRoleId(), scope.getBusiness());
            // 需要新增的部门id
            List<String> mnids = mids.stream().filter(item -> !nids.contains(item)).collect(Collectors.toList());
            for (String deptId : mnids) {
                SysBusinessScopeDept scopeDept = new SysBusinessScopeDept();
                scopeDept.setDeptId(deptId);
                scopeDept.setRoleId(scope.getRoleId());
                scopeDept.setBusiness(scope.getBusiness());
                jpaDao.save(scopeDept);
            }

            // 需要删除的部门id
            List<String> nmids = nids.stream().filter(item -> !mids.contains(item)).collect(Collectors.toList());
            if(StringUtils.isNotEmpty(nmids)) {
                List<Object> paras = new ArrayList<Object>();
                paras.add(scope.getRoleId());
                String in = HqlUtils.createHql(paras, nmids.toArray());
                jpaDao.delete(StringUtils.format("delete from SysBusinessScopeDept where roleId = ? and deptId in {}", in), paras);
            }
        }
    }

    /**
     * -根据角色id获取菜单关联id
     * @param roleId
     * @return
     */
    private List<String> selectRoleMenusByRoleId(String roleId){
        List<String> list = jpaDao.search("select menuId from SysRoleMenu where roleId = ? ", roleId, String.class);
        return StringUtils.isNotEmpty(list) ? list : Collections.emptyList();
    }

    /**
     * -根据角色id获取部门关联id
     * @param roleId
     * @return
     */
    private List<String> selectRoleDeptsByRoleId(String roleId){
        List<String> list = jpaDao.search("select deptId from SysRoleDept where roleId = ? ", roleId, String.class);
        return StringUtils.isNotEmpty(list) ? list : Collections.emptyList();
    }

    /**
     * -根据角色id获取业务类型
     * @param roleId
     * @return
     */
    private List<String> selectBusinessScopeByRoleId(String roleId){
        List<String> list = jpaDao.search("select business from SysBusinessScope where roleId = ? ", roleId, String.class);
        return StringUtils.isNotEmpty(list) ? list : Collections.emptyList();
    }
    
    /**
     * -根据角色id获取部门关联id
     * @param roleId
     * @param business
     * @return
     */
    private List<String> selectBusinessScopeDeptsByRoleId(String roleId, String business) {
        List<Object> paras = new ArrayList<Object>();
        paras.add(roleId);
        paras.add(business);
        List<String> list = jpaDao.search("select deptId from SysBusinessScopeDept where roleId = ? and business = ?", paras, String.class);
        return StringUtils.isNotEmpty(list) ? list : Collections.emptyList();
    }
}
