package com.daffodil.system.event.publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.daffodil.system.controller.model.SysNoticeParam;
import com.daffodil.system.event.SysNoticeEvent;

/**
 * 
 * @author yweijian
 * @date 2022年6月21日
 * @version 2.0.0
 * @description
 */
@Component
public class SysNoticePublisherService {

    @Autowired
    ApplicationEventPublisher publisher;

    public void publish(SysNoticeParam param, String tenantId) {
        SysNoticeEvent event = new SysNoticeEvent(param, tenantId);
        publisher.publishEvent(event);
    }
}
