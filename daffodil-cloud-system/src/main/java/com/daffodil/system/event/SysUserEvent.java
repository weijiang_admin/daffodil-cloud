package com.daffodil.system.event;

import org.springframework.context.ApplicationEvent;

/**
 * 
 * @author yweijian
 * @date 2022年6月21日
 * @version 2.0.0
 * @description
 */
public class SysUserEvent extends ApplicationEvent {

    private static final long serialVersionUID = -2376901457792846320L;

    /** 租户ID */
    private String tenantId;

    /** 操作类型 */
    private String operation;

    public SysUserEvent(Object source, String tenantId, String operation) {
        super(source);
        this.tenantId = tenantId;
        this.operation = operation;
    }

    public enum Operation {
        /** 新增 */
        INSERT,
        /** 修改 */
        UPDATE,
        /** 删除 */
        DELETE;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

}
