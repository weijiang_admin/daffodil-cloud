package com.daffodil.system.event.listener;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import com.daffodil.core.dao.JpaDao;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.system.controller.model.SysNoticeParam;
import com.daffodil.system.entity.SysNoticeTarget;
import com.daffodil.system.event.SysNoticeEvent;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2022年6月21日
 * @version 2.0.0
 * @description
 */
@Service
public class SysNoticeListener {

    @Autowired
    private JpaDao<String> jpaDao;

    @Async("systemEventExecutor")
    @Transactional
    @TransactionalEventListener(classes = SysNoticeEvent.class, phase = TransactionPhase.AFTER_COMMIT)
    public void noticeListener(SysNoticeEvent event) {
        TenantContextHolder.apply(event.getTenantId(), () -> {
            if(!(event.getSource() instanceof SysNoticeParam)) {
                return;
            }
            SysNoticeParam param = (SysNoticeParam) event.getSource();
            //全体用户
            boolean allin = null == param.getAllIn() ? false : param.getAllIn();
            if(allin) {
                SysNoticeTarget target = new SysNoticeTarget(null, param.getNoticeId(), "", SysNoticeTarget.Target.ALL.ordinal(), new Date());
                jpaDao.save(target);
                return;
            }

            //指定用户
            String[] userIds = param.getUserIds();
            this.insertNoticeTarget(param.getNoticeId(), userIds, SysNoticeTarget.Target.USER.ordinal());
            //指定部门
            String[] deptIds = param.getDeptIds();
            this.insertNoticeTarget(param.getNoticeId(), deptIds, SysNoticeTarget.Target.DEPT.ordinal());
            //指定角色
            String[] roleIds = param.getRoleIds();
            this.insertNoticeTarget(param.getNoticeId(), roleIds, SysNoticeTarget.Target.ROLE.ordinal());
            //指定岗位
            String[] postIds = param.getPostIds();
            this.insertNoticeTarget(param.getNoticeId(), postIds, SysNoticeTarget.Target.POST.ordinal());
            //指定职级
            String[] rankIds = param.getRankIds();
            this.insertNoticeTarget(param.getNoticeId(), rankIds, SysNoticeTarget.Target.RANK.ordinal());
            //指定群组
            String[] groupIds = param.getGroupIds();
            this.insertNoticeTarget(param.getNoticeId(), groupIds, SysNoticeTarget.Target.GROUP.ordinal());
        });
    }

    @Transactional
    public void insertNoticeTarget(String noticeId, String[] targetIds, int target) {
        if(StringUtils.isNotEmpty(targetIds)) {
            for(int i = 0; i < targetIds.length; i++) {
                SysNoticeTarget noticeTarget = new SysNoticeTarget(null, noticeId, targetIds[i], target, new Date());
                jpaDao.save(noticeTarget);
            }
        }
    }

}
