package com.daffodil.system.event.executor;

import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskDecorator;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.daffodil.framework.context.TenantContextHolder;

/**
 * @author yweijian
 * @date 2022年6月22日
 * @version 2.0.0
 * @description
 */
@EnableAsync
@Configuration
public class EventExecutor {

    @Bean("systemEventExecutor")
    public ThreadPoolTaskExecutor initThreadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(1);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(10);
        executor.setKeepAliveSeconds(60);
        executor.setThreadNamePrefix("system-event-executor-");
        executor.setTaskDecorator(new TenantAwareTaskDecorator());
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }

    public class TenantAwareTaskDecorator implements TaskDecorator {
        @Override
        public Runnable decorate(Runnable runnable) {
            String tenantId = TenantContextHolder.getTenantHolder(); // 获取当前租户ID
            return () -> {
                try {
                    TenantContextHolder.setTenantHolder(tenantId); // 设置租户ID
                    runnable.run();
                } finally {
                    TenantContextHolder.clearTenantHolder(); // 清除租户ID
                }
            };
        }
    }
}
