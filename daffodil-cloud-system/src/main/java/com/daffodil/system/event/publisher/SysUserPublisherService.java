package com.daffodil.system.event.publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.daffodil.system.entity.SysUser;
import com.daffodil.system.event.SysUserEvent;

/**
 * 
 * @author yweijian
 * @date 2022年6月21日
 * @version 2.0.0
 * @description
 */
@Component
public class SysUserPublisherService {

    @Autowired
    ApplicationEventPublisher publisher;

    public void publish(SysUser user, String tenantId, String operation) {
        SysUserEvent event = new SysUserEvent(user, tenantId, operation);
        publisher.publishEvent(event);
    }
}
