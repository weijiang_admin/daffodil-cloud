package com.daffodil.system.event.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.entity.user.SysUserPost;
import com.daffodil.system.entity.user.SysUserRank;
import com.daffodil.system.entity.user.SysUserRole;
import com.daffodil.system.event.SysUserEvent;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2022年6月21日
 * @version 2.0.0
 * @description
 */
@Service
public class SysUserListener {

    @Autowired
    private JpaDao<String> jpaDao;

    @Async("systemEventExecutor")
    @Transactional
    @TransactionalEventListener(classes = SysUserEvent.class, phase = TransactionPhase.AFTER_COMMIT, condition = "#event.operation == 'INSERT'")
    public void insertUserListener(SysUserEvent event) {
        TenantContextHolder.apply(event.getTenantId(), () -> {
            SysUser user = (SysUser) event.getSource();
            // 新增用户岗位关联
            this.insertUserPost(user);
            // 新增用户与角色管理
            this.insertUserRole(user);
            // 新增用户职级关联
            this.insertUserRank(user);
        });
    }

    @Async("systemEventExecutor")
    @Transactional
    @TransactionalEventListener(classes = SysUserEvent.class, phase = TransactionPhase.AFTER_COMMIT, condition = "#event.operation == 'UPDATE'")
    public void updateUserListener(SysUserEvent event) {
        TenantContextHolder.apply(event.getTenantId(), () -> {
            SysUser user = (SysUser) event.getSource();
            //系统保留的超级管理员用户不允许修改角色
            SysUser sysUser = jpaDao.find(SysUser.class, user.getId());
            if (StringUtils.isNotNull(sysUser) && !CommonConstant.YES.equals(sysUser.getIsAdmin())) {
                // 删除用户与角色关联
                this.deleteUserRole(user);
                // 新增用户与角色管理
                this.insertUserRole(user);
            }

            // 删除用户与岗位关联
            this.deleteUserPost(user);
            // 新增用户与岗位管理
            this.insertUserPost(user);
            // 删除用户与职级关联
            this.deletUserRank(user);
            // 新增用户与职级管理
            this.insertUserRank(user);
        });
    }

    @Async("systemEventExecutor")
    @Transactional
    @TransactionalEventListener(classes = SysUserEvent.class, phase = TransactionPhase.AFTER_COMMIT, condition = "#event.operation == 'DELETE'")
    public void deleteUserListener(SysUserEvent event) {
        TenantContextHolder.apply(event.getTenantId(), () -> {
            SysUser user = (SysUser) event.getSource();
            // 删除用户与角色关联
            this.deleteUserRole(user);
            // 删除用户与岗位关联
            this.deleteUserPost(user);
            // 删除用户与职级关联
            this.deletUserRank(user);
            // 删除用户与群组关联
            this.deleteUserGroup(user);
        });
    }

    @Transactional
    public void deleteUserRole(SysUser user) {
        jpaDao.delete("delete from SysUserRole where userId = ? ", user.getId());
    }

    @Transactional
    public void insertUserRole(SysUser user) {
        String[] roles = user.getRoleIds();
        if (StringUtils.isNotNull(roles)) {
            // 新增用户与角色关联
            for (String roleId : roles) {
                SysUserRole userRole = new SysUserRole();
                userRole.setUserId(user.getId());
                userRole.setRoleId(roleId);
                jpaDao.save(userRole);
            }
        }
    }

    @Transactional
    public void deleteUserPost(SysUser user) {
        jpaDao.delete("delete from SysUserPost where userId = ? ", user.getId());
    }

    @Transactional
    public void insertUserPost(SysUser user) {
        String[] posts = user.getPostIds();
        if (StringUtils.isNotNull(posts)) {
            // 新增用户与岗位关联
            for (String postId : posts) {
                SysUserPost userPost = new SysUserPost();
                userPost.setUserId(user.getId());
                userPost.setPostId(postId);
                jpaDao.save(userPost);
            }
        }
    }

    @Transactional
    public void deletUserRank(SysUser user) {
        jpaDao.delete("delete from SysUserRank where userId = ? ", user.getId());
    }

    @Transactional
    public void insertUserRank(SysUser user) {
        String[] ranks = user.getRankIds();
        if (StringUtils.isNotNull(ranks)) {
            // 新增用户与职级关联
            for (String rankId : ranks) {
                SysUserRank userRank = new SysUserRank();
                userRank.setUserId(user.getId());
                userRank.setRankId(rankId);
                jpaDao.save(userRank);
            }
        }
    }

    @Transactional
    public void deleteUserGroup(SysUser user) {
        jpaDao.delete("delete from SysUserGroup where userId = ?", user.getId());
    }
}
