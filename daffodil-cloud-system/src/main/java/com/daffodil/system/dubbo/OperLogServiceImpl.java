package com.daffodil.system.dubbo;

import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import com.daffodil.framework.model.OperLogReq;
import com.daffodil.framework.service.IOperLogService;
import com.daffodil.system.entity.SysOperLog;
import com.daffodil.system.service.ISysOperLogService;

/**
 * @author yweijian
 * @date 2025年3月6日
 * @version 2.0.0
 * @copyright Copyright 2020 - 2025 www.daffodilcloud.com.cn
 * @description
 */
@DubboService
public class OperLogServiceImpl implements IOperLogService {

    @Autowired
    private ISysOperLogService operLogService;
    
    @Override
    public void insertOperlog(OperLogReq operLogReq) {
        SysOperLog operLog = new SysOperLog();
        operLog.setTitle(operLogReq.getTitle());
        operLog.setMethod(operLogReq.getMethod());
        operLog.setBusinessName(operLogReq.getBusinessName());
        operLog.setBusinessLabel(operLogReq.getBusinessLabel());
        operLog.setBusinessRemark(operLogReq.getBusinessRemark());
        operLog.setClientName(operLogReq.getClientName());
        operLog.setClientLabel(operLogReq.getClientLabel());
        operLog.setClientRemark(operLogReq.getClientRemark());
        operLog.setOperName(operLogReq.getOperName());
        operLog.setOperUrl(operLogReq.getOperUrl());
        operLog.setOperIp(operLogReq.getOperIp());
        operLog.setOperTraceId(operLogReq.getOperTraceId());
        operLog.setOperHeader(operLogReq.getOperHeader());
        operLog.setOperParam(operLogReq.getOperParam());
        operLog.setOperResult(operLogReq.getOperResult());
        operLog.setStatus(operLogReq.getStatus());
        operLog.setErrorMsg(operLogReq.getErrorMsg());
        operLog.setCreateTime(operLogReq.getCreateTime());
        operLogService.insertOperlog(operLog);
    }

}
