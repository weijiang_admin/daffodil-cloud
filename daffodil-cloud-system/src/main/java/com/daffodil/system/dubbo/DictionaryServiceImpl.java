package com.daffodil.system.dubbo;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.daffodil.framework.model.Dictionary;
import com.daffodil.framework.service.IDictionaryService;
import com.daffodil.system.entity.SysDictionary;
import com.daffodil.system.service.ISysDictionaryService;

/**
 * 字典服务
 * @author yweijian
 * @date 2025年3月06日
 * @version 2.0.0
 * @copyright Copyright 2020-2025 www.daffodilcloud.com.cn
 * @description
 */
@DubboService
public class DictionaryServiceImpl implements IDictionaryService {

    @Autowired
    private ISysDictionaryService dictionaryService;

    @Override
    public List<Dictionary> selectDictionaryByLabel(String dictLabel) {
        List<SysDictionary> list = dictionaryService.selectDictionaryByLabel(dictLabel);
        if(CollectionUtils.isEmpty(list)){
            return Collections.emptyList();
        }
        return list.stream().map(item -> {
            Dictionary dictionary = new Dictionary();
            dictionary.setId(item.getId());
            dictionary.setDictName(item.getDictName());
            dictionary.setDictType(item.getDictType());
            dictionary.setDictLabel(item.getDictLabel());
            dictionary.setDictValue(item.getDictValue());
            dictionary.setIsDefault(item.getIsDefault());
            dictionary.setParentId(item.getParentId());
            dictionary.setAncestors(item.getAncestors());
            dictionary.setOrderNum(item.getOrderNum());
            dictionary.setStatus(item.getStatus());
            dictionary.setRemark(item.getRemark());
            return dictionary;
        }).collect(Collectors.toList());
    }

}
