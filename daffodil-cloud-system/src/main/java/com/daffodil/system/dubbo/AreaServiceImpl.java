package com.daffodil.system.dubbo;

import com.daffodil.framework.model.Area;
import com.daffodil.framework.service.IAreaService;
import com.daffodil.system.entity.SysArea;
import com.daffodil.system.service.ISysAreaService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

/**
 * 
 * @author yweijian
 * @date 2020-03-16
 * @version 2.0.0
 * @copyright Copyright 2020-2025 www.daffodilcloud.com.cn
 * @description
 */
@DubboService
public class AreaServiceImpl implements IAreaService {

    @Autowired
    private ISysAreaService areaService;

    @Override
    public List<Area> selectAreaListByParentId(String parentId) {
        List<SysArea> list = areaService.selectAreaListByParentId(parentId);
        if(CollectionUtils.isEmpty(list)){
            return Collections.emptyList();
        }
        return list.stream().map(item -> {
            Area area = new Area();
            area.setId(item.getId());
            area.setProvince(item.getProvince());
            area.setCity(item.getCity());
            area.setCounty(item.getCounty());
            area.setTown(item.getTown());
            area.setVillage(item.getVillage());
            area.setParentId(item.getParentId());
            area.setAncestors(item.getAncestors());
            area.setAreaName(item.getAreaName());
            area.setShortName(item.getShortName());
            area.setCapitalCity(item.getCapitalCity());
            area.setAreaLevel(item.getAreaLevel());
            area.setPostCode(item.getPostCode());
            area.setOrderNum(item.getOrderNum());
            area.setStatus(item.getStatus());
            area.setRemark(item.getRemark());
            return area;
        }).collect(Collectors.toList());
    }
}
