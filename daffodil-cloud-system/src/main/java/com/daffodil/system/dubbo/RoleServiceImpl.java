package com.daffodil.system.dubbo;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.model.Role;
import com.daffodil.framework.service.IRoleService;
import com.daffodil.system.entity.SysRole;
import com.daffodil.system.service.ISysRoleService;

import cn.hutool.core.collection.CollectionUtil;

/**
 * 
 * @author yweijian
 * @date 2025年3月10日
 * @version 2.0.0
 * @copyright Copyright 2020 - 2025 www.daffodilcloud.com.cn
 * @description
 */
@DubboService
public class RoleServiceImpl implements IRoleService {

    @Autowired
    private ISysRoleService roleService;

    @Override
    public List<Role> selectRoleList(Role role, Page page) {
        Query<SysRole> query = new Query<>();
        SysRole entity = new SysRole();
        entity.setRoleName(role.getRoleName());
        entity.setRoleKey(role.getRoleKey());
        entity.setDataScope(role.getDataScope());
        entity.setStatus(role.getStatus());
        entity.setRemark(role.getRemark());
        query.setEntity(entity);
        query.setPage(page);
        List<SysRole> list = roleService.selectRoleList(query);
        if (CollectionUtil.isEmpty(list)) {
            return Collections.emptyList();
        }
        return list.stream().map(item -> {
            Role role1 = new Role();
            role1.setId(item.getId());
            role1.setRoleName(item.getRoleName());
            role1.setRoleKey(item.getRoleKey());
            role1.setDataScope(item.getDataScope());
            role1.setStatus(item.getStatus());
            role1.setRemark(item.getRemark());
            return role1;
        }).collect(Collectors.toList());
    }

}
