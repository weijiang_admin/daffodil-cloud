package com.daffodil.system.dubbo;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.daffodil.framework.model.Tag;
import com.daffodil.framework.service.ITagService;
import com.daffodil.system.entity.SysTag;
import com.daffodil.system.service.ISysTagService;

/**
 * @author yweijian
 * @date 2023年2月13日
 * @version 2.0.0
 * @copyright Copyright 2020 - 2025 www.daffodilcloud.com.cn
 * @description
 */
@DubboService
public class TagServiceImpl implements ITagService {

    @Autowired
    private ISysTagService tagService;

    @Override
    public List<Tag> selectTagByLabel(String tagLabel) {
        List<SysTag> list = tagService.selectTagByLabel(tagLabel);
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }
        return list.stream().map(item -> {
            Tag tag = new Tag();
            tag.setId(item.getId());
            tag.setTagName(item.getTagName());
            tag.setTagType(item.getTagType());
            tag.setTagLabel(item.getTagLabel());
            tag.setTagValue(item.getTagValue());
            tag.setIsDefault(item.getIsDefault());
            tag.setParentId(item.getParentId());
            tag.setAncestors(item.getAncestors());
            tag.setOrderNum(item.getOrderNum());
            tag.setStatus(item.getStatus());
            tag.setRemark(item.getRemark());
            return tag;
        }).collect(Collectors.toList());
    }

}
