package com.daffodil.system.dubbo;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.model.Role;
import com.daffodil.framework.model.User;
import com.daffodil.framework.service.IUserService;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.service.ISysUserService;

import cn.hutool.core.collection.CollectionUtil;

/**
 * 
 * @author yweijian
 * @date 2025年3月10日
 * @version 2.0.0
 * @copyright Copyright 2020 - 2025 www.daffodilcloud.com.cn
 * @description
 */
@DubboService
public class UserServiceImpl implements IUserService {

    @Autowired
    private ISysUserService userService;

    @Override
    public User selectUserById(String userId) {
        SysUser sysUser = userService.selectUserById(userId);
        return this.convertToUser(sysUser);
    }

    @Override
    public List<User> selectUserList(User user, Page page) {
        Query<SysUser> query = new Query<SysUser>();
        SysUser entity = new SysUser();
        entity.setLoginName(user.getLoginName());
        entity.setUserName(user.getUserName());
        entity.setEmail(user.getEmail());
        entity.setPhone(user.getPhone());
        entity.setStatus(user.getStatus());
        query.setEntity(entity);
        query.setPage(page);

        SysUser sysUser = null;
        if (StringUtils.isNotEmpty(user.getId())) {
            sysUser = userService.selectUserById(user.getId());
        }
        List<SysUser> list = userService.selectUserList(query, sysUser);
        if (CollectionUtil.isEmpty(list)) {
            return Collections.emptyList();
        }
        return list.stream().map(item -> this.convertToUser(item)).collect(Collectors.toList());
    }

    private User convertToUser(SysUser sysUser) {
        if(sysUser == null) {
            return null;
        }
        User user = new User();
        user.setId(sysUser.getId());
        user.setAvatar(sysUser.getAvatar());
        user.setLoginName(sysUser.getLoginName());
        user.setStatus(sysUser.getStatus());
        user.setDeptId(sysUser.getDeptId());
        user.setUserName(sysUser.getUserName());
        user.setEmail(sysUser.getEmail());
        user.setPhone(sysUser.getPhone());
        user.setIsAdminRole(sysUser.isAdminRole());
        user.setRemark(sysUser.getRemark());

        // 转换角色列表
        if(sysUser.getRoles() != null) {
            List<Role> roles = sysUser.getRoles().stream().map(sysRole -> {
                Role role = new Role();
                role.setId(sysRole.getId());
                role.setRoleName(sysRole.getRoleName());
                role.setRoleKey(sysRole.getRoleKey());
                role.setDataScope(sysRole.getDataScope());
                role.setStatus(sysRole.getStatus());
                role.setRemark(sysRole.getRemark());
                return role;
            }).collect(Collectors.toList());
            user.setRoles(roles);
        }

        return user;
    }

}
