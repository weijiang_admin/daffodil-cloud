package com.daffodil.system.dubbo;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import com.daffodil.framework.service.IHolidaysService;
import com.daffodil.system.service.ISysDaysService;
import com.daffodil.util.sm.CodeUtils;

/**
 * 
 * @author yweijian
 * @date 2023年2月13日
 * @version 2.0.0
 * @copyright Copyright 2020 - 2025 www.daffodilcloud.com.cn
 * @description
 */
@DubboService
public class HolidaysServiceImpl implements IHolidaysService {

    @Autowired
    private ISysDaysService daysService;
    
    @Override
    public byte[] selectDaysByYear(Integer year) {
        String content = daysService.selectDaysHexData(year);
        return CodeUtils.hexStringToBytes(content);
    }

    @Override
    public Map<Integer, byte[]> multiselectDaysByYear(Integer startYear, Integer endYear) {
        List<String> list = daysService.multiselectDaysHexData(startYear, endYear);
        return list.stream().map(data -> CodeUtils.hexStringToBytes(String.valueOf(data)))
            .collect(Collectors.toMap(data -> {
                byte[] yearBytes = new byte[2];
                System.arraycopy(data, 0, yearBytes, 0, 2);
                return (int) CodeUtils.byteToShortBigEndian(yearBytes);
            }, data -> data));
    }

}
