package com.daffodil.system.runner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author yweijian
 * @date 2022年3月14日
 * @version 1.0
 * @description
 */
@Slf4j
@Component
public class DaffodilSystemApplicationRunner implements ApplicationRunner, Ordered {

    @Value("${spring.application.name:daffodil-cloud-system}")
    private String appName;
    
    @Value("${server.port:37077}")
    private String appPort;
    
    @Override
    public int getOrder() {
        return Integer.MAX_VALUE;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("达佛基础管理平台微服务启动成功，应用服务名：{} 应用端口：{}", appName, appPort);
        }
    }

}
