package com.daffodil.system.runner;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.framework.properties.FrameworkProperties;
import com.daffodil.system.entity.SysTenant;
import com.daffodil.system.enums.DataStatusEnum;
import com.daffodil.system.service.ISysDaysService;
import com.daffodil.system.service.ISysTenantService;

import cn.hutool.core.collection.CollectionUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author yweijian
 * @date 2023年2月6日
 * @version 2.0.0
 * @description
 */
@Slf4j
@Component
public class InitDaysConfigApplicationRunner implements ApplicationRunner, Ordered {

    @Autowired
    private ISysDaysService saysService;

    @Autowired
    private ISysTenantService tenantService;

    @Autowired
    private FrameworkProperties frameworkProperties;

    @Override
    public int getOrder() {
        return 2;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if(frameworkProperties.getTenantEnable()) {
            Query<SysTenant> query = new Query<SysTenant>();
            SysTenant entity = new SysTenant();
            entity.setStatus(DataStatusEnum.NORMAL.code());
            query.setPage(new Page(100000, 1));
            List<SysTenant> list = tenantService.selectTenantList(query);
            if(CollectionUtil.isNotEmpty(list)) {
                list.forEach(tenant -> {
                    log.info("初始化假期缓存，租户号码：{}", tenant.getId());
                    TenantContextHolder.apply(tenant.getId(), () -> saysService.initSysDaysData());
                });
            }
        }else {
            saysService.initSysDaysData();
        }
    }

}
