package com.daffodil.system.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import com.daffodil.common.constant.CommonConstant;
import com.daffodil.system.entity.config.SysConfigLoginAccount;
import com.daffodil.system.entity.config.SysConfigLoginEmail;
import com.daffodil.system.entity.config.SysConfigLoginMobile;
import com.daffodil.system.entity.config.SysConfigLoginScan;
import com.daffodil.system.entity.config.SysConfigLoginAccount.CodeType;
import com.daffodil.system.entity.config.SysConfigLoginScan.ScanType;
import com.daffodil.system.model.ConfigLogin;
import com.daffodil.system.service.ISysConfigService;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2023年2月6日
 * @version 2.0.0
 * @description
 */
@Component
public class InitConfigLoginApplicationRunner implements ApplicationRunner, Ordered {

    @Autowired
    private ISysConfigService configService;

    @Override
    public int getOrder() {
        return 1;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        SysConfigLoginAccount account = configService.selectConfigLoginAccount();
        SysConfigLoginMobile mobile = configService.selectConfigLoginMobile();
        SysConfigLoginEmail email = configService.selectConfigLoginEmail();
        SysConfigLoginScan scan = configService.selectConfigLoginScan();

        if(StringUtils.isNull(account)) {
            account = new SysConfigLoginAccount();
            account.setEnable(CommonConstant.YES);
            account.setCodeType(CodeType.PICTURE.name());
        }
        if(StringUtils.isNull(mobile)) {
            mobile = new SysConfigLoginMobile();
            mobile.setEnable(CommonConstant.NO);
            mobile.setLength(6);
            mobile.setExpireTime(5);
            mobile.setTemplateId("");
            mobile.setTemplateParam("");
            mobile.setSubject("【XXXX】短信验证码");
            mobile.setTemplate("【XXXX】尊敬的用户您好！您的验证码是：${code}，5分钟内有效，请勿泄露给他人，如非本人操作，请忽略此消息。");
        }
        if(StringUtils.isNull(email)) {
            email = new SysConfigLoginEmail();
            email.setEnable(CommonConstant.NO);
            email.setLength(6);
            email.setExpireTime(5);
            email.setSubject("【XXXX】邮箱验证码");
            email.setTemplate("【XXXX】尊敬的用户您好！您的验证码是：${code}，5分钟内有效，请勿泄露给他人，如非本人操作，请忽略此消息。");
        }
        if(StringUtils.isNull(scan)) {
            scan = new SysConfigLoginScan();
            scan.setEnable(CommonConstant.NO);
            scan.setScanType(ScanType.QQ.name());
        }
        ConfigLogin login = new ConfigLogin();
        login.setAccount(account);
        login.setMobile(mobile);
        login.setEmail(email);
        login.setScan(scan);
        configService.saveConfigLogin(login);
    }

}
