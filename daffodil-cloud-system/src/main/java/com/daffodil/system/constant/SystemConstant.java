package com.daffodil.system.constant;

/**
 * 
 * @author yweijian
 * @date 2021年9月23日
 * @version 1.0
 * @description
 */
public class SystemConstant {

    /**
     * api上下文地址
     */
    public static final String API_CONTENT_PATH = "/api-system";

}
