package com.daffodil;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 达佛基础管理平台后端微服务
 * @author yweijian
 * @date 2021年9月24日
 * @version 1.0
 * @description
 */
@EnableDubbo
@EnableDiscoveryClient
@SpringBootApplication
public class DaffodilSystemApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(DaffodilSystemApplication.class);
        application.setWebApplicationType(WebApplicationType.REACTIVE);
        application.addListeners(new ApplicationPidFileWriter());
        application.run(args);
    }
}
