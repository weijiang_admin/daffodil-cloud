package com.daffodil.auth.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author yweijian
 * @date 2022年3月16日
 * @version 1.0
 * @description
 */
@Setter
@Getter
@Component
@RefreshScope
public class KaptchaProperties {

    /** 是否开启验证码 */
    @Value("${daffodil.kaptcha.enable:true}")
    private Boolean enable;

    /** 验证码类型 默认数学 */
    @Value("${daffodil.kaptcha.text:math}")
    private String text;

    /**
     * -验证码模糊器类型 <br>
     * -水纹 com.google.code.kaptcha.impl.WaterRipple <br>
     * -鱼眼 com.google.code.kaptcha.impl.FishEyeGimpy <br>
     * -阴影 com.google.code.kaptcha.impl.ShadowGimpy
     */
    @Value("${daffodil.kaptcha.obscurificator:com.google.code.kaptcha.impl.WaterRipple}")
    private String obscurificator;
    
    /**
     * -发送短信验证码URI <br>
     * -属性参数：{ "account" : "18088888888", "subject" : "发送主题", "content" : "讯息内容", "templateId" : "模板ID", "templateParam" : "模板参数" }
     */
    @Value("${daffodil.kaptcha.sms-uri:http://sms.domain/sms/send}")
    private String sendSmsUri;
    
    /** 
     * -发送邮箱验证码URI <br>
     * -属性参数：{ "account" : "18088888888", "subject" : "发送主题", "content" : "讯息内容" }
     */
    @Value("${daffodil.kaptcha.email-uri:http://email.domain/email/send}")
    private String sendEmailUri;
    
}
