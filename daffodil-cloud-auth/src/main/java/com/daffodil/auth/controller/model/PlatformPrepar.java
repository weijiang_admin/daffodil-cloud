package com.daffodil.auth.controller.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2022年5月11日
 * @version 1.0
 * @description
 */
@ApiModel(value = "平台预备数据")
@Data
@AllArgsConstructor
public class PlatformPrepar {

    @ApiModelProperty(name = "publicKey", value = "SM2加密公钥")
    private String publicKey;

    @ApiModelProperty(name = "expireTime", value = "公钥有效时间（单位：秒）")
    private Long expireTime;

    @ApiModelProperty(name = "currentTime", value = "服务器当前时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date currentTime;
}
