package com.daffodil.auth.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.auth.constant.AuthConstant;
import com.daffodil.auth.service.ISysOnlineUserService;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness;
import com.daffodil.system.model.OnlineUser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * -在线用户管理
 * @author yweijian
 * @date 2024年6月7日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Api(value = "在线用户管理", tags = "在线用户管理")
@RestController
@RequestMapping(AuthConstant.API_CONTENT_PATH)
public class OnlineUserController extends ReactiveBaseController {

    @Autowired
    private ISysOnlineUserService onlineUserService;

    @ApiOperation("分页查询在线用户列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:onlineuser:list")
    @GetMapping("/onlineuser/list")
    public Mono<TableResult> list(Page page, @ApiIgnore ServerHttpRequest request) {
        List<OnlineUser> list = onlineUserService.selectOnlineUserList(page);
        return Mono.just(TableResult.success(list, page));
    }

    @ApiOperation("下线在线用户")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:onlineuser:remove")
    @OperLog(title = "在线用户管理", business = @OperBusiness(name = "下线", label = "DELETE"))
    @PostMapping("/onlineuser/remove")
    public Mono<JsonResult> remove(@RequestBody String[] tokens, @ApiIgnore ServerHttpRequest request) {
        onlineUserService.deleteOnlineUserByTokens(tokens);
        return Mono.just(JsonResult.success());
    }

    @ApiOperation("下线全部在线用户")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("system:onlineuser:clean")
    @OperLog(title = "在线用户管理", business = @OperBusiness(name = "下线", label = "CLEAN", remark = "下线全部在线用户"))
    @PostMapping("/onlineuser/clean")
    public Mono<JsonResult> clean(@ApiIgnore ServerHttpRequest request) {
        onlineUserService.deleteAllOnlineUser();
        return Mono.just(JsonResult.success());
    }

}
