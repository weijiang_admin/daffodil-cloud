package com.daffodil.auth.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.auth.constant.AuthConstant;
import com.daffodil.auth.controller.model.Account;
import com.daffodil.auth.controller.model.LoginResult;
import com.daffodil.auth.service.ISysLoginService;
import com.daffodil.auth.service.ISysMenuService;
import com.daffodil.auth.service.ISysUserService;
import com.daffodil.auth.util.LoginUtils;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.entity.SysUser;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.JwtTokenUtils;
import com.daffodil.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2021年9月19日
 * @version 1.0
 * @description
 */
@Api(value = "授权认证管理", tags = "授权认证管理")
@RestController
@RequestMapping(AuthConstant.API_CONTENT_PATH)
public class LoginController {

    @Autowired
    private ISysLoginService loginService;

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private ISysUserService userservice;

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    @ApiOperation("获取登录授权令牌")
    @PostMapping("/token/sign")
    public Mono<JsonResult> sign(@RequestBody Account account, @ApiIgnore ServerHttpRequest request) {
        SysUser user = loginService.login(account, request);
        LoginUser loginUser = LoginUtils.buildLoginUser(user, request);
        List<String> permissions = menuService.selectUserAuthBtnPerms(user);
        LoginResult result = LoginUtils.buildLoginResult(CommonConstant.LEVEL_TOKEN_FIRST, loginUser, permissions, tenantRedisTemplate);
        return Mono.just(JsonResult.success(result));
    }

    @ApiOperation("续期登录授权令牌")
    @ApiImplicitParams({
        @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true),
        @ApiImplicitParam(name = FrameworkConstant.X_REQUEST_REFRESH_TOKEN_HEADER, value = "刷新授权令牌 token", paramType = "header", required = true)
    })
    @GetMapping("/token/renewal")
    public Mono<JsonResult> renewal(@ApiIgnore ServerHttpRequest request) {
        String accessToken = LoginUtils.getAccessToken(request);
        String authorToken = LoginUtils.getAuthorToken(request);
        String refreshToken = LoginUtils.getRefreshToken(request);
        if(StringUtils.isEmpty(authorToken)) {
            throw new BaseException(31006);
        }

        long currentTimeMillis = System.currentTimeMillis();
        long lastTimeMillis = JwtTokenUtils.getCurrentTimeMillis(authorToken);

        //accessToken在25分钟内则不重新刷新accessToken
        if(currentTimeMillis - lastTimeMillis < 25 * 60 * 1000) {
            LoginResult result = new LoginResult(accessToken, currentTimeMillis - lastTimeMillis / 1000, refreshToken, new Date(lastTimeMillis));
            return Mono.just(JsonResult.success(result));
        }

        //accessToken已经超过25分钟小于30分钟从新签名生成一个续期
        return this.refresh(request);
    }

    @ApiOperation("刷新登录授权令牌")
    @ApiImplicitParam(name = FrameworkConstant.X_REQUEST_REFRESH_TOKEN_HEADER, value = "刷新授权令牌  token", paramType = "header", required = true)
    @GetMapping("/token/refresh")
    public Mono<JsonResult> refresh(@ApiIgnore ServerHttpRequest request) {
        String refreshToken = LoginUtils.getRefreshToken(request);
        if(StringUtils.isEmpty(refreshToken)) {
            throw new BaseException(31006);
        }

        String refreshTokenCackekey = StringUtils.format(CommonConstant.REFRESH_TOKEN_CACHEKEY, refreshToken);
        String accessToken = (String) tenantRedisTemplate.opsForValue().get(refreshTokenCackekey);
        if(StringUtils.isEmpty(accessToken)) {
            throw new BaseException(31006);
        }

        String accessTokenCachekey = StringUtils.format(CommonConstant.ACCESS_TOKEN_CACHEKEY, accessToken);
        String authorToken = (String) tenantRedisTemplate.opsForValue().get(accessTokenCachekey);
        String data = JwtTokenUtils.getData(authorToken);
        LoginUser loginUser = JacksonUtils.toJavaObject(data, LoginUser.class);
        if(StringUtils.isNull(loginUser)) {
            throw new BaseException(31006);
        }

        SysUser user = userservice.selectUserById(loginUser.getId());
        if(StringUtils.isNull(user)) {
            throw new BaseException(31006);
        }

        //LoginUtils.clearLoginResult(accessToken, redisTemplate);
        List<String> permissions = menuService.selectUserAuthBtnPerms(user);
        LoginResult result = LoginUtils.buildLoginResult(CommonConstant.LEVEL_TOKEN_FIRST, loginUser, permissions, tenantRedisTemplate);
        return Mono.just(JsonResult.success(result));
    }

    @ApiOperation("注销登录授权令牌")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @PostMapping("/token/destroy")
    public Mono<JsonResult> destroy(@ApiIgnore ServerHttpRequest request) {
        LoginUser loginUser = LoginUtils.getLoginUser(request);
        String accessToken = LoginUtils.getAccessToken(request);
        if(StringUtils.isNotEmpty(accessToken) && StringUtils.isNotNull(loginUser)) {
            LoginUtils.clearLoginResult(accessToken, tenantRedisTemplate);
            loginService.recordLoginInfo(loginUser.getLoginName(), CommonConstant.SUCCESS, "注销成功", request);
        }
        return Mono.just(JsonResult.success());
    }

}
