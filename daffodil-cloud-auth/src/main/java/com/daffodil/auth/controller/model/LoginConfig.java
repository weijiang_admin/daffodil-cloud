package com.daffodil.auth.controller.model;

import java.util.Collections;
import java.util.List;

import com.daffodil.common.constant.CommonConstant;
import com.daffodil.system.entity.config.SysConfigLoginAccount.CodeType;
import com.daffodil.system.model.ConfigLogin;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * -登录设置
 * @author yweijian
 * @date 2022年9月9日
 * @version 2.0.0
 * @description
 */
@ApiModel(value = "登录设置")
@Data
public class LoginConfig {

    @ApiModelProperty(name = "tenantEnable", value = "是否开启租户模式")
    private Boolean tenantEnable = false;

    @ApiModelProperty(name = "accountEnable", value = "是否开启账号密码登录")
    private Boolean accountEnable;

    @ApiModelProperty(name = "mobileEnable", value = "是否开启短信验证码登录")
    private Boolean mobileEnable;

    @ApiModelProperty(name = "emailEnable", value = "是否开启邮箱验证码登录")
    private Boolean emailEnable;

    @ApiModelProperty(name = "scanEnable", value = "是否开启扫码登录")
    private Boolean scanEnable;

    @ApiModelProperty(name = "scanModes", value = "扫码登录的扫码方式")
    private List<String> scanModes;

    @ApiModelProperty(name = "dynamicEnable", value = "是否开启账号密码+动态验证码登录")
    private Boolean dynamicEnable;

    public LoginConfig convert(ConfigLogin config) {

        this.setAccountEnable(config != null && config.getAccount() != null
                && CommonConstant.YES.equals(config.getAccount().getEnable()));

        this.setMobileEnable(config != null && config.getMobile() != null
                && CommonConstant.YES.equals(config.getMobile().getEnable()));

        this.setEmailEnable(config != null && config.getEmail() != null
                && CommonConstant.YES.equals(config.getEmail().getEnable()));

        this.setScanEnable(config != null && config.getScan() != null
                && CommonConstant.YES.equals(config.getScan().getEnable()));

        this.setScanModes(config != null && config.getScan() != null 
                ? config.getScan().getScanModes() : Collections.emptyList());

        this.setDynamicEnable(config != null && config.getAccount() != null
                && CommonConstant.YES.equals(config.getAccount().getEnable())
                && CodeType.DYNAMIC.name().equals(config.getAccount().getCodeType()));

        return this;
    }
}
