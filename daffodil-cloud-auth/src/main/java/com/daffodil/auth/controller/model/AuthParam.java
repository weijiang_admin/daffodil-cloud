package com.daffodil.auth.controller.model;

import javax.validation.constraints.NotBlank;

import com.daffodil.util.StringUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2023年1月11日
 * @version 2.0.0
 * @description
 */
@ApiModel(value = "授权登录参数")
@Data
public class AuthParam {

    @ApiModelProperty(name = "code", value = "授权码")
    @NotBlank(message = "授权码参数不能为空")
    private String code;

    @ApiModelProperty(name = "state", value = "状态码")
    @NotBlank(message = "状态码参数不能为空")
    private String state;
    
    /**
     * 获取模式
     * @return
     */
    public String getMode() {
        String[] p = this.state.split("_");
        return StringUtils.isNotEmpty(p) && p.length == 2 ? p[0] : null;
    }
    
    /**
     * 获取随机码
     * @return
     */
    public String getRandom() {
        String[] p = this.state.split("_");
        return StringUtils.isNotEmpty(p) && p.length == 2 ? p[1] : null;
    }
}
