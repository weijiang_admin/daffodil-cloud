package com.daffodil.auth.controller;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.auth.constant.AuthConstant;
import com.daffodil.auth.controller.model.AuthParam;
import com.daffodil.auth.controller.model.LoginResult;
import com.daffodil.auth.controller.model.ScanResult;
import com.daffodil.auth.service.IScanQRLoginService;
import com.daffodil.auth.service.ISysLoginService;
import com.daffodil.auth.service.ISysMenuService;
import com.daffodil.auth.util.LoginUtils;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.entity.SysUser;
import com.daffodil.util.StringUtils;

import cn.hutool.core.util.RandomUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 社会账号扫码授权<br>
 * QQ、微信、企业微信、飞书、钉钉等
 * @author yweijian
 * @date 2023年1月10日
 * @version 2.0.0
 * @description
 */
@Api(value = "扫码授权管理", tags = "扫码授权管理")
@RestController
@RequestMapping(AuthConstant.API_CONTENT_PATH)
public class ScanController {

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private ISysLoginService loginService;

    @Autowired
    private IScanQRLoginService scanQRLoginService;

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    @ApiOperation("获取授权地址")
    @GetMapping("/scan/authorize")
    public Mono<JsonResult> authorize(@ApiParam(value = "扫码登录模式", required = true)  String mode, @ApiIgnore ServerHttpRequest request) {
        String clientId = scanQRLoginService.getClientId(mode);
        String authorizeUri = scanQRLoginService.getAuthorizeUri(mode);
        String redirectUri = scanQRLoginService.getRedirectUri(mode);
        Properties properties = scanQRLoginService.getProperties(mode);

        ScanResult result = new ScanResult();
        result.setScanMode(mode);
        result.setClientId(clientId);
        String random = RandomUtil.randomString(6);
        String cacheKey = StringUtils.format(CommonConstant.USER_LOGIN_VERIFYCODE_CACHEKEY, random);
        tenantRedisTemplate.opsForValue().set(cacheKey, random, 5L, TimeUnit.MINUTES);
        result.setState(mode + "_" + random);
        result.setAuthorizeUri(authorizeUri.replace("STATE", result.getState()));
        result.setRedirectUri(redirectUri);

        //企业微信
        result.setAgentId(properties.getProperty("agent-id", "AGENTID"));

        return Mono.just(JsonResult.success(result));
    }

    @ApiOperation("用户扫码登录")
    @PostMapping("/scan/login")
    public Mono<JsonResult> login(@RequestBody @Validated AuthParam param, @ApiIgnore ServerHttpRequest request) {
        String cacheKey = StringUtils.format(CommonConstant.USER_LOGIN_VERIFYCODE_CACHEKEY, param.getRandom());
        String random = (String) tenantRedisTemplate.opsForValue().get(cacheKey);
        if(StringUtils.isEmpty(param.getRandom()) || !param.getRandom().equals(random)) {
            return Mono.just(JsonResult.error());
        }
        SysUser user = loginService.login(param.getMode(), param.getCode(), request);
        LoginUser loginUser = LoginUtils.buildLoginUser(user, request);
        List<String> permissions = menuService.selectUserAuthBtnPerms(user);
        LoginResult result = LoginUtils.buildLoginResult(CommonConstant.LEVEL_TOKEN_FIRST, loginUser, permissions, tenantRedisTemplate);
        return Mono.just(JsonResult.success(result));
    }

    @ApiOperation("用户扫码绑定")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @PostMapping("/scan/bind")
    public Mono<JsonResult> bind(@RequestBody @Validated AuthParam param, @ApiIgnore ServerHttpRequest request) {
        String cacheKey = StringUtils.format(CommonConstant.USER_LOGIN_VERIFYCODE_CACHEKEY, param.getRandom());
        String random = (String) tenantRedisTemplate.opsForValue().get(cacheKey);
        if(StringUtils.isEmpty(param.getRandom()) || !param.getRandom().equals(random)) {
            return Mono.just(JsonResult.error());
        }
        loginService.bind(param.getMode(), param.getCode(), request);
        return Mono.just(JsonResult.success());
    }

}
