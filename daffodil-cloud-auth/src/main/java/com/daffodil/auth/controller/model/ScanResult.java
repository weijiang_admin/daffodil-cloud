package com.daffodil.auth.controller.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2023年1月10日
 * @version 2.0.0
 * @description
 */
@ApiModel(value = "扫码登录信息")
@Data
public class ScanResult {

    @ApiModelProperty(name = "scanMode", value = "扫码登录模式")
    private String scanMode;

    @ApiModelProperty(name = "clientId", value = "应用ID,clientId,appId")
    private String clientId;

    @ApiModelProperty(name = "authorizeUri", value = "应用授权地址")
    private String authorizeUri;

    @ApiModelProperty(name = "redirectUri", value = "应用回调地址")
    private String redirectUri;

    @ApiModelProperty(name = "state", value = "状态码")
    private String state;
    
    /** 企业微信特有参数 */
    @ApiModelProperty(name = "agentId", value = "企业微信授权方的网页应用ID码")
    private String agentId;
}
