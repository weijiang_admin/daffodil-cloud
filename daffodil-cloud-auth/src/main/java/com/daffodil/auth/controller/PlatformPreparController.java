package com.daffodil.auth.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.auth.constant.AuthConstant;
import com.daffodil.auth.controller.model.PlatformPrepar;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.util.StringUtils;
import com.daffodil.util.sm.SM2Key;
import com.daffodil.util.sm.SM2Utils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2022年5月27日
 * @version 1.0
 * @description
 */
@Api(value = "平台预备数据管理", tags = "平台预备数据管理")
@RestController
@RequestMapping(AuthConstant.API_CONTENT_PATH)
public class PlatformPreparController {

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    @ApiOperation("获取平台预备数据")
    @GetMapping("/platform/prepar")
    public Mono<JsonResult> prepar(@ApiIgnore ServerHttpRequest request) {
        Calendar calendar = Calendar.getInstance();
        long expireTime = 24 * 3600L - calendar.get(Calendar.HOUR_OF_DAY) * 3600L - calendar.get(Calendar.MINUTE) * 60L - calendar.get(Calendar.SECOND);
        String publicKey = (String) redisTemplate.opsForValue().get(CommonConstant.SM2_PUBLIC_KEY_CACHEKEY);
        if(StringUtils.isNotEmpty(publicKey)) {
            PlatformPrepar prepar = new PlatformPrepar(publicKey, expireTime, new Date());
            return Mono.just(JsonResult.success(prepar));
        }

        SM2Key sm2 = SM2Utils.genSM2Key(false);
        redisTemplate.opsForValue().set(CommonConstant.SM2_PUBLIC_KEY_CACHEKEY, sm2.getPublicKey(), expireTime, TimeUnit.SECONDS);
        redisTemplate.opsForValue().set(CommonConstant.SM2_PRIVATE_KEY_CACHEKEY, sm2.getPrivateKey(), expireTime, TimeUnit.SECONDS);
        PlatformPrepar prepar = new PlatformPrepar(sm2.getPublicKey(), expireTime, new Date());
        return Mono.just(JsonResult.success(prepar));
    }
}
