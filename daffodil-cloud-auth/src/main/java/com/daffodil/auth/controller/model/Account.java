package com.daffodil.auth.controller.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2022年3月4日
 * @version 1.0
 * @description
 */
@ApiModel(value = "登录账号密码")
@Data
public class Account {
    
    @ApiModelProperty(name = "loginType", value = "登录类型")
    private Integer loginType = LoginType.ACCOUNT.value();

    @ApiModelProperty(name = "loginName", value = "账号名称，需要国密sm2加密")
    private String loginName;

    @ApiModelProperty(name = "password", value = "登录密码，需要国密sm2加密")
    private String password;
    
    @ApiModelProperty(name = "verifyKey", value = "登录验证钥匙")
    private String verifyKey;
    
    @ApiModelProperty(name = "verifyCode", value = "登录验证码")
    private String verifyCode;
    
    /**
     * -登录方式
     * @author yweijian
     * @date 2022年9月9日
     * @version 2.0.0
     * @description
     */
    public static enum LoginType {
        
        /** 账号密码登录 */
        ACCOUNT(1),
        /** 动态验证码登录 */
        VERIFYCODE(2),
        /** 扫码登录 */
        SCANCODE(3),
        /** 账号密码+动态验证码登录 */
        ACCOUNT_VERIFYCODE(4);
        
        private final Integer value;

        LoginType(Integer value) {
            this.value = value;
        }

        public Integer value() {
            return this.value;
        }
    }
}
