package com.daffodil.auth.controller;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.auth.constant.AuthConstant;
import com.daffodil.auth.controller.model.Account;
import com.daffodil.auth.service.ISysUserService;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.util.JwtTokenUtils;
import com.daffodil.util.StringUtils;
import com.daffodil.util.sm.SM2Utils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * -密码找回管理
 * @author yweijian
 * @date 2022年11月22日
 * @version 2.0.0
 * @description
 */
@Api(value = "密码找回管理", tags = "密码找回管理")
@RestController
@RequestMapping(AuthConstant.API_CONTENT_PATH)
public class ForgetPasswordController {

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    @Autowired
    private ISysUserService userService;

    @ApiOperation("动态验证码验证")
    @PostMapping("/forget/password/verify")
    public Mono<JsonResult> verify(@RequestBody Account account, @ApiIgnore ServerHttpRequest request){
        String privateKey = (String) redisTemplate.opsForValue().get(CommonConstant.SM2_PRIVATE_KEY_CACHEKEY);
        String loginName = SM2Utils.decryptData(privateKey, account.getLoginName());
        String verifyCode = account.getVerifyCode();
        this.verifyCode(loginName, verifyCode);

        String verifyKey = JwtTokenUtils.uuid();
        String forgetToken = JwtTokenUtils.sign(verifyKey, loginName);
        String cacheKey = StringUtils.format(CommonConstant.USER_TOKEN_VERIFY_CACHEKEY, verifyKey);
        tenantRedisTemplate.opsForValue().set(cacheKey, forgetToken, 5L, TimeUnit.MINUTES);

        return Mono.just(JsonResult.success(JsonResult.SUCCESS_MSG, verifyKey));
    }

    @ApiOperation("重置用户密码")
    @PostMapping("/forget/password/reset")
    public Mono<JsonResult> reset(@RequestBody Account account, @ApiIgnore ServerHttpRequest request){
        String privateKey = (String) redisTemplate.opsForValue().get(CommonConstant.SM2_PRIVATE_KEY_CACHEKEY);
        String loginName = SM2Utils.decryptData(privateKey, account.getLoginName());
        String password = SM2Utils.decryptData(privateKey, account.getPassword());
        String verifyKey = account.getVerifyKey();

        String cacheKey = StringUtils.format(CommonConstant.USER_TOKEN_VERIFY_CACHEKEY, verifyKey);
        String forgetToken = (String) tenantRedisTemplate.opsForValue().get(cacheKey);

        if(StringUtils.isEmpty(loginName) 
                || StringUtils.isEmpty(verifyKey) 
                || StringUtils.isEmpty(forgetToken) 
                || !loginName.equals(JwtTokenUtils.getData(forgetToken)) 
                || !verifyKey.equals(JwtTokenUtils.getSecret(forgetToken))) {
            throw new BaseException(31013);
        }

        redisTemplate.delete(cacheKey);
        userService.resetUserPassword(loginName, password);
        return Mono.just(JsonResult.success());
    }

    /**
     * -校验动态验证码
     * @param loginName
     * @param verifyCode
     */
    private void verifyCode(String loginName, String verifyCode) {
        if(StringUtils.isEmpty(loginName) || StringUtils.isEmpty(verifyCode)) {
            throw new BaseException(31008);
        }
        String cacheKey = StringUtils.format(CommonConstant.USER_LOGIN_VERIFYCODE_CACHEKEY, loginName);
        String cacheCode = (String) tenantRedisTemplate.opsForValue().get(cacheKey);
        if(!verifyCode.equals(cacheCode)) {
            throw new BaseException(31008);
        }
        redisTemplate.delete(cacheKey);
    }
}
