package com.daffodil.auth.controller;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.auth.constant.AuthConstant;
import com.daffodil.auth.controller.model.Account;
import com.daffodil.auth.controller.model.MenuRouter;
import com.daffodil.auth.service.ISysMenuService;
import com.daffodil.auth.service.ISysUserService;
import com.daffodil.auth.util.LoginUtils;
import com.daffodil.auth.util.RouterUtils;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.entity.SysMenu;
import com.daffodil.system.entity.SysUser;
import com.daffodil.util.JwtTokenUtils;
import com.daffodil.util.PasswordUtils;
import com.daffodil.util.StringUtils;
import com.daffodil.util.sm.SM2Utils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2022年5月27日
 * @version 1.0
 * @description
 */
@Api(value = "登录用户管理", tags = "登录用户管理")
@RestController
@RequestMapping(AuthConstant.API_CONTENT_PATH)
public class LoginUserController {

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    @ApiOperation("用户操作密码二次验证")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @PostMapping("/password/verify")
    public Mono<JsonResult> verify(@RequestBody Account account, @ApiIgnore ServerHttpRequest request) {
        String privateKey = (String) redisTemplate.opsForValue().get(CommonConstant.SM2_PRIVATE_KEY_CACHEKEY);
        String newPassword = SM2Utils.decryptData(privateKey , account.getPassword());
        LoginUser loginUser = LoginUtils.getLoginUser(request);
        SysUser user = userService.selectUserById(loginUser.getId());
        String password = PasswordUtils.encryptPassword(user.getLoginName(), newPassword, user.getSalt());
        if(user.getPassword().equals(password)) {
            String accessToken = LoginUtils.getAccessToken(request);
            String verifyToken = JwtTokenUtils.uuid();
            String cacheKey = StringUtils.format(CommonConstant.USER_TOKEN_VERIFY_CACHEKEY, accessToken);
            tenantRedisTemplate.opsForValue().set(cacheKey, verifyToken, 5L, TimeUnit.MINUTES);
            return Mono.just(JsonResult.success(JsonResult.SUCCESS_MSG, verifyToken));
        }
        return Mono.just(JsonResult.error());
    }

    @ApiOperation("获取登录用户信息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/user/info")
    public Mono<JsonResult> userInfo(@ApiIgnore ServerHttpRequest request) {
        LoginUser loginUser = LoginUtils.getLoginUser(request);
        SysUser user = userService.selectUserById(loginUser.getId());
        user.setSalt(null);
        user.setPassword(null);
        return Mono.just(JsonResult.success(user));
    }

    @ApiOperation("获取用户授权页面")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/user/auth/page")
    public Mono<JsonResult> authPage(@ApiIgnore ServerHttpRequest request) {
        LoginUser loginUser = LoginUtils.getLoginUser(request);
        SysUser user = userService.selectUserById(loginUser.getId());
        List<String> pages = menuService.selectUserAuthPagePerms(user);
        return Mono.just(JsonResult.success(pages));
    }

    @ApiOperation("获取用户授权操作")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/user/auth/button")
    public Mono<JsonResult> authButton(@ApiIgnore ServerHttpRequest request) {
        String accessToken = LoginUtils.getAccessToken(request);
        String cacheKey = StringUtils.format(CommonConstant.PERMISSION_TOKEN_CACHEKEY, accessToken);
        Set<Object> result = tenantRedisTemplate.opsForSet().members(cacheKey);
        if(StringUtils.isNotNull(result)) {
            return Mono.just(JsonResult.success(result));
        }

        LoginUser loginUser = LoginUtils.getLoginUser(request);
        SysUser user = userService.selectUserById(loginUser.getId());
        List<String> permissions = menuService.selectUserAuthBtnPerms(user);
        tenantRedisTemplate.opsForSet().add(cacheKey, permissions.toArray());

        return Mono.just(JsonResult.success(permissions));
    }

    @ApiOperation("获取用户路由菜单")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/user/router/menu")
    public Mono<JsonResult> menu(@ApiIgnore ServerHttpRequest request) {
        LoginUser loginUser = LoginUtils.getLoginUser(request);
        SysUser user = userService.selectUserById(loginUser.getId());
        List<SysMenu> menus = menuService.selectUserMenuList(user);
        List<MenuRouter> routers = RouterUtils.changeToRouter(menus);
        return Mono.just(JsonResult.success(routers));
    }
}
