package com.daffodil.auth.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.auth.constant.AuthConstant;
import com.daffodil.auth.controller.model.Account;
import com.daffodil.auth.controller.model.LoginConfig;
import com.daffodil.auth.properties.KaptchaProperties;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.model.ConfigLogin;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.StringUtils;
import com.daffodil.util.sm.SM2Utils;
import com.google.code.kaptcha.Producer;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2022年5月27日
 * @version 1.0
 * @description
 */
@Slf4j
@Api(value = "验证码管理", tags = "验证码管理")
@RestController
@RequestMapping(AuthConstant.API_CONTENT_PATH)
public class KaptchaController {

    @Resource(name = "mathProducer")
    private Producer mathProducer;

    @Resource(name = "textProducer")
    private Producer textProducer;

    @Autowired
    private KaptchaProperties kaptchaProperties;

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    @ApiOperation("获取图片验证码")
    @GetMapping("/kaptcha/{verifyKey}")
    public Mono<Void> kaptcha(@ApiParam(value = "验证码标识", required = true) @PathVariable("verifyKey") String verifyKey, @ApiIgnore ServerHttpRequest request, @ApiIgnore ServerHttpResponse response) {
        if(!kaptchaProperties.getEnable()) {
            return Mono.empty();
        }
        String code = null;
        BufferedImage img = null;
        double random = Math.floor(Math.random() * 10);
        if (random < 5) {//验证码类型随机生成
            //if (AuthConstant.DEFAULT_KAPTCHA_TEXT.equalsIgnoreCase(kaptchaProperties.getText())) {
            String text = mathProducer.createText();
            String kaptcha = text.substring(0, text.lastIndexOf("@"));
            code = text.substring(text.lastIndexOf("@") + 1);
            img = mathProducer.createImage(kaptcha);
        } else {
            String kaptcha = code = textProducer.createText();
            img = textProducer.createImage(kaptcha);
        }
        response.getHeaders().setContentType(MediaType.IMAGE_PNG);

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            ImageIO.write(img, "png", out);
            String cacheKey = StringUtils.format(CommonConstant.USER_LOGIN_KAPTCHA_CACHEKEY, verifyKey);
            redisTemplate.opsForValue().set(cacheKey, code, 5L, TimeUnit.MINUTES);
            DataBuffer dataBuffer = response.bufferFactory().wrap(out.toByteArray());
            return response.writeWith(Mono.just(dataBuffer));
        } catch (IOException ignore) {

        }
        return Mono.empty();
    }

    @ApiOperation("发送短信验证码或邮箱验证码")
    @PostMapping("/kaptcha/send")
    public Mono<JsonResult> kaptcha(@RequestBody Account account, @ApiIgnore ServerHttpRequest request){
        String privateKey = (String) redisTemplate.opsForValue().get(CommonConstant.SM2_PRIVATE_KEY_CACHEKEY);
        this.sendVerifyCode(SM2Utils.decryptData(privateKey, account.getLoginName()));
        return Mono.just(JsonResult.success());
    }

    /**
     * -发送验证码
     * @param account
     */
    private void sendVerifyCode(String account) {
        if(StringUtils.isEmpty(account) || (!account.matches(CommonConstant.MOBILE_PHONE_NUMBER_PATTERN) && !account.matches(CommonConstant.EMAIL_PATTERN))) {
            throw new BaseException(31009);
        }
        
        ConfigLogin config = (ConfigLogin) redisTemplate.opsForValue().get(CommonConstant.CONFIG_LOGIN_CACHEKEY);
        LoginConfig login = new LoginConfig().convert(config);
        
        if(account.matches(CommonConstant.MOBILE_PHONE_NUMBER_PATTERN)) {
            if(!login.getDynamicEnable() && !login.getMobileEnable()) {
                throw new BaseException(31010);
            }
            String senduri = kaptchaProperties.getSendSmsUri();
            Integer length = config.getMobile().getLength();
            Integer expireTime = config.getMobile().getExpireTime();
            String subject = config.getMobile().getSubject();
            String template = config.getMobile().getTemplate();
            String templateId = config.getMobile().getTemplateId();
            String templateParam = config.getMobile().getTemplateParam();
            this.sendVerifyCode(senduri, account, subject, template, templateId, templateParam, length, expireTime);
        }
        if(account.matches(CommonConstant.EMAIL_PATTERN)) {
            if(!login.getDynamicEnable() && !login.getEmailEnable()) {
                throw new BaseException(31010);
            }
            String senduri = kaptchaProperties.getSendEmailUri();
            Integer length = config.getEmail().getLength();
            Integer expireTime = config.getEmail().getExpireTime();
            String subject = config.getEmail().getSubject();
            String template = config.getEmail().getTemplate();
            this.sendVerifyCode(senduri, account, subject, template, "", "", length, expireTime);
        }
    }
    
    /**
     * 
     * @param senduri
     * @param account
     * @param subject
     * @param template
     * @param templateId
     * @param templateParam
     * @param length
     * @param expireTime
     */
    private void sendVerifyCode(String senduri, String account, String subject, String template, String templateId, String templateParam, int length, int expireTime) {
        String code = RandomUtil.randomNumbers(length);
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("account", account);
        variables.put("code", code);
        variables.put("date", DateUtil.format(new Date(), "yyyy-MM-dd"));
        variables.put("datetime", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        template = StringUtils.juelExpression(template, variables);
        templateParam = StringUtils.juelExpression(templateParam, variables);

        Map<String, Object> json = new HashMap<String, Object>();
        json.put("account", account);
        json.put("subject", subject);
        json.put("content", template);
        json.put("templateId", templateId);
        json.put("templateParam", templateParam);
        String body = JacksonUtils.toJSONString(json);
        
        String cacheKey = StringUtils.format(CommonConstant.USER_LOGIN_VERIFYCODE_CACHEKEY, account);
        long cacheExpireTime = redisTemplate.opsForValue().getOperations().getExpire(cacheKey);
        if(expireTime * 60 - cacheExpireTime <= 10) {
            throw new BaseException(31012);
        }
        
        HttpResponse response = null;
        try {
            response = HttpRequest.post(senduri).body(body, MediaType.APPLICATION_JSON_VALUE).execute();
        }catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new BaseException(31011);
        }
        if(response == null || response.getStatus() != HttpStatus.OK.value()) {
            throw new BaseException(31011);
        }
        redisTemplate.opsForValue().set(cacheKey, code, expireTime, TimeUnit.MINUTES);
    }

}
