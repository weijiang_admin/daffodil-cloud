package com.daffodil.auth.controller.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Data;

/**
 * Router 系统菜单路由
 * @author yweijian
 * @date 2021年9月26日
 * @version 1.0
 * @description
 */
@Data
public class MenuRouter implements Serializable {

    private static final long serialVersionUID = -5223927170171841082L;
    
    private String id;
    
    private String parentId;
    
    /** 路由名称 */
    private String name;
    
    /** 路由标识 必须以反斜杠:[ / ]开头 */
    private String path;
    
    /** 路由组件 */
    private String component;
    
    /** 重定向路由地址 */
    private String redirect;
    
    /** 路由元数据 */
    private Map<String, Object> meta;
    
    /** 嵌套子路由 */
    private List<MenuRouter> children;
}
