package com.daffodil.auth.controller.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2022年3月4日
 * @version 1.0
 * @description
 */
@ApiModel(value = "登录结果信息")
@Data
@AllArgsConstructor
public class LoginResult {

    @ApiModelProperty(name = "accessToken", value = "登录授权令牌")
    private String accessToken;

    @ApiModelProperty(name = "expireTime", value = "授权令牌有效时间")
    private Long expireTime;

    @ApiModelProperty(name = "refreshToken", value = "刷新授权令牌")
    private String refreshToken;

    @ApiModelProperty(name = "createTime", value = "授权令牌生成时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
