package com.daffodil.auth.controller;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.auth.constant.AuthConstant;
import com.daffodil.auth.controller.model.LoginResult;
import com.daffodil.auth.controller.model.OauthUser;
import com.daffodil.auth.service.ISysThirdAppService;
import com.daffodil.auth.service.ISysUserService;
import com.daffodil.auth.util.LoginUtils;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.annotation.OpenPermission;
import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.framework.model.LoginApp;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.entity.SysThirdApp;
import com.daffodil.system.entity.SysUser;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.JwtTokenUtils;
import com.daffodil.util.StringUtils;

import cn.hutool.crypto.symmetric.AES;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * -第三方应用授权登录管理
 * @author yweijian
 * @date 2023年1月3日
 * @version 2.0.0
 * @description
 */
@Api(value = "第三方应用授权登录管理", tags = "第三方应用授权登录管理")
@RestController
@RequestMapping(AuthConstant.API_CONTENT_PATH)
public class OauthController {

    @Autowired
    private ISysThirdAppService thirdAppService;

    @Autowired
    private ISysUserService userservice;

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    @ApiOperation("获取第三方应用基本信息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/oauth/info")
    public Mono<JsonResult> info(@ApiParam(value = "应用ID") String appId, @ApiIgnore ServerHttpRequest request) {
        if(StringUtils.isEmpty(appId)) {
            throw new BaseException(31101);
        }
        SysThirdApp app = thirdAppService.selectThirdAppById(appId);
        app.setAppSecret(null);
        return Mono.just(JsonResult.success(app));
    }

    @ApiOperation("获取第三方应用授权码")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/oauth/grant")
    public Mono<JsonResult> grant(@ApiParam(value = "应用ID") String appId, @ApiIgnore ServerHttpRequest request) throws NoSuchAlgorithmException {
        if(StringUtils.isEmpty(appId)) {
            throw new BaseException(31101);
        }
        SysThirdApp app = thirdAppService.selectThirdAppById(appId);
        LoginUser loginUser = LoginUtils.getLoginUser(request);
        if(app == null || loginUser == null) {
            throw new BaseException(31101);
        }
        LoginApp loginApp = LoginUtils.buildLoginApp(app, request);
        String code = JwtTokenUtils.uuid();
        String cacheKey = StringUtils.format(CommonConstant.THIRDAPP_OAUTH_CODE_CACHEKEY, code);
        String encryptData = JacksonUtils.toJSONString(loginApp);
        AES aes = LoginUtils.getAes(app.getId() + app.getAppSecret());
        String encryptStr = aes.encryptHex(encryptData, StandardCharsets.UTF_8);
        tenantRedisTemplate.opsForValue().set(cacheKey, encryptStr, 10L, TimeUnit.MINUTES);
        return Mono.just(JsonResult.success(JsonResult.SUCCESS_MSG, code));
    }

    @ApiOperation("根据应用授权码获取令牌")
    @GetMapping("/oauth/token")
    public Mono<JsonResult> token(@ApiParam(value = "应用ID") String appId, @ApiParam(value = "应用秘钥") String appSecret, @ApiParam(value = "授权码") String code, @ApiIgnore ServerHttpRequest request) throws NoSuchAlgorithmException {
        if(StringUtils.isEmpty(appId)) {
            throw new BaseException(31101);
        }
        if(StringUtils.isEmpty(appSecret)) {
            throw new BaseException(31102);
        }
        String cacheKey = StringUtils.format(CommonConstant.THIRDAPP_OAUTH_CODE_CACHEKEY, code);
        Object object = tenantRedisTemplate.opsForValue().get(cacheKey);
        if(null == object) {
            throw new BaseException(31103);
        }
        AES aes = LoginUtils.getAes(appId + appSecret);
        String decryptStr = aes.decryptStr((String) object, StandardCharsets.UTF_8);
        LoginApp loginApp = JacksonUtils.toJavaObject(decryptStr, LoginApp.class);
        if(null == loginApp || !appId.equals(loginApp.getAppId())) {
            throw new BaseException(31103);
        }

        List<String> permissions = thirdAppService.selectAppAuthPerms(appId);
        LoginResult result = LoginUtils.buildLoginResult(CommonConstant.LEVEL_TOKEN_SECOND, loginApp, permissions, tenantRedisTemplate);
        tenantRedisTemplate.delete(cacheKey);

        return Mono.just(JsonResult.success(result));
    }

    @ApiOperation("刷新登录应用授权令牌")
    @ApiImplicitParam(name = FrameworkConstant.X_REQUEST_REFRESH_TOKEN_HEADER, value = "刷新授权令牌  token", paramType = "header", required = true)
    @GetMapping("/oauth/refresh")
    public Mono<JsonResult> refresh(@ApiIgnore ServerHttpRequest request) {
        String refreshToken = LoginUtils.getRefreshToken(request);
        if(StringUtils.isEmpty(refreshToken)) {
            throw new BaseException(31006);
        }

        String refreshTokenCackekey = StringUtils.format(CommonConstant.REFRESH_TOKEN_CACHEKEY, refreshToken);
        String accessToken = (String) tenantRedisTemplate.opsForValue().get(refreshTokenCackekey);
        if(StringUtils.isEmpty(accessToken)) {
            throw new BaseException(31006);
        }

        String accessTokenCachekey = StringUtils.format(CommonConstant.ACCESS_TOKEN_CACHEKEY, accessToken);
        String authorToken = (String) tenantRedisTemplate.opsForValue().get(accessTokenCachekey);
        String data = JwtTokenUtils.getData(authorToken);
        LoginApp loginApp = JacksonUtils.toJavaObject(data, LoginApp.class);
        if(StringUtils.isNull(loginApp)) {
            throw new BaseException(31006);
        }

        LoginUtils.clearLoginResult(accessToken, tenantRedisTemplate);
        List<String> permissions = thirdAppService.selectAppAuthPerms(loginApp.getAppId());
        LoginResult result = LoginUtils.buildLoginResult(CommonConstant.LEVEL_TOKEN_FIRST, loginApp, permissions, tenantRedisTemplate);
        return Mono.just(JsonResult.success(result));
    }

    /**
     * -根据授权认证令牌获取用户信息
     * @param request /api-auth/oauth/user?access_token=ACCESS_TOKEN or Set request headers Authorization: Bearer [ACCESS_TOKEN]
     * @return
     */
    @ApiOperation("根据授权认证令牌获取用户信息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @OpenPermission("oauth:user:info")
    @GetMapping("/oauth/user")
    public Mono<JsonResult> user(@ApiIgnore ServerHttpRequest request) {
        LoginApp loginApp = LoginUtils.getLoginApp(request);
        if(null == loginApp) {
            return Mono.just(JsonResult.success());
        }
        SysThirdApp app = thirdAppService.selectThirdAppById(loginApp.getAppId());
        SysUser user = userservice.selectUserById(loginApp.getId());
        if(StringUtils.isNull(app) || StringUtils.isNull(user)) {
            return Mono.just(JsonResult.success());
        }
        OauthUser info = LoginUtils.getUserInfoByScope(user, app.getScopes());
        return Mono.just(JsonResult.success(info));
    }

}
