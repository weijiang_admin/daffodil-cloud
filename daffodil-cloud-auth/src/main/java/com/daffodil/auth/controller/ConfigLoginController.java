package com.daffodil.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.auth.constant.AuthConstant;
import com.daffodil.auth.controller.model.LoginConfig;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.properties.FrameworkProperties;
import com.daffodil.system.model.ConfigLogin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2022年9月9日
 * @version 2.0.0
 * @description
 */
@Api(value = "登录设置管理", tags = "登录设置管理")
@RestController
@RequestMapping(AuthConstant.API_CONTENT_PATH)
public class ConfigLoginController {

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private FrameworkProperties frameworkProperties;

    @ApiOperation("获取登录设置")
    @GetMapping("/config/login")
    public Mono<JsonResult> config(@ApiIgnore ServerHttpRequest request) {
        ConfigLogin config = (ConfigLogin) redisTemplate.opsForValue().get(CommonConstant.CONFIG_LOGIN_CACHEKEY);
        LoginConfig loginConfig = new LoginConfig().convert(config);
        loginConfig.setTenantEnable(frameworkProperties.getTenantEnable());
        return Mono.just(JsonResult.success(loginConfig));
    }
}
