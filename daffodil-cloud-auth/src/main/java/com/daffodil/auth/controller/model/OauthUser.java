package com.daffodil.auth.controller.model;

import java.util.List;

import com.daffodil.system.entity.SysDept;
import com.daffodil.system.entity.SysRank;
import com.daffodil.system.entity.SysRole;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * Oauth授权用户
 * @author yweijian
 * @date 2023年1月5日
 * @version 2.0.0
 * @description
 */
@ApiModel(value = "Oauth授权用户")
@Data
public class OauthUser {

    private String id;
    
    private String deptId;
    
    private String loginName;
    
    private String userName;
    
    private String email;
    
    private String phone;
    
    private String sex;
    
    private String avatar;
    
    private SysDept dept;
    
    private List<SysRole> roles;
    
    private String[] roleIds;
    
    private String[] postIds;
    
    private List<SysRank> ranks;
    
    private String[] rankIds;
}
