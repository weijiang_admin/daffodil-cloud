package com.daffodil.auth.factory.strategy;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.http.MediaType;

import com.daffodil.auth.factory.model.SocialUser;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.StringUtils;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * 微信扫码授权登录
 * @author yweijian
 * @date 2023年1月10日
 * @version 2.0.0
 * @description 微信开放平台必须完整的网站审核申请，麻烦又费劲，如需请自行查阅官网文档补充实现。
 */
@Slf4j
public class WeixinScanQRLoginStrategy extends ScanQRLoginStrategy {

    private static final String CLIENT_ID = "CLIENT_ID";

    private static final String CLIENT_SECRET = "CLIENT_SECRET";

    private static final String AUTHORIZE_URI = "https://open.weixin.qq.com/connect/qrconnect?appid=CLIENT_ID&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_login&state=STATE#wechat_redirec";

    private static final String REDIRECT_URI = "REDIRECT_URI";

    private static final String OAUTH_TOKEN_URI = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=CLIENT_ID&secret=CLIENT_SECRET&code=CODE&grant_type=authorization_code";

    private static final String OAUTH_USERINFO_URI = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID";

    private final ConcurrentHashMap<String, String> openIdMap = new ConcurrentHashMap<String, String>();

    public WeixinScanQRLoginStrategy(Properties properties) {
        super(properties);
    }

    @Override
    public String getClientId() {
        return this.properties.getProperty("client-id", CLIENT_ID);
    }

    @Override
    public String getClientSecret() {
        return this.properties.getProperty("client-secret", CLIENT_SECRET);
    }

    @Override
    public String getAuthorizeUri() {
        String authorizeUri = this.properties.getProperty("authorize-uri", AUTHORIZE_URI);
        authorizeUri = authorizeUri.replace(CLIENT_ID, this.getClientId());
        authorizeUri = authorizeUri.replace(REDIRECT_URI, URLEncoder.encode(this.getRedirectUri(), StandardCharsets.UTF_8));
        return authorizeUri;
    }

    @Override
    public String getRedirectUri() {
        return this.properties.getProperty("redirect-uri", REDIRECT_URI);
    }

    @Override
    @SuppressWarnings("unchecked")
    public String getUserAccessToken(String code) {
        String url = OAUTH_TOKEN_URI.replace(CLIENT_ID, this.getClientId())
                .replace(CLIENT_SECRET, this.getClientSecret())
                .replace("CODE", code);
        HttpResponse response = HttpRequest.get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .charset(StandardCharsets.UTF_8)
                .execute();

        if(!response.isOk() && log.isInfoEnabled()) {
            log.info("Get user access token uri={} status={} result={}", 
                    OAUTH_TOKEN_URI, response.getStatus(), response.body());
        }

        Map<String, Object> result = JacksonUtils.toJavaObject(response.body(), Map.class);
        String accessToken = JacksonUtils.getAsString(result, "access_token");
        String openid = JacksonUtils.getAsString(result, "openid");
        if(StringUtils.isNotEmpty(accessToken) && StringUtils.isNotEmpty(openid)) {
            this.openIdMap.put(accessToken, openid);
        }
        return accessToken;
    }

    @Override
    @SuppressWarnings("unchecked")
    public SocialUser getUserInfo(String accessToken) {
        String url = OAUTH_USERINFO_URI.replace("ACCESS_TOKEN", accessToken).replace("OPENID", openIdMap.get(accessToken));
        this.openIdMap.remove(accessToken);
        HttpResponse response = HttpRequest.get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .charset(StandardCharsets.UTF_8)
                .execute();
        
        if(!response.isOk() && log.isInfoEnabled()) {
            log.info("Get user info uri={} status={} result={}", 
                    OAUTH_USERINFO_URI, response.getStatus(), response.body());
        }

        Map<String, String> result = JacksonUtils.toJavaObject(response.body(), Map.class);
        SocialUser socialUser = new SocialUser();
        socialUser.putAll(result);
        socialUser.setUnionId(result.get("unionid"));
        socialUser.setOpenId(result.get("openid"));
        socialUser.setAvatar(result.get("headimgurl"));
        socialUser.setUserName(result.get("nickname"));

        return socialUser;
    }

}
