package com.daffodil.auth.factory.model;

import java.util.HashMap;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 第三方应用社会用户身份信息
 * @author yweijian
 * @date 2023年1月11日
 * @version 2.0.0
 * @description 使用继承HashMap，除了定义的公共用户参数信息以外，更方便不同应用响应用户参数信息扩展。
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SocialUser extends HashMap<String, Object> {

    private static final long serialVersionUID = 2489771647495953630L;

    /** 用户统一ID，在同一租户开发的所有应用内的唯一标识 */
    private String unionId;

    /** 用户在应用内的唯一标识 */
    private String openId;

    /** 用户姓名 */
    private String userName;

    /** 用户头像  picture */
    private String avatar;

    /** 邮箱账号 */
    private String email;

    /** 手机号码  phone */
    private String mobile;

}
