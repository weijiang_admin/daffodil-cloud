package com.daffodil.auth.factory.strategy;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import com.aliyun.dingtalkcontact_1_0.models.GetUserHeaders;
import com.aliyun.dingtalkcontact_1_0.models.GetUserResponseBody;
import com.aliyun.dingtalkoauth2_1_0.models.GetUserTokenRequest;
import com.aliyun.dingtalkoauth2_1_0.models.GetUserTokenResponse;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import com.daffodil.auth.factory.model.SocialUser;
import com.daffodil.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 钉钉扫码授权登录
 * @author yweijian
 * @date 2023年1月10日
 * @version 2.0.0
 * @description
 */
@Slf4j
public class DingDingScanQRLoginStrategy extends ScanQRLoginStrategy {

    private static final String CLIENT_ID = "CLIENT_ID";

    private static final String CLIENT_SECRET = "CLIENT_SECRET";

    private static final String AUTHORIZE_URI = "https://login.dingtalk.com/oauth2/auth?client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&response_type=code&scope=openid&state=STATE&prompt=consent";

    private static final String REDIRECT_URI = "REDIRECT_URI";

    public DingDingScanQRLoginStrategy(Properties properties) {
        super(properties);
    }

    private static com.aliyun.dingtalkoauth2_1_0.Client authClient() throws Exception {
        Config config = new Config();
        config.protocol = "https";
        config.regionId = "central";
        return new com.aliyun.dingtalkoauth2_1_0.Client(config);
    }
    
    private static com.aliyun.dingtalkcontact_1_0.Client contactClient() throws Exception {
        Config config = new Config();
        config.protocol = "https";
        config.regionId = "central";
        return new com.aliyun.dingtalkcontact_1_0.Client(config);
    }

    @Override
    public String getClientId() {
        return this.properties.getProperty("client-id", CLIENT_ID);
    }

    @Override
    public String getClientSecret() {
        return this.properties.getProperty("client-secret", CLIENT_SECRET);
    }

    @Override
    public String getAuthorizeUri() {
        String authorizeUri = this.properties.getProperty("authorize-uri", AUTHORIZE_URI);
        authorizeUri = authorizeUri.replace(CLIENT_ID, this.getClientId());
        authorizeUri = authorizeUri.replace(REDIRECT_URI, URLEncoder.encode(this.getRedirectUri(), StandardCharsets.UTF_8));
        return authorizeUri;
    }

    @Override
    public String getRedirectUri() {
        return this.properties.getProperty("redirect-uri", REDIRECT_URI);
    }

    @Override
    public String getUserAccessToken(String code) {
        try {
            com.aliyun.dingtalkoauth2_1_0.Client client = authClient();
            GetUserTokenRequest getUserTokenRequest = new GetUserTokenRequest()
                    .setClientId(this.getClientId())
                    .setClientSecret(this.getClientSecret())
                    .setCode(code)
                    .setGrantType("authorization_code");

            GetUserTokenResponse getUserTokenResponse = client.getUserToken(getUserTokenRequest);
            String accessToken = getUserTokenResponse.getBody().getAccessToken();
            return accessToken;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public SocialUser getUserInfo(String accessToken) {
        try {
            com.aliyun.dingtalkcontact_1_0.Client client = contactClient();
            GetUserHeaders getUserHeaders = new GetUserHeaders();
            getUserHeaders.xAcsDingtalkAccessToken = accessToken;
            GetUserResponseBody result = client.getUserWithOptions("me", getUserHeaders, new RuntimeOptions()).getBody();
            
            SocialUser socialUser = new SocialUser();
            socialUser.setUnionId(result.getUnionId());
            socialUser.setOpenId(result.getOpenId());
            socialUser.setAvatar(result.getAvatarUrl());
            socialUser.setUserName(result.getNick());
            socialUser.setEmail(result.getEmail());
            String mobile = result.getMobile();
            //只处理国内电话
            if(StringUtils.isNotEmpty(mobile) && mobile.startsWith("+86")) {
                mobile = mobile.substring(3);
            }
            socialUser.setMobile(mobile);

            return socialUser;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return null;
    }

}
