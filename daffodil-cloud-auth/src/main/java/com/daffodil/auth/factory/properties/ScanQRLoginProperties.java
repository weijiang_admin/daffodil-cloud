package com.daffodil.auth.factory.properties;

import java.util.Properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author yweijian
 * @date 2023年1月11日
 * @version 2.0.0
 * @description
 */
@Setter
@Getter
@Component
@RefreshScope
@ConfigurationProperties(prefix = "daffodil.scanqr")
public class ScanQRLoginProperties {

    public Properties qq = new Properties();

    public Properties weixin = new Properties();

    public Properties qiyeweixin = new Properties();

    public Properties feishu = new Properties();

    public Properties dingding = new Properties();

}
