package com.daffodil.auth.factory.strategy;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.http.MediaType;

import com.daffodil.auth.factory.model.SocialUser;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.StringUtils;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * QQ扫码授权登录
 * @author yweijian
 * @date 2023年1月10日
 * @version 2.0.0
 * @description 成为腾讯开发者比较费劲，过分收集开发者信息，为此预留代码不实现，如需请自行实现。
 */
@Slf4j
public class QQScanQRLoginStrategy extends ScanQRLoginStrategy {

    private static final String CLIENT_ID = "CLIENT_ID";

    private static final String CLIENT_SECRET = "CLIENT_SECRET";

    private static final String AUTHORIZE_URI = "https://graph.qq.com/oauth2.0/authorize?client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&response_type=code&state=STATE&scope=get_user_info";

    private static final String REDIRECT_URI = "REDIRECT_URI";
    
    private static final String OAUTH_TOKEN_URI = "https://graph.qq.com/oauth2.0/token";

    private static final String OAUTH_OPENID_URI = "https://graph.qq.com/oauth2.0/me?access_token=ACCESS_TOKEN";
    
    private static final String OAUTH_USERINFO_URI = "https://graph.qq.com/user/get_user_info?openid=OPENID&oauth_consumer_key=CLIENT_ID&access_token=ACCESS_TOKEN&format=json";
    
    public QQScanQRLoginStrategy(Properties properties) {
        super(properties);
    }

    @Override
    public String getClientId() {
        return this.properties.getProperty("client-id", CLIENT_ID);
    }

    @Override
    public String getClientSecret() {
        return this.properties.getProperty("client-secret", CLIENT_SECRET);
    }

    @Override
    public String getAuthorizeUri() {
        String authorizeUri = this.properties.getProperty("authorize-uri", AUTHORIZE_URI);
        authorizeUri = authorizeUri.replace(CLIENT_ID, this.getClientId());
        authorizeUri = authorizeUri.replace(REDIRECT_URI, URLEncoder.encode(this.getRedirectUri(), StandardCharsets.UTF_8));
        return authorizeUri;
    }

    @Override
    public String getRedirectUri() {
        return this.properties.getProperty("redirect-uri", REDIRECT_URI);
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getUserAccessToken(String code) {
        Map<String, Object> form = new HashMap<String, Object>();
        form.put("grant_type", "authorization_code");
        form.put("client_id", this.getClientId());
        form.put("client_secret", this.getClientSecret());
        form.put("redirect_uri", this.getRedirectUri());
        form.put("code", code);

        HttpResponse response = HttpRequest.post(OAUTH_TOKEN_URI)
                .form(form)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .charset(StandardCharsets.UTF_8)
                .execute();

        if(!response.isOk() && log.isInfoEnabled()) {
            log.info("Get user access token uri={} status={} form={} result={}", 
                    OAUTH_TOKEN_URI, response.getStatus(), JacksonUtils.toJSONString(form), response.body());
        }

        Map<String, Object> result = JacksonUtils.toJavaObject(response.body(), Map.class);
        return JacksonUtils.getAsString(result, "access_token");
    }

    @SuppressWarnings("unchecked")
    @Override
    public SocialUser getUserInfo(String accessToken) {
        String openId = this.getUserOpenId(accessToken);
        String url = OAUTH_USERINFO_URI.replace("OPENID", openId)
                .replace(CLIENT_ID, this.getClientId())
                .replace("ACCESS_TOKEN", accessToken);
        
        HttpResponse response = HttpRequest.get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .charset(StandardCharsets.UTF_8)
                .execute();
        
        if(!response.isOk() && log.isInfoEnabled()) {
            log.info("Get user info uri={} status={} result={}", 
                    OAUTH_USERINFO_URI, response.getStatus(), response.body());
        }

        Map<String, String> result = JacksonUtils.toJavaObject(response.body(), Map.class);
        SocialUser socialUser = new SocialUser();
        socialUser.putAll(result);
        socialUser.setOpenId(openId);
        socialUser.setAvatar(result.get("figureurl"));
        socialUser.setUserName(result.get("nickname"));
        socialUser.setEmail(result.get("email"));
        String mobile = result.get("mobile");
        //只处理国内电话
        if(StringUtils.isNotEmpty(mobile) && mobile.startsWith("+86")) {
            mobile = mobile.substring(3);
        }
        socialUser.setMobile(mobile);

        return socialUser;
    }
    
    private String getUserOpenId(String accessToken) {
        String url = OAUTH_OPENID_URI.replace("ACCESS_TOKEN", accessToken);
        HttpResponse response = HttpRequest.get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .charset(StandardCharsets.UTF_8)
                .execute();

        if(!response.isOk() && log.isInfoEnabled()) {
            log.info("Get user access token uri={} status={} result={}", 
                    OAUTH_TOKEN_URI, response.getStatus(), response.body());
        }
        Matcher matcher = Pattern.compile("\"openid\"\\s*:\\s*\"(\\w+)\"").matcher(response.body());
        return matcher.find() ? matcher.group(1) : "";
    }

}
