package com.daffodil.auth.factory.strategy;

import java.util.Properties;

import com.daffodil.auth.factory.model.SocialUser;

/**
 * 扫码登录授权策略
 * @author yweijian
 * @date 2023年1月10日
 * @version 2.0.0
 * @description
 */
public abstract class ScanQRLoginStrategy {

    public Properties properties;

    public ScanQRLoginStrategy() {
        super();
    }

    public ScanQRLoginStrategy(Properties properties) {
        super();
        this.properties = properties;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public abstract String getClientId();

    public abstract String getClientSecret();

    public abstract String getAuthorizeUri();

    public abstract String getRedirectUri();

    public abstract String getUserAccessToken(String code);

    public abstract SocialUser getUserInfo(String accessToken);
}
