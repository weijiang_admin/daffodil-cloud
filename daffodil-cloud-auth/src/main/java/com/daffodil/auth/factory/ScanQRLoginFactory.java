package com.daffodil.auth.factory;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.daffodil.auth.factory.properties.ScanQRLoginProperties;
import com.daffodil.auth.factory.strategy.DingDingScanQRLoginStrategy;
import com.daffodil.auth.factory.strategy.FeishuScanQRLoginStrategy;
import com.daffodil.auth.factory.strategy.QQScanQRLoginStrategy;
import com.daffodil.auth.factory.strategy.QiyeWeixinScanQRLoginStrategy;
import com.daffodil.auth.factory.strategy.ScanQRLoginStrategy;
import com.daffodil.auth.factory.strategy.WeixinScanQRLoginStrategy;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.config.SysConfigLoginScan.ScanType;

import lombok.extern.slf4j.Slf4j;

/**
 * 扫码登录授权策略工厂
 * @author yweijian
 * @date 2023年1月10日
 * @version 2.0.0
 * @description
 */
@Slf4j
@Component
public class ScanQRLoginFactory {

    private final ConcurrentHashMap<ScanType, ScanQRLoginStrategy> scanQRLoginStrategyClassMap = new ConcurrentHashMap<ScanType, ScanQRLoginStrategy>();

    public ScanQRLoginFactory(ScanQRLoginProperties properties) {
        super();
        this.register(ScanType.QQ, new QQScanQRLoginStrategy(properties.getQq()));
        this.register(ScanType.WEIXIN, new WeixinScanQRLoginStrategy(properties.getWeixin()));
        this.register(ScanType.QIYE_WEIXIN, new QiyeWeixinScanQRLoginStrategy(properties.getQiyeweixin()));
        this.register(ScanType.FEISHU, new FeishuScanQRLoginStrategy(properties.getFeishu()));
        this.register(ScanType.DINGDING, new DingDingScanQRLoginStrategy(properties.getDingding()));
    }

    /**
     * 注册
     * @param type
     * @param strategy
     */
    public void register(ScanType type, ScanQRLoginStrategy strategy) {
        this.scanQRLoginStrategyClassMap.put(type, strategy);
        if(log.isInfoEnabled()) {
            log.info( "Registering scan qrlogin strategy type={} -> {}", type.name(), strategy.getClass().getName());
        }
    }

    public ScanQRLoginStrategy getScanQRLoginStrategy(String mode) {
        ScanType type = this.getAndCheckScanType(mode);
        return this.getScanQRLoginStrategy(type);
    }

    public ScanQRLoginStrategy getScanQRLoginStrategy(ScanType type) {
        return this.scanQRLoginStrategyClassMap.get(type);
    }

    /**
     * 检查并获取扫码授权扫码登录类型
     * @param mode
     * @return
     */
    public ScanType getAndCheckScanType(String mode) {
        ScanType type = null;
        try {
            type = ScanType.valueOf(mode);
        }catch (Exception e) {
            if(log.isWarnEnabled()) {
                log.warn(e.getMessage());
            }
            throw new BaseException(31015);
        }
        if(null == type) {
            throw new BaseException(31015);
        }
        return type;
    }
}
