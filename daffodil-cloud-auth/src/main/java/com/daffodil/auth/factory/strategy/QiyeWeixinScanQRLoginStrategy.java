package com.daffodil.auth.factory.strategy;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Properties;

import org.springframework.http.MediaType;

import com.daffodil.auth.factory.model.SocialUser;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.StringUtils;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * 企业微信扫码授权登录
 * @author yweijian
 * @date 2023年1月10日
 * @version 2.0.0
 * @description 成为企业微信开放平台开放也是麻烦又费劲，如需请自行查阅官网文档补充实现。
 */
@Slf4j
public class QiyeWeixinScanQRLoginStrategy extends ScanQRLoginStrategy {

    private static final String CLIENT_ID = "CLIENT_ID";

    private static final String CLIENT_SECRET = "CLIENT_SECRET";
    
    private static final String AGENTID = "AGENTID";

    private static final String AUTHORIZE_URI = "https://open.work.weixin.qq.com/wwopen/sso/qrConnect?appid=CLIENT_ID&agentid=AGENTID&redirect_uri=REDIRECT_URI&state=STATE";

    private static final String REDIRECT_URI = "REDIRECT_URI";
    
    private static final String OAUTH_TOKEN_URI = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=CLIENT_ID&corpsecret=CLIENT_SECRET";
    
    private static final String OAUTH_USERINFO_URI = "https://qyapi.weixin.qq.com/cgi-bin/auth/getuserinfo?access_token=ACCESS_TOKEN&code=CODE";
    
    private static final String USER_USERINFO_URI = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&userid=USERID";
    
    public QiyeWeixinScanQRLoginStrategy(Properties properties) {
        super(properties);
    }

    @Override
    public String getClientId() {
        return this.properties.getProperty("client-id", CLIENT_ID);
    }

    @Override
    public String getClientSecret() {
        return this.properties.getProperty("client-secret", CLIENT_SECRET);
    }

    @Override
    public String getAuthorizeUri() {
        String authorizeUri = this.properties.getProperty("authorize-uri", AUTHORIZE_URI);
        authorizeUri = authorizeUri.replace(CLIENT_ID, this.getClientId());
        authorizeUri = authorizeUri.replace(REDIRECT_URI, URLEncoder.encode(this.getRedirectUri(), StandardCharsets.UTF_8));
        authorizeUri = authorizeUri.replace(AGENTID, this.getAgentId());
        return authorizeUri;
    }

    @Override
    public String getRedirectUri() {
        return this.properties.getProperty("redirect-uri", REDIRECT_URI);
    }

    @Override
    public String getUserAccessToken(String code) {
        return this.getUserId(code);
    }

    @Override
    @SuppressWarnings("unchecked")
    public SocialUser getUserInfo(String userId) {
        String url = USER_USERINFO_URI.replace("ACCESS_TOKEN", this.getAccessToken()).replace("USERID", userId);
        HttpResponse response = HttpRequest.get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .charset(StandardCharsets.UTF_8)
                .execute();

        if(!response.isOk() && log.isInfoEnabled()) {
            log.info("Get user info uri={} status={} result={}", 
                    USER_USERINFO_URI, response.getStatus(), response.body());
        }

        Map<String, String> result = JacksonUtils.toJavaObject(response.body(), Map.class);
        SocialUser socialUser = new SocialUser();
        socialUser.putAll(result);
        socialUser.setUnionId(result.get("open_userid"));
        socialUser.setOpenId(result.get("userid"));
        socialUser.setAvatar(result.get("avatar"));
        socialUser.setUserName(result.get("name"));
        socialUser.setEmail(result.get("email"));
        String mobile = result.get("mobile");
        //只处理国内电话
        if(StringUtils.isNotEmpty(mobile) && mobile.startsWith("+86")) {
            mobile = mobile.substring(3);
        }
        socialUser.setMobile(mobile);

        return socialUser;
    }
    
    public String getAgentId() {
        return this.properties.getProperty("agent-id", AGENTID);
    }
    
    @SuppressWarnings("unchecked")
    public String getAccessToken() {
        String url = OAUTH_TOKEN_URI.replace(CLIENT_ID, this.getClientId()).replace(CLIENT_SECRET, this.getClientSecret());
        HttpResponse response = HttpRequest.get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .charset(StandardCharsets.UTF_8)
                .execute();

        if(!response.isOk() && log.isInfoEnabled()) {
            log.info("Get access token uri={} status={} result={}", 
                    OAUTH_TOKEN_URI, response.getStatus(), response.body());
        }

        Map<String, Object> result = JacksonUtils.toJavaObject(response.body(), Map.class);
        return JacksonUtils.getAsString(result, "access_token");
    }
    
    /**
     * 获取用户ID
     * @param code
     * @return
     */
    @SuppressWarnings("unchecked")
    public String getUserId(String code) {
        String url = OAUTH_USERINFO_URI.replace("ACCESS_TOKEN", this.getAccessToken()).replace("CODE", code);
        HttpResponse response = HttpRequest.get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .charset(StandardCharsets.UTF_8)
                .execute();

        if(!response.isOk() && log.isInfoEnabled()) {
            log.info("Get userid uri={} status={} result={}", 
                    OAUTH_USERINFO_URI, response.getStatus(), response.body());
        }

        Map<String, Object> result = JacksonUtils.toJavaObject(response.body(), Map.class);
        return JacksonUtils.getAsString(result, "userid");
    }

}
