package com.daffodil.auth.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daffodil.auth.service.ISysMenuService;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.system.entity.SysMenu;
import com.daffodil.system.entity.SysUser;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2021年9月30日
 * @version 1.0
 * @description
 */
@Service
public class SysMenuServiceImpl implements ISysMenuService{

    @Autowired
    private JpaDao<String> jpaDao;
    
    private static final String AUTH_PAGE = "page";
    
    private static final String AUTH_BUTTON = "button";
    
    @Override
    public List<String> selectUserAuthPagePerms(SysUser user) {
        if(user.isAdminRole()) {
            return selectAdminUserPerms(AUTH_PAGE);
        }else {
            return selectLoginUserPerms(user.getId(), AUTH_PAGE);
        }
    }

    @Override
    public List<String> selectUserAuthBtnPerms(SysUser user) {
        if(user.isAdminRole()) {
            return selectAdminUserPerms(AUTH_BUTTON);
        }else {
            return selectLoginUserPerms(user.getId(), AUTH_BUTTON);
        }
    }
    
    @Override
    public List<SysMenu> selectUserMenuList(SysUser user) {
        List<SysMenu> menus = null, cmons = null;
        // 管理员显示所有菜单信息
        if (user.isAdminRole()) {
            String hql = "from SysMenu m where m.menuType in ('catalog', 'menu') order by m.orderNum asc, m.ancestors asc";
            menus = jpaDao.search(hql, SysMenu.class);
        } else {
            String hql = "select m from SysMenu m " +
                    "left join SysRoleMenu rm on m.id = rm.menuId " +
                    "left join SysUserRole ur on rm.roleId = ur.roleId " +
                    "left join SysRole r on ur.roleId = r.id " +
                    "where m.menuType in ('catalog', 'menu') and r.status = '0' and ur.userId = ? ";
            menus = jpaDao.search(hql, user.getId(), SysMenu.class);
            
            //公共资源
            hql = "from SysMenu m where m.menuType in ('catalog', 'menu') and m.isCommon= 'Y' ";
            cmons = jpaDao.search(hql, SysMenu.class);
            
            menus = StringUtils.isNull(menus) ? new ArrayList<SysMenu>() : menus;
            menus.addAll(cmons);
            
            menus = menus.stream().distinct().collect(Collectors.toList());
            Collections.sort(menus, new Comparator<SysMenu>() {
                @Override
                public int compare(SysMenu o1, SysMenu o2) {
                    return (int) (o1.getOrderNum() - o2.getOrderNum());
                }
            });
        }
        return menus;
    }
    
    /**
     * 获取系统超级管理员菜单权限
     * @param authType
     * @return
     */
    private List<String> selectAdminUserPerms(String authType) {
        StringBuffer hql = AUTH_BUTTON.equals(authType) ? new StringBuffer("from SysMenu m where m.menuType = 'button' ") 
                : new StringBuffer("from SysMenu m where m.menuType in ('catalog', 'menu') ");
        List<SysMenu> menus = jpaDao.search(hql.toString(), SysMenu.class);
        
        menus = StringUtils.isNull(menus) ? new ArrayList<SysMenu>() : menus;
        
        List<String> perms = new ArrayList<String>();
        for (SysMenu menu : menus) {
            if (StringUtils.isNotEmpty(menu.getPerms()) && !perms.contains(menu.getPerms().trim())) {
                perms.add(menu.getPerms().trim());
            }
        }
        return perms.stream().distinct().collect(Collectors.toList());
    }
    
    /**
     * 获取登录用户菜单权限
     * @param userId
     * @param menuauthTypeType
     * @return
     */
    private List<String> selectLoginUserPerms(String userId, String authType){
        StringBuffer hql = new StringBuffer("select m from SysMenu m ");
        hql.append("left join SysRoleMenu rm on m.id = rm.menuId ");
        hql.append("left join SysUserRole ur on rm.roleId = ur.roleId ");
        hql.append("left join SysRole r on ur.roleId = r.id ");
        hql.append("where r.status = '0' and ur.userId = ? ");
        if(AUTH_BUTTON.equals(authType)) {
            hql.append("and m.menuType = 'button' ");
        }else {
            hql.append("and m.menuType in ('catalog', 'menu') ");
        }
        List<SysMenu> menus = jpaDao.search(hql.toString(), userId, SysMenu.class);
        
        //公共资源
        hql = new StringBuffer("from SysMenu m where m.isCommon = 'Y' ");
        if(AUTH_BUTTON.equals(authType)) {
            hql.append("and m.menuType = 'button' ");
        }else {
            hql.append("and m.menuType in ('catalog', 'menu') ");
        }
        List<SysMenu> cmons = jpaDao.search(hql.toString(), SysMenu.class);
        
        menus = StringUtils.isNull(menus) ? new ArrayList<SysMenu>() : menus;
        menus.addAll(cmons);
        
        List<String> perms = new ArrayList<String>();
        for (SysMenu menu : menus) {
            if (StringUtils.isNotEmpty(menu.getPerms()) && !perms.contains(menu.getPerms().trim())) {
                perms.add(menu.getPerms().trim());
            }
        }
        return perms.stream().distinct().collect(Collectors.toList());
    }

}
