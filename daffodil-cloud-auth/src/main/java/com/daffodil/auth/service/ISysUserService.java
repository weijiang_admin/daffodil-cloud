package com.daffodil.auth.service;

import com.daffodil.system.entity.SysSocialUser;
import com.daffodil.system.entity.SysUser;

/**
 * 
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
public interface ISysUserService {

    /**
     * 通过用户名查询用户
     * @param loginName
     * @return
     */
    public SysUser selectUserByLoginName(String loginName);

    /**
     * 通过手机号码查询用户
     * @param phone
     * @return
     */
    public SysUser selectUserByPhone(String phone);

    /**
     * 通过邮箱查询用户
     * @param email
     * @return
     */
    public SysUser selectUserByEmail(String email);

    /**
     * 通过用户ID查询用户
     * @param userId
     * @return
     */
    public SysUser selectUserById(String userId);
    
    /**
     * 通过用户OpenID查询用户
     * @param userMode 用户身份所属
     * @param openId
     */
    public SysUser selectUserByOpenId(String userMode, String openId);

    /**
     * 保存用户信息
     * @param user
     */
    public void updateUser(SysUser user);

    /**
     * -重置用户密码（用途：密码找回）
     * @param loginName
     * @param password
     */
    public void resetUserPassword(String loginName, String password);
    
    /**
     * 保存第三方用户
     * @param user
     */
    public void insertSocialUser(SysSocialUser user);

}
