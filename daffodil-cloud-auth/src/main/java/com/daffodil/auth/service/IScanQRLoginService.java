package com.daffodil.auth.service;

import java.util.Properties;

/**
 * 扫码授权登录
 * @author yweijian
 * @date 2023年1月10日
 * @version 2.0.0
 * @description
 */
public interface IScanQRLoginService {

    /**
     * 获取配置
     * @param mode
     * @return
     */
    public Properties getProperties(String mode);

    /**
     * 获取应用ID
     * @param mode
     * @return
     */
    public String getClientId(String mode);

    /**
     * 获取应用秘钥
     * @param mode
     * @return
     */
    public String getClientSecret(String mode);

    /**
     * 获取扫码登录授权地址
     * @param mode
     * @return
     */
    public String getAuthorizeUri(String mode);

    /**
     * 获取应用回调地址
     * @param mode
     * @return
     */
    public String getRedirectUri(String mode);

}
