package com.daffodil.auth.service;

import java.util.List;

import com.daffodil.system.entity.SysMenu;
import com.daffodil.system.entity.SysUser;

/**
 * 
 * @author yweijian
 * @date 2021年9月30日
 * @version 1.0
 * @description
 */
public interface ISysMenuService {

    /**
     * 根据用户查询用户页面访问授权权限
     * @param user
     * @return
     */
    public List<String> selectUserAuthPagePerms(SysUser user);
    
    /**
     * 根据用户查询按钮操作权限
     * @param user
     * @return
     */
    public List<String> selectUserAuthBtnPerms(SysUser user);
    
    /**
     * 根据登录的用户查询菜单（目录+菜单）
     * @return
     */
    public List<SysMenu> selectUserMenuList(SysUser user);
}
