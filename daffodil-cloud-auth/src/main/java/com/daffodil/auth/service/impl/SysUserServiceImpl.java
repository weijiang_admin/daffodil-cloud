package com.daffodil.auth.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.dao.JpaDao;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.system.entity.SysDept;
import com.daffodil.system.entity.SysPost;
import com.daffodil.system.entity.SysRank;
import com.daffodil.system.entity.SysRole;
import com.daffodil.system.entity.SysSocialUser;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.enums.PasswordReasonEnum;
import com.daffodil.util.PasswordUtils;
import com.daffodil.util.StringUtils;
import com.daffodil.auth.service.ISysUserService;
import com.daffodil.common.constant.CommonConstant;

/**
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
@Service
public class SysUserServiceImpl implements ISysUserService {
    
    @Autowired
    private JpaDao<String> jpaDao;

    @Override
    public SysUser selectUserByLoginName(String loginName) {
        return jpaDao.find("from SysUser where status = '0' and loginName = ?", loginName, SysUser.class);
    }

    @Override
    public SysUser selectUserByPhone(String phone) {
        return jpaDao.find("from SysUser where status = '0' and phone = ?", phone, SysUser.class);
    }

    @Override
    public SysUser selectUserByEmail(String email) {
        return jpaDao.find("from SysUser where status = '0' and email = ?", email, SysUser.class);
    }

    @Override
    public SysUser selectUserById(String userId) {
        SysUser user = jpaDao.find(SysUser.class, userId);
        
        // 部门
        SysDept dept = jpaDao.find(SysDept.class, user.getDeptId());
        user.setDept(null == dept ? new SysDept() : dept);
        
        // 角色
        List<SysRole> roles = jpaDao.search("select r from SysRole r,SysUserRole ur where r.id = ur.roleId and r.status = '0' and ur.userId = ?", userId, SysRole.class);
        user.setRoles(roles);
        List<String> roleIds = roles.stream().map(SysRole::getId).collect(Collectors.toList());
        user.setRoleIds(roleIds.toArray(new String[roleIds.size()]));
        
        // 岗位
        List<SysPost> posts = jpaDao.search("select p from SysPost p,SysUserPost up where p.id = up.postId and p.status = '0' and up.userId = ?", userId, SysPost.class);
        user.setPosts(posts);
        List<String> postIds = posts.stream().map(SysPost::getId).collect(Collectors.toList());
        user.setPostIds(postIds.toArray(new String[postIds.size()]));
        
        // 职级
        List<SysRank> ranks = jpaDao.search("select r from SysRank r,SysUserRank ur where r.id = ur.rankId and r.status = '0' and ur.userId = ?", userId, SysRank.class);
        user.setRanks(ranks);
        List<String> rankIds = ranks.stream().map(SysRank::getId).collect(Collectors.toList());
        user.setRankIds(rankIds.toArray(new String[rankIds.size()]));
        return user;
    }

    @Override
    @Transactional
    public void updateUser(SysUser user) {
        jpaDao.update(user);
    }

    @Override
    @Transactional
    public void resetUserPassword(String loginName, String password) {
        if(StringUtils.isEmpty(loginName) || StringUtils.isEmpty(password)) {
            throw new BaseException(31014);
        }
        SysUser user = null;
        if(loginName.matches(CommonConstant.MOBILE_PHONE_NUMBER_PATTERN)) {
            user = this.selectUserByPhone(loginName);
        }else if(loginName.matches(CommonConstant.EMAIL_PATTERN)) {
            user = this.selectUserByEmail(loginName);
        }
        if(null == user) {
            throw new BaseException(31014);
        }
        String salt = PasswordUtils.randomSalt();
        user.setSalt(salt);
        String encryptPassword = PasswordUtils.encryptPassword(user.getLoginName(), password, salt);
        user.setPassword(encryptPassword);
        user.setUpdateBy(loginName);
        user.setUpdateTime(new Date());
        user.setExpireTime(DateUtils.addMonths(new Date(), 6));
        user.setChangeReason(PasswordReasonEnum.REASON_PASSWORD_NOMAL.code());
        jpaDao.update(user);
    }

    @Override
    public SysUser selectUserByOpenId(String userMode, String openId) {
        List<Object> paras = new ArrayList<Object>();
        paras.add(userMode);
        paras.add(openId);
        SysSocialUser socialUser = jpaDao.find("from SysSocialUser where status = '0' and userMode = ? and openId = ? ", paras, SysSocialUser.class);
        return null != socialUser ? jpaDao.find("from SysUser where status = '0' and id = ?", socialUser.getUserId(), SysUser.class) : null;
    }

    @Override
    @Transactional
    public void insertSocialUser(SysSocialUser user) {
        List<Object> paras = new ArrayList<Object>();
        paras.add(user.getUserMode());
        paras.add(user.getOpenId());
        SysSocialUser socialUser = jpaDao.find("from SysSocialUser where userMode = ? and openId = ? ", paras, SysSocialUser.class);
        if(null == socialUser) {
            user.setCreateTime(new Date());
            jpaDao.save(user);
        }else {
            user.setId(socialUser.getId());
            user.setUpdateTime(new Date());
            jpaDao.update(user);
        }
    }
}
