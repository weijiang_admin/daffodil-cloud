package com.daffodil.auth.service.impl;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daffodil.auth.factory.ScanQRLoginFactory;
import com.daffodil.auth.service.IScanQRLoginService;

/**
 * 
 * @author yweijian
 * @date 2023年1月10日
 * @version 2.0.0
 * @description
 */
@Service
public class ScanQRLoginServiceImpl implements IScanQRLoginService {

    @Autowired
    private ScanQRLoginFactory factory;
    
    @Override
    public Properties getProperties(String mode) {
        return factory.getScanQRLoginStrategy(mode).getProperties();
    }
    
    @Override
    public String getClientId(String mode) {
        return factory.getScanQRLoginStrategy(mode).getClientId();
    }

    @Override
    public String getClientSecret(String mode) {
        return factory.getScanQRLoginStrategy(mode).getClientSecret();
    }
    
    @Override
    public String getAuthorizeUri(String mode) {
        return factory.getScanQRLoginStrategy(mode).getAuthorizeUri();
    }

    @Override
    public String getRedirectUri(String mode) {
        return factory.getScanQRLoginStrategy(mode).getRedirectUri();
    }

}
