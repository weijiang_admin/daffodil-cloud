package com.daffodil.auth.service;

import java.util.List;

import com.daffodil.core.entity.Page;
import com.daffodil.system.model.OnlineUser;

/**
 * 
 * @author yweijian
 * @date 2023年1月17日
 * @version 2.0.0
 * @description
 */
public interface ISysOnlineUserService {

    /**
     * -分页查询在线用户
     * @param page
     * @return
     */
    public List<OnlineUser> selectOnlineUserList(Page page);
    
    /**
     * -批量下线在线用户
     * @param tokens
     */
    public void deleteOnlineUserByTokens(String[] tokens);

    /**
     * -全部下线在线用户
     */
    public void deleteAllOnlineUser();
}
