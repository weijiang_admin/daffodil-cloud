package com.daffodil.auth.service;

import java.util.List;

import com.daffodil.system.entity.SysThirdApp;

/**
 * -第三方应用信息
 * @author yweijian
 * @date 2023年1月3日
 * @version 2.0.0
 * @description
 */
public interface ISysThirdAppService {

    /**
     * -查询第三方应用信息
     * @param appId
     * @return
     */
    public SysThirdApp selectThirdAppById(String appId);

    /**
     * -获取第三方应用授权接口权限
     * @param appId
     * @return
     */
    public List<String> selectAppAuthPerms(String appId);
}
