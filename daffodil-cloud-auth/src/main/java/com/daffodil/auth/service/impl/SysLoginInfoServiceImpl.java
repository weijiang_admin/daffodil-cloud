package com.daffodil.auth.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.auth.service.ISysLoginInfoService;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.system.entity.SysLoginInfo;

/**
 * 系统访问日志
 * @author yweijian
 * @date 2021年9月19日
 * @version 1.0
 * @description
 */
@Service
public class SysLoginInfoServiceImpl implements ISysLoginInfoService {

    @Autowired
    private JpaDao<String> jpaDao;

    @Override
    @Transactional
    public void insertLoginInfo(SysLoginInfo loginInfo) {
        jpaDao.save(loginInfo);
    }
}
