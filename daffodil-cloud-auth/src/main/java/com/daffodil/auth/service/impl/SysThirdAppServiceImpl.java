package com.daffodil.auth.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daffodil.auth.service.ISysThirdAppService;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.system.entity.SysResource;
import com.daffodil.system.entity.SysThirdApp;
import com.daffodil.system.entity.SysThirdAppScope;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2023年1月4日
 * @version 2.0.0
 * @description
 */
@Service
public class SysThirdAppServiceImpl implements ISysThirdAppService {

    @Autowired
    private JpaDao<String> jpaDao;
    
    @Override
    public SysThirdApp selectThirdAppById(String appId) {
        SysThirdApp app = jpaDao.find(SysThirdApp.class, appId);
        List<SysThirdAppScope> list = jpaDao.search("from SysThirdAppScope where appId = ?", appId, SysThirdAppScope.class);
        List<String> scopes = list.stream().map(SysThirdAppScope::getScope).collect(Collectors.toList());
        app.setScopes(scopes.toArray(new String[scopes.size()]));
        return app;
    }

    @Override
    public List<String> selectAppAuthPerms(String appId) {
        StringBuffer hql = new StringBuffer("select r from SysResource r left join SysThirdAppResource ar on r.id = ar.resourceId ");
        hql.append("left join SysThirdApp a on a.id = ar.appId ");
        hql.append("where a.id = ? and r.type = 'resource' and r.status = '0' ");
        List<SysResource> list = jpaDao.search(hql.toString(), appId, SysResource.class);
        List<String> perms = new ArrayList<String>();
        if(StringUtils.isNotEmpty(list)) {
            for(SysResource resource : list) {
                perms.add(resource.getPerms());
            }
        }
        return perms;
    }

}
