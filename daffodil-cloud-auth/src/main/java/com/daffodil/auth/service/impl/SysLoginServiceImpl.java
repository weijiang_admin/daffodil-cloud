package com.daffodil.auth.service.impl;

import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;

import com.daffodil.auth.service.ISysLoginService;
import com.daffodil.auth.controller.model.Account;
import com.daffodil.auth.factory.ScanQRLoginFactory;
import com.daffodil.auth.factory.model.SocialUser;
import com.daffodil.auth.factory.strategy.ScanQRLoginStrategy;
import com.daffodil.auth.properties.KaptchaProperties;
import com.daffodil.auth.service.ISysLoginInfoService;
import com.daffodil.auth.service.ISysUserService;
import com.daffodil.auth.util.LoginUtils;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.entity.SysLoginInfo;
import com.daffodil.system.entity.SysSocialUser;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.enums.DataStatusEnum;
import com.daffodil.system.enums.PasswordReasonEnum;
import com.daffodil.util.IpUtils;
import com.daffodil.util.PasswordUtils;
import com.daffodil.util.StringUtils;
import com.daffodil.util.sm.SM2Utils;

import eu.bitwalker.useragentutils.UserAgent;

/**
 * 
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
@Service
public class SysLoginServiceImpl implements ISysLoginService {

    @Autowired
    private KaptchaProperties kaptchaProperties;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysLoginInfoService loginInfoService;

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    @Autowired
    private ScanQRLoginFactory factory;

    /** 密码输入错误最多重复次数 */
    private static final Integer MAX_RETRY_LOGIN_COUNT = 5;

    /** 一分钟内允许最多登录次数，防暴力登录 */
    private static final Integer MAX_VERIFY_LOGIN_COUNT = 12;

    @Override
    public SysUser login(Account account, ServerHttpRequest request) {
        Integer loginType = account.getLoginType();
        String loginName = account.getLoginName();
        String password = account.getPassword();
        String verifyKey = account.getVerifyKey();
        String verifyCode = account.getVerifyCode();
        String privateKey = (String) redisTemplate.opsForValue().get(CommonConstant.SM2_PRIVATE_KEY_CACHEKEY);
        loginName = SM2Utils.decryptData(privateKey, loginName);
        password = SM2Utils.decryptData(privateKey, password);

        // 登录前校验
        SysUser user = this.verifyLogin(loginType, loginName, password, verifyKey, verifyCode, request);

        SysUser loginUser = userService.selectUserById(user.getId());
        // 去除敏感信息
        loginUser.setSalt(null);
        loginUser.setPassword(null);

        return loginUser;
    }

    /**
     * -登录验证
     * @param account
     * @param request
     */
    private SysUser verifyLogin(Integer loginType, String loginName, String password, String verifyKey, String verifyCode, ServerHttpRequest request) {
        //登录计数验证
        this.verifyCount(loginName);

        if(Account.LoginType.ACCOUNT.value().equals(loginType)) {
            this.verifyAccount(loginName, password, verifyKey, verifyCode, request);
        }else if(Account.LoginType.VERIFYCODE.value().equals(loginType)){
            this.verifyCode(loginName, verifyCode, request);
        }else if(Account.LoginType.ACCOUNT_VERIFYCODE.value().equals(loginType)) {
            this.verifyCode(loginName, verifyCode, request);
            //this.verifyPassword(user, password, request);后面验证
        }

        // 查询用户信息
        SysUser user = this.getLoginUser(loginName, request);
        if (user == null) {
            if(Account.LoginType.VERIFYCODE.value().equals(loginType) || Account.LoginType.ACCOUNT_VERIFYCODE.value().equals(loginType)) {
                this.recordLoginInfo(loginName, CommonConstant.ERROR, "用户不存在或验证码错误", request);
                throw new BaseException(31008);
            }
            this.recordLoginInfo(loginName, CommonConstant.ERROR, "用户不存在或密码错误", request);
            throw new BaseException(31001);
        }

        if (DataStatusEnum.DELETED.code().equals(user.getStatus())) {
            this.recordLoginInfo(loginName, CommonConstant.ERROR, "用户不存在或已被禁用", request);
            throw new BaseException(31003);
        }

        // 校验登录密码
        if(Account.LoginType.ACCOUNT.value().equals(loginType) || Account.LoginType.ACCOUNT_VERIFYCODE.value().equals(loginType)) {
            this.verifyPassword(user, password, request);
        }

        // 检查登录密码是否过期
        if(StringUtils.isNull(user.getExpireTime()) || user.getExpireTime().getTime() - new Date().getTime() <= 0) {
            user.setChangeReason(PasswordReasonEnum.REASON_PASSWORD_EXPIRE.code());
        }

        // 每次登录成功密码重新加密
        if(Account.LoginType.ACCOUNT.value().equals(loginType) || Account.LoginType.ACCOUNT_VERIFYCODE.value().equals(loginType)) {
            String salt = PasswordUtils.randomSalt();
            user.setSalt(salt);
            String encryptPassword = PasswordUtils.encryptPassword(user.getLoginName(), password, salt);
            user.setPassword(encryptPassword);
        }

        String ip = IpUtils.getIpAddrByServerHttpRequest(request);
        user.setLoginIp(ip);
        user.setLoginTime(new Date());
        userService.updateUser(user);

        // 记录登录成功日志
        this.recordLoginInfo(loginName, CommonConstant.SUCCESS, "登录成功", request);

        return user;
    }

    /**
     * -记录登陆信息
     * @param loginName
     * @param status
     * @param message
     */
    @Override
    public void recordLoginInfo(String loginName, String status, String message, ServerHttpRequest request) {
        String tenantId = TenantContextHolder.getTenantHolder();
        CompletableFuture.runAsync(() -> {
            TenantContextHolder.apply(tenantId, () -> {
                String ip = IpUtils.getIpAddrByServerHttpRequest(request);
                UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeaders().getFirst("User-Agent"));
                String address = this.getRealIpAddressName(ip);
                // 获取客户端操作系统
                String os = userAgent.getOperatingSystem().getName();
                // 获取客户端浏览器
                String browser = userAgent.getBrowser().getName();
                // 封装对象
                SysLoginInfo loginInfo = new SysLoginInfo();
                loginInfo.setLoginName(loginName);
                loginInfo.setStatus(status);
                loginInfo.setIpaddr(ip);
                loginInfo.setLoginLocation(address);
                loginInfo.setBrowser(browser);
                loginInfo.setOs(os);
                loginInfo.setMsg(message);
                loginInfo.setCreateTime(new Date());
                // 插入数据
                loginInfoService.insertLoginInfo(loginInfo);
            });
        });
    }

    /**
     * -用户登录计数验证
     * @param loginName
     */
    private void verifyCount(String loginName) {
        if(StringUtils.isNotEmpty(loginName)) {
            String cacheKey = StringUtils.format(CommonConstant.USER_LOGIN_COUNT_CACHEKEY, loginName);
            ValueOperations<String, Object> operations = tenantRedisTemplate.opsForValue();
            AtomicInteger verifyCount = (AtomicInteger) operations.get(cacheKey);
            if (verifyCount == null) {
                verifyCount = new AtomicInteger(0);
                operations.set(cacheKey, verifyCount, 1, TimeUnit.MINUTES);
            }
            if(verifyCount.incrementAndGet() > MAX_VERIFY_LOGIN_COUNT) {
                throw new BaseException(31007);
            }
            operations.set(cacheKey, verifyCount, 1, TimeUnit.MINUTES);
        }
    }

    /**
     * -账号密码验证
     * @param loginName
     * @param password
     * @param verifyKey
     * @param verifyCode
     * @param request
     */
    private void verifyAccount(String loginName, String password, String verifyKey, String verifyCode, ServerHttpRequest request) {
        // 用户名为空
        if (StringUtils.isEmpty(loginName)) {
            throw new BaseException(31001);
        }
        // 用户名不在指定范围内
        if (loginName.length() < CommonConstant.USERNAME_MIN_LENGTH || loginName.length() > CommonConstant.USERNAME_MAX_LENGTH) {
            this.recordLoginInfo(loginName, CommonConstant.ERROR, "用户不存在或密码错误", request);
            throw new BaseException(31001);
        }
        //密码为空
        if(StringUtils.isEmpty(password)) {
            this.recordLoginInfo(loginName, CommonConstant.ERROR, "用户不存在或密码错误", request);
            throw new BaseException(31001);
        }
        // 密码如果不在指定范围内
        if (password.length() < CommonConstant.PASSWORD_MIN_LENGTH || password.length() > CommonConstant.PASSWORD_MAX_LENGTH) {
            this.recordLoginInfo(loginName, CommonConstant.ERROR, "用户不存在或密码错误", request);
            throw new BaseException(31001);
        }

        // 是否开启图片验证码验证
        if(kaptchaProperties.getEnable()) {
            String cacheKey = StringUtils.format(CommonConstant.USER_LOGIN_KAPTCHA_CACHEKEY, verifyKey);
            String code = (String) redisTemplate.opsForValue().get(cacheKey);
            // 登录验证码不正确
            if(StringUtils.isEmpty(verifyCode) || !verifyCode.equalsIgnoreCase(code)) {
                this.recordLoginInfo(loginName, CommonConstant.ERROR, "登录验证码不正确", request);
                throw new BaseException(31002);
            }
            //验证码验证通过
            redisTemplate.delete(cacheKey);
        }
    }

    /**
     * -验证账号密码，10分钟内连续5次错误账号将锁定
     * @param user
     * @param password
     */
    private void verifyPassword(SysUser user, String password, ServerHttpRequest request) {
        String loginName = user.getLoginName();
        String cacheKey = StringUtils.format(CommonConstant.USER_RETRY_COUNT_CACHEKEY, loginName);
        ValueOperations<String, Object> operations = tenantRedisTemplate.opsForValue();
        AtomicInteger retryCount = (AtomicInteger) operations.get(cacheKey);

        if (retryCount == null) {
            retryCount = new AtomicInteger(0);
            operations.set(cacheKey, retryCount, 10, TimeUnit.MINUTES);
        }
        if (retryCount.incrementAndGet() > MAX_RETRY_LOGIN_COUNT) {
            this.recordLoginInfo(loginName, CommonConstant.ERROR, StringUtils.format("密码连续输入错误{}次，帐户锁定10分钟", MAX_RETRY_LOGIN_COUNT), request);
            throw new BaseException(31004, MAX_RETRY_LOGIN_COUNT);
        }

        String encryptPassword = PasswordUtils.encryptPassword(user.getLoginName(), password, user.getSalt());
        if (user.getPassword().equals(encryptPassword)) {
            tenantRedisTemplate.delete(cacheKey);
        } else {
            this.recordLoginInfo(loginName, CommonConstant.ERROR, StringUtils.format("密码连续输入错误{}次", retryCount), request);
            operations.set(cacheKey, retryCount, 10, TimeUnit.MINUTES);
            throw new BaseException(31005, retryCount);
        }
    }

    /**
     * -验证验证码
     * @param loginName
     * @param verifyCode
     * @param request
     */
    private void verifyCode(String loginName, String verifyCode, ServerHttpRequest request) {
        // 用户名为空
        if (StringUtils.isEmpty(loginName)) {
            throw new BaseException(31008);
        }
        if(StringUtils.isEmpty(verifyCode)) {
            this.recordLoginInfo(loginName, CommonConstant.ERROR, "用户不存在或验证码错误", request);
            throw new BaseException(31008);
        }
        String cacheKey = StringUtils.format(CommonConstant.USER_LOGIN_VERIFYCODE_CACHEKEY, loginName);
        String cacheCode = (String) redisTemplate.opsForValue().get(cacheKey);
        if(!verifyCode.equals(cacheCode)) {
            this.recordLoginInfo(loginName, CommonConstant.ERROR, "用户不存在或验证码错误", request);
            throw new BaseException(31008);
        }
        redisTemplate.delete(cacheKey);
    }

    /**
     * -获取登录用户
     * @param loginName
     * @param request
     * @return
     */
    private SysUser getLoginUser(String loginName, ServerHttpRequest request) {
        SysUser user = userService.selectUserByLoginName(loginName);

        if (user == null && loginName.matches(CommonConstant.MOBILE_PHONE_NUMBER_PATTERN)) {
            user = userService.selectUserByPhone(loginName);
        }

        if (user == null && loginName.matches(CommonConstant.EMAIL_PATTERN)) {
            user = userService.selectUserByEmail(loginName);
        }
        return user;
    }

    /**
     * -获取IP真实地址
     * @param ip
     * @return
     */
    private String getRealIpAddressName(String ip) {
        String address = (String) redisTemplate.opsForHash().get(CommonConstant.REALIP_ADDRESS_CACHEKEY, ip);
        if(StringUtils.isEmpty(address)) {
            address = IpUtils.getRealIpAddressName(ip, true);
            if(address.indexOf("未知IP") < 0) {
                redisTemplate.opsForHash().put(CommonConstant.REALIP_ADDRESS_CACHEKEY, ip, address);
            }
        }
        return address;
    }

    @Override
    public SysUser login(String mode, String code, ServerHttpRequest request) {
        ScanQRLoginStrategy strategy = factory.getScanQRLoginStrategy(mode);
        if(StringUtils.isEmpty(code)) {
            throw new BaseException(31016);
        }
        String accessToken = strategy.getUserAccessToken(code);
        if(StringUtils.isEmpty(accessToken)) {
            throw new BaseException(31016);
        }
        SocialUser socialUser = strategy.getUserInfo(accessToken);
        SysUser user = userService.selectUserByOpenId(mode, socialUser.getOpenId());

        if(user == null) {
            this.recordLoginInfo(socialUser.getUserName(), CommonConstant.ERROR, "未绑定平台用户或已禁用", request);
            throw new BaseException(31017);
        }

        String ip = IpUtils.getIpAddrByServerHttpRequest(request);
        user.setLoginIp(ip);
        user.setLoginTime(new Date());
        userService.updateUser(user);

        // 记录登录成功日志
        this.recordLoginInfo(user.getLoginName(), CommonConstant.SUCCESS, "登录成功", request);

        return user;
    }

    @Override
    public void bind(String mode, String code, ServerHttpRequest request) {
        ScanQRLoginStrategy strategy = factory.getScanQRLoginStrategy(mode);
        if(StringUtils.isEmpty(code)) {
            throw new BaseException(31016);
        }
        String accessToken = strategy.getUserAccessToken(code);
        if(StringUtils.isEmpty(accessToken)) {
            throw new BaseException(31016);
        }
        SocialUser socialUser = strategy.getUserInfo(accessToken);
        LoginUser loginUser = LoginUtils.getLoginUser(request);

        SysSocialUser user = new SysSocialUser();
        user.setUserMode(mode);
        user.setUserId(loginUser.getId());
        user.setUnionId(socialUser.getUnionId());
        user.setOpenId(socialUser.getOpenId());
        user.setUserName(socialUser.getUserName());
        user.setAvatar(socialUser.getAvatar());
        user.setEmail(socialUser.getEmail());
        user.setMobile(socialUser.getMobile());
        user.setStatus(DataStatusEnum.NORMAL.code());

        userService.insertSocialUser(user);
    }

}
