package com.daffodil.auth.service;

import com.daffodil.system.entity.SysLoginInfo;

/**
 * 系统访问日志服务
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
public interface ISysLoginInfoService {

    /**
     * 新增系统登录日志
     * @param loginInfo
     */
    public void insertLoginInfo(SysLoginInfo loginInfo);

}
