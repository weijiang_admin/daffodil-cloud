package com.daffodil.auth.service;

import org.springframework.http.server.reactive.ServerHttpRequest;

import com.daffodil.auth.controller.model.Account;
import com.daffodil.system.entity.SysUser;

/**
 * 
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
public interface ISysLoginService {
    
    /**
     * 用户登录
     * @param account
     * @return
     */
    public SysUser login(Account account, ServerHttpRequest request);
    
    /**
     * 记录登录日志
     * @param loginName
     * @param status
     * @param message
     */
    public void recordLoginInfo(String loginName, String status, String message, ServerHttpRequest request);
    
    /**
     * 第三方应用授权码登录
     * @param mode
     * @param code
     * @return
     */
    public SysUser login(String mode, String code, ServerHttpRequest request);
    
    /**
     * 绑定第三方应用用户身份
     * @param mode
     * @param code
     */
    public void bind(String mode, String code, ServerHttpRequest request);
    
}
