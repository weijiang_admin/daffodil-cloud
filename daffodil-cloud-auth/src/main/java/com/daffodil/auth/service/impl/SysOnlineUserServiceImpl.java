package com.daffodil.auth.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.daffodil.auth.service.ISysOnlineUserService;
import com.daffodil.auth.util.LoginUtils;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.entity.Page;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.system.model.OnlineUser;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.JwtTokenUtils;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2023年1月17日
 * @version 2.0.0
 * @description
 */
@Service
public class SysOnlineUserServiceImpl implements ISysOnlineUserService {

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    @Override
    public List<OnlineUser> selectOnlineUserList(Page page) {
        List<OnlineUser> list = new ArrayList<OnlineUser>();
        Set<Object> accessTokens = tenantRedisTemplate.opsForSet().members(CommonConstant.ACCESS_TOKEN_POOL_CACHEKEY);
        if(StringUtils.isNotEmpty(accessTokens)) {
            page.setTotalRow(accessTokens.size());
            List<Object> tokens = accessTokens.stream().skip(page.getFromIndex()).limit(page.getPageSize() * 2).collect(Collectors.toList());
            if(StringUtils.isNotEmpty(tokens)) {
                tokens.forEach(token -> {
                    String accessToken = (String) token;
                    String levelTokenCachekey = StringUtils.format(CommonConstant.LEVEL_TOKEN_CACHEKEY, accessToken);
                    Integer levelToken = (Integer) tenantRedisTemplate.opsForValue().get(levelTokenCachekey);
                    String cacheKey = StringUtils.format(CommonConstant.ACCESS_TOKEN_CACHEKEY, accessToken);
                    String authorToken = (String) tenantRedisTemplate.opsForValue().get(cacheKey);
                    String data = JwtTokenUtils.getData(authorToken);
                    if(CommonConstant.LEVEL_TOKEN_FIRST.equals(levelToken)) {
                        LoginUser user = JacksonUtils.toJavaObject(data, LoginUser.class);
                        if(null != user) {
                            OnlineUser onlineUser = new OnlineUser();
                            onlineUser.setId(user.getId());
                            onlineUser.setDeptId(user.getDeptId());
                            onlineUser.setLoginName(user.getLoginName());
                            onlineUser.setUserName(user.getUserName());
                            onlineUser.setEmail(user.getEmail());
                            onlineUser.setPhone(user.getPhone());
                            onlineUser.setStatus(user.getStatus());
                            onlineUser.setLoginIp(user.getLoginIp());
                            onlineUser.setLoginAd(user.getLoginAd());
                            onlineUser.setLoginOs(user.getLoginOs());
                            onlineUser.setLoginBr(user.getLoginBr());
                            onlineUser.setLoginTime(user.getLoginTime());
                            onlineUser.setAccessToken(accessToken);
                            onlineUser.setIsAdminRole(user.getIsAdminRole());
                            list.add(onlineUser);
                        }
                    }
                });
            }
        }

        return list.stream().skip(page.getFromIndex()).limit(page.getPageSize()).collect(Collectors.toList());
    }

    @Override
    public void deleteOnlineUserByTokens(String[] tokens) {
        if(StringUtils.isNotEmpty(tokens)) {
            for (int i = 0; i < tokens.length; i++) {
                LoginUtils.clearLoginResult(tokens[i], tenantRedisTemplate);
            }
        }
    }

    @Override
    public void deleteAllOnlineUser() {
        Set<Object> accessTokens = tenantRedisTemplate.opsForSet().members(CommonConstant.ACCESS_TOKEN_POOL_CACHEKEY);
        if(StringUtils.isNotEmpty(accessTokens)) {
            this.deleteOnlineUserByTokens(accessTokens.toArray(new String[accessTokens.size()]));
        }
    }

}
