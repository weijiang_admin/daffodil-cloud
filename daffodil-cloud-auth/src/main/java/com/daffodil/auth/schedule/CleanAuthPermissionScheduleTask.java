package com.daffodil.auth.schedule;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.daffodil.auth.util.LoginUtils;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.framework.properties.FrameworkProperties;
import com.daffodil.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 定时清理用户登录授权缓存
 * @author yweijian
 * @date 2021年12月20日
 * @version 1.0
 * @description
 */
@Slf4j
@Component
@Configuration
@EnableScheduling
public class CleanAuthPermissionScheduleTask {

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    @Autowired
    private FrameworkProperties frameworkProperties;

    /**
     * -每2小时清理一次
     */
    @Scheduled(cron = "0 0 0/2 * * ?")
    public void run() {
        log.info("开始定时清理登录用户授权认证缓存信息...");
        String pattern = "*:" + CommonConstant.ACCESS_TOKEN_POOL_CACHEKEY;
        ScanOptions options = ScanOptions.scanOptions().match(pattern).count(1000).build();
        redisTemplate.execute((RedisCallback<Void>) connection -> {
            try (Cursor<byte[]> cursor = connection.scan(options)) {
                while (cursor.hasNext()) {
                    String key = new String(cursor.next());
                    if(frameworkProperties.getTenantEnable()) {
                        String tenantId = key.substring(0, key.indexOf(":"));
                        Set<Object> accessTokens = redisTemplate.opsForSet().members(key);
                        if(StringUtils.isNotEmpty(accessTokens)) {
                            for(Object accessToken : accessTokens) {
                                TenantContextHolder.apply(tenantId, () -> LoginUtils.clearLoginResult((String) accessToken, tenantRedisTemplate));
                            }
                        }
                    }else {
                        Set<Object> accessTokens = redisTemplate.opsForSet().members(key);
                        if(StringUtils.isNotEmpty(accessTokens)) {
                            for(Object accessToken : accessTokens) {
                                LoginUtils.clearLoginResult((String) accessToken, redisTemplate);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                log.error("Error scanning keys : {}",e.getMessage(), e);
            }
            return null;
        });
        log.info("定时清理登录用户授权认证缓存信息结束。");
    }
}
