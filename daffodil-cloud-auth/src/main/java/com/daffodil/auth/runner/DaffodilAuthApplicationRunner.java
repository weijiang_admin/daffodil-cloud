package com.daffodil.auth.runner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author yweijian
 * @date 2023年8月8日
 * @version 2.0.0
 * @copyright Copyright 2020-2023 www.daffodilcloud.com.cn
 * @description
 */
@Slf4j
@Component
public class DaffodilAuthApplicationRunner implements ApplicationRunner, Ordered {
    
    @Value("${spring.application.name:daffodil-cloud-auth}")
    private String appName;

    @Value("${server.port:31918}")
    private String appPort;

    @Override
    public int getOrder() {
        return Integer.MAX_VALUE;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("达佛统一授权认证微服务启动成功，应用服务名：{} 应用端口：{}", appName, appPort);
        }
    }
}
