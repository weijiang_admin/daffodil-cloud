package com.daffodil.auth.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.daffodil.auth.controller.model.MenuRouter;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.system.entity.SysMenu;
import com.daffodil.util.StringUtils;

/**
 * Router 路由工具类，将SysMenu转换成路由
 * @author yweijian
 * @date 2021年9月26日
 * @version 1.0
 * @description
 * <pre>
 * Router路由meta对象参数说明：meta: {<br>
 *      title:       菜单栏及 tagsView 栏、菜单搜索名称（国际化）<br>
 *      isLink:      是否超链接菜单，开启外链条件，`1、isLink:true 2、链接地址不为空`<br>
 *      isHide:      是否隐藏此路由<br>
 *      isKeepAlive: 是否缓存组件状态<br>
 *      isAffix:     是否固定在 tagsView 栏上<br>
 *      isIframe:    是否内嵌窗口，，开启条件，`1、isIframe:true 2、链接地址不为空`<br>
 *      auth:        当前路由权限标识（多个请用逗号隔开），最后转成数组格式，用于与当前用户权限进行对比，控制路由显示、隐藏<br>
 *      icon:        菜单、tagsView 图标，阿里：加 `iconfont xxx`，fontawesome：加 `fa xxx`<br>
 * }
 * </pre>
 */
public class RouterUtils {

    public final static String ROUTER_LINK = "layout/routerView/link";

    public final static String ROUTER_IFRAMES = "layout/routerView/iframes";

    public final static String ROUTER_PARENT = "layout/routerView/parent";

    /**
     * 将sysMenu转换成Router路由
     * @param menus
     * @return
     */
    public static List<MenuRouter> changeToRouter(List<SysMenu> menus){
        List<MenuRouter> routers = new ArrayList<MenuRouter>();
        if(StringUtils.isNotEmpty(menus)) {
            for(int i = 0; i < menus.size(); i++) {
                SysMenu menu = menus.get(i);
                MenuRouter router = RouterUtils.menuToRouter(menu);
                //如果是目录类型，需要给重定向路径
                if("catalog".equals(menu.getMenuType())) {
                    for(SysMenu item : menus) {
                        if(menu.getId().equals(item.getParentId()) && CommonConstant.NO.equals(menu.getIsHide())) {
                            String path = StringUtils.isEmpty(item.getName()) ? item.getId() : item.getName();
                            router.setRedirect("/" + path);
                            break;
                        }
                    }
                }
                routers.add(router);
            }
        }
        return routers;
    }

    /**
     * 将菜单转成路由
     * @param menu
     * @return
     */
    private static MenuRouter menuToRouter(SysMenu menu) {
        MenuRouter router = new MenuRouter();
        router.setId(menu.getId());
        router.setParentId(menu.getParentId());
        String path = StringUtils.isEmpty(menu.getName()) ? menu.getId() : menu.getName();
        router.setPath(path.startsWith("/") ? path : ("/" + path));
        router.setName(menu.getName());
        String component = ROUTER_PARENT;
        if(CommonConstant.YES.equals(menu.getIsLink())) {
            component = ROUTER_LINK;
            if(CommonConstant.YES.equals(menu.getIsIframe())) {
                component = ROUTER_IFRAMES;
            }
        }else if(StringUtils.isNotEmpty(menu.getComponent())){
            component = menu.getComponent();
        }
        router.setComponent(component);
        //组装路由元数据
        Map<String, Object> meta = new HashMap<String, Object>();
        meta.put("title", menu.getMenuName());
        if(CommonConstant.YES.equals(menu.getIsLink()) && StringUtils.isNotEmpty(menu.getLinkUrl())) {
            meta.put("isLink", menu.getLinkUrl());
        }else {
            meta.put("isLink", "");
        }
        meta.put("isHide", CommonConstant.YES.equals(menu.getIsHide()));
        meta.put("isKeepAlive", CommonConstant.YES.equals(menu.getIsKeepAlive()));
        meta.put("isAffix", CommonConstant.YES.equals(menu.getIsAffix()));
        meta.put("isIframe", CommonConstant.YES.equals(menu.getIsIframe()));
        meta.put("isCommon", CommonConstant.YES.equals(menu.getIsCommon()));
        meta.put("auth", new String[] {menu.getPerms()});
        meta.put("icon", menu.getIcon());
        meta.put("remark", menu.getRemark());
        router.setMeta(meta);
        return router;
    }
}
