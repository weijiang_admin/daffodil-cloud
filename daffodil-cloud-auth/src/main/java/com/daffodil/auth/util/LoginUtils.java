package com.daffodil.auth.util;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.server.reactive.ServerHttpRequest;

import com.daffodil.auth.controller.model.LoginResult;
import com.daffodil.auth.controller.model.OauthUser;
import com.daffodil.common.constant.CommonConstant;
import com.daffodil.framework.model.LoginApp;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.framework.model.Role;
import com.daffodil.framework.util.LoginUserUtils;
import com.daffodil.system.entity.SysDept;
import com.daffodil.system.entity.SysRole;
import com.daffodil.system.entity.SysThirdApp;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.entity.SysThirdAppScope.ThirdAppScope;
import com.daffodil.util.IpUtils;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.JwtTokenUtils;
import com.daffodil.util.StringUtils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.crypto.KeyUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import eu.bitwalker.useragentutils.UserAgent;

/**
 * 
 * @author yweijian
 * @date 2023年1月17日
 * @version 2.0.0
 * @description
 */
public class LoginUtils {

    /**
     * -构建登录用户
     * @param user
     * @param request
     * @return
     */
    public static LoginUser buildLoginUser(SysUser user, ServerHttpRequest request) {
        if(null == user || null == request) {
            return null;
        }
        String loginIp = IpUtils.getIpAddrByServerHttpRequest(request);
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeaders().getFirst("User-Agent"));
        String loginAd = IpUtils.getRealIpAddressName(loginIp, true);
        String loginOs = userAgent.getOperatingSystem().getName();
        String loginBr = userAgent.getBrowser().getName();
        String deptName = Optional.ofNullable(user.getDept()).map(SysDept::getDeptName).orElse(null);
        LoginUser loginUser = new LoginUser();
        loginUser.setId(user.getId());
        loginUser.setDeptId(user.getDeptId());
        loginUser.setDeptName(deptName);
        loginUser.setLoginName(user.getLoginName());
        loginUser.setUserName(user.getUserName());
        loginUser.setEmail(user.getEmail());
        loginUser.setPhone(user.getPhone());
        loginUser.setStatus(user.getStatus());
        loginUser.setLoginIp(loginIp);
        loginUser.setLoginAd(loginAd);
        loginUser.setLoginOs(loginOs);
        loginUser.setLoginBr(loginBr);
        loginUser.setLoginTime(new Date());
        loginUser.setIsAdminRole(user.isAdminRole());
        List<SysRole> roles = user.getRoles();
        if(CollectionUtil.isNotEmpty(roles)) {
            loginUser.setRoles(roles.stream().map(item -> {
                Role role = new Role();
                role.setId(item.getId());
                role.setRoleName(item.getRoleName());
                role.setRoleKey(item.getRoleKey());
                role.setDataScope(item.getDataScope());
                role.setStatus(item.getStatus());
                role.setRemark(item.getRemark());
                return role;
            }).collect(Collectors.toList()));
        }
        return loginUser;
    }

    /**
     * -构建登录应用
     * @param app
     * @param request
     * @return
     */
    public static LoginApp buildLoginApp(SysThirdApp app, ServerHttpRequest request) {
        if(null == app || null == request) {
            return null;
        }
        LoginUser user = LoginUtils.getLoginUser(request);
        if(null == user) {
            return null;
        }
        String loginIp = IpUtils.getIpAddrByServerHttpRequest(request);
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeaders().getFirst("User-Agent"));
        String loginAd = IpUtils.getRealIpAddressName(loginIp, true);
        String loginOs = userAgent.getOperatingSystem().getName();
        String loginBr = userAgent.getBrowser().getName();
        LoginApp loginApp = new LoginApp();
        loginApp.setId(user.getId());
        loginApp.setLoginName(user.getLoginName());
        loginApp.setAppId(app.getId());
        loginApp.setAppName(app.getAppName());
        loginApp.setStatus(app.getStatus());
        loginApp.setLoginIp(loginIp);
        loginApp.setLoginAd(loginAd);
        loginApp.setLoginOs(loginOs);
        loginApp.setLoginBr(loginBr);
        loginApp.setLoginTime(new Date());
        return loginApp;
    }

    /**
     * -设置令牌等级、登录令牌、授权令牌、授权权限
     * <p>accessToken --> levelToken</p>
     * <p>accessToken --> authorToken --> refreshToken --> accessToken</p>
     * @param levelToken 登录令牌token级别 1:基础平台，2:开放平台
     * @param object 登录数据对象
     * @param permissions 授权权限
     * @param redisTemplate
     * @return
     */
    public static LoginResult buildLoginResult(int levelToken, Object object, List<String> permissions, RedisTemplate<String, Object> redisTemplate) {
        if(StringUtils.isNull(object)) {
            return null;
        }

        String accessToken = JwtTokenUtils.uuid();
        String refreshToken = JwtTokenUtils.uuid();

        String data = JacksonUtils.toJSONString(object);
        String authorToken = JwtTokenUtils.sign(refreshToken, data);

        String levelTokenCachekey = StringUtils.format(CommonConstant.LEVEL_TOKEN_CACHEKEY, accessToken);
        String accessTokenCachekey = StringUtils.format(CommonConstant.ACCESS_TOKEN_CACHEKEY, accessToken);
        String refreshTokenCachekey = StringUtils.format(CommonConstant.REFRESH_TOKEN_CACHEKEY, refreshToken);
        String routerPermissionCachekey = StringUtils.format(CommonConstant.PERMISSION_TOKEN_CACHEKEY, accessToken);

        //accessToken --> levelToken
        redisTemplate.opsForValue().set(levelTokenCachekey, levelToken, JwtTokenUtils.EXPIRE_TIME * 2, TimeUnit.MILLISECONDS);
        //accessToken --> authorToken --> refreshToken --> accessToken
        redisTemplate.opsForValue().set(accessTokenCachekey, authorToken, JwtTokenUtils.EXPIRE_TIME * 2, TimeUnit.MILLISECONDS);
        //refreshToken --> accessToken
        redisTemplate.opsForValue().set(refreshTokenCachekey, accessToken, JwtTokenUtils.EXPIRE_TIME * 2, TimeUnit.MILLISECONDS);
        //accessToken --> permissions
        if(CollectionUtil.isNotEmpty(permissions)) {
            redisTemplate.opsForSet().add(routerPermissionCachekey, permissions.toArray());
        }
        redisTemplate.opsForSet().add(CommonConstant.ACCESS_TOKEN_POOL_CACHEKEY, accessToken);

        return new LoginResult(accessToken, JwtTokenUtils.EXPIRE_TIME / 1000, refreshToken, new Date());
    }

    /**
     * -删除旧授权令牌和授权权限
     * @param accessToken
     * @param redisTemplate
     */
    public static void clearLoginResult(String accessToken, RedisTemplate<String, Object> redisTemplate) {
        if(StringUtils.isEmpty(accessToken)) {
            return;
        }
        String accessTokenCachekey = StringUtils.format(CommonConstant.ACCESS_TOKEN_CACHEKEY, accessToken);
        String authorToken = (String) redisTemplate.opsForValue().get(accessTokenCachekey);
        if(StringUtils.isNotEmpty(authorToken)) {
            String refreshToken = JwtTokenUtils.getSecret(authorToken);
            redisTemplate.delete(StringUtils.format(CommonConstant.REFRESH_TOKEN_CACHEKEY, refreshToken));
        }
        redisTemplate.delete(StringUtils.format(CommonConstant.LEVEL_TOKEN_CACHEKEY, accessToken));
        redisTemplate.delete(StringUtils.format(CommonConstant.ACCESS_TOKEN_CACHEKEY, accessToken));
        redisTemplate.delete(StringUtils.format(CommonConstant.PERMISSION_TOKEN_CACHEKEY, accessToken));
        redisTemplate.opsForSet().remove(CommonConstant.ACCESS_TOKEN_POOL_CACHEKEY, accessToken);
    }

    /**
     * -根据授权范围获取用户信息
     * @param user
     * @param scopes
     * @return
     */
    public static OauthUser getUserInfoByScope(SysUser user, String[] scopes) {
        OauthUser info = new OauthUser();
        info.setId(user.getId());

        if(StringUtils.isNotEmpty(scopes)) {
            for(String scode : scopes) {
                if(ThirdAppScope.USER.name().equals(scode)) {
                    info.setLoginName(user.getLoginName());
                    info.setUserName(user.getUserName());
                    info.setAvatar(user.getAvatar());
                    info.setSex(user.getSex());
                }else if(ThirdAppScope.EMAIL.name().equals(scode)) {
                    info.setEmail(user.getEmail());
                }else if(ThirdAppScope.PHONE.name().equals(scode)) {
                    info.setPhone(user.getPhone());
                }else if(ThirdAppScope.DEPT.name().equals(scode)) {
                    info.setDeptId(user.getDeptId());
                    info.setDept(user.getDept());
                }else if(ThirdAppScope.ROLE.name().equals(scode)) {
                    info.setRoleIds(user.getRoleIds());
                    info.setRoles(user.getRoles());
                }else if(ThirdAppScope.RANK.name().equals(scode)) {
                    info.setRankIds(user.getRankIds());
                    info.setRanks(user.getRanks());
                }
            }
        }

        return info;
    }

    /**
     * -获取AES算法
     * @param secret
     * @return
     * @throws NoSuchAlgorithmException 
     */
    public static AES getAes(String secret) throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(secret.getBytes(StandardCharsets.UTF_8));
        byte[] key = KeyUtil.generateKey(SymmetricAlgorithm.AES.getValue(), 128, random).getEncoded();
        AES aes = SecureUtil.aes(key);
        return aes;
    }

    /**
     * -获取当前登录的用户
     * @param request
     * @return
     */
    public static LoginUser getLoginUser(ServerHttpRequest request) {
        return LoginUserUtils.getLoginUser(request);
    }

    /**
     * -获取请求的AccessToken令牌
     * @param request
     * @return
     */
    public static String getAccessToken(ServerHttpRequest request) {
        return LoginUserUtils.getAccessToken(request);
    }

    /**
     * -获取授权信息AuthorToken
     * @param request
     * @return
     */
    public static String getAuthorToken(ServerHttpRequest request) {
        return LoginUserUtils.getAuthorToken(request);
    }

    /**
     * -获取刷新令牌RefreshToken
     * @param request
     * @return
     */
    public static String getRefreshToken(ServerHttpRequest request) {
        return LoginUserUtils.getRefreshToken(request);
    }

    /**
     * -获取当前登录应用
     * @param request
     * @return
     */
    public static LoginApp getLoginApp(ServerHttpRequest request) {
        if(StringUtils.isNull(request)) {
            return null;
        }
        String authorToken = LoginUtils.getAuthorToken(request);
        if(JwtTokenUtils.isValid(authorToken)) {
            String data = JwtTokenUtils.getData(authorToken);
            if(StringUtils.isNotEmpty(data)) {
                LoginApp app = JacksonUtils.toJavaObject(data, LoginApp.class);
                return app;
            }
        }
        return null;
    }
}
