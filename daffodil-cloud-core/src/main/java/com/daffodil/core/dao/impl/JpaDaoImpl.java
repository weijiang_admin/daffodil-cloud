package com.daffodil.core.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.dao.helper.DaoHelper;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.core.entity.Page;
import com.daffodil.core.exception.BusinessException;

/**
 * 
 * @author yweijian
 * @date 2022年6月9日
 * @version 2.0.0
 * @description
 */
@Repository
public class JpaDaoImpl<ID> implements JpaDao<ID> {

    private static final int PAGE_MAX_DEPTH = 1000;

    @Autowired
    private EntityManager entityManager;

    @Override
    public ID save(BaseEntity<ID> entity) throws BusinessException {
        Assert.notNull(entity, "[JPA] Entity is not be null");
        try {
            this.entityManager.persist(entity);
            return entity.getId();
        } catch (Exception e) {
            throw new BusinessException("[JPA] Error in executing HQL statement ...", e);
        }
    }

    @Override
    public void delete(BaseEntity<ID> entity) throws BusinessException {
        Assert.notNull(entity, "[JPA] Entity is not be null");
        try {
            BaseEntity<ID> object = this.find(entity.getClass(), entity.getId());
            if(null != object) {
                this.entityManager.remove(this.entityManager.contains(object) ? object : this.entityManager.merge(object));
            }
        } catch (Exception e) {
            throw new BusinessException("[JPA] Error in executing HQL statement ...", e);
        }
    }

    @Override
    public void delete(BaseEntity<ID>[] entitys) throws BusinessException {
        Assert.notEmpty(entitys, "[JPA] Entitys is not be empty");
        for (int i = 0; i < entitys.length; i++) {
            this.delete(entitys[i]);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void delete(List<?> entitys) throws BusinessException {
        Assert.notEmpty(entitys, "[JPA] Entitys is not be empty");
        for (int i = 0; i < entitys.size(); i++) {
            BaseEntity<ID> entity = (BaseEntity<ID>) entitys.get(i);
            if(null != entity) {
                this.delete(entity);
            }
        }
    }

    @Override
    public void delete(Class<?> clazz, ID id) throws BusinessException {
        Assert.notNull(id, "[JPA] Entity primary key id is not be null");
        BaseEntity<ID> entity = this.find(clazz, id);
        if(null != entity) {
            this.delete(entity);
        }
    }

    @Override
    public void delete(Class<?> clazz, ID[] ids) throws BusinessException {
        Assert.notEmpty(ids, "[JPA] Entity primary key ids is not be empty");
        for (int i = 0; i < ids.length; i++) {
            this.delete(clazz, ids[i]);
        }
    }

    @Override
    public void delete(String hql) throws BusinessException {
        this.execute(hql, null);
    }

    @Override
    public void delete(String hql, Object para) throws BusinessException {
        List<Object> paras = new ArrayList<Object>();
        paras.add(para);
        this.execute(hql, paras);
    }

    @Override
    public void delete(String hql, List<Object> paras) throws BusinessException {
        this.execute(hql, paras);
    }

    @Override
    public void update(String hql) throws BusinessException {
        this.execute(hql, null);
    }

    @Override
    public void update(String hql, Object para) throws BusinessException {
        List<Object> paras = new ArrayList<Object>();
        paras.add(para);
        this.execute(hql, paras);
    }

    @Override
    public void update(String hql, List<Object> paras) throws BusinessException {
        this.execute(hql, paras);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(BaseEntity<ID> entity) throws BusinessException {
        try {
            BaseEntity<ID> baseEntity = this.find(entity.getClass(), entity.getId());
            baseEntity = (BaseEntity<ID>) DaoHelper.combineBean(entity, baseEntity);
            this.entityManager.merge(baseEntity);
        } catch (Exception e) {
            throw new BusinessException("[JPA] Error in executing HQL statement ...", e);
        }
    }

    @Override
    public void updateAll(BaseEntity<ID> entity) throws BusinessException {
        try {
            if (!this.entityManager.contains(entity)) {
                this.entityManager.merge(entity);
            }
        } catch (Exception e) {
            throw new BusinessException("[JPA] Error in executing HQL statement ...", e);
        }
    }

    @Override
    public void execute(String hql, List<Object> paras) throws BusinessException {
        try {
            String queryHql = DaoHelper.getQueryHql(hql);
            Query query = this.entityManager.createQuery(queryHql);
            DaoHelper.setQueryParas(query, paras);
            query.executeUpdate();
        } catch (Exception e) {
            throw new BusinessException("[JPA] Error in executing HQL statement ...", e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T find(Class<?> clazz, ID id) throws BusinessException {
        Assert.notNull(id, "[JPA] Entity primary key id is not be null");
        T entity = null;
        try {
            entity = (T) this.entityManager.find(clazz, id);
        } catch (Exception e) {
            throw new BusinessException("[JPA] Error in executing HQL statement ...", e);
        }
        return entity;
    }

    @Override
    public <T> T find(String hql, Class<?> clazz) throws BusinessException {
        return this.find(hql, null, clazz);
    }

    @Override
    public <T> T find(String hql, Object para, Class<?> clazz) throws BusinessException {
        List<Object> paras = new ArrayList<Object>();
        paras.add(para);
        return this.find(hql, paras, clazz);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T find(String hql, List<Object> paras, Class<?> clazz) throws BusinessException {
        T entity = null;
        try {
            String queryHql = DaoHelper.getQueryHql(hql);
            Query query = this.entityManager.createQuery(queryHql, clazz);
            DaoHelper.setQueryParas(query, paras);
            List<T> result = query.getResultList();
            Iterator<T> iterator = result.iterator();
            if (iterator.hasNext()) {
                entity = iterator.next();
            }
        } catch (Exception e) {
            throw new BusinessException("[JPA] Error in executing HQL statement ...", e);
        }
        return entity;
    }

    @Override
    public <T> T find(String hql, Class<?> clazz, LockModeType type) throws BusinessException {
        return this.find(hql, null, clazz, type);
    }

    @Override
    public <T> T find(String hql, Object para, Class<?> clazz, LockModeType type) throws BusinessException {
        List<Object> paras = new ArrayList<Object>();
        paras.add(para);
        return this.find(hql, paras, clazz, type);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T find(String hql, List<Object> paras, Class<?> clazz, LockModeType type) throws BusinessException {
        T entity = null;
        try {
            String queryHql = DaoHelper.getQueryHql(hql);
            Query query = this.entityManager.createQuery(queryHql, clazz);
            query.setLockMode(type);
            DaoHelper.setQueryParas(query, paras);
            List<T> result = query.getResultList();
            Iterator<T> iterator = result.iterator();
            if (iterator.hasNext()) {
                entity = iterator.next();
            }
        } catch (Exception e) {
            throw new BusinessException("[JPA] Error in executing HQL statement ...", e);
        }
        return entity;
    }

    @Override
    public <T> List<T> search(String hql, Class<?> clazz, Page page) throws BusinessException {
        return this.search(hql, null, clazz, page, null);
    }

    @Override
    public <T> List<T> search(String hql, Object para, Class<?> clazz, Page page) throws BusinessException {
        List<Object> paras = new ArrayList<Object>();
        paras.add(para);
        return this.search(hql, paras, clazz, page, null);
    }

    @Override
    public <T> List<T> search(String hql, List<Object> paras, Class<?> clazz, Page page) throws BusinessException {
        return this.search(hql, paras, clazz, page, null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> List<T> search(String hql, List<Object> paras, Class<?> clazz, Page page, String distinctField) throws BusinessException {
        if(null == page){
            page = new Page();
        }
        //避免深度分页查询
        if(page.getPageNumber() > PAGE_MAX_DEPTH) {
            return Collections.EMPTY_LIST;
        }
        int count = this.count(hql, paras, distinctField);
        page.setTotalRow(count);
        if(count <= 0) {
            return Collections.EMPTY_LIST;
        }
        try {
            String queryHql = DaoHelper.getQueryHql(hql);
            Query query = this.entityManager.createQuery(queryHql, clazz);
            DaoHelper.setQueryParas(query, paras);
            query.setFirstResult(page.getFromIndex()).setMaxResults(page.getPageSize());
            return query.getResultList();
        } catch (Exception e) {
            throw new BusinessException("[JPA] Error in executing HQL statement ...", e);
        }
    }

    @Override
    public <T> List<T> search(String hql, Class<?> clazz) throws BusinessException {
        return this.search(hql, null, clazz);
    }

    @Override
    public <T> List<T> search(String hql, Object para, Class<?> clazz) throws BusinessException {
        List<Object> paras = new ArrayList<Object>();
        paras.add(para);
        return this.search(hql, paras, clazz);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> List<T> search(String hql, List<Object> paras, Class<?> clazz) throws BusinessException {
        List<T> list = null;
        try {
            String queryHql = DaoHelper.getQueryHql(hql);
            Query query = this.entityManager.createQuery(queryHql, clazz);
            DaoHelper.setQueryParas(query, paras);
            list = query.getResultList();
        } catch (Exception e) {
            throw new BusinessException("[JPA] Error in executing HQL statement ...", e);
        }
        return list;
    }

    @Override
    public int count(String hql) throws BusinessException {
        return this.count(hql, null, null);
    }

    @Override
    public int count(String hql, Object para) throws BusinessException {
        List<Object> paras = new ArrayList<Object>();
        paras.add(para);
        return this.count(hql, paras, null);
    }

    @Override
    public int count(String hql, List<Object> paras) throws BusinessException {
        return this.count(hql, paras, null);
    }

    @Override
    public int count(String hql, List<Object> paras, String distinctField) throws BusinessException {
        try {
            String queryHql = DaoHelper.getQueryHql(hql);
            String queryCountHql = DaoHelper.getQueryCountHql(queryHql, distinctField);
            Query query = this.entityManager.createQuery(queryCountHql);
            DaoHelper.setQueryParas(query, paras);
            return ((Long) query.getResultList().iterator().next()).intValue();
        } catch (Exception e) {
            throw new BusinessException("[JPA] Error in executing HQL statement ...", e);
        }
    }

}
