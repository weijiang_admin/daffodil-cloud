package com.daffodil.core.dao;

import java.util.List;
import java.util.Map;

import com.daffodil.core.entity.KeyFormatter;
import com.daffodil.core.entity.Page;
import com.daffodil.core.exception.BusinessException;

/**
 * 
 * @author yweijian
 * @date 2020年7月16日
 * @version 1.0
 * @description
 */
public interface JdbcDao {

    /**
     * -执行更新或者删除预编译SQL
     * @param sql
     * @throws BusinessException
     */
    public void execute(String sql) throws BusinessException;

    /**
     * -执行更新或者删除预编译SQL
     * @param sql
     * @param para
     * @throws BusinessException
     */
    public void execute(String sql, Object para) throws BusinessException;
    
    /**
     * -执行更新或者删除预编译SQL
     * @param sql
     * @param paras
     * @throws BusinessException
     */
    public void execute(String sql, List<Object> paras) throws BusinessException;

    /**
     * -根据SQL查询数据对象
     * @param sql
     * @return
     * @throws BusinessException
     */
    public List<Map<String, String>> search(String sql) throws BusinessException;
    
    /**
     * -根据SQL查询数据对象
     * @param sql
     * @param formatter 返回对象中Map.key的格式
     * @return
     * @throws BusinessException
     */
    public List<Map<String, String>> search(String sql, KeyFormatter formatter) throws BusinessException;

    /**
     * -根据SQL查询数据对象
     * @param sql
     * @param para
     * @return
     * @throws BusinessException
     */
    public List<Map<String, String>> search(String sql, Object para) throws BusinessException;
    
    /**
     * -根据SQL查询数据对象
     * @param sql
     * @param para
     * @param formatter 返回对象中Map.key的格式
     * @return
     * @throws BusinessException
     */
    public List<Map<String, String>> search(String sql, Object para, KeyFormatter formatter) throws BusinessException;
    
    /**
     * -根据SQL查询数据对象
     * @param sql
     * @param paras
     * @return
     * @throws BusinessException
     */
    public List<Map<String, String>> search(String sql, List<Object> paras) throws BusinessException;
    
    /**
     * -根据SQL查询数据对象
     * @param sql
     * @param paras
     * @param formatter 返回对象中Map.key的格式
     * @return
     * @throws BusinessException
     */
    public List<Map<String, String>> search(String sql, List<Object> paras, KeyFormatter formatter) throws BusinessException;

    /**
     * -根据SQL分页查询数据对象
     * @param sql
     * @param page
     * @return
     * @throws BusinessException
     */
    public List<Map<String, String>> search(String sql, Page page) throws BusinessException;
    
    /**
     * -根据SQL分页查询数据对象
     * @param sql
     * @param page
     * @param formatter 返回对象中Map.key的格式
     * @return
     * @throws BusinessException
     */
    public List<Map<String, String>> search(String sql, Page page, KeyFormatter formatter) throws BusinessException;
    
    /**
     * -根据SQL分页查询数据对象
     * @param sql
     * @param para
     * @param page
     * @return
     * @throws BusinessException
     */
    public List<Map<String, String>> search(String sql, Object para, Page page) throws BusinessException;
    
    /**
     * -根据SQL分页查询数据对象
     * @param sql
     * @param para
     * @param page
     * @param formatter 返回对象中Map.key的格式
     * @return
     * @throws BusinessException
     */
    public List<Map<String, String>> search(String sql, Object para, Page page, KeyFormatter formatter) throws BusinessException;
    
    /**
     * -根据SQL分页查询数据对象
     * @param sql
     * @param paras
     * @param page
     * @return
     * @throws BusinessException
     */
    public List<Map<String, String>> search(String sql, List<Object> paras, Page page) throws BusinessException;
    
    /**
     * -根据SQL分页查询数据对象
     * @param sql
     * @param paras
     * @param page
     * @param formatter 返回对象中Map.key的格式
     * @return
     * @throws BusinessException
     */
    public List<Map<String, String>> search(String sql, List<Object> paras, Page page, KeyFormatter formatter) throws BusinessException;

    /**
     * -根据SQL查询数据对象
     * @param sql
     * @return
     * @throws BusinessException
     */
    public Map<String, String> find(String sql) throws BusinessException;
    
    /**
     * -根据SQL查询数据对象
     * @param sql
     * @param formatter 返回对象中Map.key的格式
     * @return
     * @throws BusinessException
     */
    public Map<String, String> find(String sql, KeyFormatter formatter) throws BusinessException;

    /**
     * -根据SQL查询数据对象
     * @param sql
     * @param para
     * @return
     * @throws BusinessException
     */
    public Map<String, String> find(String sql, Object para) throws BusinessException;
    
    /**
     * -根据SQL查询数据对象
     * @param sql
     * @param para
     * @param formatter 返回对象中Map.key的格式
     * @return
     * @throws BusinessException
     */
    public Map<String, String> find(String sql, Object para, KeyFormatter formatter) throws BusinessException;

    /**
     * -根据SQL查询数据对象
     * @param sql
     * @param paras
     * @return
     * @throws BusinessException
     */
    public Map<String, String> find(String sql, List<Object> paras) throws BusinessException;
    
    /**
     * -根据SQL查询数据对象
     * @param sql
     * @param paras
     * @param formatter 返回对象中Map.key的格式
     * @return
     * @throws BusinessException
     */
    public Map<String, String> find(String sql, List<Object> paras, KeyFormatter formatter) throws BusinessException;

    /**
     * -根据SQL查询统计数据对象
     * @param sql
     * @return
     * @throws BusinessException
     */
    public int count(String sql) throws BusinessException;

    /**
     * -根据SQL查询统计数据对象
     * @param sql
     * @param para
     * @return
     * @throws BusinessException
     */
    public int count(String sql, Object para) throws BusinessException;
    
    /**
     * -根据SQL查询统计数据对象
     * @param sql
     * @param paras
     * @return
     * @throws BusinessException
     */
    public int count(String sql, List<Object> paras) throws BusinessException;

}
