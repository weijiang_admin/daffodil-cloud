package com.daffodil.core.service;

import java.util.List;

import com.daffodil.core.entity.Query;

/**
 * 
 * @author yweijian
 * @date 2023年1月30日
 * @version 2.0.0
 * @description
 */
public interface IBaseEntityService<ID, T> {

    /**
     * -分页查询实体信息集合
     * @param query
     * @return
     */
    public List<T> selectEntityList(Query<T> query);

    /**
     * -通过实体ID查询职级信息
     * @param id
     * @return
     */
    public T selectEntityById(ID id);

    /**
     * -批量删除实体信息
     * @param ids
     */
    public void deleteEntityByIds(ID[] ids);

    /**
     * -新增保存实体信息
     * @param entity
     */
    public void insertEntity(T entity);

    /**
     * -修改保存实体信息
     * @param entity
     */
    public void updateEntity(T entity);
}
