package com.daffodil.core.jdbc;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnProperty(name = DaffodilDataSourceConstant.DATASOURCE_MULTIPLE_PREFIX_KEY, havingValue = "true")
@Import(DaffodilDataSourceRegister.class)
public class DaffodilDataSourceConfiguration {

}
