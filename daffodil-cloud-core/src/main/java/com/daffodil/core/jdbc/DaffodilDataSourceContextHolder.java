package com.daffodil.core.jdbc;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.util.Assert;

/**
 * 动态数据源上下文容器
 * @author yweijian
 * @date 2021年8月31日
 * @version 1.0
 * @description
 */
public class DaffodilDataSourceContextHolder {

    private static final ThreadLocal<String> contextHolder = ThreadLocal.withInitial(() -> DaffodilDataSourceConstant.DATASOURCE_DEFALUT_KEY);

    private static final Map<String, DaffodilDataSourceProperties> dataSourcePropertiesMap = new ConcurrentHashMap<String, DaffodilDataSourceProperties>();

    private static final Set<String> dataSourceLookupKeys = new HashSet<String>();

    public static void setDataSourceLookupKey(String lookupKey) {
        contextHolder.set(lookupKey);
    }

    public static String getDataSourceLookupKey() {
        return contextHolder.get();
    }

    public static void clearDataSourceLookupKey() {
        contextHolder.remove();
    }

    public static boolean containsDataSource(String lookupKey){
        return dataSourceLookupKeys.contains(lookupKey);
    }

    public static String getDataSourceLookupKey(String tenantId) {
        return dataSourcePropertiesMap.entrySet().stream()
                .filter(entry -> entry.getValue().getTenantId().equals(tenantId))
                .findFirst().map(entry -> entry.getKey()).orElse("");
    }

    public static DaffodilDataSourceProperties getDataSourceProperties(String lookupKey) {
        return dataSourcePropertiesMap.get(lookupKey);
    }

    public static void setDataSourcePropertiesMap(Map<String, DaffodilDataSourceProperties> dataSources) {
        Assert.notEmpty(dataSources, "Parameter 'dataSources' must not be empty");
        dataSourcePropertiesMap.putAll(dataSources);
        dataSourcePropertiesMap.forEach((lookupKey, properties) -> dataSourceLookupKeys.add(lookupKey));
    }
}
