package com.daffodil.core.jdbc;

/**
 * 
 * @author yweijian
 * @date 2024年5月10日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public class DaffodilDataSourceConstant {

    public static final String DATASOURCE_DEFALUT_KEY = "primary";

    public static final String DATASOURCE_PREFIX_KEY = "daffodil.datasource";

    public static final String DATASOURCE_MULTIPLE_PREFIX_KEY = "daffodil.multiple.datasource";

    public static final String DATASOURCE_DEFAULT_TYPE = "com.zaxxer.hikari.HikariDataSource";

    public static final String JASYPT_ENCRYPTOR_PREFIX_KEY = "jasypt.encryptor";

}
