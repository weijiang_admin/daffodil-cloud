package com.daffodil.core.jdbc;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;

/**
 * 
 * @author yweijian
 * @date 2024年5月8日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public class DaffodilDataSourceProperties extends DataSourceProperties {

    /** 租户标识ID */
    private String tenantId;

    /** 数据源标识 */
    private String lookupKey;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

}
