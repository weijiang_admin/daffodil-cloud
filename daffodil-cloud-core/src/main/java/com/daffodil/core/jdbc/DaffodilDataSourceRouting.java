package com.daffodil.core.jdbc;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author yweijian
 * @date 2021年9月1日
 * @version 1.0
 * @description
 */
@Slf4j
public class DaffodilDataSourceRouting extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        String lookupKey = DaffodilDataSourceContextHolder.getDataSourceLookupKey();
        log.info("Determine target DataSource for lookup key [{}]", lookupKey);
        return lookupKey;
    }

}
