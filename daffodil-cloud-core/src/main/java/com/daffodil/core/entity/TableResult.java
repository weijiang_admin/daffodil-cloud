package com.daffodil.core.entity;

import java.util.List;

/**
 * 表格分页数据对象
 * @author yweijian
 * @date 2022年10月17日
 * @version 2.0.0
 * @description
 */
public class TableResult extends JsonResult {
    
    private static final long serialVersionUID = 1L;

    /** 数据对象 */
    public static final String TOTAL_TAG = "total";
    
    /** 数据对象 */
    public static final String ROWS_TAG = "rows";

    /** 表格数据对象 */
    public TableResult() {
        
    }
    
    /**
     * 带参表格数据对象
     * @param code
     * @param msg
     * @param data
     * @param total
     */
    public TableResult(int code, String msg, List<?> data, int total) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        super.put(DATA_TAG, data);
        super.put(TOTAL_TAG, total);
    }
    
    /**
     * 
     * @param rows
     * @return
     */
    public TableResult rows(List<?> rows) {
        this.put(ROWS_TAG, rows);
        return this;
    }
    
    /**
     * 返回分页数据
     * @param data
     * @param query
     * @return
     */
    public static TableResult success(List<?> data, Query<?> query) {
        return TableResult.success(data, query.getPage());
    }
    
    /**
     * 返回分页数据
     * @param data
     * @param page
     * @return
     */
    public static TableResult success(List<?> data, Page page) {
        int total = null != page ? page.getTotalRow() : null != data ? data.size() : 0;
        return new TableResult(SUCCESS, SUCCESS_MSG, data, total);
    }
    
    /**
     * 返回分页数据
     * @param data
     * @return
     */
    public static TableResult success(List<?> data) {
        return new TableResult(SUCCESS, SUCCESS_MSG, data, null != data ? data.size() : 0);
    }
    
    /**
     * 返回分页数据
     * @param msg
     * @param data
     * @return
     */
    public static TableResult success(String msg, List<?> data) {
        return new TableResult(SUCCESS, msg, data, null != data ? data.size() : 0);
    }
    
    /**
     * 返回分页数据
     * @param data
     * @param total
     * @return
     */
    public static TableResult success(List<?> data, int total) {
        return new TableResult(SUCCESS, SUCCESS_MSG, data, total);
    }
    
    /**
     * 返回分页数据
     * @param msg
     * @param data
     * @param total
     * @return
     */
    public static TableResult success(String msg, List<?> data, int total) {
        return new TableResult(SUCCESS, msg, data, total);
    }
    
}