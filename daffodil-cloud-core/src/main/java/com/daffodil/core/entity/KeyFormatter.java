package com.daffodil.core.entity;

/**
 * 
 * @author yweijian
 * @date 2023年2月7日
 * @version 2.0.0
 * @description
 */
public enum KeyFormatter {
    
    /** 默认模式 */
    NONECASE,
    /** 小写模式 */
    LOWERCASE,
    /** 大写模式 */
    UPPERCASE,
    /** 驼峰模式 */
    CAMELCASE
}
