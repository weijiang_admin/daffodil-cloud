package com.daffodil.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * -标记作用谨慎私密字段
 * @author yweijian
 * @date 2022年6月28日
 * @version 2.0.0
 * @description
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface SecretField {

    /** 加密方式 */
    Secret value() default Secret.NONE;
    
    /**
     * -加密方式<br>
     * -MD5, SM2, SM3, SM4, NONE(无)
     * @author yweijian
     * @date 2022年6月28日
     * @version 2.0.0
     * @description
     */
    public enum Secret {
        MD5("MD5"),
        SM2("SM2"),
        SM3("SM3"),
        SM4("SM4"),
        NONE("");
        private final String value;

        Secret(String value) {
            this.value = value;
        }

        public String value() {
            return this.value;
        }
    }
}
