package com.daffodil.core.id;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author yweijian
 * @date 2022年3月7日
 * @version 1.0
 * @description
 */
@Setter
@Getter
@Component
public class SequenceProperties {
    
    @Value(value = "${daffodil.sequence.data-center-id:1}")
    private Long dataCenterId;
    
    @Value(value = "${daffodil.sequence.worker-id:1}")
    private Long workerId;
    
    @Value(value = "${daffodil.sequence.clock:false}")
    private Boolean clock;
    
    @Value(value = "${daffodil.sequence.time-offset:5}")
    private Long timeOffset;
    
    @Value(value = "${daffodil.sequence.random-sequence:false}")
    private Boolean randomSequence;

    @Override
    public String toString() {
        return "SequenceProperties [dataCenterId=" + dataCenterId + ", workerId=" + workerId + ", clock=" + clock
                + ", timeOffset=" + timeOffset + ", randomSequence=" + randomSequence + "]";
    }
    
}
