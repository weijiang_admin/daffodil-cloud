package com.daffodil.core.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.daffodil.util.JacksonUtils;
import com.daffodil.util.StringUtils;

/**
 * 操作消息提醒
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
public class JsonResult extends HashMap<String, Object> {
    
    private static final long serialVersionUID = 1L;

    /** 状态码 */
    public static final String CODE_TAG = "code";

    /** 返回内容 */
    public static final String MSG_TAG = "msg";

    /** 数据对象 */
    public static final String DATA_TAG = "data";

    /** 成功 */
    public static final int SUCCESS = 0;
    
    /** 操作成功 */
    public static final String SUCCESS_MSG = "操作成功";
    
    /** 错误 */
    public static final int ERROR = 500;
    
    /** 操作失败 */
    public static final String ERROR_MSG = "操作失败";
    
    /**
     * 初始化一个新创建的 JsonResult 对象，使其表示一个空消息。
     */
    public JsonResult() {
        
    }
    
    /**
     * 初始化一个新创建的 JsonResult 对象
     * @param code 状态码
     * @param msg 信息
     * @param data 数据
     */
    public JsonResult(int code, String msg, Object data) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        super.put(DATA_TAG, data);
    }

    /**
     * 返回成功消息
     * 
     * @return 成功消息
     */
    public static JsonResult success() {
        return JsonResult.success(SUCCESS_MSG);
    }

    /**
     * 返回成功数据
     * 
     * @return 成功消息
     */
    public static JsonResult success(Object data) {
        return JsonResult.success(SUCCESS_MSG, data);
    }

    /**
     * 返回成功消息
     * @param msg
     * @return
     */
    public static JsonResult success(String msg) {
        return JsonResult.success(msg, null);
    }

    /**
     * 返回成功消息
     * @param msg
     * @param data
     * @return
     */
    public static JsonResult success(String msg, Object data) {
        return new JsonResult(SUCCESS, msg, data);
    }

    /**
     * 返回错误消息
     * 
     * @return
     */
    public static JsonResult error() {
        return JsonResult.error(ERROR_MSG);
    }

    /**
     * 返回错误消息
     * @param msg
     * @return
     */
    public static JsonResult error(String msg) {
        return JsonResult.error(ERROR, msg);
    }

    /**
     * 返回错误消息
     * @param code
     * @param msg
     * @return
     */
    public static JsonResult error(int code, String msg) {
        return new JsonResult(code, msg, null);
    }
    
    /**
     * 获取状态码
     * @return
     */
    public Integer getCode() {
        return JacksonUtils.toJavaObject(this.get(CODE_TAG), Integer.class);
    }
    
    /**
     * 获取消息内容
     * @return
     */
    public String getMsg() {
        return JacksonUtils.toJavaObject(this.get(MSG_TAG), String.class);
    }
    
    /**
     * 获取数据对象
     * @return
     */
    public Object getData() {
        return this.get(DATA_TAG);
    }
    
    /**
     * 获取指定数据对象T
     * @param clazz
     * @return
     */
    public <T> T getData(Class<T> clazz) {
        return JacksonUtils.toJavaObject(this.get(DATA_TAG), clazz);
    }
    
    /**
     * 获取数据对象字符串
     * @return
     */
    public String getDataAsString() {
        return null != this.getData() ? this.getData().toString() : null;
    }
    
    /**
     * 获取泛型数据对象List<?>
     * @return
     */
    public List<?> getDataAsList() {
        return JacksonUtils.toJavaObject(this.getData(), List.class);
    }
    
    /**
     * 获取指定数据对象List<T>
     * @return
     */
    public <T> List<T> getDataAsList(Class<T> clazz) {
        List<T> list = new ArrayList<T>();
        List<?> datas = JacksonUtils.toJavaObject(this.getData(), List.class);
        if(StringUtils.isNotEmpty(datas)) {
            for(Object obj : datas) {
                list.add(JacksonUtils.toJavaObject(obj, clazz));
            }
        }
        return list;
    }
    
}
