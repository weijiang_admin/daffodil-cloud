package com.daffodil.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author yweijian
 * @date 2019年12月16日
 * @version 1.0
 */
public class Query<T> implements Serializable {
    
    private static final long serialVersionUID = 1L;

    /** 实体对象 */
    private T entity;
    
    /** 请求参数 */
    private Map<String, Object> params;
    
    /** 分页 */
    private Page page;
    
    /** 数据范围 */
    private String dataScope;
    
    /** 开始时间 */
    private Date startTime;
    
    /** 结束时间 */
    private Date endTime;
    
    /** 排序 */
    private String orderBy;
    
    public Query() {
        super();
    }

    public Query(T entity, Map<String, Object> params, Page page, String dataScope, Date startTime, Date endTime, String orderBy) {
        super();
        this.page = page;
        this.params = params;
        this.entity = entity;
        this.dataScope = dataScope;
        this.startTime = startTime;
        this.endTime = endTime;
        this.orderBy = orderBy;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Map<String, Object> getParams() {
        if (params == null) {
            params = new HashMap<>();
        }
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }
    
    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }
    
    public String getDataScope() {
        return dataScope;
    }

    public void setDataScope(String dataScope) {
        this.dataScope = dataScope;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
    
    @SuppressWarnings("rawtypes")
    public static Builder builder() {
        return new Builder();
    }
    
    public static class Builder<T> {
        /** 实体对象 */
        private T entity;
        
        /** 请求参数 */
        private Map<String, Object> params;
        
        /** 分页 */
        private Page page;
        
        /** 数据范围 */
        private String dataScope;
        
        /** 开始时间 */
        private Date startTime;
        
        /** 结束时间 */
        private Date endTime;
        
        /** 排序 */
        private String orderBy;

        public Builder<T> entity(T entity) {
            this.entity = entity;
            return this;
        }
        
        public Builder<T> params(Map<String, Object> params) {
            this.params = params;
            return this;
        }
        
        public Builder<T> page(Page page) {
            this.page = page;
            return this;
        }
        
        public Builder<T> dataScope(String dataScope) {
            this.dataScope = dataScope;
            return this;
        }
        
        public Builder<T> startTime(Date startTime) {
            this.startTime = startTime;
            return this;
        }
        
        public Builder<T> endTime(Date endTime) {
            this.endTime = endTime;
            return this;
        }
        
        public Builder<T> orderBy(String orderBy) {
            this.orderBy = orderBy;
            return this;
        }
        
        public Query<T> build() {
            return new Query<T>(entity, params, page, dataScope, startTime, endTime, orderBy);
        }
    }
}
