package com.daffodil.framework.request;

import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 
 * @author yweijian
 * @date 2024年5月9日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public class HttpServletRequestContextHolder {

    private static final ThreadLocal<ServletRequestAttributes> contextHolder = new ThreadLocal<ServletRequestAttributes>();

    public static void setContextHolder(ServletRequestAttributes attributes) {
        contextHolder.set(attributes);
    }

    public static ServletRequestAttributes getContextHolder() {
        return contextHolder.get();
    }

    public static void clearContextHolder() {
        contextHolder.remove();
    }
}
