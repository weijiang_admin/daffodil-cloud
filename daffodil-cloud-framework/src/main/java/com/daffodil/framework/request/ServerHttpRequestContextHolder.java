package com.daffodil.framework.request;

import org.springframework.web.server.ServerWebExchange;

/**
 * 
 * @author yweijian
 * @date 2024年5月9日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public class ServerHttpRequestContextHolder {

    private static final ThreadLocal<ServerWebExchange> contextHolder = new ThreadLocal<ServerWebExchange>();

    public static void setContextHolder(ServerWebExchange exchange) {
        contextHolder.set(exchange);
    }

    public static ServerWebExchange getContextHolder() {
        return contextHolder.get();
    }

    public static void clearContextHolder() {
        contextHolder.remove();
    }
}
