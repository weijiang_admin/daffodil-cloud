package com.daffodil.framework.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 权限认证
 * @author yweijian
 * @date 2023年7月20日
 * @version 2.4.8+
 * @since 2.4.8+
 * @description
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Permission {

    public final static String COMN = "COMN";

    /** 权限归属 */
    public String[] belongs() default { COMN };

    /** 权限名称 */
    public String name() default "";

    /** 权限字符，一般组成结构为[模块]:[模块]:[功能] 例：system:user:add */
    public String value() default "";
}
