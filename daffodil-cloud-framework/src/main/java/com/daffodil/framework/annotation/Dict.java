package com.daffodil.framework.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * -系统字典配置<br>
 * @author yweijian
 * @date 2020年3月14日
 * @version 2.0
 * @Deprecated 2.4.0 废弃使用仅做标记
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Documented
public @interface Dict {

    public String value() default "";
}
