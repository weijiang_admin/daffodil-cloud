package com.daffodil.framework.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.daffodil.framework.annotation.OperBusiness.Business;

/**
 * -操作日志记录注解
 * @author yweijian
 * @date 2023年3月23日
 * @version 2.0.0
 * @description
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperLog {

    /** 所属模块 */
    public String title() default "";

    /** 操作类型 */
    public Business type() default Business.OTHER;

    /** 操作业务信息 */
    public OperBusiness business() default @OperBusiness(name = "", label = "", remark = "");

    /** 操作客户端 */
    public OperClient client() default @OperClient(name = "管理端", label = "MANAGE", remark = "");

    /** 是否保存请求的参数 */
    public boolean isSaveRequestData() default true;

}
