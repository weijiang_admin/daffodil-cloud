package com.daffodil.framework.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 操作客户端信息
 * @author yweijian
 * @date 2023年3月23日
 * @version 2.0.0
 * @description
 */
@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperClient {

    /** 客户端名称 */
    public String name() default "";

    /** 客户端标签 */
    public String label() default "";

    /** 客户端描述 */
    public String remark() default "";
}
