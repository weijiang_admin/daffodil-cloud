package com.daffodil.framework.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 操作业务信息
 * @author yweijian
 * @date 2023年3月23日
 * @version 2.0.0
 * @description
 */
@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperBusiness {

    /** 操作名称 */
    public String name() default "";

    /** 操作标签 建议全大写字符串 */
    public String label() default "";

    /** 操作描述 */
    public String remark() default "";

    public enum Business {

        /** 其它 */
        OTHER("其它", "OTHER", ""),
        /** 新增 */
        INSERT("新增", "INSERT", ""),
        /** 修改 */
        UPDATE("修改", "UPDATE", ""),
        /** 删除 */
        DELETE("删除", "DELETE", ""),
        /** 清空 */
        CLEAN("清空", "CLEAN", "");

        private final String name;
        private final String label;
        private final String remark;

        private Business(String name, String label, String remark) {
            this.name = name;
            this.label = label;
            this.remark = remark;
        }

        public String getName() {
            return name;
        }
        public String getLabel() {
            return label;
        }
        public String getRemark() {
            return remark;
        }
    }
}
