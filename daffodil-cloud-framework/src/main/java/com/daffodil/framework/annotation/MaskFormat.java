package com.daffodil.framework.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.fasterxml.jackson.annotation.JacksonAnnotation;

/**
 * -数据脱敏
 * @author yweijian
 * @date 2024年6月13日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotation
public @interface MaskFormat {

    MaskType type();

    int count() default 0;

    int begin() default 0;

    int end() default 0;

    char chart() default '*';

    public enum MaskType {
        /** 全部 */
        ALL(0), 
        /** 头部 */
        HEAD(1), 
        /** 中间 */
        MIDDLE(2), 
        /** 尾部 */
        TAIL(3),
        /** 移动电话 */
        MOBILE(4),
        /** 邮箱 */
        EAMIL(5),
        /** 身份证 */
        ID_CARD(6),
        /** 银行卡 */
        BANK_CARD(7),
        /** 简体中文姓名 */
        ZH_CN_NAME(8),
        /** 街道地址 */
        ADDRESS(9),
        /** 密码 */
        PASSWORD(10);

        private final int value;

        MaskType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
