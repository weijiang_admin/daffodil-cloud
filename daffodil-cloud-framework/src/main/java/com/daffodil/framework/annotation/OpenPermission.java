package com.daffodil.framework.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

/**
 * -授权认证（开放）
 * @author yweijian
 * @date 2023年4月28日
 * @version 2.0.0
 * @description
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Permission(belongs = OpenPermission.OPEN)
@Documented
public @interface OpenPermission {

    public final static String OPEN = "OPEN";

    /** 开放权限名称 */
    @AliasFor(annotation = Permission.class)
    public String name() default "";

    /** 开放权限字符，一般组成结构为[模块]:[模块]:[功能] 例：system:dictionary:label */
    @AliasFor(annotation = Permission.class)
    public String value() default "";
}
