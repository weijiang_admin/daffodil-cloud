package com.daffodil.framework.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

/**
 * 授权认证
 * @author yweijian
 * @date 2021年12月17日
 * @version 1.0
 * @description
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Permission(belongs = AuthPermission.AUTH)
@Documented
public @interface AuthPermission {

    public final static String AUTH = "AUTH";

    /** 授权权限名称 */
    @AliasFor(annotation = Permission.class)
    public String name() default "";

    /** 授权权限字符，一般组成结构为[模块]:[模块]:[功能] 例：system:user:add */
    @AliasFor(annotation = Permission.class)
    public String value() default "";
}
