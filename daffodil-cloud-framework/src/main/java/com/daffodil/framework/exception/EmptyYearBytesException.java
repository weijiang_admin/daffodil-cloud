package com.daffodil.framework.exception;

/**
 * -年份内容字节数组空异常
 * @author yweijian
 * @date 2023年2月13日
 * @version 2.0.0
 * @description
 */
public class EmptyYearBytesException extends BaseException {

    private static final long serialVersionUID = 1L;

    public EmptyYearBytesException() {
        super();
        super.code = 500;
        super.message = "Empty year content byte array exception";
    }

}
