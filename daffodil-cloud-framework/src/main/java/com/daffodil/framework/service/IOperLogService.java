package com.daffodil.framework.service;

import com.daffodil.framework.model.OperLogReq;

/**
 * @author yweijian
 * @date 2025年3月6日
 * @version 2.0.0
 * @copyright Copyright 2020 - 2025 www.daffodilcloud.com.cn
 * @description 
 */
public interface IOperLogService {

    /**
     * 插入操作日志
     * @param operLogReq
     */
    public void insertOperlog(OperLogReq operLogReq);
}
