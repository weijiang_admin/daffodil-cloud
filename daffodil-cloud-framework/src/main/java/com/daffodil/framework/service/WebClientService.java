package com.daffodil.framework.service;

import java.net.URI;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestBodySpec;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.framework.request.HttpServletRequestContextHolder;
import com.daffodil.framework.request.ServerHttpRequestContextHolder;
import com.daffodil.framework.util.TenantUtils;
import com.daffodil.util.ServletUtils;
import com.daffodil.util.SpringBeanUtils;
import com.daffodil.util.StringUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * 
 * @author yweijian
 * @date 2023年3月27日
 * @version 2.0.0
 * @description
 */
@Slf4j
@Setter
@Getter
@Component
public class WebClientService {

    @Autowired
    private WebClient webClient;

    public String exchangeToString(HttpMethod method, URI uri, HttpHeaders httpHeaders, Object body) {
        return this.exchange(method, uri, httpHeaders, body, String.class);
    }

    public JsonResult exchangeToJsonResult(HttpMethod method, URI uri) {
        HttpHeaders httpHeaders = this.getCurrentRequestContextHolderHttpHeaders();
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return Optional.ofNullable(this.exchange(method, uri, httpHeaders, null, JsonResult.class)).orElse(JsonResult.error());
    }

    public JsonResult exchangeToJsonResult(HttpMethod method, URI uri, Object body) {
        HttpHeaders httpHeaders = this.getCurrentRequestContextHolderHttpHeaders();
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return Optional.ofNullable(this.exchange(method, uri, httpHeaders, body, JsonResult.class)).orElse(JsonResult.error());
    }

    public JsonResult exchangeToJsonResult(HttpMethod method, URI uri, HttpHeaders httpHeaders, Object body) {
        return Optional.ofNullable(this.exchange(method, uri, httpHeaders, body, JsonResult.class)).orElse(JsonResult.error());
    }

    public TableResult exchangeToTableResult(HttpMethod method, URI uri) {
        HttpHeaders httpHeaders = this.getCurrentRequestContextHolderHttpHeaders();
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return Optional.ofNullable(this.exchange(method, uri, httpHeaders, null, TableResult.class)).orElse(TableResult.success(Collections.emptyList()));
    }

    public TableResult exchangeToTableResult(HttpMethod method, URI uri, Object body) {
        HttpHeaders httpHeaders = this.getCurrentRequestContextHolderHttpHeaders();
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return Optional.ofNullable(this.exchange(method, uri, httpHeaders, body, TableResult.class)).orElse(TableResult.success(Collections.emptyList()));
    }

    public TableResult exchangeToTableResult(HttpMethod method, URI uri, HttpHeaders httpHeaders, Object body) {
        return Optional.ofNullable(this.exchange(method, uri, httpHeaders, body, TableResult.class)).orElse(TableResult.success(Collections.emptyList()));
    }

    public <T> T exchange(HttpMethod method, URI uri, HttpHeaders httpHeaders, Object body, Class<T> clazz) {
        Mono<T> result = this.exchangeToMono(method, uri, httpHeaders, body, clazz);
        try {
            return result.toFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public <T> Mono<T> exchangeToMono(HttpMethod method, URI uri, HttpHeaders httpHeaders, Object body, Class<T> clazz) {
        RequestBodySpec bodySpec = this.webClient.method(method).uri(uri).headers(headers -> {
            headers.addAll(httpHeaders);
            String traceId = MDC.get(FrameworkConstant.MDC_TRACE_ID);
            if(StringUtils.isNotEmpty(traceId)) {
                headers.set(FrameworkConstant.X_REQUEST_TRACEID_HEADER, traceId);
            }
            String tenantId = TenantUtils.getTenantId();
            if(StringUtils.isNotEmpty(tenantId)) {
                headers.set(FrameworkConstant.X_REQUEST_TENANTID_HEADER, tenantId);
            }
        });
        RequestHeadersSpec<?> headersSpec = this.requiresBody(method) ? bodySpec.bodyValue(body) : bodySpec;
        return headersSpec.retrieve().bodyToMono(clazz).doOnError(e -> log.error(e.getMessage()));
    }

    public HttpHeaders getCurrentRequestContextHolderHttpHeaders() {
        if(SpringBeanUtils.isReactiveApplication()) {
            ServerWebExchange exchange = ServerHttpRequestContextHolder.getContextHolder();
            return Optional.ofNullable(exchange).map(ServerWebExchange::getRequest).map(request -> {
                HttpHeaders headers = new HttpHeaders();
                request.getHeaders().forEach((key, value) -> headers.put(key, value));
                return headers;
            }).orElse(new HttpHeaders());
        }else {
            ServletRequestAttributes attributes = HttpServletRequestContextHolder.getContextHolder();
            return Optional.ofNullable(attributes).map(ServletRequestAttributes::getRequest).map(request -> ServletUtils.getHeaders(request)).orElse(new HttpHeaders());
        }
    }

    private boolean requiresBody(HttpMethod method) {
        switch (method) {
        case PUT:
        case POST:
        case PATCH:
            return true;
        default:
            return false;
        }
    }

}
