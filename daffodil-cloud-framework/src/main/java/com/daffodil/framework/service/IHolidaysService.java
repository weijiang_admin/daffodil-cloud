package com.daffodil.framework.service;

import java.util.Map;

/**
 * @author yweijian
 * @date 2023年2月13日
 * @version 2.0.0
 * @copyright Copyright 2020 - 2025 www.daffodilcloud.com.cn
 * @description
 */
public interface IHolidaysService {

    /**
     * -获取对应年份日期字节数组（52字节，416位）
     * -前2个字节为年份，中间2个字节为闰年标识（0：平年，1：闰年），尾部为12个月日期数据，每个月用4个字节32位二进制表示
     * [年份][闰年标识][1月][2月][3月]...[12月]
     * -例：0000011111100111 0000000000000000 00000000111100000110000011000001 00000011...
     * */
    public byte[] selectDaysByYear(Integer year);

    /**
     * -获取对应年份日期字节数组Map,key为年份，value为字节数组（52字节，416位）
     * -前2个字节为年份，中间2个字节为闰年标识（0：平年，1：闰年），尾部为12个月日期数据，每个月用4个字节32位二进制表示
     * [年份][闰年标识][1月][2月][3月]...[12月]
     * -例：0000011111100111 0000000000000000 00000000111100000110000011000001 00000011...
     * */
    public Map<Integer, byte[]> multiselectDaysByYear(Integer startYear, Integer endYear);
}
