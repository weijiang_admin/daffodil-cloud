package com.daffodil.framework.service;

import java.util.List;

import com.daffodil.core.entity.Page;
import com.daffodil.framework.model.User;

/**
 * 
 * @author yweijian
 * @date 2025年3月10日
 * @version 2.0.0
 * @copyright Copyright 2020-2025 www.daffodilcloud.com.cn
 * @description
 */
public interface IUserService {

    /**
     * 根据用户ID查询用户
     * 
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public User selectUserById(String userId);

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user 用户信息
     * @param page 分页对象
     * @return 用户信息集合信息
     */
    public List<User> selectUserList(User user, Page page);

}
