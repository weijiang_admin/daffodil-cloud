package com.daffodil.framework.service;

import java.util.List;

import com.daffodil.core.entity.Page;
import com.daffodil.framework.model.Role;

/**
 * 
 * @author yweijian
 * @date 2025年3月10日
 * @version 2.0.0
 * @copyright Copyright 2020 - 2025 www.daffodilcloud.com.cn
 * @description
 */
public interface IRoleService {

    /**
     * 查询角色列表
     * @param role
     * @param page
     * @return
     */
    public List<Role> selectRoleList(Role role, Page page);
}
