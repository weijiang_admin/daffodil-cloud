package com.daffodil.framework.service;

import com.daffodil.framework.model.Tag;

import java.util.List;

/**
 * @author yweijian
 * @date 2025年3月6日
 * @version 2.0.0
 * @copyright Copyright 2020 - 2025 www.daffodilcloud.com.cn
 * @description 标签服务接口，定义标签相关操作的抽象方法
 */
public interface ITagService {

    /**
     * 根据标签名称查询标签列表
     * @param tagLabel 标签名称
     * @return 符合条件的标签列表
     */
    List<Tag> selectTagByLabel(String tagLabel);
}