package com.daffodil.framework.service;

import java.util.List;

import com.daffodil.framework.model.Dictionary;

/**
 * 字典服务
 * @author yweijian
 * @date 2025年3月06日
 * @version 2.0.0
 * @copyright Copyright 2020-2025 www.daffodilcloud.com.cn
 * @description
 */
public interface IDictionaryService {

    /**
     * 根据字典标签查询字典数据信息
     * @param dictLabel 字典标签
     * @return 参数键值
     */
    public List<Dictionary> selectDictionaryByLabel(String dictLabel);
}
