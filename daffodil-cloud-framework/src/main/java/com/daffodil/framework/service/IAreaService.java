package com.daffodil.framework.service;

import java.util.List;

import com.daffodil.framework.model.Area;

/**
 * 
 * @author yweijian
 * @date 2025年3月06日
 * @version 2.0.0
 * @copyright Copyright 2020-2025 www.daffodilcloud.com.cn
 * @description
 */
public interface IAreaService {

    /**
     * -根据区划父级ID查询子级区划列表
     * @param parentId
     * @return
     */
    public List<Area> selectAreaListByParentId(String parentId);
}
