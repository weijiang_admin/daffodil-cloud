package com.daffodil.framework.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author yweijian
 * @date 2024年12月20日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Setter
@Getter
@Component
@RefreshScope
public class FrameworkProperties {

    @Value("${spring.application.name:}")
    private String appName;

    /**
     * -是否记录操作日志，默认true
     */
    @Value("${daffodil.operlog.enable:true}")
    private Boolean operlogEnable;

    /**
     * -是否演示模式，演示模式下不允许操作，默认false
     */
    @Value("${daffodil.demo.enable:false}")
    private Boolean demoEnable;

    /**
     * -演示模式下，不允许操作集合多个操作用逗号分割
     */
    @Value("${daffodil.demo.not-allowed:DELETE,IMPORT,CLEAN,TRASH,UPLOAD}")
    private String notAllowed;

    /**
     * -是否开启路由追踪，默认true
     */
    @Value("${daffodil.trace.enable:true}")
    private Boolean traceEnable;

    /**
     * -是否开启注册路由，默认true
     */
    @Value("${daffodil.router.register.enable:true}")
    private Boolean registerRouterEnable;

    /**
     * -是否开启租户，默认false
     */
    @Value("${daffodil.tenant.enable:false}")
    private Boolean tenantEnable;

    /**
     * 是否多数据源，默认false
     */
    @Value("${daffodil.multiple.datasource:false}")
    private Boolean multipleDatasource;
}
