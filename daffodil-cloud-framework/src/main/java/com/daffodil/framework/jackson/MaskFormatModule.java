package com.daffodil.framework.jackson;

import com.fasterxml.jackson.databind.module.SimpleModule;

public class MaskFormatModule extends SimpleModule {

    private static final long serialVersionUID = 1L;

    public MaskFormatModule() {
        super("MaskFormatModule");
        addSerializer(new MaskFormatSerializer(null));
    }

}
