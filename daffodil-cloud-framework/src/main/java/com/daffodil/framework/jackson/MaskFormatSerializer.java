package com.daffodil.framework.jackson;

import com.daffodil.framework.annotation.MaskFormat;
import com.daffodil.framework.annotation.MaskFormat.MaskType;
import com.daffodil.util.StringUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * -数据脱敏序列化
 * @author yweijian
 * @date 2024年6月13日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public class MaskFormatSerializer extends StdSerializer<String> implements ContextualSerializer {

    private static final long serialVersionUID = 1L;

    private MaskFormat maskFormat;

    protected MaskFormatSerializer(MaskFormat maskFormat) {
        super(String.class);
        this.maskFormat = maskFormat;
    }

    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (maskFormat != null && StringUtils.isNotEmpty(value)) {
            MaskType type = maskFormat.type();
            int count = maskFormat.count();
            int begin = maskFormat.begin();
            int end = maskFormat.end();
            char chart = maskFormat.chart();
            String maskedValue = this.maskFormatValue(value, type, count, begin, end, chart);
            gen.writeString(maskedValue);
        }else {
            if(value == null) {
                gen.writeNull();
            }else {
                gen.writeString(value);
            }
        }
    }

    private String maskFormatValue(String value, MaskType type, int count, int begin, int end, char chart) {
        if(StringUtils.isBlank(value)) {
            return value;
        }
        switch (type) {
        case ALL:
        case PASSWORD:
            // 全部脱敏逻辑
            begin = 0;
            end = value.length();
            break;
        case HEAD:
            // 头部脱敏逻辑
            begin = 0;
            end = count > 0 ? count : end;
            break;
        case MIDDLE:
            // 中间脱敏逻辑
            begin = begin < 0 ? 0 : begin;
            end = count > 0 ? begin + count : end;
            break;
        case TAIL:
            // 尾部脱敏逻辑
            begin = count > 0 ? (value.length() > count ? value.length() - count : 0) : begin;
            end = value.length();
            break;
        case MOBILE:
            // 移动电话脱敏逻辑
            begin = 3;
            end = value.length() - 4;
            break;
        case EAMIL:
            // 邮箱脱敏逻辑
            begin = 1;
            end = value.indexOf("@");
            break;
        case ID_CARD:
        case BANK_CARD:
            // 身份证 银行卡 脱敏逻辑
            begin = 4;
            end = value.length() - 4;
            break;
        case ZH_CN_NAME:
            // 中文姓名脱敏逻辑
            begin = 1;
            end = value.length() > 2 ? value.length() - 1 : value.length();
            break;
        case ADDRESS:
            // 街道地址脱敏逻辑
            begin = value.length() - 4;
            end = value.length() - 1;
            break;
        default:
            throw new IllegalArgumentException("Unsupported MaskType: " + type);
        }
        if (value == null || begin < 0 || end > value.length() || begin >= end) {
            return value;
        }
        StringBuilder maskedValue = new StringBuilder(value);
        for (int i = begin; i < end; i++) {
            maskedValue.setCharAt(i, chart);
        }
        return maskedValue.toString();
    }

    @Override
    public JsonSerializer<?> createContextual(SerializerProvider provider, BeanProperty property) throws JsonMappingException {
        if(null == property) {
            return this;
        }
        MaskFormat maskFormat = property.getAnnotation(MaskFormat.class);
        if(null != maskFormat) {
            return new MaskFormatSerializer(maskFormat);
        }
        return this;
    }

}
