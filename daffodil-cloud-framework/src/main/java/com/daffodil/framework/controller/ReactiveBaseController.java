package com.daffodil.framework.controller;

import java.beans.PropertyEditorSupport;
import java.util.Date;
import java.util.Map;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.daffodil.core.entity.BaseEntity;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.util.StringUtils;
import com.daffodil.util.text.Convert;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

/**
 * web层通用数据处理
 * @author yweijian
 * @date 2023年2月24日
 * @version 2.0.0
 * @description
 */
public class ReactiveBaseController {
    
    private static int DEFAULT_PAGE_NUM  = 1;
    
    private static int DEFAULT_PAGE_SIZE  = 15;
    
    @SuppressWarnings("rawtypes")
    protected Query query;
    
    /**
     * -初始化Query
     */
    protected void initQuery(){
        this.initQuery(null, null, null, null);
    }
    
    /**
     * -初始化Query
     */
    protected void initQuery(ServerHttpRequest request){
        this.initQuery(null, null, null, request);
    }
    
    /**
     * -初始化Query
     * @param entity
     */
    protected void initQuery(BaseEntity<?> entity, ServerHttpRequest request){
        this.initQuery(entity, null, null, request);
    }
    
    /**
     * -初始化Query
     * @param params
     */
    protected void initQuery(Map<String, Object> params, ServerHttpRequest request){
        this.initQuery(null, params, null, request);
    }
    
    /**
     * -初始化Query
     * @param page
     */
    protected void initQuery(Page page, ServerHttpRequest request){
        this.initQuery(null, null, page, request);
    }
    
    /**
     * -初始化Query
     * @param entity
     * @param params
     */
    protected void initQuery(BaseEntity<?> entity, Map<String, Object> params, ServerHttpRequest request){
        this.initQuery(entity, params, null, request);
    }
    
    /**
     * -初始化Query
     * @param entity
     * @param page
     */
    protected void initQuery(BaseEntity<?> entity, Page page, ServerHttpRequest request){
        this.initQuery(entity, null, page, request);
    }
    
    /**
     * -初始化Query
     * @param params
     * @param page
     */
    protected void initQuery(Map<String, Object> params, Page page, ServerHttpRequest request){
        this.initQuery(null, params, page, request);
    }
    
    /**
     * -初始化Query
     * @param entity 实体参数
     * @param params Map参数
     * @param page 分页，每页显示默认15条记录
     */
    protected void initQuery(BaseEntity<?> entity, Map<String, Object> params, Page page, ServerHttpRequest request){
        Query<Object> query = new Query<Object>();
        if(StringUtils.isNotNull(page) && StringUtils.isNotNull(request)){
            page.setPageNumber(Convert.toInt(request.getQueryParams().getFirst(FrameworkConstant.PAGE_NUM), DEFAULT_PAGE_NUM));
            page.setPageSize(Convert.toInt(request.getQueryParams().getFirst(FrameworkConstant.PAGE_SIZE), DEFAULT_PAGE_SIZE));
            query.setPage(page);
        }
        if(StringUtils.isNotNull(request)) {
            String column = request.getQueryParams().getFirst(FrameworkConstant.ORDER_BY_COLUMN);
            if(StringUtils.isNotEmpty(column)){
                String orderBy = column;
                if(orderBy.startsWith(",")) {
                    orderBy = orderBy.substring(1);
                }
                if(orderBy.endsWith(",")) {
                    orderBy = orderBy.substring(0, orderBy.length() - 1);
                }
                String asc = request.getQueryParams().getFirst(FrameworkConstant.IS_ASC);
                if(StringUtils.isNotEmpty(asc)) {
                    orderBy += "asc".equalsIgnoreCase(asc) ? " asc" : " desc";
                }
                query.setOrderBy(orderBy);
            }
            if(StringUtils.isNotEmpty(request.getQueryParams().getFirst(FrameworkConstant.START_TIME))){
                DateTime startTime = DateUtil.parse(request.getQueryParams().getFirst(FrameworkConstant.START_TIME));
                query.setStartTime(startTime);
            }
            if(StringUtils.isNotEmpty(request.getQueryParams().getFirst(FrameworkConstant.END_TIME))){
                DateTime endTime = DateUtil.parse(request.getQueryParams().getFirst(FrameworkConstant.END_TIME));
                query.setEndTime(endTime);
            }
        }
        query.setEntity(entity);
        query.setParams(params);
        this.setQuery(query);
    }

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                if(StringUtils.isNotEmpty(text)) {
                    setValue(DateUtil.parse(text));
                }
            }
        });
    }

    @SuppressWarnings("rawtypes")
    public Query getQuery() {
        return query;
    }

    @SuppressWarnings("rawtypes")
    public void setQuery(Query query) {
        this.query = query;
    }
    
}
