package com.daffodil.framework.runner;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.reactive.result.method.RequestMappingInfo;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.util.pattern.PathPattern;

import com.daffodil.framework.annotation.Permission;
import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.framework.model.Router;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.SpringBeanUtils;
import com.daffodil.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author yweijian
 * @date 2022年3月2日
 * @version 1.0
 * @description
 */
@Component
@ConditionalOnWebApplication(type = Type.REACTIVE)
public class RegisterRouterFromWebfluxApplication implements RegisterRouterFromApplication {

    private static final String EMPTY_STRING = "";

    @Override
    @SuppressWarnings("unchecked")
    public void register() {
        RequestMappingHandlerMapping requestMapping = SpringBeanUtils.getBean(RequestMappingHandlerMapping.class);
        if (StringUtils.isNull(requestMapping)) {
            return;
        }
        RedisTemplate<String, Object> redisTemplate = SpringBeanUtils.getBean(RedisTemplate.class);
        if (StringUtils.isNull(redisTemplate)) {
            return;
        }
        Pattern pattern = Pattern.compile("\\{.*\\}", Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        Map<RequestMappingInfo, HandlerMethod> handlerMethods = requestMapping.getHandlerMethods();
        handlerMethods.forEach((mapper, method) -> {
            Api api = method.getBeanType().getAnnotation(Api.class);
            ApiOperation operation = method.getMethodAnnotation(ApiOperation.class);
            Permission permission = method.getMethodAnnotation(Permission.class);
            Set<PathPattern> pathPatterns = mapper.getPatternsCondition().getPatterns();
            for(PathPattern pathPattern : pathPatterns){
                String path = pathPattern.getPatternString();
                Matcher matcher = pattern.matcher(path);
                String key = matcher.find() ? FrameworkConstant.ROUTER_DYNAMIC_CACHEKEY : FrameworkConstant.ROUTER_STATIC_CACHEKEY;
                String perm = Optional.ofNullable(permission).map(p -> p.value()).orElse(EMPTY_STRING);
                String name1 = Optional.ofNullable(permission).map(p -> p.name()).orElse(EMPTY_STRING);
                String name2 = Optional.ofNullable(operation).map(o -> o.value()).orElse(EMPTY_STRING);
                String name = StringUtils.isNotEmpty(name1) ? name1 : StringUtils.isNotEmpty(name2) ? name2 : path;
                String module = Optional.ofNullable(api).map(a -> {
                    String[] tags = a.tags();
                    return tags != null && tags.length > 0 ? tags[0] : EMPTY_STRING;
                }).orElse(EMPTY_STRING);
                String remark = Optional.ofNullable(operation).map(o -> o.notes()).orElse(EMPTY_STRING);
                if(StringUtils.isNotNull(permission)) {
                    for(String belong : permission.belongs()) {
                        Router router = Router.builder().key(key).path(path).perm(perm).name(name).module(module).remark(remark).belong(belong).build();
                        redisTemplate.opsForHash().put(key, belong + path, JacksonUtils.toJSONString(router));
                    }
                }else {
                    Router router = Router.builder().key(key).path(path).perm(perm).name(name).module(module).remark(remark).belong(Permission.COMN).build();
                    redisTemplate.opsForHash().put(key, Permission.COMN + path, JacksonUtils.toJSONString(router));
                }
            }
        });
    }

}
