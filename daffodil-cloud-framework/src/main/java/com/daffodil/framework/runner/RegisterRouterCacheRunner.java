package com.daffodil.framework.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.daffodil.framework.properties.FrameworkProperties;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author yweijian
 * @date 2024年5月10日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Slf4j
@Component
public class RegisterRouterCacheRunner implements ApplicationRunner {

    @Autowired
    private FrameworkProperties frameworkProperties;

    @Autowired
    private RegisterRouterFromApplication registerRouterFromApplication;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if(frameworkProperties.getRegisterRouterEnable()) {
            if(log.isInfoEnabled()) {
                log.info("The registration route cache running program has started");
                log.info("Set [daffodil.router.register.enable = false] if you want to turn it off");
            }
            registerRouterFromApplication.register();
        }
    }

}
