package com.daffodil.framework.runner;

/**
 * 
 * @author yweijian
 * @date 2024年5月10日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 * <p>springboot2.6.x以及上版本默认使用的是PATH_PATTERN_PARSER，而2.6.0以下版本默认使用的是ANT_PATH_MATCHER </p>
 * <p>使用ANT_PATH_MATCHER则yaml配置为 spring.mvc.pathmatch.matching-strategy: ANT_PATH_MATCHER </p>
 */
public interface RegisterRouterFromApplication {

    public void register();
}
