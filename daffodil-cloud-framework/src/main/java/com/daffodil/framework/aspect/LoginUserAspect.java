package com.daffodil.framework.aspect;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.framework.model.LoginUser;
import com.daffodil.framework.request.HttpServletRequestContextHolder;
import com.daffodil.framework.request.ServerHttpRequestContextHolder;
import com.daffodil.framework.util.LoginUserUtils;
import com.daffodil.util.SpringBeanUtils;

/**
 * 
 * @author yweijian
 * @date 2023年2月14日
 * @version 2.0.0
 * @description
 */
@Aspect
@Component
public class LoginUserAspect {

    @Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping)")
    public void getPointCut() {

    }

    @Pointcut("@annotation(org.springframework.web.bind.annotation.PostMapping)")
    public void postPointCut() {

    }

    @Pointcut("@annotation(org.springframework.web.bind.annotation.PutMapping)")
    public void putPointCut() {

    }

    @Pointcut("@annotation(org.springframework.web.bind.annotation.PatchMapping)")
    public void patchPointCut() {

    }

    @Pointcut("@annotation(org.springframework.web.bind.annotation.DeleteMapping)")
    public void deletePointCut() {

    }

    @Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public void requestPointCut() {

    }

    @Around("getPointCut() || postPointCut() || putPointCut() || patchPointCut() || deletePointCut() || requestPointCut()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        if(SpringBeanUtils.isReactiveApplication()) {
            ServerWebExchange exchange = ServerHttpRequestContextHolder.getContextHolder();
            ServerHttpRequest request = Optional.ofNullable(exchange).map(o -> o.getRequest()).orElse(null);
            this.setLoginUserInfo(args, request);
        }else {
            ServletRequestAttributes attributes = HttpServletRequestContextHolder.getContextHolder();
            HttpServletRequest request = Optional.ofNullable(attributes).map(o -> o.getRequest()).orElse(null);
            this.setLoginUserInfo(args, request);
        }
        return joinPoint.proceed(args);
    }

    private void setLoginUserInfo(Object[] args, ServerHttpRequest request) {
        if(args != null && args.length > 0) {
            for(int i = 0; i < args.length; i++) {
                if(args[i] instanceof LoginUser) {
                    args[i] = LoginUserUtils.getLoginUser(request);
                }
            }
        }
    }

    private void setLoginUserInfo(Object[] args, HttpServletRequest request) {
        if(args != null && args.length > 0) {
            for(int i = 0; i < args.length; i++) {
                if(args[i] instanceof LoginUser) {
                    args[i] = LoginUserUtils.getLoginUser(request);
                }
            }
        }
    }

}
