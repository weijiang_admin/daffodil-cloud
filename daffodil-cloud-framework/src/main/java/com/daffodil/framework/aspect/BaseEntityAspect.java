package com.daffodil.framework.aspect;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.framework.request.HttpServletRequestContextHolder;
import com.daffodil.framework.request.ServerHttpRequestContextHolder;
import com.daffodil.framework.util.LoginUserUtils;
import com.daffodil.util.ReflectUtils;
import com.daffodil.util.SpringBeanUtils;
import com.daffodil.util.StringUtils;

/**
 * -Entity基类前置处理
 * @author yweijian
 * @date 2022年12月13日
 * @version 2.0.0
 * @description
 */
@Aspect
@Order(0)
@Component
public class BaseEntityAspect {

    /**
     *- 配置织入点
     */
    @Pointcut("@annotation(com.daffodil.framework.annotation.OperLog)")
    public void logPointCut() {

    }

    /**
     * -请求之前
     * @param point
     * @throws Throwable
     */
    @Before("logPointCut()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        OperLog log = this.getAnnotationLog(joinPoint);
        if (log == null) {
            return;
        }
        if(StringUtils.isEmpty(joinPoint.getArgs())) {
            return;
        }
        if(Business.INSERT.equals(log.type())) {
            this.invokeEntitySetter(joinPoint, "create");
            this.invokeEntitySetter(joinPoint, "update");
        }else if(Business.UPDATE.equals(log.type())) {
            this.invokeEntitySetter(joinPoint, "update");
        }
    }

    /**
     * -设置实体对象值
     * @param joinPoint
     * @param keyname
     */
    private void invokeEntitySetter(JoinPoint joinPoint, String keyname) {
        String loginName = this.getLoginName(joinPoint);
        for(Object entity : joinPoint.getArgs()) {
            if(entity instanceof BaseEntity) {
                ReflectUtils.invokeSetter(entity, keyname + "By", loginName);
                ReflectUtils.invokeSetter(entity, keyname + "Time", new Date());
            }
        }
    }

    /**
     * -获取登录用户名
     * @param joinPoint
     * @return
     */
    private String getLoginName(JoinPoint joinPoint) {
        if(SpringBeanUtils.isReactiveApplication()) {
            ServerWebExchange exchange = ServerHttpRequestContextHolder.getContextHolder();
            ServerHttpRequest request = Optional.ofNullable(exchange).map(o -> o.getRequest()).orElse(null);
            LoginUser user = LoginUserUtils.getLoginUser(request);
            return Optional.ofNullable(user).map(o -> o.getLoginName()).orElse(null);
        }else {
            ServletRequestAttributes attributes = HttpServletRequestContextHolder.getContextHolder();
            HttpServletRequest request = Optional.ofNullable(attributes).map(o -> o.getRequest()).orElse(null);
            LoginUser user = LoginUserUtils.getLoginUser(request);
            return Optional.ofNullable(user).map(o -> o.getLoginName()).orElse(null);
        }
    }

    /**
     * -是否存在注解，如果存在就获取日志
     * @param joinPoint
     * @return
     * @throws Exception
     */
    private OperLog getAnnotationLog(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        if (method != null) {
            return method.getAnnotation(OperLog.class);
        }
        return null;
    }

}
