package com.daffodil.framework.aspect;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.dubbo.config.annotation.DubboReference;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMessage;
import org.springframework.http.codec.multipart.Part;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.framework.exception.BaseException;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.framework.model.OperLogReq;
import com.daffodil.framework.model.OperLogReq.OperLogReqBuilder;
import com.daffodil.framework.properties.FrameworkProperties;
import com.daffodil.framework.request.HttpServletRequestContextHolder;
import com.daffodil.framework.request.ServerHttpRequestContextHolder;
import com.daffodil.framework.service.IOperLogService;
import com.daffodil.framework.util.LoginUserUtils;
import com.daffodil.framework.util.OperLogUtils;
import com.daffodil.util.IpUtils;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.ServletUtils;
import com.daffodil.util.SpringBeanUtils;
import com.daffodil.util.StringUtils;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * -操作日志记录
 * @author yweijian
 * @date 2022年12月13日
 * @version 2.0.0
 * @description
 */
@Slf4j
@Aspect
@Order(1)
@Component
public class OperLogAspect {

    private ThreadLocal<String> threadLocalOperParam = new ThreadLocal<String>();

    @Autowired
    private FrameworkProperties frameworkProperties;

    @DubboReference
    private IOperLogService operLogService;

    private ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    /**
     *- 配置织入点
     */
    @Pointcut("@annotation(com.daffodil.framework.annotation.OperLog)")
    public void logPointCut() {

    }

    /**
     * -请求之前
     * @param point
     * @throws Throwable
     */
    @Before("logPointCut()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        if(frameworkProperties.getDemoEnable()){
            OperLog log = this.getAnnotationLog(joinPoint);
            if (log == null) {
                return;
            }
            String label = OperLogUtils.getBusinessLabel(log);
            if(frameworkProperties.getNotAllowed().contains(label)) {
                throw new BaseException("演示模式下不允许操作");
            }
        }
        if(frameworkProperties.getOperlogEnable()) {
            String params = this.filterJoinPointArgs(joinPoint);
            this.threadLocalOperParam.set(params);
        }
    }

    /**
     * -处理完请求后执行
     * @param joinPoint
     */
    @AfterReturning(pointcut = "logPointCut()", returning = "results")
    public void doAfterReturning(JoinPoint joinPoint, Object results) {
        if(frameworkProperties.getOperlogEnable()) {
            this.handleOperLog(joinPoint, results, null);
            this.threadLocalOperParam.remove();
        }
    }

    /**
     * -拦截异常操作
     * @param joinPoint
     * @param e
     */
    @AfterThrowing(value = "logPointCut()", throwing = "exception")
    public void doAfterThrowing(JoinPoint joinPoint, Exception exception) {
        if(frameworkProperties.getOperlogEnable()) {
            this.handleOperLog(joinPoint, null, exception);
            this.threadLocalOperParam.remove();
        }
    }

    /**
     * -处理日志
     * @param joinPoint
     * @param exception
     */
    protected void handleOperLog(final JoinPoint joinPoint, Object results, final Exception exception) {
        OperLog operLog = this.getAnnotationLog(joinPoint);
        if (operLog == null) {
            return;
        }
        HttpHeaders headers = null;
        OperLogReqBuilder builder = OperLogReq.builder();
        if(SpringBeanUtils.isReactiveApplication()) {
            ServerWebExchange exchange = ServerHttpRequestContextHolder.getContextHolder();
            ServerHttpRequest request = null == exchange ? null : exchange.getRequest();
            LoginUser loginUser = LoginUserUtils.getLoginUser(request);
            builder.operName(Optional.ofNullable(loginUser).map(o -> o.getLoginName()).orElse(""));
            builder.operUrl(Optional.ofNullable(request).map(o -> o.getPath().value()).orElse(""));
            builder.operIp(IpUtils.getIpAddrByServerHttpRequest(request));
            headers = null != request ? request.getHeaders() : null;
        }else {
            ServletRequestAttributes attributes = HttpServletRequestContextHolder.getContextHolder();
            HttpServletRequest request = null == attributes ? null : attributes.getRequest();
            LoginUser loginUser = LoginUserUtils.getLoginUser(request);
            builder.operName(Optional.ofNullable(loginUser).map(o -> o.getLoginName()).orElse(""));
            builder.operUrl(Optional.ofNullable(request).map(o -> o.getRequestURI()).orElse(""));
            builder.operIp(IpUtils.getIpAddrByHttpServletRequest(request));
            headers = null != request ? ServletUtils.getHeaders(request) : null;
        }
        builder.title(operLog.title());
        builder.method(StringUtils.format("{}.{}()", joinPoint.getTarget().getClass().getName(), joinPoint.getSignature().getName()));
        builder.businessName(OperLogUtils.getBusinessName(operLog));
        builder.businessLabel(OperLogUtils.getBusinessLabel(operLog));
        builder.businessRemark(OperLogUtils.getBusinessRemark(operLog));
        builder.clientName(operLog.client().name());
        builder.clientLabel(operLog.client().label());
        builder.clientRemark(operLog.client().remark());
        builder.status(null != exception ? FrameworkConstant.ERROR : FrameworkConstant.SUCCESS);
        builder.errorMsg(null != exception ? exception.getMessage() : "");
        builder.operParam(this.threadLocalOperParam.get());
        builder.operResult(this.getOperResult(results));
        builder.createTime(new Date());
        builder.operTraceId(Optional.ofNullable(headers).map(o -> o.getFirst(FrameworkConstant.X_REQUEST_TRACEID_HEADER)).orElse(""));
        builder.operHeader(JacksonUtils.toJSONString(headers));
        builder.tenantId(Optional.ofNullable(headers).map(o -> o.getFirst(FrameworkConstant.X_REQUEST_TENANTID_HEADER)).orElse(null));

        CompletableFuture.runAsync(() -> {
            OperLogReq operLogReq = builder.build();
            TenantContextHolder.apply(operLogReq.getTenantId(), operLogReq.getOperTraceId(), () -> operLogService.insertOperlog(operLogReq));
        }, executor);
    }

    /**
     * -尽可能的过滤并获取请求参数转成字符串
     * @param joinPoint
     * @return
     */
    @SuppressWarnings("rawtypes")
    private String filterJoinPointArgs(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        List<Object> list = Arrays.stream(args).filter(arg -> {
            if(null == arg) {
                return false;
            }
            Class<?> clazz = arg.getClass();
            if(clazz.isArray()) {
                if(clazz.getComponentType().isAssignableFrom(MultipartFile.class) || clazz.getComponentType().isAssignableFrom(Part.class)) {
                    return false;
                }
            }else if(Collection.class.isAssignableFrom(clazz)) {
                Collection<?> collection = (Collection<?>) arg;
                for (Iterator<?> iterator = collection.iterator(); iterator.hasNext();) {
                    Object object = (Object) iterator.next();
                    if(object instanceof MultipartFile || object instanceof Part) {
                        return false;
                    }
                }
            }else if(Map.class.isAssignableFrom(clazz)) {
                Map<?, ?> map = (Map<?, ?>) arg;
                for (Map.Entry entry : map.entrySet()) {
                    Object object = entry.getValue();
                    if(object instanceof MultipartFile || object instanceof Part) {
                        return false;
                    }
                }
            }else {
                if(arg instanceof MultipartFile || arg instanceof Part || arg instanceof ServletRequest || arg instanceof ServletResponse || arg instanceof HttpMessage) {
                    return false;
                }
                if(arg instanceof Serializable) {
                    return true;
                }
            }
            return true;
        }).collect(Collectors.toList());

        StringBuilder params = new StringBuilder(64);
        params.append(StrUtil.BRACKET_START);
        for (int i = 0; i < list.size(); i++) {
            Object obj = list.get(i);
            String str = JacksonUtils.toJSONString(obj);
            if(StringUtils.isNotEmpty(str)) {
                if(obj instanceof String) {
                    params.append("\"");
                    params.append(obj.toString());
                    params.append("\"");
                }else {
                    params.append(str);
                }
                params.append(((i != list.size() - 1) ? StrUtil.COMMA : StrUtil.EMPTY));
            }
        }
        params.append(StrUtil.BRACKET_END);

        return params.toString();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private String getOperResult(Object results) {
        String operResult = "";
        if(null == results) {
            return operResult;
        }
        try {
            if(results instanceof Mono) {
                Mono<JsonResult> result = (Mono<JsonResult>) results;
                JsonResult jsonResult = result.toFuture().get();
                if(null != jsonResult) {
                    operResult = JacksonUtils.toJSONString(jsonResult.getData());
                }else {
                    Object obj = ((Mono) results).toFuture().get();
                    operResult = JacksonUtils.toJSONString(obj);
                }
            }else if(results instanceof JsonResult){
                JsonResult jsonResult = (JsonResult) results;
                if(null != jsonResult) {
                    operResult = JacksonUtils.toJSONString(jsonResult.getData());
                }
            }else {
                operResult = JacksonUtils.toJSONString(results);
            }
        }catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
        return operResult;
    }

    /**
     * -是否存在注解，如果存在就获取日志
     * @param joinPoint
     * @return
     * @throws Exception
     */
    private OperLog getAnnotationLog(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        if (method != null) {
            return method.getAnnotation(OperLog.class);
        }
        return null;
    }

}
