package com.daffodil.framework.model;

import java.io.Serializable;

import lombok.Data;

/**
 * 数据标签
 * @author yweijian
 * @date 2023年2月14日
 * @version 2.0.0
 * @description
 */
@Data
public class Tag implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /** 标签编号 */
    private String id;

    /** 标签（目录）名称 */
    private String tagName;

    /** 标签类型（catalog目录 tag标签） */
    private String tagType;

    /** 目录键值 */
    private String tagLabel;

    /** 标签键值 */
    private String tagValue;

    /** 是否默认（Y是 N否） */
    private String isDefault;

    /** 父标签ID */
    private String parentId;

    /** 祖级列表 */
    private String ancestors;

    /** 显示顺序 */
    private Long orderNum;

    /** 标签状态 */
    private String status;

    /** 备注 */
    private String remark;

}
