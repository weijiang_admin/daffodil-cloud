package com.daffodil.framework.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2022年12月21日
 * @version 2.0.0
 * @description
 */
@Data
@Builder
public class Router implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 路由类型 */
    private String key;

    /** 路由名称 */
    private String path;

    /** 权限标识 */
    private String perm;

    /** 权限名称 */
    private String name;

    /** 权限归属 */
    private String belong;

    /** 权限模块 */
    private String module;

    /** 权限备注 */
    private String remark;
}
