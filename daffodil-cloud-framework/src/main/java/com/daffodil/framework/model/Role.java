package com.daffodil.framework.model;

import java.io.Serializable;

import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2025年3月10日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Data
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 角色ID */
    private String id;

    /** 角色名称 */
    private String roleName;

    /** 角色权限 */
    private String roleKey;

    /** 数据范围（1：所有数据权限；2：自定义数据权限；3：本部门数据权限；4：本部门及以下数据权限；5：仅本用户数据权限） */
    private String dataScope;

    /** 角色状态（0正常 1停用 2删除） */
    private String status;

    /** 备注 */
    private String remark;
}
