package com.daffodil.framework.model;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -登录成功用户
 * @author yweijian
 * @date 2022年12月7日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class LoginUser extends LoginAccess {

    private static final long serialVersionUID = 1L;

    /** 部门ID */
    private String deptId;

    /** 部门名称 */
    private String deptName;

    /** 用户名称 */
    private String userName;

    /** 用户邮箱 */
    private String email;

    /** 手机号码 */
    private String phone;

    /** 是否是超级管理员角色 */
    private Boolean isAdminRole;

    /** 角色集合 */
    private List<Role> roles;

}
