package com.daffodil.framework.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * -登录成功结果
 * @author yweijian
 * @date 2023年4月27日
 * @version 2.0.0
 * @description
 */
@Data
public class LoginAccess implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    /** 登录名称 */
    private String loginName;

    /** 帐号状态（0正常 1停用 2删除） */
    private String status;

    /** 最后登陆IP */
    private String loginIp;

    /** 真实地址 */
    private String loginAd;

    /** 操作系统 */
    private String loginOs;

    /** 浏览器类型 */
    private String loginBr;

    /** 登陆时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date loginTime;

}
