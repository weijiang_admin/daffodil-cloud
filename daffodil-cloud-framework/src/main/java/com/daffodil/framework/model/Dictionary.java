package com.daffodil.framework.model;

import java.io.Serializable;

import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2023年2月14日
 * @version 2.0.0
 * @description
 */
@Data
public class Dictionary implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 字典编号 */
    private String id;

    /** 字典（目录）名称 */
    private String dictName;

    /** 字典类型（catalog目录 dictionary字典） */
    private String dictType;

    /** 目录键值 */
    private String dictLabel;

    /** 字典键值 */
    private String dictValue;

    /** 是否默认（Y是 N否） */
    private String isDefault;

    /** 父字典ID */
    private String parentId;

    /** 祖级列表 */
    private String ancestors;

    /** 显示顺序 */
    private Long orderNum;

    /** 字典状态 */
    private String status;

    /** 备注 */
    private String remark;

}
