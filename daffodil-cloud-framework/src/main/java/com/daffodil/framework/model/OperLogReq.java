package com.daffodil.framework.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Builder;
import lombok.Data;

/**
 * -操作日志记
 * @author yweijian
 * @date 2022年12月13日
 * @version 2.0.0
 * @description
 */
@Data
@Builder
public class OperLogReq implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 操作模块 */
    private String title;

    /** 请求方法 */
    private String method;

    /** 业务名称 */
    private String businessName;

    /** 业务标签 */
    private String businessLabel;

    /** 业务备注 */
    private String businessRemark;

    /** 操作客户端 */
    private String clientName;

    /** 操作客户端标签 */
    private String clientLabel;

    /** 操作客户端备注 */
    private String clientRemark;

    /** 操作人员 */
    private String operName;

    /** 请求url */
    private String operUrl;

    /** 操作地址 */
    private String operIp;

    /** 请求追踪标识 */
    private String operTraceId;

    /** 请求头部 */
    private String operHeader;

    /** 请求参数 */
    private String operParam;

    /** 响应结果 */
    private String operResult;

    /** 操作状态（0正常 1异常） */
    private String status;

    /** 错误消息 */
    private String errorMsg;

    /** 操作时间 */
    private Date createTime;

    /** 租户标识 */
    private String tenantId;

    /** 租户顺序 */
    private Integer orderId;

}
