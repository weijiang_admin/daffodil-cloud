package com.daffodil.framework.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -登录成功应用
 * @author yweijian
 * @date 2023年4月27日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class LoginApp extends LoginAccess {

    private static final long serialVersionUID = 1L;

    /** 应用编码 */
    private String appId;

    /** 应用名称 */
    private String appName;

}
