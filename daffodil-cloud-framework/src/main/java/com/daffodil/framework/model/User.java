package com.daffodil.framework.model;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * 
 * @author yweijian
 * @date 2025年3月10日
 * @version 2.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String id;

    /** 用户头像 */
    private String avatar;

    /** 登录名称 */
    private String loginName;

    /** 帐号状态（0正常 1停用 2删除） */
    private String status;

    /** 部门ID */
    private String deptId;

    /** 部门名称 */
    private String deptName;

    /** 用户名称 */
    private String userName;

    /** 用户邮箱 */
    private String email;

    /** 手机号码 */
    private String phone;

    /** 是否是超级管理员角色 */
    private Boolean isAdminRole;

    /** 角色集合 */
    private List<Role> roles;

    /** 备注 */
    private String remark;
}
