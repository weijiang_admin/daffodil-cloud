package com.daffodil.framework.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * -日期统计数据
 * @author EP
 * @date 2023年1月31日
 * @version 1.0.0
 * @description
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HolidaysStats implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 总天数 */
    private Integer dayCount;

    /** 工作日天数 */
    private Integer workDayCount;

    /** 节假日天数 */
    private Integer holidayCount;

}
