package com.daffodil.framework.model;

import java.io.Serializable;

import lombok.Data;

/**
 * -中国行政区划代码表<br>
 * -中国行政区划代码由 1～12 位代码构成，结构为：<br>
 * -□ □ □ □ □ □ □ □ □ □ □ □ <br>
 * -1 2 3 4 5 6 7 8 9 10 11 12 <br>
 * -中国行政区划代码由 1～12 位代码构成，表示为：<br>
 * -第 1～2 位，为省级代码；<br>
 * -第 3～4 位，为地级代码；<br>
 * -第 5～6 位，为县级代码；<br>
 * -第 7～9 位，为乡级代码；<br>
 * -第 10～12 位，为村级代码。<br>
 * 
 * -资源来源根据中华人民共和国民政部
 * -2020年版 http://xzqh.mca.gov.cn/statistics/2020.html
 * 
 * @author yweijian
 * @date 2023年2月14日
 * @version 2.0.0
 * @description
 */
@Data
public class Area implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 区划代码 1～12 位代码构成 */
    private String id;

    /** 省级代码 第1～2 位 */
    private String province;

    /** 地级代码 第3～4 位 */
    private String city;

    /** 县级代码 第5～6 位 */
    private String county;

    /** 乡级代码 第7～9 位 */
    private String town;

    /** 村级代码 第10～12 位 */
    private String village;

    /** 父区划ID */
    private String parentId;

    /** 祖级列表 */
    private String ancestors;

    /** 区划名称 */
    private String areaName;

    /** 区划简称 */
    private String shortName;

    /** 省会城市 */
    private String capitalCity;

    /** 区划等级 省级=1 地级=2 县级=3 乡级=4 村级=5 */
    private Integer areaLevel;

    /** 邮政编码 */
    private String postCode;

    /** 显示顺序 */
    private Long orderNum;

    /** 区划状态（0正常 1停用 2删除） */
    private String status;

    /** 备注 */
    private String remark;

}
