package com.daffodil.framework.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.daffodil.framework.filter.HttpServletRequestTraceFilter;

/**
 * 
 * @author yweijian
 * @date 2022年12月13日
 * @version 2.0.0
 * @description
 */
@Configuration
@ConditionalOnWebApplication(type = Type.SERVLET)
public class TraceFilterConfiguration implements WebMvcConfigurer {

    @Autowired
    private HttpServletRequestTraceFilter interceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptor);
    }

}
