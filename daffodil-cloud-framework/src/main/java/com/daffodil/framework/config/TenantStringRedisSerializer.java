package com.daffodil.framework.config;

import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.daffodil.framework.context.TenantContextHolder;

/**
 * 
 * @author yweijian
 * @date 2024年12月11日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public class TenantStringRedisSerializer extends StringRedisSerializer {

    @Override
    public byte[] serialize(String key) {
        if (key == null) {
            return null;
        }
        String tenantId = TenantContextHolder.getTenantHolder();
        if (tenantId != null && !tenantId.isEmpty()) {
            key = tenantId + ":" + key;
        }
        return super.serialize(key);
    }

}
