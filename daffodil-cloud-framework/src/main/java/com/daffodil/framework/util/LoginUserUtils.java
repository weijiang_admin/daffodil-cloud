package com.daffodil.framework.util;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.JwtTokenUtils;
import com.daffodil.util.StringUtils;

/**
 * -登录用户工具集合
 * @author yweijian
 * @date 2021年9月24日
 * @version 1.0
 * @description
 */
@Component
public class LoginUserUtils {

    @Autowired
    @Qualifier("tenantRedisTemplate")
    private RedisTemplate<String, Object> tenantRedisTemplate;

    private static RedisTemplate<String, Object> redisTemplate;

    @PostConstruct
    public void init() {
        redisTemplate = this.tenantRedisTemplate;
    }

    /**
     * -获取当前登录的用户
     * @param request
     * @return
     */
    public static LoginUser getLoginUser(ServerHttpRequest request) {
        if(StringUtils.isNull(request)) {
            return null;
        }
        String authorToken = LoginUserUtils.getAuthorToken(request);
        if(JwtTokenUtils.isValid(authorToken)) {
            String data = JwtTokenUtils.getData(authorToken);
            if(StringUtils.isNotEmpty(data)) {
                LoginUser user = JacksonUtils.toJavaObject(data, LoginUser.class);
                return user;
            }
        }
        return null;
    }

    /**
     * -获取当前登录的用户
     * @param request
     * @return
     */
    public static LoginUser getLoginUser(HttpServletRequest request) {
        if(StringUtils.isNull(request)) {
            return null;
        }
        String authorToken = LoginUserUtils.getAuthorToken(request);
        if(JwtTokenUtils.isValid(authorToken)) {
            String data = JwtTokenUtils.getData(authorToken);
            if(StringUtils.isNotEmpty(data)) {
                LoginUser user = JacksonUtils.toJavaObject(data, LoginUser.class);
                return user;
            }
        }
        return null;
    }

    /**
     * -获取请求的AccessToken令牌
     * @param request
     * @return
     */
    public static String getAccessToken(ServerHttpRequest request) {
        String accessToken = null;
        if(StringUtils.isNull(request)) {
            return accessToken;
        }
        String authorization = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        if(StringUtils.isNotEmpty(authorization)) {
            accessToken = authorization.replace(FrameworkConstant.BEARER, "");
        }
        if(StringUtils.isEmpty(accessToken)) {
            accessToken = request.getQueryParams().getFirst(FrameworkConstant.ACCESS_TOKEN1);
        }
        if(StringUtils.isEmpty(accessToken)) {
            accessToken = request.getQueryParams().getFirst(FrameworkConstant.ACCESS_TOKEN2);
        }
        return accessToken;
    }

    /**
     * -获取请求的AccessToken令牌
     * @param request
     * @return
     */
    public static String getAccessToken(HttpServletRequest request) {
        String accessToken = null;
        if(StringUtils.isNull(request)) {
            return accessToken;
        }
        String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
        if(StringUtils.isNotEmpty(authorization)) {
            accessToken = authorization.replace(FrameworkConstant.BEARER, "");
        }
        if(StringUtils.isEmpty(accessToken)) {
            accessToken = request.getParameter(FrameworkConstant.ACCESS_TOKEN1);
        }
        if(StringUtils.isEmpty(accessToken)) {
            accessToken = request.getParameter(FrameworkConstant.ACCESS_TOKEN2);
        }
        return accessToken;
    }

    /**
     * -获取授权信息AuthorToken
     * @param request
     * @return
     */
    public static String getAuthorToken(ServerHttpRequest request) {
        String authorToken = null;
        if(StringUtils.isNotNull(request)) {
            authorToken = request.getHeaders().getFirst(FrameworkConstant.X_REQUEST_AUTHOR_TOKEN_HEADER);
            if(StringUtils.isEmpty(authorToken)) {
                String cacheKey = StringUtils.format(FrameworkConstant.ACCESS_TOKEN_CACHEKEY, LoginUserUtils.getAccessToken(request));
                authorToken = (String) redisTemplate.opsForValue().get(cacheKey);
            }
        }
        return authorToken;
    }

    /**
     * -获取授权信息AuthorToken
     * @param request
     * @return
     */
    public static String getAuthorToken(HttpServletRequest request) {
        String authorToken = null;
        if(StringUtils.isNotNull(request)) {
            authorToken = request.getHeader(FrameworkConstant.X_REQUEST_AUTHOR_TOKEN_HEADER);
            if(StringUtils.isEmpty(authorToken)) {
                String cacheKey = StringUtils.format(FrameworkConstant.ACCESS_TOKEN_CACHEKEY, LoginUserUtils.getAccessToken(request));
                authorToken = (String) redisTemplate.opsForValue().get(cacheKey);
            }
        }
        return authorToken;
    }

    /**
     * -获取刷新令牌RefreshToken
     * @param request
     * @return
     */
    public static String getRefreshToken(ServerHttpRequest request) {
        if(StringUtils.isNotNull(request)) {
            return request.getHeaders().getFirst(FrameworkConstant.X_REQUEST_REFRESH_TOKEN_HEADER);
        }
        return null;
    }

    /**
     * -获取刷新令牌RefreshToken
     * @param request
     * @return
     */
    public static String getRefreshToken(HttpServletRequest request) {
        if(StringUtils.isNotNull(request)) {
            return request.getHeader(FrameworkConstant.X_REQUEST_REFRESH_TOKEN_HEADER);
        }
        return null;
    }

}
