package com.daffodil.framework.util;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

import com.daffodil.framework.exception.EmptyYearBytesException;
import com.daffodil.framework.model.HolidaysStats;
import com.daffodil.framework.service.IHolidaysService;
import com.daffodil.util.StringUtils;
import com.daffodil.util.sm.CodeUtils;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import java.time.LocalDate;
import java.util.Date;
import java.util.Map;

/**
 * -日期管理工具类，用于查询指定日期或者范围的日期数据，比如节假日、工作日
 * @author EP
 * @date 2023年1月31日
 * @version 1.0
 * @description
 */
@Component
public class HolidaysUtils {

    @DubboReference
    private IHolidaysService holidaysService;

    private static IHolidaysService daysService;

    @PostConstruct
    public void init() {
        daysService = this.holidaysService;
    }

    /**
     * -获取日期范围内的日期统计数据，左右闭区间 [start, end]
     * 解析日期字符串，忽略时分秒，支持的格式包括：
     * <pre>
     * yyyy-MM-dd
     * yyyy/MM/dd
     * yyyy.MM.dd
     * yyyy年MM月dd日
     * </pre>
     * @param start String
     * @param end String
     * @return
     */
    public static HolidaysStats getDaysStats(String start, String end) throws EmptyYearBytesException {
        return getDaysStats(DateUtil.parseDate(start).toJdkDate(), DateUtil.parseDate(end).toJdkDate());
    }

    /**
     * -获取日期范围内的日期统计数据，左右闭区间 [start, end]
     * 解析日期字符串，忽略时分秒，支持的格式包括：
     * <pre>
     * yyyy-MM-dd
     * yyyy/MM/dd
     * yyyy.MM.dd
     * yyyy年MM月dd日
     * </pre>
     * @param start Date
     * @param end Date
     * @return
     */
    public static HolidaysStats getDaysStats(LocalDate start, LocalDate end) throws EmptyYearBytesException {
        return getDaysStats(DateUtil.parse(start.toString()).toJdkDate(), DateUtil.parse(end.toString()).toJdkDate()); 
    }

    /**
     * -获取日期范围内的日期统计数据，左右闭区间 [start, end]
     * 解析日期字符串，忽略时分秒，支持的格式包括：
     * <pre>
     * yyyy-MM-dd
     * yyyy/MM/dd
     * yyyy.MM.dd
     * yyyy年MM月dd日
     * </pre>
     * @param start Date
     * @param end Date
     * @return
     */
    public static HolidaysStats getDaysStats(Date start, Date end) throws EmptyYearBytesException {
        int dayCount = 0;
        int workDayCount = 0;
        int holidayCount = 0;

        DateTime startDate = new DateTime(start);
        int firstYear = startDate.year();
        int firstMonth = startDate.month() + 1;
        int firstDayMonthIndex = startDate.dayOfMonth() - 1;

        DateTime endDate = new DateTime(end);
        int lastYear = endDate.year();
        int lastMonth = endDate.month() + 1;
        int lastDayMonthIndex = endDate.dayOfMonth() - 1;

        //批量获取年份数据
        Map<Integer, byte[]> yearContentMap = daysService.multiselectDaysByYear(firstYear, lastYear);
        if(null == yearContentMap || yearContentMap.isEmpty()) {
            throw new EmptyYearBytesException();
        }

        for (int year = firstYear; year <= lastYear; year++) {
            byte[] yearContentBytes = yearContentMap.get(year);
            boolean isLeapYear = isLeapYear(yearContentBytes);

            int startMonth = year == firstYear ? firstMonth : 1;
            int endMonth =  year == lastYear ? lastMonth : 12;

            //统计每个月节假日数据
            for (int month = startMonth; month <= endMonth; month++) {
                int monthDayCount = getNormalDayCount(month, isLeapYear);
                int startDayIndex = year == firstYear ? firstDayMonthIndex : 0;
                int endDayIndex =  year == lastYear ? lastDayMonthIndex : (monthDayCount - 1);
                //统计当月天数
                int tempDayCount = monthDayCount;
                //月份数据
                int monthIntValue = getMonthIntValueFromBytes(yearContentBytes, month);

                //不足整月的，通过重置范围外的二进制位为0再进行天数统计
                if(firstMonth == month && firstYear == year && startDayIndex != 0 ){
                    monthIntValue = resetBitValue(monthIntValue, 0, startDayIndex - 1);
                    tempDayCount = tempDayCount - startDayIndex;
                }
                if(lastMonth == month && lastYear == year && endDayIndex != (monthDayCount - 1)) {
                    monthIntValue = resetBitValue(monthIntValue, endDayIndex + 1, monthDayCount - 1);
                    tempDayCount = tempDayCount - (monthDayCount - 1 - endDayIndex);
                }
                //获取二进制中1的数量即为节假日数量
                int monthBitCount = Integer.bitCount(monthIntValue);

                dayCount += tempDayCount; //总天数
                holidayCount += monthBitCount; //总节假日数
                workDayCount += (tempDayCount - monthBitCount); //总工作日数
            }
        }

        return HolidaysStats.builder()
                .dayCount(dayCount)
                .workDayCount(workDayCount)
                .holidayCount(holidayCount)
                .build();
    }

    /**
     * -获取指定日期开始第N个工作日后的日期(左右闭区间，即指定日期起第N个工作日)
     * 解析日期字符串，忽略时分秒，支持的格式包括：
     * <pre>
     * yyyy-MM-dd
     * yyyy/MM/dd
     * yyyy.MM.dd
     * yyyy年MM月dd日
     * </pre>
     * @param start
     * @param work
     * @return
     */
    public static Date getAfterWorkingDay(String start, int work) throws EmptyYearBytesException {
        return getAfterWorkingDay(DateUtil.parseDate(start).toJdkDate(), work);
    }

    /**
     * -获取指定日期开始第N个工作日后的日期(左右闭区间，即指定日期起第N个工作日)
     * 解析日期字符串，忽略时分秒，支持的格式包括：
     * <pre>
     * yyyy-MM-dd
     * yyyy/MM/dd
     * yyyy.MM.dd
     * yyyy年MM月dd日
     * </pre>
     * @param start
     * @param work
     * @return
     */
    public static Date getAfterWorkingDay(LocalDate start, int work) throws EmptyYearBytesException {
        return getAfterWorkingDay(DateUtil.parse(start.toString()).toJdkDate(), work);
    }

    /**
     * -获取指定日期开始第N个工作日后的日期(左右闭区间，即指定日期起第N个工作日)
     * 解析日期字符串，忽略时分秒，支持的格式包括：
     * <pre>
     * yyyy-MM-dd
     * yyyy/MM/dd
     * yyyy.MM.dd
     * yyyy年MM月dd日
     * </pre>
     * @param start
     * @param work
     * @return
     */
    public static Date getAfterWorkingDay(Date start, int work) throws EmptyYearBytesException {
        int workCount = 0;
        Date afterWorkingDay = null;

        DateTime startDateTime = new DateTime(start);
        int firstYear = startDateTime.year();
        int firstMonth = startDateTime.month() + 1;
        int firstDayMonthIndex = startDateTime.dayOfMonth() - 1;

        int lastYear = startDateTime.year() + 1;

        //批量获取年份数据，每次取两年数据，不够再进行递归
        Map<Integer, byte[]> yearContentMap = daysService.multiselectDaysByYear(firstYear, lastYear);
        if(null == yearContentMap || yearContentMap.isEmpty()) {
            throw new EmptyYearBytesException();
        }

        //遍历统计工作日数
        counter: for (int year = firstYear; year <= lastYear; year++) {
            byte[] yearContentBytes = yearContentMap.get(year);
            boolean isLeapYear = isLeapYear(yearContentBytes);

            int startMonth = year == firstYear ? firstMonth : 1;
            int endMonth =  12;

            for (int month = startMonth; month <= endMonth; month++) {
                //月份数据
                int monthIntValue = getMonthIntValueFromBytes(yearContentBytes, month);

                int monthDayCount = getNormalDayCount(month, isLeapYear);
                int startDayIndex = year == firstYear ? firstDayMonthIndex : 0;
                int endDayIndex =  monthDayCount - 1;

                for(int day = startDayIndex; day <= endDayIndex; day++) {
                    if((monthIntValue & (1 << day)) == 0) { //是否是工作日
                        workCount++;
                        if(work == workCount) {
                            afterWorkingDay = DateUtil.parseDate(
                                    StringUtils.format("{}-{}-{}", year, month, day + 1))
                                    .toJdkDate();
                            break counter;
                        }
                    }
                }
            }
        }

        //超过两年，递归计算下一批数据
        if (afterWorkingDay == null && workCount < work) {
            return getAfterWorkingDay(StringUtils.format("{}-01-01", lastYear + 1), work - workCount);
        }

        return afterWorkingDay;
    }

    /**
     * -获取指定日期是否为节假日
     * 解析日期字符串，忽略时分秒，支持的格式包括：
     * <pre>
     * yyyy-MM-dd
     * yyyy/MM/dd
     * yyyy.MM.dd
     * yyyy年MM月dd日
     * </pre>
     * @param holiday 日期
     * @return Boolean
     */
    public static Boolean isHoliday(String holiday) throws EmptyYearBytesException {
        return isHoliday(DateUtil.parseDate(holiday).toJdkDate());
    }

    /**
     * -获取指定日期是否为节假日
     * 解析日期字符串，忽略时分秒，支持的格式包括：
     * <pre>
     * yyyy-MM-dd
     * yyyy/MM/dd
     * yyyy.MM.dd
     * yyyy年MM月dd日
     * </pre>
     * @param holiday 日期
     * @return
     */
    public static Boolean isHoliday(LocalDate holiday) throws EmptyYearBytesException {
        return isHoliday(DateUtil.parse(holiday.toString()).toJdkDate());
    }

    /**
     * -获取指定日期是否为节假日
     * 解析日期字符串，忽略时分秒，支持的格式包括：
     * <pre>
     * yyyy-MM-dd
     * yyyy/MM/dd
     * yyyy.MM.dd
     * yyyy年MM月dd日
     * </pre>
     * @param holiday 日期
     * @return
     */
    public static Boolean isHoliday(Date holiday) throws EmptyYearBytesException {
        DateTime date = new DateTime(holiday);
        int year = date.year();
        int month = date.month() + 1;
        int dayIndex = date.dayOfMonth() - 1;
        int holidayFlag = 1 << dayIndex;

        byte[] yearContentBytes = daysService.selectDaysByYear(year);
        if(null == yearContentBytes || yearContentBytes.length <= 0) {
            throw new EmptyYearBytesException();
        }
        byte[] monthBytes = new byte[4];
        System.arraycopy(yearContentBytes, month * 4, monthBytes, 0, monthBytes.length);
        int monthData = CodeUtils.byteToIntBigEndian(monthBytes);

        return (monthData & holidayFlag) != 0;
    }

    /**
     * 根据日期元数据判断该年份是否为闰年
     * @param yearContentBytes 指定年份的日期数据字节数组
     * @return
     */
    private static boolean isLeapYear(byte[] yearContentBytes) {
        //获取闰年标识，2个字节，用short类型接收
        byte[] leapYearBytes = new byte[2];
        System.arraycopy(yearContentBytes, 2, leapYearBytes, 0, leapYearBytes.length);
        return CodeUtils.byteToShortBigEndian(leapYearBytes) == 1;
    }

    /**
     * 根据日期元数据获取指定月份数据
     * @param month 月份 从1开始
     * @return
     */
    private static Integer getMonthIntValueFromBytes(byte[] yearContentBytes, int month) {
        byte[] monthBytes = new byte[4];
        System.arraycopy(yearContentBytes, month * 4, monthBytes, 0, monthBytes.length);
        return CodeUtils.byteToIntBigEndian(monthBytes);
    }

    /**
     * 32位整型，重置指定范围二级制位数据为0
     * @param src
     * @param startPos
     * @param endPos
     * @return
     */
    private static int resetBitValue(int src, int startPos, int endPos) {
        int result = src;
        startPos = Math.max(startPos, 0);
        endPos = Math.min(endPos, 31);
        for(int i = startPos; i <= endPos; i++) {
            int flag = 1 << i;
            result &= ~flag;
        }
        return result;
    }

    /**
     * 获取默认平年中每个月的天数
     * @param month
     * @param isLeapYear
     * @return
     */
    private static int getNormalDayCount(Integer month, Boolean isLeapYear) {
        Integer[] dic = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        if (month > 12 || month <= 0) {
            return 31;
        }
        return (isLeapYear && month == 2) ? (dic[month - 1] + 1) : dic[month - 1];
    }
}
