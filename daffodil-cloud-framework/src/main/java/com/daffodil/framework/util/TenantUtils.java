package com.daffodil.framework.util;

import java.util.Optional;

import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.framework.request.HttpServletRequestContextHolder;
import com.daffodil.framework.request.ServerHttpRequestContextHolder;
import com.daffodil.util.SpringBeanUtils;

/**
 * 
 * @author yweijian
 * @date 2024年5月10日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public class TenantUtils {

    /**
     * -获取租户ID
     * @return
     */
    public static String getTenantId() {
        if(SpringBeanUtils.isReactiveApplication()) {
            ServerWebExchange exchange = ServerHttpRequestContextHolder.getContextHolder();
            String tenantId = Optional.ofNullable(exchange).map(e -> e.getRequest().getHeaders().getFirst(FrameworkConstant.X_REQUEST_TENANTID_HEADER)).orElse("");
            return tenantId;
        }else {
            ServletRequestAttributes attributes = HttpServletRequestContextHolder.getContextHolder();
            String tenantId = Optional.ofNullable(attributes).map(e -> attributes.getRequest().getHeader(FrameworkConstant.X_REQUEST_TENANTID_HEADER)).orElse("");
            return tenantId;
        }
    }

    /**
     * -获取traceId
     * @return
     */
    public static String getTraceId(){
        if (SpringBeanUtils.isReactiveApplication()) {
            ServerWebExchange exchange = ServerHttpRequestContextHolder.getContextHolder();
            String traceId = Optional.ofNullable(exchange).map(e -> e.getRequest().getHeaders().getFirst(FrameworkConstant.X_REQUEST_TRACEID_HEADER)).orElse("");
            return traceId;
        }else {
            ServletRequestAttributes attributes = HttpServletRequestContextHolder.getContextHolder();
            String traceId = Optional.ofNullable(attributes).map(e -> attributes.getRequest().getHeader(FrameworkConstant.X_REQUEST_TRACEID_HEADER)).orElse("");
            return traceId;
        }
    }
}
