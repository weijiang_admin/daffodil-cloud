package com.daffodil.framework.util;

import com.daffodil.framework.annotation.OperLog;
import com.daffodil.util.StringUtils;

/**
 * -操作日志记录工具类
 * @author yweijian
 * @date 2023年3月23日
 * @version 2.0.0
 * @description
 */
public class OperLogUtils {

    /**
     * -获取操作日志记录类型名称
     * @param log
     * @return
     */
    public static String getBusinessName(OperLog log) {
        if(null == log) {
            return "";
        }
        String name = log.business().name();
        return StringUtils.isNotEmpty(name) ? name : log.type().getName();
    }

    /**
     * -获取操作日志记录类型标签
     * @param log
     * @return
     */
    public static String getBusinessLabel(OperLog log) {
        if(null == log) {
            return "";
        }
        String label = log.business().label();
        return StringUtils.isNotEmpty(label) ? label : log.type().getLabel();
    }

    /**
     * -获取操作日志记录类型备注
     * @param log
     * @return
     */
    public static String getBusinessRemark(OperLog log) {
        if(null == log) {
            return "";
        }
        String remark = log.business().remark();
        return StringUtils.isNotEmpty(remark) ? remark : log.type().getRemark();
    }
}
