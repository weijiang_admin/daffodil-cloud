package com.daffodil.framework.context;

import java.util.function.Supplier;

/**
 * 
 * @author yweijian
 * @date 2024年5月9日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public class TenantContextHolder {

    private static final ThreadLocal<String> tenantHolder = ThreadLocal.withInitial(() -> "");

    private static final ThreadLocal<String> traceHolder = ThreadLocal.withInitial(() -> "");

    public static void setTenantHolder(String tenantId) {
        tenantHolder.set(tenantId);
    }

    public static String getTenantHolder() {
        return tenantHolder.get();
    }

    public static void clearTenantHolder() {
        tenantHolder.remove();
    }

    public static void setTraceHolder(String traceId) {
        traceHolder.set(traceId);
    }

    public static String getTraceHolder() {
        return traceHolder.get();
    }

    public static void clearTraceHolder() {
        traceHolder.remove();
    }

    public static <T> T supply(String tenantId, Supplier<T> supplier){
        try {
            TenantContextHolder.setTenantHolder(tenantId);
            return supplier.get();
        } finally {
            TenantContextHolder.clearTenantHolder();
        }
    }

    public static void apply(String tenantId, Runnable runnable){
        try {
            TenantContextHolder.setTenantHolder(tenantId);
            runnable.run();
        } finally {
            TenantContextHolder.clearTenantHolder();
        }
    }

    public static <T> T supply(String tenantId, String traceId, Supplier<T> supplier){
        try {
            TenantContextHolder.setTraceHolder(traceId);
            TenantContextHolder.setTenantHolder(tenantId);
            return supplier.get();
        } finally {
            TenantContextHolder.clearTraceHolder();
            TenantContextHolder.clearTenantHolder();
        }
    }

    public static void apply(String tenantId, String traceId, Runnable runnable){
        try {
            TenantContextHolder.setTraceHolder(traceId);
            TenantContextHolder.setTenantHolder(tenantId);
            runnable.run();
        } finally {
            TenantContextHolder.clearTraceHolder();
            TenantContextHolder.clearTenantHolder();
        }
    }

}
