package com.daffodil.framework.constant;

/**
 * 
 * @author yweijian
 * @date 2022年12月13日
 * @version 2.0.0
 * @description
 */
public class FrameworkConstant {

    /** 当前页码 */
    public static final String PAGE_NUM = "pageNum";

    /** 每页显示记录数 */
    public static final String PAGE_SIZE = "pageSize";

    /** 排序关键字 */
    public static final String ORDER_BY_KEY = "order by";

    /** 排序列 */
    public static final String ORDER_BY_COLUMN = "orderBy";

    /** 排序 倒序 =desc，正序=asc */
    public static final String IS_ASC = "isAsc";

    /** 开始时间 */
    public static final String START_TIME = "startTime";

    /** 结束时间 */
    public static final String END_TIME = "endTime";

    /** 时间后缀 */
    public static final String DATE_TIME_SUBFIX = " 00:00:00";

    /** 成功标识  */
    public static final String SUCCESS = "0";

    /** 失败标识 */
    public static final String ERROR = "1";

    /** 是 */
    public static final String YES = "Y";

    /** 否 */
    public static final String NO = "N";

    /** token认证前缀 */
    public static final String BEARER = "Bearer ";

    /** 认证token关键字 */
    public static final String ACCESS_TOKEN1 = "access_token";

    /** 认证token关键字 */
    public static final String ACCESS_TOKEN2 = "accessToken";

    /** 授权token关键字 */
    public static final String X_REQUEST_AUTHOR_TOKEN_HEADER = "X-Request-Author-Token";

    /** 刷新token关键字 */
    public static final String X_REQUEST_REFRESH_TOKEN_HEADER = "X-Request-Refresh-Token";

    /** 请求头的追踪标识 */
    public static final String X_REQUEST_TRACEID_HEADER = "X-Request-Trace-Id";

    /** 请求头的租户标识 */
    public static final String X_REQUEST_TENANTID_HEADER = "X-Request-Tenant-Id";

    /** 追踪开始时间 */
    public static final String TRACE_START_TIME = "traceStartTime";

    /** 追踪应用标识 */
    public static final String MDC_TRACE_APP = "appName";

    /** 追踪标识 */
    public static final String MDC_TRACE_ID = "traceId";

    /** 系统资源 */
    public static final String AUTH_PERMISSION_ROUTER = "AUTH";

    /** 开放资源 */
    public static final String OPEN_PERMISSION_ROUTER = "OPEN";

    /** 动态路由 */
    public static final String ROUTER_DYNAMIC_CACHEKEY = "system:router:dynamic";

    /** 静态路由 */
    public static final String ROUTER_STATIC_CACHEKEY = "system:router:static";

    /** 登录令牌 */
    public static final String ACCESS_TOKEN_CACHEKEY = "system:token:access:{}";

    /** 本地JVM协议 */
    public static final String INJVM_PROTOCOL = "injvm";
}
