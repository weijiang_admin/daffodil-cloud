package com.daffodil.framework.filter;

import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.framework.context.TenantContextHolder;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * 
 * @author yweijian
 * @date 2024年5月9日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Slf4j
@Component
@ConditionalOnWebApplication(type = Type.REACTIVE)
public class ServerHttpRequestTenantFilter implements WebFilter, Ordered {

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 1;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String tenantId = Optional.ofNullable(request.getHeaders().getFirst(FrameworkConstant.X_REQUEST_TENANTID_HEADER)).orElse("");
        TenantContextHolder.setTenantHolder(tenantId);
        if(log.isDebugEnabled()) {
            log.debug("Current tenantId [{}]", tenantId);
        }
        return chain.filter(exchange).doFinally(onFinally -> {
            TenantContextHolder.clearTenantHolder();
        });
    }

}
