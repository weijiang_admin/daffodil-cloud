package com.daffodil.framework.filter;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerInterceptor;

import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.framework.properties.FrameworkProperties;
import com.daffodil.framework.request.HttpServletRequestContextHolder;
import com.daffodil.util.ServletUtils;
import com.daffodil.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * -链路追踪过滤
 * @author yweijian
 * @date 2022年3月9日
 * @version 1.0
 * @description
 */
@Slf4j
@Component
@ConditionalOnWebApplication(type = Type.SERVLET)
public class HttpServletRequestTraceFilter implements HandlerInterceptor, Ordered {

    @Autowired
    private FrameworkProperties frameworkProperties;

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Long startTime = System.currentTimeMillis();
        request.setAttribute(FrameworkConstant.TRACE_START_TIME, startTime);

        String requestTraceId = request.getHeader(FrameworkConstant.X_REQUEST_TRACEID_HEADER);
        final String traceId = StringUtils.isEmpty(requestTraceId) ? UUID.randomUUID().toString() : requestTraceId;
        MDC.put(FrameworkConstant.MDC_TRACE_APP, frameworkProperties.getAppName());
        MDC.put(FrameworkConstant.MDC_TRACE_ID, traceId);

        if(StringUtils.isEmpty(response.getHeader(FrameworkConstant.X_REQUEST_TRACEID_HEADER))) {
            response.setHeader(FrameworkConstant.X_REQUEST_TRACEID_HEADER, traceId);
        }

        TenantContextHolder.setTraceHolder(traceId);
        ServletRequestAttributes attributes = new ServletRequestAttributes(request, response);
        HttpServletRequestContextHolder.setContextHolder(attributes);

        if (log.isInfoEnabled()) {
            log.info("request method={} uri={} params={} body={} headers={} cookies={}", request.getMethod(),
                    request.getRequestURI(), ServletUtils.getParameterMap(request).toSingleValueMap(), "", 
                    ServletUtils.getHeaders(request).toSingleValueMap(), ServletUtils.getCookieMap(request).toSingleValueMap());
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if (log.isInfoEnabled()) {
            long startTime = (long) request.getAttribute(FrameworkConstant.TRACE_START_TIME);
            Long executeTime = System.currentTimeMillis() - startTime;
            log.info("request total execution time : {} ms", executeTime);
        }
        TenantContextHolder.clearTraceHolder();
        HttpServletRequestContextHolder.clearContextHolder();
        MDC.remove(FrameworkConstant.MDC_TRACE_APP);
        MDC.remove(FrameworkConstant.MDC_TRACE_ID);
    }

}
