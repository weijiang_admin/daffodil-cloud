package com.daffodil.framework.filter;

import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.framework.context.TenantContextHolder;
import com.daffodil.framework.properties.FrameworkProperties;
import com.daffodil.framework.request.ServerHttpRequestContextHolder;
import com.daffodil.util.StringUtils;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * -链路追踪过滤
 * @author yweijian
 * @date 2022年12月13日
 * @version 2.0.0
 * @description
 */
@Slf4j
@Component
@ConditionalOnWebApplication(type = Type.REACTIVE)
public class ServerHttpRequestTraceFilter implements WebFilter, Ordered {

    @Autowired
    private FrameworkProperties frameworkProperties;

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        final Instant startTime = Instant.now();
        ServerHttpRequest request = exchange.getRequest();
        String requestTraceId = request.getHeaders().getFirst(FrameworkConstant.X_REQUEST_TRACEID_HEADER);
        final String traceId = StringUtils.isEmpty(requestTraceId) ? UUID.randomUUID().toString() : requestTraceId;
        MDC.put(FrameworkConstant.MDC_TRACE_APP, frameworkProperties.getAppName());
        MDC.put(FrameworkConstant.MDC_TRACE_ID, traceId);

        if(StringUtils.isEmpty(exchange.getResponse().getHeaders().getFirst(FrameworkConstant.X_REQUEST_TRACEID_HEADER))) {
            exchange.getResponse().getHeaders().add(FrameworkConstant.X_REQUEST_TRACEID_HEADER, traceId);
        }
        
        TenantContextHolder.setTraceHolder(traceId);
        ServerHttpRequestContextHolder.setContextHolder(exchange);

        if (log.isInfoEnabled()) {
            log.info("request method={} uri={} params={} body={} headers={} cookies={}", 
                    request.getMethodValue(), request.getPath(), request.getQueryParams().toSingleValueMap(), "", 
                    request.getHeaders().toSingleValueMap(), request.getCookies().toSingleValueMap());
        }

        return chain.filter(exchange).doFinally(onFinally -> {
            if (log.isInfoEnabled()) {
                Long executeTime = Duration.between(startTime, Instant.now()).toMillis();
                log.info("request total execution time : {} ms", executeTime);
            }

            MDC.remove(FrameworkConstant.MDC_TRACE_APP);
            MDC.remove(FrameworkConstant.MDC_TRACE_ID);
            
            TenantContextHolder.clearTraceHolder();
            ServerHttpRequestContextHolder.clearContextHolder();
        });
    }

}
