package com.daffodil.framework.filter;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.framework.context.TenantContextHolder;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author yweijian
 * @date 2024年5月9日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Slf4j
@Component
@ConditionalOnWebApplication(type = Type.SERVLET)
public class HttpServletRequestTenantFilter implements HandlerInterceptor, Ordered {

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 1;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String tenantId = Optional.ofNullable(request.getHeader(FrameworkConstant.X_REQUEST_TENANTID_HEADER)).orElse("");
        TenantContextHolder.setTenantHolder(tenantId);
        if(log.isDebugEnabled()) {
            log.debug("Current tenantId [{}]", tenantId);
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        TenantContextHolder.clearTenantHolder();
    }

}
