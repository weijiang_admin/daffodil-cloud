package com.daffodil.framework.filter;

import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.Filter;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.apache.dubbo.rpc.RpcContext;
import org.apache.dubbo.rpc.RpcException;

import com.daffodil.framework.constant.FrameworkConstant;
import com.daffodil.framework.context.TenantContextHolder;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author yweijian
 * @date 2025年3月7日
 * @version 2.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Slf4j
@Activate(group = {CommonConstants.PROVIDER, CommonConstants.CONSUMER}, order = 1000)
public class DubboGrpcTenantFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        final boolean isInJvm = FrameworkConstant.INJVM_PROTOCOL.equals(invoker.getUrl().getProtocol());
        final boolean isConsumer = CommonConstants.CONSUMER_SIDE.equals(invoker.getUrl().getParameter(CommonConstants.SIDE_KEY));
        if(!isInJvm) {
            if (isConsumer) {
                // 消费端逻辑
                String tenantId = TenantContextHolder.getTenantHolder();
                RpcContext.getClientAttachment().setAttachment(FrameworkConstant.X_REQUEST_TENANTID_HEADER, tenantId);
                log.info("[Dubbo Filter] Consumer side set tenantId: {}", tenantId);
            } else {
                // 服务端逻辑
                String tenantId = RpcContext.getServerAttachment().getAttachment(FrameworkConstant.X_REQUEST_TENANTID_HEADER);
                TenantContextHolder.setTenantHolder(tenantId);
                log.info("[Dubbo Filter] Provider side received tenantId: {}", tenantId);
            }
        }

        try {
            return invoker.invoke(invocation);
        } finally {
            if (!isInJvm && !isConsumer) {
                TenantContextHolder.clearTenantHolder();
                log.info("[Dubbo Filter] Provider side cleared tenant context");
            }
        }
    }
}
