package com.daffodil.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据权限过滤注解
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 2.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataScope {
    
    /**
     * 业务类型标识
     */
    public Class<?> business() default Void.class;

    /**
     * 部门ID的别名
     */
    public String deptAlias() default "";

    /**
     * 用户ID的别名
     */
    public String userAlias() default "";
}
