package com.daffodil.common.constant;

/**
 * -达佛基础管理平台公共缓存键值常量定义
 * @author yweijian
 * @date 2023年3月13日
 * @version 2.0.0
 * @description
 */
public class CacheKeyConstant {

    /** 登录令牌 */
    public static final String ACCESS_TOKEN_CACHEKEY = "system:token:access:{}";

    /** 刷新令牌 */
    public static final String REFRESH_TOKEN_CACHEKEY = "system:token:refresh:{}";

    /** 令牌等级*/
    public static final String LEVEL_TOKEN_CACHEKEY = "system:token:level:{}";

    /** 令牌权限 */
    public static final String PERMISSION_TOKEN_CACHEKEY = "system:token:permission:{}";

    /** 已授令牌池 */
    public static final String ACCESS_TOKEN_POOL_CACHEKEY = "system:token:access:pool";

    /** 动态路由 */
    public static final String ROUTER_DYNAMIC_CACHEKEY = "system:router:dynamic";

    /** 静态路由 */
    public static final String ROUTER_STATIC_CACHEKEY = "system:router:static";

    /** 登录服务 */
    public static final String CONFIG_LOGIN_CACHEKEY = "system:config:login";

    /** 真实IP地址 */
    public static final String REALIP_ADDRESS_CACHEKEY = "system:ip:address";

    /** 国密私钥 */
    public static final String SM2_PRIVATE_KEY_CACHEKEY = "system:sm2:private-key";

    /** 国密公钥 */
    public static final String SM2_PUBLIC_KEY_CACHEKEY = "system:sm2:public-key";

    /** 用户账号密码登录图片验证码 */
    public static final String USER_LOGIN_KAPTCHA_CACHEKEY = "user:login:kaptcha:{}";

    /** 用户验证码登录验证码 */
    public static final String USER_LOGIN_VERIFYCODE_CACHEKEY = "user:login:verify-code:{}";

    /** 用户登录计数验证 */
    public static final String USER_LOGIN_COUNT_CACHEKEY = "user:login:count:{}";

    /** 用户登录密码错误重试验证 */
    public static final String USER_RETRY_COUNT_CACHEKEY = "user:retry:count:{}";

    /** 用户操作二次token验证 */
    public static final String USER_TOKEN_VERIFY_CACHEKEY = "user:token:verify:{}";

    /** 第三方应用授权码 */
    public static final String THIRDAPP_OAUTH_CODE_CACHEKEY = "thirdapp:oauth:code:{}";

    /** 第三方应用认证令牌 */
    public static final String THIRDAPP_OAUTH_TOKEN_CACHEKEY = "thirdapp:oauth:token:{}";

    /** 日期管理数据 */
    public static final String DAYS_DATA_CACHEKEY = "system:days:data";

    /** 字典管理数据 */
    public static final String DICTIONARY_DATA_CACHEKEY = "system:dictionary:data:{}";

    /** 标签管理数据 */
    public static final String TAG_DATA_CACHEKEY = "system:tag:data:{}";

    /** 区划管理数据 */
    public static final String AREA_DATA_CACHEKEY = "system:area:data:{}";
}
