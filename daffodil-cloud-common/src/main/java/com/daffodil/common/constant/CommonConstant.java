package com.daffodil.common.constant;

/**
 * 
 * @author yweijian
 * @date 2021年9月23日
 * @version 1.0
 * @description
 */
public class CommonConstant extends CacheKeyConstant {

    /** 平台内系统根组织 */
    public static final String ROOT = "root";

    /** 平台内系统超级管理员关键字 */
    public static final String SYSADMIN = "SysAdmin";

    /** 成功标识  */
    public static final String SUCCESS = "0";

    /** 失败标识 */
    public static final String ERROR = "1";

    /** 是 */
    public static final String YES = "Y";

    /** 否 */
    public static final String NO = "N";

    /** 用户名长度限制最小长度2个字符 */
    public static final int USERNAME_MIN_LENGTH = 2;

    /** 用户名长度限制最大长度20个字符 */
    public static final int USERNAME_MAX_LENGTH = 64;

    /** 密码长度限制最小长度6个字符 */
    public static final int PASSWORD_MIN_LENGTH = 6;

    /** 密码长度限制最大长度32个字符 */
    public static final int PASSWORD_MAX_LENGTH = 32;

    /**手机号码格式限制 */
    public static final String MOBILE_PHONE_NUMBER_PATTERN = "^((12[0-9])|(13[0-9])|(14[5|7])|(15([0-9]))|(18[0-9]))\\d{8}";

    /** 邮箱格式限制 */
    public static final String EMAIL_PATTERN = "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))";

    /** 一类登录令牌 */
    public static final Integer LEVEL_TOKEN_FIRST = 1;

    /** 二类登录令牌 */
    public static final Integer LEVEL_TOKEN_SECOND = 2;

}
