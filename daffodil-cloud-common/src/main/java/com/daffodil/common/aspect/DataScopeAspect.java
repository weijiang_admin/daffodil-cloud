package com.daffodil.common.aspect;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.daffodil.common.annotation.DataScope;
import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysBusinessScopeDept;
import com.daffodil.system.entity.SysDept;
import com.daffodil.system.entity.SysRole;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.enums.DataScopeEnum;
import com.daffodil.util.StringUtils;
import org.springframework.util.CollectionUtils;

/**
 * 
 * @author yweijian
 * @date 2025年3月13日
 * @version 2.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Aspect
@Component
public class DataScopeAspect {

    /**
     * -配置织入点
     */
    @Pointcut("@annotation(com.daffodil.common.annotation.DataScope)")
    public void dataScopePointCut() {

    }

    @Before("dataScopePointCut()")
    public void doBefore(JoinPoint point) throws Throwable {
        this.handleDataScope(point);
    }

    protected void handleDataScope(final JoinPoint joinPoint) {
        DataScope dataScope = this.getAnnotationLog(joinPoint);
        if (dataScope == null) {
            return;
        }
        Query<?> query = this.getQuery(joinPoint);
        if(query == null) {
            return;
        }
        SysUser user = this.getSysUser(joinPoint);
        if(user == null) {
            return;
        }
        this.dataScopeFilter(query, user, dataScope.business(), dataScope.deptAlias(), dataScope.userAlias());
    }

    /**
     * -数据范围过滤
     * @param query
     * @param user
     * @param business 业务类型标识
     * @param deptAlias 部门ID别名, d.deptId
     * @param userAlias 用户ID别名, u.userId
     */
    public void dataScopeFilter(Query<?> query, SysUser user, Class<?> business, String deptAlias, String userAlias) {
        //检查用户是否是超级管理员角色
        if(user.isAdminRole()) {
            return;
        }

        //检查用户角色集合为空则“仅本用户数据权限”
        if(StringUtils.isEmpty(user.getRoles())) {
            StringBuilder sqlString = new StringBuilder();
            sqlString.append(StringUtils.format(" or {} = '{}' ", userAlias, user.getId()));
            query.setDataScope(" and (" + sqlString.substring(4) + ") ");
            return;
        }

        if (this.checkDataScopeBusinessCustom(user.getRoles(), business)) {
            //角色业务配置的数据权限
            List<DataScopeRole> scopes = this.convertToDataScopeRoles(user.getRoles(), business);
            this.handleDataScopeFilter(query, user, scopes, business, deptAlias, userAlias);
        }else {
            //角色全局配置的数据权限
            List<DataScopeRole> scopes = this.convertToDataScopeRoles(user.getRoles());
            this.handleDataScopeFilter(query, user, scopes, null, deptAlias, userAlias);
        }
    }

    private void handleDataScopeFilter(Query<?> query, SysUser user, List<DataScopeRole> scopes, Class<?> business, String deptAlias, String userAlias) {
        //检查全部业务权限是否包含“全部数据权限”
        if(this.hasAllDataScope(scopes)) {
            return;
        }
        StringBuilder sqlString = new StringBuilder();
        //检查全部角色是否包含“仅本用户数据权限”
        if(this.hasSelfDataScope(scopes) && StringUtils.isNotBlank(userAlias)) {
            sqlString.append(StringUtils.format(" or {} = '{}' ", userAlias, user.getId()));
        }
        if(StringUtils.isNotBlank(deptAlias)){
            //检查全部角色“自定义部门数据权限”
            List<DataScopeRole> customScopes = this.getCustomDataScopeScopes(scopes);
            if(StringUtils.isNotEmpty(customScopes)) {
                String entity = business != null ? SysBusinessScopeDept.class.getSimpleName() : SysDept.class.getSimpleName();
                String roleIds = customScopes.stream().map(scope -> "'" + scope.getRoleId() + "'").collect(Collectors.joining(","));
                String condition = business != null ? StringUtils.format(" and business= '{}'", business.getSimpleName()) : "";
                sqlString.append(StringUtils.format(" or {} in ( select deptId from {} where roleId in({}) {} )", deptAlias, entity, roleIds, condition));
            }
            //检查全部角色是否包含“本部门及以下数据权限”
            if(this.hasDeptAndChildDataScope(scopes)) {
                sqlString.append(StringUtils.format(" or {} in ( select id from SysDept where id = '{}' or ancestors like '%{}%' )", deptAlias, user.getDeptId(), user.getDeptId()));
                query.setDataScope(" and (" + sqlString.substring(4) + ") ");
                return;
            }
            //检查全部角色是否包含“仅本部门数据权限”
            if(this.hasDeptDataScope(scopes)) {
                sqlString.append(StringUtils.format(" or {} = '{}' ", deptAlias, user.getDeptId()));
                query.setDataScope(" and (" + sqlString.substring(4) + ") ");
                return;
            }
        }
        if(StringUtils.isNotBlank(sqlString.toString())) {
            query.setDataScope(" and (" + sqlString.substring(4) + ") ");
        }
    }

    private boolean hasAllDataScope(List<DataScopeRole> scopes) {
        return scopes.stream().anyMatch(scope -> DataScopeEnum.DATA_SCOPE_ALL.equals(DataScopeEnum.test(scope.getDataScope())));
    }

    private boolean hasSelfDataScope(List<DataScopeRole> scopes) {
        return scopes.stream().anyMatch(scope -> DataScopeEnum.DATA_SCOPE_SELF.equals(DataScopeEnum.test(scope.getDataScope())));
    }

    private List<DataScopeRole> getCustomDataScopeScopes(List<DataScopeRole> scopes) {
        return scopes.stream().filter(scope -> DataScopeEnum.DATA_SCOPE_CUSTOM.equals(DataScopeEnum.test(scope.getDataScope()))).collect(Collectors.toList());
    }

    private boolean hasDeptAndChildDataScope(List<DataScopeRole> scopes) {
        return scopes.stream().anyMatch(scope -> DataScopeEnum.DATA_SCOPE_DEPT_AND_CHILD.equals(DataScopeEnum.test(scope.getDataScope())));
    }

    private boolean hasDeptDataScope(List<DataScopeRole> scopes) {
        return scopes.stream().anyMatch(scope -> DataScopeEnum.DATA_SCOPE_DEPT.equals(DataScopeEnum.test(scope.getDataScope())));
    }

    private List<DataScopeRole> convertToDataScopeRoles(List<SysRole> roles) {
        return roles.stream().map(role -> new DataScopeRole(role.getId(), role.getDataScope())).collect(Collectors.toList());
    }

    private List<DataScopeRole> convertToDataScopeRoles(List<SysRole> roles, Class<?> business) {
        List<DataScopeRole> scopes = new ArrayList<>();
        roles.forEach(role -> {
            if (!CollectionUtils.isEmpty(role.getBusinessScopes())){
                role.getBusinessScopes().forEach(businessScope -> {
                    if(businessScope.getBusiness() != null && businessScope.getBusiness().equalsIgnoreCase(business.getSimpleName())) {
                        scopes.add(new DataScopeRole(businessScope.getRoleId(), businessScope.getDataScope()));
                    }
                });
            }
        });
        return scopes;
    }

    /**
     * -检查用户角色集合是否包含对当前业务类型配置数据权限
     * @param roles
     * @param business
     * @return
     */
    private Boolean checkDataScopeBusinessCustom(List<SysRole> roles, Class<?> business){
        return roles.stream().anyMatch(role -> {
            if(CollectionUtils.isEmpty(role.getBusinessScopes())) {
                return false;
            }
            return role.getBusinessScopes().stream().anyMatch(businessScope -> businessScope.getBusiness() != null && businessScope.getBusiness().equalsIgnoreCase(business.getSimpleName()));
        });
    }

    /**
     * -是否存在注解，如果存在就获取
     */
    private DataScope getAnnotationLog(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        if (method != null) {
            return method.getAnnotation(DataScope.class);
        }
        return null;
    }

    /**
     * -获取查询参数
     * @param joinPoint
     * @return
     */
    private Query<?> getQuery(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        if(args != null) {
            for (Object arg : args) {
                if (arg instanceof Query) {
                    return (Query<?>) arg;
                }
            }
        }
        return null;
    }

    /**
     * -获取当前登录用户
     * @param joinPoint
     * @return
     */
    private SysUser getSysUser(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        if(args != null) {
            for (Object arg : args) {
                if (arg instanceof SysUser) {
                    return (SysUser) arg;
                }
            }
        }
        return null;
    }

    private class DataScopeRole {
        private String roleId;
        private String dataScope;

        public DataScopeRole(String roleId, String dataScope) {
            this.roleId = roleId;
            this.dataScope = dataScope;
        }

        public String getRoleId() {
            return roleId;
        }

        public String getDataScope() {
            return dataScope;
        }

    }
}

