package com.daffodil.system.enums;

/**
 * 
 * @author yweijian
 * @date 2024年8月15日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public enum DataScopeEnum {

    /** 全部数据权限 */
    DATA_SCOPE_ALL("1"),

    /** 自定义部门数据权限 */
    DATA_SCOPE_CUSTOM("2"),

    /** 仅本部门数据权限 */
    DATA_SCOPE_DEPT("3"),

    /** 本部门及以下数据权限 */
    DATA_SCOPE_DEPT_AND_CHILD("4"),

    /** 仅本用户数据权限 */
    DATA_SCOPE_SELF("5");

    private final String code;

    private DataScopeEnum(String code) {
        this.code = code;
    }

    public String code() {
        return code;
    }

    public static DataScopeEnum test(String code) {
        for (DataScopeEnum dataScopeEnum : DataScopeEnum.values()) {
            if(dataScopeEnum.code().equals(code)) {
                return dataScopeEnum;
            }
        }
        throw new IllegalArgumentException("Invalid data scope code:" + code);
    }

}
