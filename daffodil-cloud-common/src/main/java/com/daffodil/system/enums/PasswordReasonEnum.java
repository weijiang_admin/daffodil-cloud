package com.daffodil.system.enums;

/**
 * 
 * @author yweijian
 * @date 2024年8月15日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public enum PasswordReasonEnum {

    /** 用户密码正常 */
    REASON_PASSWORD_NOMAL("0"),
    /** 用户密码新建 */
    REASON_PASSWORD_NEW("1"),
    /** 用户密码过期 */
    REASON_PASSWORD_EXPIRE("2"),
    /** 用户密码重置 */
    REASON_PASSWORD_RESET("3");

    private final String code;

    private PasswordReasonEnum(String code) {
        this.code = code;
    }

    public String code() {
        return code;
    }
}
