package com.daffodil.system.enums;

/**
 * 
 * @author yweijian
 * @date 2024年8月15日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public enum DataStatusEnum {

    /** 正常状态 */
    NORMAL("0"),

    /** 异常状态 */
    UNUSED("1"),

    /** 删除状态 */
    DELETED("2");

    private final String code;

    private DataStatusEnum(String code) {
        this.code = code;
    }

    public String code() {
        return code;
    }

}
