package com.daffodil.system.entity;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * -节假日配置表
 * 
 * @author EP
 * @date 2023年1月16日
 * @version 1.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_holiday")
public class SysHoliday extends BaseEntity<String> {

    private static final long serialVersionUID = -905219350814164903L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;

    /** 年份 */
    @Column(name = "year")
    @Hql(type = Logical.EQ)
    private Integer year;

    /** 月份，1 ... 12 */
    @Column(name = "month")
    @Hql(type = Logical.EQ)
    private Integer month;

    /** 星期，星期一=1 ... 星期日=7*/
    @Column(name = "week")
    @Hql(type = Logical.EQ)
    private Integer week;

    /** 日期，例2023-01-21 */
    @JsonFormat(pattern="yyyy-MM-dd")
    @NotNull(message = "设置的日期不能为空")
    @Column(name="date")
    @Hql(type = Logical.EQ)
    private Date date;

    /** 是否为工作日*/
    @NotNull(message = "是否为工作日不能为空")
    @Column(name = "work_day")
    @Hql(type = Logical.EQ)
    private Boolean workDay;

    /** 是否是周末*/
    @Column(name = "weekend")
    @Hql(type = Logical.EQ)
    private Boolean weekend;

    /** 节假日类型 1-平常日，2-节假日 3-补班日 */
    @Column(name = "holiday_type")
    @Hql(type = Logical.EQ)
    private Integer holidayType;

    /** 节日名称*/
    @Column(name="name", length = 8)
    @Size(min = 0, max = 8, message = "节日名称长度不能超过8个字符")
    @Hql(type = Logical.LIKE)
    private String name;
    
    /** 备注*/
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;

    /** 创建者 */
    @Column(name="create_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;

    /**
     * -假期类型
     * @author EP
     * @date 2023年1月16日
     * @version 1.0.0
     * @description
     */
    public static enum HolidayType {

        /** 平常日 */
        NORMAL(1),
        /** 节假日 */
        HOLIDAY(2),
        /** 补班日 */
        OVERTIME(3);

        private final Integer value;

        HolidayType(Integer value) {
            this.value = value;
        }

        public Integer value() {
            return this.value;
        }
    }
}
