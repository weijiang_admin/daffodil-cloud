package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 社会账号用户，如QQ、微信、飞书、钉钉等等
 * @author yweijian
 * @date 2023年1月11日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_social_user")
public class SysSocialUser extends BaseEntity<String> {

    private static final long serialVersionUID = -4756591206218710397L;

    /** 用户ID */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;

    /** 用户ID 绑定系统用户ID */
    @Column(name = "user_id", length = 32)
    @Hql(type = Logical.EQ)
    private String userId;
    
    /** 用户身份所属 */
    @Column(name = "user_mode", length = 32)
    @Hql(type = Logical.EQ)
    private String userMode;

    /** 用户统一ID，在同一租户开发的所有应用内的唯一标识 */
    @Column(name = "union_id", length = 128)
    @Hql(type = Logical.EQ)
    private String unionId;

    /** 用户在应用内的唯一标识 */
    @Column(name = "open_id", length = 128)
    @Hql(type = Logical.EQ)
    private String openId;

    /** 用户姓名 */
    @Column(name = "user_name", length = 128)
    @Hql(type = Logical.LIKE)
    private String userName;

    /** 用户头像  picture */
    @Column(name = "avatar", length = 512)
    private String avatar;

    /** 邮箱账号 */
    @Column(name = "email", length = 128)
    @Hql(type = Logical.LIKE)
    private String email;

    /** 手机号码  phone */
    @Column(name = "mobile", length = 11)
    @Hql(type = Logical.LIKE)
    private String mobile;
    
    /** 帐号状态（0正常 1解绑） */
    @Column(name = "status", length = 1)
    @Dict(value = "sys_data_status")
    @Hql(type = Logical.EQ)
    private String status;

    /** 创建（绑定）时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;
    
    /** 解除（解绑）时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="free_time")
    private Date freeTime;

}
