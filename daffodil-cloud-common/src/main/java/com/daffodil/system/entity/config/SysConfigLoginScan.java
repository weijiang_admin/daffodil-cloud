package com.daffodil.system.entity.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.daffodil.util.StringUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -扫码登录配置
 * @author yweijian
 * @date 2022年11月14日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_config_login_scan")
public class SysConfigLoginScan extends BaseEntity<String>{

    private static final long serialVersionUID = -164929396500985304L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;
    
    /** 是否开启 */
    @Column(name = "enable", length = 1)
    @Dict(value = "sys_yes_no")
    private String enable;
    
    /** 扫码类型*/
    @Column(name = "scan_type", length = 128)
    private String scanType;
    
    @Transient
    private List<String> scanModes;
    
    public List<String> getScanModes(){
        if(StringUtils.isNotEmpty(this.scanType)) {
            return Arrays.asList(this.scanType.split(","));
        }
        return Collections.emptyList();
    }

    /**
     * -扫码类型
     * @author yweijian
     * @date 2022年11月14日
     * @version 2.0.0
     * @description
     */
    public static enum ScanType {
        /** QQ */
        QQ,
        /** 微信 */
        WEIXIN,
        /** 企业微信 */
        QIYE_WEIXIN,
        /** 飞书 */
        FEISHU,
        /** 钉钉 */
        DINGDING;
    }
}
