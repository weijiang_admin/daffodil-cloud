package com.daffodil.system.entity;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.util.sm.CodeUtils;
import com.daffodil.util.text.Convert;

import cn.hutool.core.date.DateUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;

import org.hibernate.annotations.GenericGenerator;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * -日期配置表
 * 
 * @author EP
 * @date 2023年1月16日
 * @version 1.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_days")
public class SysDays extends BaseEntity<String> {

    private static final long serialVersionUID = 2344729304144174224L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;

    /** 年份 */
    @NotNull(message = "年份不能为空")
    @Column(name = "year")
    @Hql(type = Logical.EQ)
    private Integer year;

    /** 当年二进制字符串数据 */
    @NotBlank(message = "年份二进制字符串不能为空")
    @Column(name = "content", length = 416)
    @Hql(type = Logical.EQ)
    private String content;

    /** 当年二进制数据 */
    @NotEmpty(message = "年份二进制数据不能为空")
    @Column(name = "data", columnDefinition = "BINARY(52)", length = 52)
    @Hql(type = Logical.EQ)
    private byte[] data;

    public SysDays() { }

    /**
     * -组装日期二进制数据到data字段（52字节，416位）
     * -前2个字节为年份，中间2个字节为闰年标识（0：平年，1：闰年），尾部为12个月日期数据，每个月用4个字节32位二进制表示
     * [年份][闰年标识][1月][2月][3月]...[12月]
     * -例：0000011111100111 0000000000000000 00000000111100000110000011000001 00000011...
     * @param year
     * @param holidayMap
     * @return
     */
    @SneakyThrows
    public SysDays(Integer year, Map<String, SysHoliday> holidayMap) {
        Calendar cal = Calendar.getInstance();//获取Calendar
        cal.set(Calendar.YEAR, year); //设置日期
        cal.set(Calendar.MONTH,0); //设置月份从1月开始
        cal.set(Calendar.DATE, 1);
        List<String> weekEnd = Arrays.asList("6", "7");

        byte[] yearBytes = CodeUtils.shortToByteBigEndian(year.shortValue());
        byte[] leapYearBytes = this.getLeapYearBytes(year);

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        os.write(yearBytes);
        os.write(leapYearBytes);

        for (int i = 0; i < 12; i++,cal.add(Calendar.MONTH, 0)) {//循环输出一年中的12个月,cal.add()方法设置每次增加一个月
            int month = cal.get(Calendar.MONTH) + 1;
            int count = countDayOfMonth(year, month); //计算每个月有多少天
            cal.set(Calendar.DAY_OF_MONTH, 1);

            int monthResult = 0;
            for (int x = 0; x < count; x++, cal.add(Calendar.DATE, 1)) {//循环输出一个月中的每一天，cal.add()方法设置每次增加一天
                Integer week = cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY ? 7 : cal.get(Calendar.DAY_OF_WEEK) - 1;
                boolean isWeekEnd = weekEnd.contains(Convert.toStr(week));

                //判断是否在后台配置的节假日数据中是否为工作日
                SysHoliday sysHoliday = holidayMap.get(DateUtil.format(cal.getTime(), "yyyy-MM-dd"));
                boolean isWorkDay;
                if(sysHoliday != null) {
                    isWorkDay = sysHoliday.getWorkDay();
                } else {
                    isWorkDay = !isWeekEnd;
                }
                //用32位二进制存储每日节假日状态
                int holidayFlag = 1 << x;
                if (!isWorkDay) {
                    monthResult |= holidayFlag;
                }
            }
            os.write(CodeUtils.intToBytesBigEndian(monthResult));
        }
        
        this.year = year;
        this.data = os.toByteArray();
        this.content = CodeUtils.bytes2BinaryString(os.toByteArray());
    }

    /**
     * -通过年份计算平年闰年，标识结果返回字节数组
     * @param year
     * @return
     */
    private byte[] getLeapYearBytes(Integer year) {
        int days = this.countDayOfMonth(year, 2);
        short leapYear = (short) (days == 29 ? 1 : 0);//标识 0-平年 1-闰年
        return CodeUtils.shortToByteBigEndian(leapYear);
    }
    
    /**
     * -计算当前月份总天数
     * @param month
     * @param year
     * @return
     */
    private Integer countDayOfMonth(Integer year, Integer month) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DATE, 1);
        c.roll(Calendar.DATE, -1);
        return c.get(Calendar.DATE);
    }
}
