package com.daffodil.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * @author yweijian
 * @date 2025年3月13日
 * @version 2.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_business_scope", indexes = {
        @Index(name = "business", columnList = "business"),
        @Index(name = "role_id", columnList = "role_id")
})
public class SysBusinessScope extends BaseEntity<String> {

    private static final long serialVersionUID = -5301457447031824020L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;

    /** 业务标识 */
    @Column(name = "business", length = 128)
    @Hql(type = Logical.EQ)
    private String business;

    /** 角色ID */
    @Column(name = "role_id", length = 32)
    @Hql(type = Logical.IN)
    private String roleId;

    /** 数据范围（1：所有数据权限；2：自定义部门数据权限；3：本部门数据权限；4：本部门及以下数据权限；5：仅本用户数据权限） */
    @Column(name = "data_scope", length = 1)
    @Hql(type = Logical.EQ)
    private String dataScope;

    /** 部门组（数据权限） */
    @Transient
    private String[] deptIds;
}
