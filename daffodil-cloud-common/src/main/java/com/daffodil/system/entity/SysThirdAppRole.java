package com.daffodil.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 第三方应用角色权限
 * @author yweijian
 * @date 2023年1月6日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_third_app_role")
public class SysThirdAppRole extends BaseEntity<String> {
    
    private static final long serialVersionUID = 8862294636360325148L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;
    
    /** 应用ID */
    @Column(name = "app_id", length = 32)
    private String appId;
    
    /** 角色ID */
    @Column(name = "role_id", length = 32)
    private String roleId;
}
