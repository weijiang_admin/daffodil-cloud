package com.daffodil.system.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户和岗位关联
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_user_post", indexes = {
        @Index(name = "user_id", columnList = "user_id"),
        @Index(name = "post_id", columnList = "post_id")
})
public class SysUserPost extends BaseEntity<String> {
    private static final long serialVersionUID = -255423089769176273L;

    /** 角用户和岗位关联编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "user_post_id", length = 32)
    private String id;
    
    /** 用户ID */
    @Column(name = "user_id", length = 32)
    private String userId;

    /** 岗位ID */
    @Column(name = "post_id", length = 32)
    private String postId;

}
