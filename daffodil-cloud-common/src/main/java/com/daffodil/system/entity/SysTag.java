package com.daffodil.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import java.util.Date;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -数据标签
 * @author yweijian
 * @date 2022年6月13日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_tag")
public class SysTag extends BaseEntity<String> {

    private static final long serialVersionUID = -6200051494147588534L;

    /** 标签编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "tag_id", length = 32)
    private String id;

    /** 标签（目录）名称 */
    @NotBlank(message = "标签名称不能为空")
    @Size(min = 0, max = 64, message = "标签名称长度不能超过64个字符")
    @Hql(type = Logical.LIKE)
    @Column(name = "tag_name", length = 64)
    private String tagName;

    /** 标签类型（catalog目录 tag标签） */
    @Dict("sys_tag_type")
    @Hql(type = Logical.EQ)
    @Column(name = "tag_type", length = 32)
    private String tagType;

    /** 目录键值 */
    @Size(min = 0, max = 32, message = "标签键值长度不能超过32个字符")
    @Hql(type = Logical.EQ)
    @Column(name = "tag_label", length = 32)
    private String tagLabel;

    /** 标签键值 */
    @Size(min = 0, max = 128, message = "标签键值长度不能超过128个字符")
    @Hql(type = Logical.EQ)
    @Column(name = "tag_value", length = 128)
    private String tagValue;

    /** 是否默认（Y是 N否） */
    @Dict("sys_yes_no")
    @Hql(type = Logical.EQ)
    @Column(name = "is_default", length = 1)
    private String isDefault;

    /** 父标签ID */
    @Hql(type = Logical.EQ)
    @Column(name = "parent_id", length = 32)
    private String parentId;

    /** 祖级列表 */
    @Column(name = "ancestors", length = 256)
    private String ancestors;

    /** 显示顺序 */
    @Column(name = "order_num")
    private Long orderNum;

    /** 标签状态 */
    @Dict("sys_data_status")
    @Hql(type = Logical.EQ)
    @Column(name = "status", length = 1)
    private String status;

    /** 创建者 */
    @Column(name="create_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;

    @Transient
    private Boolean hasChildren;

    public Boolean getHasChildren() {
        return "catalog".equals(this.tagType);
    }

}
