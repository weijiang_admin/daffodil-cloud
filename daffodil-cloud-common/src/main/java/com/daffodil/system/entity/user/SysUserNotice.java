package com.daffodil.system.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -用户和通知关联
 * @author yweijian
 * @date 2022年6月22日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_user_notice", indexes = {
        @Index(name = "user_id", columnList = "user_id"),
        @Index(name = "notice_id", columnList = "notice_id")
})
public class SysUserNotice extends BaseEntity<String> {

    private static final long serialVersionUID = -7275085864660830913L;

    /** 角用户和通知关联编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "user_notice_id", length = 32)
    private String id;
    
    /** 用户ID */
    @Column(name = "user_id", length = 32)
    private String userId;

    /** 通知ID */
    @Column(name = "notice_id", length = 32)
    private String noticeId;
    
    /** 公告状态 0=未读 1=已读 */
    @Column(name = "status", length = 1)
    private String status;

}
