package com.daffodil.system.entity.config;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -邮箱验证码配置
 * @author yweijian
 * @date 2022年9月8日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_config_login_email")
public class SysConfigLoginEmail extends BaseEntity<String> {
    
    private static final long serialVersionUID = -1332995811028090507L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;
    
    /** 是否开启 */
    @Column(name = "enable", length = 1)
    @Dict(value = "sys_yes_no")
    private String enable;
    
    /** 验证码长度 */
    @Column(name = "length")
    private Integer length;
    
    /** 验证码有效时长 单位分钟 */
    @Column(name = "expire_time")
    private Integer expireTime;
    
    /** 验证码邮件发送主题 */
    @Column(name = "subject", length = 128)
    @Size(min = 0, max = 128, message = "邮件主题长度不能超过128个字符")
    private String subject;
    
    /** 验证码邮件发送模板 */
    @Column(name = "template", length = 256)
    @Size(min = 0, max = 256, message = "邮件模板长度不能超过256个字符")
    private String template;

}
