package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 第三方应用表
 * @author yweijian
 * @date 2023年1月3日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_third_app")
public class SysThirdApp extends BaseEntity<String> {

    private static final long serialVersionUID = -8666371683597152778L;

    /** 应用ID */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "app_id", length = 32)
    private String id;

    /** 应用名称 */
    @Column(name="app_name", length = 64)
    @Size(min = 0, max = 64, message = "应用名称长度不能超过64个字符")
    @Hql(type = Logical.LIKE)
    private String appName;

    /** 应用秘钥 */
    @Column(name="app_secret", length = 256)
    private String appSecret;

    /** 应用图标 */
    @Column(name="app_logo", length = 256)
    @Size(min = 0, max = 256, message = "应用图标长度不能超过256个字符")
    private String appLogo;

    /** 应用主页 */
    @Column(name="redirect_uri", length = 256)
    @Size(min = 0, max = 256, message = "应用主页长度不能超过256个字符")
    private String redirectUri;

    /** 回调地址 */
    @Column(name="callback_uri", length = 256)
    @Size(min = 0, max = 256, message = "回调地址长度不能超过256个字符")
    private String callbackUri;

    /** 帐号状态（0正常 1停用 2删除） */
    @Column(name = "status", length = 1)
    @Dict(value = "sys_data_status")
    @Hql(type = Logical.EQ)
    private String status;

    /** 创建者 */
    @Column(name="create_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;

    /** 应用描述 */
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;

    /** 权限范围 */
    @Transient
    private String[] scopes;

    /** 资源组id */
    @Transient
    private String[] resourceIds;

    /** 角色组id */
    @Transient
    private String[] roleIds;
}
