package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.daffodil.framework.annotation.MaskFormat;
import com.daffodil.framework.annotation.MaskFormat.MaskType;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_datasource")
public class SysDatasource extends BaseEntity<String> {

    private static final long serialVersionUID = -1390039175896290205L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;

    /** 数据源名称 */
    @NotBlank(message = "数据源名称不能为空")
    @Column(name="name", length = 255)
    @Hql(type = Logical.LIKE)
    private String name;

    /** 数据库类型：MYSQL ORACLE */
    @NotBlank(message = "数据库类型不能为空")
    @Column(name="database_type", length = 32)
    @Hql(type = Logical.EQ)
    private String databaseType;

    /**
     * 数据源类型说明
     */
    @NotBlank(message = "数据源类型不能为空")
    @Column(name="datasource_type", length = 255)
    private String datasourceType;

    /**
     * 数据源驱动类名称
     */
    @NotBlank(message = "数据源驱动类名不能为空")
    @Column(name="driver_class_name", length = 255)
    private String driverClassName;

    /**
     * 数据库连接地址
     */
    @MaskFormat(type = MaskType.MIDDLE, begin = 10, end = 30)
    @Column(name="url", length = 1024)
    private String url;

    /**
     * 数据库用户名
     */
    @MaskFormat(type = MaskType.ZH_CN_NAME)
    @Column(name="username", length = 255)
    private String username;

    /**
     * 数据库密码
     */
    @MaskFormat(type = MaskType.PASSWORD)
    @Column(name="password", length = 255)
    private String password;

    /** 数据源状态*/
    @Dict("sys_data_status")
    @Hql(type = Logical.EQ)
    @Column(name = "status", length = 1)
    private String status;

    /** 创建者 */
    @Column(name="create_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;
}
