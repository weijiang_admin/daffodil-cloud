package com.daffodil.system.entity;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.*;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 菜单权限表
 * @author yweijian
 * @date 2021年9月24日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_menu")
public class SysMenu2 extends BaseEntity<String> {

    private static final long serialVersionUID = -6046860077089893897L;

    /** 菜单编号 */
    @Id
    @Column(name = "menu_id", length = 32)
    private String id;

    /** 菜单名称 */
    @Column(name = "menu_name", length = 32)
    @NotBlank(message = "菜单名称不能为空")
    @Size(min = 0, max = 32, message = "菜单名称长度不能超过32个字符")
    @Hql(type = Logical.LIKE)
    private String menuName;

    /** 父菜单ID */
    @Column(name = "parent_id", length = 32)
    @Hql(type = Logical.EQ)
    private String parentId;

    /** 祖级列表 */
    @Column(name = "ancestors", length = 256)
    private String ancestors;

    /** 显示顺序 */
    @Column(name = "order_num")
    @PositiveOrZero(message = "菜单排序只能是正整数或零")
    private Long orderNum;

    /** 类型:0目录,1菜单,2按钮 */
    @Column(name = "menu_type", length = 32)
    @Dict(value = "sys_menu_type")
    @NotBlank(message = "菜单类型不能为空")
    @Hql(type = Logical.EQ)
    private String menuType;

    /** 权限字符串 */
    @Column(name = "perms", length = 128)
    @Size(min = 0, max = 128, message = "权限标识长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String perms;

    /** 菜单图标 */
    @Column(name = "icon", length = 64)
    private String icon;

    /** 创建者 */
    @Column(name="create_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;

    /** 路由名称 */
    @Column(name = "name", length = 64)
    @Size(min = 0, max = 64, message = "路由名称长度不能超过64个字符")
    @Hql(type = Logical.LIKE)
    private String name;

    /** 组件地址 */
    @Column(name = "component", length = 256)
    @Size(min = 0, max = 256, message = "组件地址不能超过256个字符")
    @Hql(type = Logical.LIKE)
    private String component;

    /** 是否外链:N=显示,Y=隐藏 */
    @Column(name = "is_link", length = 1)
    @Dict(value = "sys_yes_no")
    @Hql(type = Logical.EQ)
    private String isLink;

    /** 外链地址 */
    @Column(name = "link_url", length = 256)
    @Size(min = 0, max = 256, message = "外链地址不能超过256个字符")
    @Hql(type = Logical.LIKE)
    private String linkUrl;

    /** 是否隐藏:N=显示,Y=隐藏 */
    @Column(name = "is_hide", length = 1)
    @Dict(value = "sys_yes_no")
    @Hql(type = Logical.EQ)
    private String isHide;

    /** 是否缓存:N=否,Y=是 */
    @Column(name = "is_keep_alive", length = 1)
    @Dict(value = "sys_yes_no")
    @Hql(type = Logical.EQ)
    private String isKeepAlive;

    /** 是否固定:N=否,Y=是 */
    @Column(name = "is_affix", length = 1)
    @Dict(value = "sys_yes_no")
    @Hql(type = Logical.EQ)
    private String isAffix;

    /** 是否内嵌窗口:N=否,Y=是 */
    @Column(name = "is_iframe", length = 1)
    @Dict(value = "sys_yes_no")
    @Hql(type = Logical.EQ)
    private String isIframe;

    /** 是否公共资源:N=否,Y=是 */
    @Column(name = "is_common", length = 1)
    @Dict(value = "sys_yes_no")
    @Hql(type = Logical.EQ)
    private String isCommon;

    /** 是否租户资源:N=否,Y=是 */
    @Column(name = "is_tenant", length = 1)
    @Dict(value = "sys_yes_no")
    @Hql(type = Logical.EQ)
    private String isTenant;

    /** 子菜单 */
    @Transient
    private List<SysMenu2> children = new ArrayList<SysMenu2>();

}
