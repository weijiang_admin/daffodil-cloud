package com.daffodil.system.entity;

import com.daffodil.core.entity.BaseEntity;
import com.daffodil.system.model.ConfigLogin;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -系统设置
 * @author yweijian
 * @date 2022年9月8日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysConfig extends BaseEntity<String> {

    private static final long serialVersionUID = 913754711645111023L;

    private ConfigLogin login;

}
