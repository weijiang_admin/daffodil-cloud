package com.daffodil.system.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -用户和职级关联
 * @author yweijian
 * @date 2022年6月13日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_user_rank", indexes = {
        @Index(name = "user_id", columnList = "user_id"),
        @Index(name = "rank_id", columnList = "rank_id")
})
public class SysUserRank extends BaseEntity<String> {

    private static final long serialVersionUID = 6810085351012760419L;

    /** 角用户和职级关联编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "user_rank_id", length = 32)
    private String id;
    
    /** 用户ID */
    @Column(name = "user_id", length = 32)
    private String userId;

    /** 职级ID */
    @Column(name = "rank_id", length = 32)
    private String rankId;

}
