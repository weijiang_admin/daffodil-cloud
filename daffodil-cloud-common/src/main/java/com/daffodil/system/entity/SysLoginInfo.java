package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统访问记录表
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_login_info")
public class SysLoginInfo extends BaseEntity<String> {
    private static final long serialVersionUID = 4756609331723695083L;

    /** 登录信息编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "login_info_id", length = 32)
    private String id;
    
    /** 用户账号 */
    @Column(name = "login_name", length = 32)
    @Hql(type = Logical.EQ)
    private String loginName;

    /** 登录状态 0成功 1失败 */
    @Column(name = "status", length = 1)
    @Dict(value = "sys_success_status")
    @Hql(type = Logical.EQ)
    private String status;

    /** 登录IP地址 */
    @Column(name = "ipaddr", length = 32)
    @Hql(type = Logical.LIKE)
    private String ipaddr;

    /** 登录地点 */
    @Column(name = "login_location", length = 128)
    @Hql(type = Logical.LIKE)
    private String loginLocation;

    /** 浏览器类型 */
    @Column(name = "browser", length = 256)
    @Hql(type = Logical.LIKE)
    private String browser;

    /** 操作系统 */
    @Column(name = "os", length = 64)
    @Hql(type = Logical.LIKE)
    private String os;

    /** 提示消息 */
    @Column(name = "msg", length = 256)
    @Hql(type = Logical.LIKE)
    private String msg;

    /** 访问时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "create_time")
    private Date createTime;
    
}