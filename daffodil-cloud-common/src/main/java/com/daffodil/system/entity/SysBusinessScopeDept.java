package com.daffodil.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * @author yweijian
 * @date 2025年3月13日
 * @version 2.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_business_scope_dept", indexes = {
        @Index(name = "business", columnList = "business"),
        @Index(name = "role_id", columnList = "role_id")
})
public class SysBusinessScopeDept extends BaseEntity<String> {

    private static final long serialVersionUID = 3094606061130547467L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;

    /** 业务标识 */
    @Column(name = "business", length = 128)
    @Hql(type = Logical.EQ)
    private String business;

    /** 角色ID */
    @Column(name = "role_id", length = 32)
    @Hql(type = Logical.IN)
    private String roleId;

    /** 部门ID */
    @Column(name = "dept_id", length = 32)
    @Hql(type = Logical.IN)
    private String deptId;
}
