package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.*;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -职级表
 * @author yweijian
 * @date 2022年6月13日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_rank")
public class SysRank extends BaseEntity<String> {

    private static final long serialVersionUID = -9035111498851354768L;

    /** 职级编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "rank_id", length = 32)
    private String id;
    
    /** 职级编码 */
    @Column(name = "rank_code", length = 64)
    @NotBlank(message = "职级编码不能为空")
    @Size(min = 0, max = 64, message = "职级编码长度不能超过64个字符")
    @Hql(type = Logical.LIKE)
    private String rankCode;

    /** 职级名称 */
    @Column(name = "post_name", length = 32)
    @NotBlank(message = "职级名称不能为空")
    @Size(min = 0, max = 32, message = "职级名称长度不能超过32个字符")
    @Hql(type = Logical.LIKE)
    private String rankName;

    /** 显示顺序 */
    @Column(name = "order_num")
    @PositiveOrZero(message = "职级排序只能是正整数或零")
    private Long orderNum;

    /** 状态（0正常 1停用 2删除） */
    @Column(name = "status", length = 1)
    @Dict(value = "sys_data_status")
    @Hql(type = Logical.EQ)
    private String status;
    
    /** 创建者 */
    @Column(name="create_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;
    
}
