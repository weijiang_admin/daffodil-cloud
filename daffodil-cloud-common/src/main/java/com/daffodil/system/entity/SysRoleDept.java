package com.daffodil.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色和部门关联
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_role_dept", indexes = {
        @Index(name = "role_id", columnList = "role_id"),
        @Index(name = "dept_id", columnList = "dept_id")
})
public class SysRoleDept extends BaseEntity<String> {
    private static final long serialVersionUID = -5035205512231813514L;

    /** 角色和部门关联编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "role_dept_id", length = 32)
    private String id;
    
    /** 角色ID */
    @Column(name = "role_id", length = 32)
    private String roleId;

    /** 部门ID */
    @Column(name = "dept_id", length = 32)
    private String deptId;

}
