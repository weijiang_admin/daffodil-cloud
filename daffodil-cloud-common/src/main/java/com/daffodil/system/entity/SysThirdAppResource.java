package com.daffodil.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -第三方应用授权接口权限
 * @author yweijian
 * @date 2023年4月27日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_third_app_resource")
public class SysThirdAppResource extends BaseEntity<String> {

    private static final long serialVersionUID = -7001766441276727624L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;

    /** 应用ID */
    @Column(name = "app_id", length = 32)
    private String appId;

    /** 资源ID */
    @Column(name = "resource_id", length = 32)
    private String resourceId;
}
