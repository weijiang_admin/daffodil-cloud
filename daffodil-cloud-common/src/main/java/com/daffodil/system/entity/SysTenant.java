package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_tenant")
public class SysTenant extends BaseEntity<String> {

    private static final long serialVersionUID = -347025565288539264L;

    @Id
    @NotBlank(message = "租户号码不能为空")
    @Size(min = 4, max = 8, message = "租户号码长度限制为4~8个字符")
    @Column(name = "tenant_id", length = 8)
    private String id;

    /** 租户名称 */
    @NotBlank(message = "租户名称不能为空")
    @Size(min = 1, max = 64, message = "租户名称长度不能超过64个字符")
    @Column(name = "tenant_name", length = 64)
    @Hql(type = Logical.LIKE)
    private String tenantName;

    /** 数据源ID */
    @NotBlank(message = "数据源不能为空")
    @Column(name = "datasource_id", length = 32)
    private String datasourceId;

    /** 租户状态*/
    @Dict("sys_data_status")
    @Hql(type = Logical.EQ)
    @Column(name = "status", length = 1)
    private String status;

    /** 创建者 */
    @Column(name="create_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;

    /** 菜单组 */
    @Transient
    private String[] menuIds;
}
