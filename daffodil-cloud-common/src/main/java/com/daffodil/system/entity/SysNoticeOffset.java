package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -用户通知消息偏移表
 * @author yweijian
 * @date 2022年6月23日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_notice_offset", indexes = {
        @Index(name = "user_id", columnList = "user_id")
})
public class SysNoticeOffset extends BaseEntity<String> {
    
    private static final long serialVersionUID = -2009440818362515659L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;
    
    /** 用户ID */
    @Column(name = "user_id", length = 32)
    private String userId;
    
    /** 偏移时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="offset_time")
    private Date offsetTime;
}
