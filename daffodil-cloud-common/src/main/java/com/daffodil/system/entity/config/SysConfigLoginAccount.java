package com.daffodil.system.entity.config;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -账号密码登录配置
 * @author yweijian
 * @date 2022年9月8日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_config_login_account")
public class SysConfigLoginAccount extends BaseEntity<String> {
    
    private static final long serialVersionUID = -4914663368379898773L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;
    
    /** 是否开启 (账号密码登录默认必须开启=Y)*/
    @Column(name = "enable", length = 1)
    @Dict(value = "sys_yes_no")
    private String enable = "Y";
    
    /** 验证码类型 (图片验证码=picture 动态验证码=dynamic)*/
    @Column(name = "code_type", length = 32)
    private String codeType = CodeType.PICTURE.name();

    /**
     * -验证码类型
     * @author yweijian
     * @date 2022年11月14日
     * @version 2.0.0
     * @description
     */
    public static enum CodeType {
        /** 图片验证码 */
        PICTURE,
        /** 动态验证码 */
        DYNAMIC;
    }
}
