package com.daffodil.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 租户和菜单关联
 * @author yweijian
 * @date 2024年12月12日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_tenant_menu", indexes = {
        @Index(name = "tenant_id", columnList = "tenant_id"),
        @Index(name = "menu_id", columnList = "menu_id")
})
public class SysTenantMenu extends BaseEntity<String> {

    private static final long serialVersionUID = 5828545339671197893L;

    /** 租户和菜单关联编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "tenant_menu_id", length = 32)
    private String id;

    /** 租户ID */
    @Column(name = "tenant_id", length = 32)
    private String tenantId;

    /** 菜单ID */
    @Column(name = "menu_id", length = 32)
    private String menuId;
}
