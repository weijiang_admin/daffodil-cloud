package com.daffodil.system.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.common.constant.CommonConstant;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.daffodil.framework.annotation.MaskFormat;
import com.daffodil.framework.annotation.MaskFormat.MaskType;
import com.daffodil.util.StringUtils;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户表
 * 
 * @author yweijian
 * @date 2019年12月12日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_user", indexes = {
        @Index(name = "dept_id", columnList = "dept_id"),
        @Index(name = "login_name", columnList = "login_name"),
        @Index(name = "email", columnList = "email"),
        @Index(name = "phone", columnList = "phone")
})
public class SysUser extends BaseEntity<String> {
    private static final long serialVersionUID = 2563124405284230396L;

    /** 用户ID */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "user_id", length = 32)
    private String id;

    /** 是否是系统内置超级管理员 (Y=是,N=否) */
    @Column(name = "is_admin", length = 1)
    @Hql(type = Logical.EQ)
    private String isAdmin;

    /** 部门ID */
    @Column(name = "dept_id", length = 32)
    private String deptId;

    /** 登录名称 */
    @Column(name = "login_name", length = 32)
    @NotBlank(message = "登录账号不能为空")
    @Size(min = 0, max = 32, message = "登录账号长度不能超过32个字符")
    @Hql(type = Logical.LLIKE)
    private String loginName;

    /** 用户名称 */
    @Column(name = "user_name", length = 32)
    @Size(min = 0, max = 32, message = "用户昵称长度不能超过32个字符")
    @Hql(type = Logical.LIKE)
    @MaskFormat(type = MaskType.ZH_CN_NAME)
    private String userName;

    /** 用户邮箱 */
    @Column(name = "email", length = 64)
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 64, message = "邮箱长度不能超过64个字符")
    @Hql(type = Logical.LLIKE)
    @MaskFormat(type = MaskType.EAMIL)
    private String email;

    /** 手机号码 */
    @Column(name = "phone", length = 11)
    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    @Hql(type = Logical.LLIKE)
    @MaskFormat(type = MaskType.MOBILE)
    private String phone;

    /** 用户性别 */
    @Column(name = "sex", length = 1)
    @Dict(value = "sys_user_sex")
    @Hql(type = Logical.EQ)
    private String sex;

    /** 用户头像 */
    @Column(name = "avatar", length = 256)
    private String avatar;

    /** 密码 */
    @Column(name = "password", length = 256)
    @MaskFormat(type = MaskType.PASSWORD)
    private String password;

    /** 盐加密 */
    @Column(name = "salt", length = 32)
    @MaskFormat(type = MaskType.ALL)
    private String salt;

    /** 密码过期时间 */
    @Column(name = "expire_time")
    private Date expireTime;

    /** 修改密码原由 */
    @Column(name = "change_reason", length = 1)
    private String changeReason;

    /** 帐号状态（0正常 1停用 2删除） */
    @Column(name = "status", length = 1)
    @Dict(value = "sys_data_status")
    @Hql(type = Logical.EQ)
    private String status;

    /** 最后登陆IP */
    @Column(name = "login_ip", length = 32)
    @Hql(type = Logical.LIKE)
    private String loginIp;

    /** 最后登陆时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "login_time")
    private Date loginTime;

    /** 创建者 */
    @Column(name="create_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;

    /** 部门对象 */
    @Transient
    private SysDept dept;

    /** 是否已分配 Y=是 N=否 */
    @Transient
    private String allocated;

    /** 角色对象 */
    @Transient
    private List<SysRole> roles;

    /** 角色组id */
    @Transient
    private String[] roleIds;

    /** 岗位对象 */
    @Transient
    private List<SysPost> posts;

    /** 岗位组id */
    @Transient
    private String[] postIds;

    /** 职级对象 */
    @Transient
    private List<SysRank> ranks;

    /** 职级组id */
    @Transient
    private String[] rankIds;

    /** 群组对象 */
    @Transient
    private List<SysGroup> groups;

    /** 群组组id */
    @Transient
    private String[] groupIds;

    /**
     * -判断用户是否是超级管理员角色
     * @return
     */
    public boolean isAdminRole() {
        if(CommonConstant.YES.equals(this.getIsAdmin())){
            return true;
        }
        List<SysRole> roles = this.getRoles();
        if(StringUtils.isNotEmpty(roles)){
            for(SysRole role : roles){
                if(CommonConstant.SYSADMIN.equals(role.getRoleKey())){
                    return true;
                }
            }
        }
        return false;
    }
}
