package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * -通知消息对象表
 * @author yweijian
 * @date 2022年6月22日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sys_notice_target", indexes = {
        @Index(name = "notice_id", columnList = "notice_id"),
        @Index(name = "target_id", columnList = "target_id")
})
public class SysNoticeTarget extends BaseEntity<String> {
    
    private static final long serialVersionUID = -2340873636946755036L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;
    
    /** 通知ID */
    @Column(name = "notice_id", length = 32)
    private String noticeId;
    
    /** 对象ID */
    @Column(name = "target_id", length = 32)
    private String targetId;
    
    /** 对象类型 0=ALL 1=USER 2=DEPT 3=ROLE 4=POST 5=RANK 6=GROUP */
    @Column(name = "target")
    private Integer target;
    
    /** 通知时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;
    
    public enum Target {
        /** 全体 */
        ALL,
        /** 用户 */
        USER,
        /** 部门 */
        DEPT,
        /** 角色 */
        ROLE,
        /** 岗位 */
        POST,
        /** 职级 */
        RANK,
        /** 群组 */
        GROUP;
    }
}
