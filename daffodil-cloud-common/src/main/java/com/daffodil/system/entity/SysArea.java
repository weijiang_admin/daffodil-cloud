package com.daffodil.system.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.*;

import com.daffodil.core.entity.BaseEntity;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.framework.annotation.Dict;
import com.daffodil.util.StringUtils;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -中国行政区划代码表<br>
 * -中国行政区划代码由 1～12 位代码构成，结构为：<br>
 * -□ □ □ □ □ □ □ □ □ □ □ □ <br>
 * -1 2 3 4 5 6 7 8 9 10 11 12 <br>
 * -中国行政区划代码由 1～12 位代码构成，表示为：<br>
 * -第 1～2 位，为省级代码；<br>
 * -第 3～4 位，为地级代码；<br>
 * -第 5～6 位，为县级代码；<br>
 * -第 7～9 位，为乡级代码；<br>
 * -第 10～12 位，为村级代码。<br>
 * 
 * -资源来源根据中华人民共和国民政部
 * -2020年版 http://xzqh.mca.gov.cn/statistics/2020.html
 * 
 * @author yweijian
 * @date 2022年6月14日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_area")
public class SysArea extends BaseEntity<String> {

    private static final long serialVersionUID = -6568830431876236573L;

    /** 区划代码 1～12 位代码构成 */
    @Id
    @NotBlank(message = "区划代码不能为空")
    @Column(name = "area_id", length = 12)
    private String id;

    /** 省级代码 第1～2 位 */
    @Column(name = "province", length = 2)
    private String province;

    /** 地级代码 第3～4 位 */
    @Column(name = "city", length = 2)
    private String city;

    /** 县级代码 第5～6 位 */
    @Column(name = "county", length = 2)
    private String county;

    /** 乡级代码 第7～9 位 */
    @Column(name = "town", length = 3)
    private String town;

    /** 村级代码 第10～12 位 */
    @Column(name = "village", length = 3)
    private String village;

    /** 父区划ID */
    @Column(name = "parent_id", length = 12)
    @Hql(type = Logical.EQ)
    private String parentId;

    /** 祖级列表 */
    @Column(name = "ancestors", length = 256)
    @Hql(type = Logical.LIKE)
    private String ancestors;

    /** 区划名称 */
    @Column(name = "area_name", length = 32)
    @NotBlank(message = "区划名称不能为空")
    @Size(min = 0, max = 30, message = "区划名称长度不能超过30个字符")
    @Hql(type = Logical.LIKE)
    private String areaName;
    
    /** 区划简称 */
    @Column(name = "short_name", length = 16)
    @Size(min = 0, max = 10, message = "区划简称长度不能超过10个字符")
    @Hql(type = Logical.EQ)
    private String shortName;
    
    /** 省会城市 */
    @Column(name = "capital_city", length = 32)
    @Size(min = 0, max = 30, message = "省会城市长度不能超过30个字符")
    @Hql(type = Logical.LIKE)
    private String capitalCity;

    /** 区划等级 省级=1 地级=2 县级=3 乡级=4 村级=5 */
    @Column(name = "area_level")
    @Hql(type = Logical.EQ)
    private Integer areaLevel;

    /** 邮政编码 */
    @Column(name = "post_code", length = 6)
    @Hql(type = Logical.EQ)
    private String postCode;

    /** 显示顺序 */
    @Column(name = "order_num")
    @PositiveOrZero(message = "区划排序只能是正整数或零")
    private Long orderNum;

    /** 区划状态（0正常 1停用 2删除） */
    @Column(name = "status", length = 1)
    @Dict(value = "sys_data_status")
    @Hql(type = Logical.EQ)
    private String status;

    /** 创建者 */
    @Column(name="create_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;
    
    @Transient
    private List<SysArea> children;
    
    @Transient
    private Boolean hasChildren;
    
    @Override
    public void setId(String id) {
        this.id = id;
        if(StringUtils.isNotEmpty(id) && id.trim().length() == 12) {
            this.id = id.trim();
            this.province = this.id.substring(0, 2);
            this.city = this.id.substring(2, 4);
            this.county = this.id.substring(4, 6);
            this.town = this.id.substring(6, 9);
            this.village = this.id.substring(9, 12);
        }
    }
    
    public Boolean getHasChildren() {
        return this.areaLevel < AreaLevel.village.level;
    }

    public enum AreaLevel {
        
        province(1),
        city(2),
        county(3),
        town(4),
        village(5);
        
        public static final String ZERO1 = "0";
        public static final String ZERO2 = "00";
        public static final String ZERO3 = "000";
        
        /** 区划等级 */
        private final int level;
        
        AreaLevel(int level) {
            this.level = level;
        }
    }
    
}
