package com.daffodil.system.entity.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -用户和群组关联
 * @author yweijian
 * @date 2022年6月21日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_user_group", indexes = {
        @Index(name = "user_id", columnList = "user_id"),
        @Index(name = "group_id", columnList = "group_id")
})
public class SysUserGroup extends BaseEntity<String> {
    private static final long serialVersionUID = -255423089769176273L;

    /** 角用户和群组关联编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "user_group_id", length = 32)
    private String id;
    
    /** 用户ID */
    @Column(name = "user_id", length = 32)
    private String userId;
    
    /** 登录名称 */
    @Column(name = "login_name", length = 32)
    @Hql(type = Logical.LIKE)
    private String loginName;

    /** 用户名称 */
    @Column(name = "user_name", length = 128)
    @Hql(type = Logical.LIKE)
    private String userName;

    /** 群组ID */
    @Column(name = "group_id", length = 32)
    @Hql(type = Logical.EQ)
    private String groupId;
    
    /** 用户权限 1=群主 2=管理员 3=成员 */
    @Column(name = "user_perm")
    @Hql(type = Logical.EQ)
    private Integer userPerm;
    
    /** 入群状态 0=正常 1=退群 */
    @Column(name = "status", length = 1)
    @Hql(type = Logical.EQ)
    private String status;

    /** 入群时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

}
