package com.daffodil.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色和菜单关联
 * @author yweijian
 * @date 2021年9月24日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_role_menu", indexes = {
        @Index(name = "role_id", columnList = "role_id"),
        @Index(name = "menu_id", columnList = "menu_id")
})
public class SysRoleMenu extends BaseEntity<String> {
    private static final long serialVersionUID = 2749530865139654800L;

    /** 角色和菜单关联编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "role_menu_id", length = 32)
    private String id;
    
    /** 角色ID */
    @Column(name = "role_id", length = 32)
    private String roleId;

    /** 菜单ID */
    @Column(name = "menu_id", length = 32)
    private String menuId;
}
