package com.daffodil.system.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色表
 * 
 * @author yweijian
 * @date 2019年12月12日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_role")
public class SysRole extends BaseEntity<String> {
    private static final long serialVersionUID = 970552735364961665L;

    /** 角色ID */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "role_id", length = 32)
    private String id;

    /** 角色名称 */
    @Column(name = "role_name", length = 32)
    @NotBlank(message = "角色名称不能为空")
    @Size(min = 0, max = 32, message = "角色名称长度不能超过32个字符")
    @Hql(type = Logical.LIKE)
    private String roleName;

    /** 角色权限 */
    @Column(name = "role_key", length = 64)
    @NotBlank(message = "权限字符不能为空")
    @Size(min = 0, max = 64, message = "权限字符长度不能超过64个字符")
    @Hql(type = Logical.LIKE)
    private String roleKey;

    /** 角色排序 */
    @Column(name = "order_num")
    @PositiveOrZero(message = "角色排序只能是正整数或零")
    private Long orderNum;

    /** （全局）数据权限（1：所有数据权限；2：自定义部门数据权限；3：本部门数据权限；4：本部门及以下数据权限；5：仅本用户数据权限） */
    @Column(name = "data_scope", length = 1)
    @Hql(type = Logical.EQ)
    private String dataScope;

    /** 角色状态（0正常 1停用 2删除） */
    @Column(name = "status", length = 1)
    @Dict(value = "sys_data_status")
    @Hql(type = Logical.EQ)
    private String status;

    /** 创建者 */
    @Column(name="create_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;

    /** 菜单组 */
    @Transient
    private String[] menuIds;

    /** 部门组（数据权限） */
    @Transient
    private String[] deptIds;

    /** （业务）数据权限 */
    @Transient
    private List<SysBusinessScope> businessScopes;

}
