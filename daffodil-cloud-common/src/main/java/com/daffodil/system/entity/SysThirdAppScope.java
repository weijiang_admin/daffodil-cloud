package com.daffodil.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 第三方应用权限表
 * @author yweijian
 * @date 2023年1月3日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_third_app_scope")
public class SysThirdAppScope extends BaseEntity<String> {

    private static final long serialVersionUID = 6874250163259157746L;

    /** 主键ID */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;

    /** 应用ID */
    @Column(name = "app_id", length = 32)
    private String appId;

    /** 权限标识 */
    @Column(name = "scope", length = 32)
    private String scope;

    /**
     * 第三方应用权限范围枚举
     * @author yweijian
     * @date 2023年1月3日
     * @version 2.0.0
     * @description
     */
    public enum ThirdAppScope {
        /** 用户基本信息--必须，包括user_id、login_name、user_name、avatar、sex */
        USER,
        /** 邮箱账号 */
        EMAIL,
        /** 手机号码 */
        PHONE,
        /** 部门信息 */
        DEPT,
        /** 角色信息 */
        ROLE,
        /** 职级信息 */
        RANK;
    }
}
