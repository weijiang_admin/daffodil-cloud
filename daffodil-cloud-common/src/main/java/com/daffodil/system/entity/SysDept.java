package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.*;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 部门表
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_dept")
public class SysDept extends BaseEntity<String> {
    private static final long serialVersionUID = 3409036871964820614L;

    /** 部门编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "dept_id", length = 32)
    private String id;

    /** 父部门ID */
    @Column(name = "parent_id", length = 32)
    @Hql(type = Logical.EQ)
    private String parentId;

    /** 祖级列表 */
    @Column(name = "ancestors", length = 256)
    private String ancestors;

    /** 部门名称 */
    @Column(name = "dept_name", length = 32)
    @NotBlank(message = "部门名称不能为空")
    @Size(min = 0, max = 32, message = "部门名称长度不能超过32个字符")
    @Hql(type = Logical.LIKE)
    private String deptName;

    /** 显示顺序 */
    @Column(name = "order_num")
    @PositiveOrZero(message = "部门排序只能是正整数或零")
    private Long orderNum;

    /** 负责人 */
    @Column(name = "leader", length = 64)
    @Hql(type = Logical.LIKE)
    private String leader;

    /** 联系电话 */
    @Column(name = "phone", length = 11)
    @Size(min = 0, max = 11, message = "联系电话长度不能超过11个字符")
    @Hql(type = Logical.LIKE)
    private String phone;

    /** 邮箱 */
    @Column(name = "email", length = 64)
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 64, message = "邮箱长度不能超过64个字符")
    @Hql(type = Logical.LIKE)
    private String email;

    /** 部门状态（0正常 1停用 2删除） */
    @Column(name = "status", length = 1)
    @Dict(value = "sys_data_status")
    @Hql(type = Logical.EQ)
    private String status;

    /** 创建者 */
    @Column(name="create_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;

}
