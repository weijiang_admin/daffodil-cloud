package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.*;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -开放资源信息对象
 * @author yweijian
 * @date 2023年4月27日
 * @version 2.0.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_resource", indexes = {
        @Index(name = "parent_id", columnList = "parent_id"),
        @Index(name = "path", columnList = "path")
})
public class SysResource extends BaseEntity<String> {

    private static final long serialVersionUID = 1L;

    /** 主键编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;

    /** 父级编号 */
    @Column(name = "parent_id", length = 32)
    private String parentId;

    /** 祖级序列 */
    @Column(name = "ancestors", length = 256)
    private String ancestors;

    /** 资源类型 group=分组  resource=资源 */
    @Column(name = "resource_type", length = 32)
    @Dict(value = "sys_resource_type")
    @NotBlank(message = "资源类型不能为空")
    @Hql(type = Logical.EQ)
    private String type;

    /** 资源归属 auth=系统资源  open=开放资源 */
    @Column(name = "belong", length = 32)
    @Size(min = 0, max = 32, message = "资源归属不能超过32个字符")
    @Hql(type = Logical.EQ)
    private String belong;

    /** 路由模式 static=静态路由  dynamic=动态路由 */
    @Column(name = "mode", length = 32)
    @Size(min = 0, max = 32, message = "路由模式不能超过32个字符")
    @Hql(type = Logical.EQ)
    private String mode;

    /** 路由键值 */
    @Column(name = "mode_key", length = 32)
    @Size(min = 0, max = 32, message = "路由键值不能超过32个字符")
    @Hql(type = Logical.EQ)
    private String modeKey;

    /** 资源名称 */
    @Column(name = "name", length = 512)
    @NotBlank(message = "资源名称不能为空")
    @Size(min = 0, max = 512, message = "资源名称长度不能超过512个字符")
    @Hql(type = Logical.LIKE)
    private String name;

    /** 权限字符 */
    @Column(name = "perms", length = 128)
    @NotNull(message = "权限字符不能为空")
    @Size(min = 0, max = 128, message = "权限字符长度不能超过128个字符")
    private String perms;

    /** 资源路径 */
    @Column(name = "path", length = 512)
    @Size(min = 0, max = 512, message = "资源路径长度不能超过512个字符")
    @Hql(type = Logical.LIKE)
    private String path;
    
    /** 资源模块 */
    @Column(name = "module", length = 128)
    @Size(min = 0, max = 128, message = "资源模块不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String module;

    /** 显示顺序 */
    @Column(name = "order_num")
    private Long orderNum;

    /** 数据状态 */
    @Dict("sys_data_status")
    @Hql(type = Logical.EQ)
    @Column(name = "status", length = 1)
    private String status;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 备注 */
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;

    public enum Type {
        /** 分组 */
        GROUP,
        /** 资源 */
        RESOURCE;
    }

    public enum Mode {
        /** 静态路由 */
        STATIC,
        /** 动态路由 */
        DYNAMIC;
    }
}
