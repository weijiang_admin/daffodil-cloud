package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 操作日志记录表
 * @author yweijian
 * @date 2019年12月16日
 * @version 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_oper_log")
public class SysOperLog extends BaseEntity<String> {
    private static final long serialVersionUID = 2376490068865616568L;

    /** 操作编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "oper_log_id", length = 32)
    private String id;
    
    /** 操作模块 */
    @Column(name = "title", length = 64)
    @Hql(type = Logical.LIKE)
    private String title;
    
    /** 请求方法 */
    @Column(name = "method", length = 1024)
    @Hql(type = Logical.EQ)
    private String method;

    /** 业务名称 */
    @Column(name = "business_name", length = 32)
    @Hql(type = Logical.LIKE)
    private String businessName;
    
    /** 业务标签 */
    @Column(name = "business_label", length = 32)
    @Hql(type = Logical.EQ)
    private String businessLabel;
    
    /** 业务备注 */
    @Column(name = "business_remark", length = 64)
    private String businessRemark;

    /** 操作客户端 */
    @Column(name = "client_name", length = 32)
    private String clientName;

    /** 操作客户端标签 */
    @Column(name = "client_label", length = 32)
    private String clientLabel;
    
    /** 操作客户端备注 */
    @Column(name = "client_remark", length = 64)
    private String clientRemark;

    /** 操作人员 */
    @Column(name = "oper_name", length = 32)
    @Hql(type = Logical.EQ)
    private String operName;

    /** 请求url */
    @Column(name = "oper_url", length=2048)
    @Hql(type = Logical.LIKE)
    private String operUrl;

    /** 操作地址 */
    @Column(name = "oper_ip", length = 32)
    @Hql(type = Logical.LIKE)
    private String operIp;

    /** 操作地点 */
    @Column(name = "oper_location", length = 128)
    @Hql(type = Logical.LIKE)
    private String operLocation;
    
    /** 请求追踪标识 */
    @Column(name = "oper_trace_id", length = 36)
    @Hql(type = Logical.LIKE)
    private String operTraceId;
    
    /** 请求头部 */
    @Column(name = "oper_header", columnDefinition = "text")
    @Hql(type = Logical.LIKE)
    private String operHeader;

    /** 请求参数 */
    @Column(name = "oper_param", columnDefinition = "text")
    @Hql(type = Logical.LIKE)
    private String operParam;
    
    /** 请求结果 */
    @Column(name = "oper_result", columnDefinition = "longtext")
    @Hql(type = Logical.LIKE)
    private String operResult;

    /** 操作状态（0正常 1异常） */
    @Column(name = "status", length = 1)
    @Dict(value = "sys_success_status")
    @Hql(type = Logical.EQ)
    private String status;

    /** 错误消息 */
    @Column(name = "error_msg", columnDefinition = "text")
    @Hql(type = Logical.LIKE)
    private String errorMsg;

    /** 操作时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "create_time")
    private Date createTime;
    
}
