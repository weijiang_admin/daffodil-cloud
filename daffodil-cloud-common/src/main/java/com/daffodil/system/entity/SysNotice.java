package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.*;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.framework.annotation.Dict;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -通知公告表
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_notice")
public class SysNotice extends BaseEntity<String> {

    private static final long serialVersionUID = -871715684131984817L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "notice_id", length = 32)
    private String id;

    /** 公告标题 */
    @Column(name = "notice_title", length = 64)
    @NotBlank(message = "公告标题不能为空")
    @Size(min = 0, max = 64, message = "公告标题不能超过64个字符")
    @Hql(type = Logical.LIKE)
    private String noticeTitle;

    /** 公告类型（1通知 2公告） */
    @Column(name = "notice_type", length = 1)
    @Dict(value = "sys_notice_type")
    @Hql(type = Logical.EQ)
    private String noticeType;

    /** 公告内容 */
    @Column(name = "notice_content", length=2048)
    @Hql(type = Logical.LIKE)
    private String noticeContent;

    /** 公告状态 0=草稿 1=已发 */
    @Column(name = "status", length = 1)
    @Hql(type = Logical.EQ)
    private String status;

    /** 创建者 */
    @Column(name="create_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by", length = 32)
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name="update_time")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark", length = 128)
    @Size(min = 0, max = 128, message = "备注长度不能超过128个字符")
    @Hql(type = Logical.LIKE)
    private String remark;

    @Transient
    private String readStatus;

    public SysNotice() {
        super();
    }

    public SysNotice(String id,String noticeTitle,
            String noticeType, String noticeContent, String status, String remark, Date createTime, String readStatus) {
        super();
        this.id = id;
        this.noticeTitle = noticeTitle;
        this.noticeType = noticeType;
        this.noticeContent = noticeContent;
        this.status = status;
        this.remark = remark;
        this.createTime = createTime;
        this.readStatus = readStatus;
    }

}
