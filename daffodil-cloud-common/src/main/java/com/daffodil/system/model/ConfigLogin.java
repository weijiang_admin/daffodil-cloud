package com.daffodil.system.model;

import java.io.Serializable;

import com.daffodil.system.entity.config.SysConfigLoginAccount;
import com.daffodil.system.entity.config.SysConfigLoginEmail;
import com.daffodil.system.entity.config.SysConfigLoginMobile;
import com.daffodil.system.entity.config.SysConfigLoginScan;

import lombok.Data;

/**
 * -登录服务设置
 * @author yweijian
 * @date 2022年9月8日
 * @version 2.0.0
 * @description
 */
@Data
public class ConfigLogin implements Serializable {
    
    private static final long serialVersionUID = -1982574367308799453L;
    
    private SysConfigLoginAccount account;
    
    private SysConfigLoginMobile mobile;
    
    private SysConfigLoginEmail email;
    
    private SysConfigLoginScan scan;

}
