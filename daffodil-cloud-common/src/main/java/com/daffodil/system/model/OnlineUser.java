package com.daffodil.system.model;

import java.io.Serializable;
import java.util.Date;

import com.daffodil.framework.annotation.MaskFormat;
import com.daffodil.framework.annotation.MaskFormat.MaskType;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * -在线用户
 * @author yweijian
 * @date 2023年1月17日
 * @version 2.0.0
 * @description
 */
@Data
public class OnlineUser implements Serializable {

    private static final long serialVersionUID = 2672395198918681750L;

    private String id;

    /** 部门ID */
    private String deptId;

    /** 登录名称 */
    private String loginName;

    /** 用户名称 */
    @MaskFormat(type = MaskType.MIDDLE, begin = 1, count = 1)
    private String userName;

    /** 用户邮箱 */
    @MaskFormat(type = MaskType.MIDDLE, begin = 3, count = 3)
    private String email;

    /** 手机号码 */
    @MaskFormat(type = MaskType.MIDDLE, begin = 3, count = 4)
    private String phone;

    /** 帐号状态（0正常 1停用 2删除） */
    private String status;

    /** 最后登陆IP */
    private String loginIp;

    /** 真实地址 */
    private String loginAd;

    /** 操作系统 */
    private String loginOs;

    /** 浏览器类型 */
    private String loginBr;

    /** 登陆时间 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date loginTime;

    /** 登录令牌 */
    private String accessToken;

    /** 是否是超级管理员角色 (Y=是,N=否)*/
    private Boolean isAdminRole;
}
