package com.daffodil.gateway.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.cloud.gateway.config.GatewayProperties;
import org.springframework.cloud.gateway.event.EnableBodyCachingEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author yweijian
 * @date 2023年5月24日
 * @version 2.0.0
 * @description
 */
@Slf4j
@Component
public class DaffodilGatewayApplicationRunner implements ApplicationRunner, Ordered {

    @Value("${spring.application.name:daffodil-cloud-gateway}")
    private String appName;

    @Value("${server.port:49101}")
    private String appPort;
    
    @Autowired
    private GatewayProperties gatewayProperties;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public int getOrder() {
        return 1;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("达佛基础网关路由微服务启动成功，应用服务名：{} 应用端口：{}", appName, appPort);
        }
        //默认将所有的路由请求body加入缓存
        gatewayProperties.getRoutes().forEach(item -> {
            EnableBodyCachingEvent event = new EnableBodyCachingEvent(item, item.getId());
            applicationContext.publishEvent(event);
        });
    }

}
