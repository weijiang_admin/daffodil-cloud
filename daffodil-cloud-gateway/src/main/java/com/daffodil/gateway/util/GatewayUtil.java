package com.daffodil.gateway.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;

import com.daffodil.gateway.constant.GatewayConstant;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2023年1月30日
 * @version 2.0.0
 * @description
 */
public class GatewayUtil {

    /**
     * -是否是ApplicationJson请求
     * @param request
     * @return
     */
    public static boolean isApplicationJson(ServerHttpRequest request) {
        return StringUtils.startsWithIgnoreCase(request.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE), MediaType.APPLICATION_JSON_VALUE);
    }

    /**
     * -是否是Multipart请求
     * @param request
     * @return
     */
    public static boolean isMultipart(ServerHttpRequest request) {
        return StringUtils.startsWithIgnoreCase(request.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE), "multipart/");
    }

    /**
     * -获取accessToken
     * @param request
     * @return
     */
    public static String getAccessToken(ServerHttpRequest request) {
        String accessToken = null;
        String authorization = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        if(StringUtils.isNotEmpty(authorization)) {
            accessToken = authorization.replace(GatewayConstant.BEARER, "");
        }
        if(StringUtils.isEmpty(accessToken)) {
            accessToken = request.getQueryParams().getFirst(GatewayConstant.ACCESS_TOKEN1);
        }
        if(StringUtils.isEmpty(accessToken)) {
            accessToken = request.getQueryParams().getFirst(GatewayConstant.ACCESS_TOKEN2);
        }
        return accessToken;
    }

    /**
     * -获取租户ID
     * @param request
     * @return
     */
    public static String getTenantId(ServerHttpRequest request) {
        String tenantId = request.getHeaders().getFirst(GatewayConstant.X_REQUEST_TENANTID_HEADER);
        return StringUtils.isNotEmpty(tenantId) ? tenantId : "";
    }
}
