package com.daffodil.gateway.filter.properties;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Xss渗透漏洞攻击配置
 * @author yweijian
 * @date 2021年9月14日
 * @version 1.0
 * @description
 */
@Setter
@Getter
public class XssAttackProperties {

    /**
     * Xss匹配 是否开启xss渗透验证
     */
    private Boolean enable = true;

    /**
     * Xss匹配 自定义匹配正则表达式
     */
    private List<String> regexs = new ArrayList<String>();
    
    /**
     * Xss匹配 配置不进行检验的url
     */
    private List<String> ignoreUrls = new ArrayList<String>();

}
