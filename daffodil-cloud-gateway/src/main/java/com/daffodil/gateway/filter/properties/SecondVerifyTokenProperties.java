package com.daffodil.gateway.filter.properties;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author yweijian
 * @date 2022年10月21日
 * @version 2.0.0
 * @description
 */
@Getter
@Setter
public class SecondVerifyTokenProperties {
    /**
     * 二次验证TOKEN拦截是否开启，默认开启
     */
    private Boolean enable = true;
    
    /**
     * 二次验证拦截 配置需要进行二次检验的url
     */
    private List<String> urls = new ArrayList<String>();
}
