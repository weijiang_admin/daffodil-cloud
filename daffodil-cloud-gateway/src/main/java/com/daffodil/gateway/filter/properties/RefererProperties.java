package com.daffodil.gateway.filter.properties;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author yweijian
 * @date 2022年5月11日
 * @version 1.0
 * @description
 */
@Setter
@Getter
public class RefererProperties {

    /**
     * 允许访问来源
     */
    private List<String> allowedOrigin = new ArrayList<String>();
    
    /**
     * 忽略拦截的URL
     */
    private List<String> ignoreUrls = new ArrayList<String>();
}
