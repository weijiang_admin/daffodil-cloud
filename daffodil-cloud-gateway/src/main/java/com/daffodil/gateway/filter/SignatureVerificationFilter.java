package com.daffodil.gateway.filter;

import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.MultiValueMap;
import org.springframework.util.MultiValueMapAdapter;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.gateway.constant.GatewayConstant;
import com.daffodil.gateway.filter.properties.SecurityProperties;
import com.daffodil.gateway.util.GatewayUtil;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.StringUtils;
import com.daffodil.util.WebFluxUtils;
import com.daffodil.util.sm.SM3Utils;

import reactor.core.publisher.Mono;

/**
 * -接口参数签名验签<br>
 * -1、一定程度上保证接口的数据不会被篡改<br>
 * -2、一定程度上保障接口避免重复多次调用<br>
 * -3、一定程度上保证接口调用幂等性问题<br>
 * -4、一定程度上签名验证会损耗响应性能<br>
 * <p>必须在org.springframework.cloud.gateway.filter.AdaptCachedBodyGlobalFilter.java之后  即 getOrder() > Ordered.HIGHEST_PRECEDENCE + 1000;</p>
 * @author yweijian
 * @date 2022年12月22日
 * @version 2.0.0
 * @description
 */
@Component
public class SignatureVerificationFilter implements GlobalFilter, Ordered {

    @Autowired
    private SecurityProperties securityProperties;

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 4000;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        if(!securityProperties.getSignature().getEnable()) {
            return chain.filter(exchange);
        }
        ServerHttpRequest request = exchange.getRequest();
        //忽略签名验签请求
        List<String> ignoreUrls = securityProperties.getSignature().getIgnoreUrls();
        for (String path : ignoreUrls) {
            if (antPathMatcher.match(path, request.getURI().getPath())) {
                return chain.filter(exchange);
            }
        }
        //签名验签请求
        String requestBody = null;
        DataBuffer dataBuffer = exchange.getAttributeOrDefault(GatewayConstant.CACHED_REQUEST_BODY_ATTR_KEY, null);
        if(dataBuffer != null && dataBuffer.readableByteCount() > 0) {
            CharBuffer charBuffer = StandardCharsets.UTF_8.decode(dataBuffer.asByteBuffer());
            requestBody = charBuffer.toString();
        }
        Map<String, List<String>> paramsMap = exchange.getAttributeOrDefault(GatewayConstant.CACHED_REQUEST_PARAMS_ATTR_KEY, Collections.emptyMap());
        if(this.verifySignature(request, paramsMap, requestBody)) {
            return chain.filter(exchange);
        }
        return WebFluxUtils.responseWriter(exchange.getResponse(), HttpStatus.BAD_REQUEST, "参数签名验签不通过");
    }

    /**
     * -接口参数验签<br>
     * -请求的params和body的参数均必须按ASCII编码自然排序
     * @param request
     * @param paramsMap 请求url的params参数
     * @param requestBody 请求url的body参数
     * @return
     */
    private Boolean verifySignature(ServerHttpRequest request, Map<String, List<String>> paramsMap, String requestBody) {

        String signature = request.getHeaders().getFirst(GatewayConstant.SIGN_SIGNATURE);
        String secret = request.getHeaders().getFirst(GatewayConstant.SIGN_SECRET);
        String timestamp = request.getHeaders().getFirst(GatewayConstant.SIGN_TIMESTAMP);

        //验证签名时效性是否满足正负闭区间的时间区范围内[-duration, +duration]
        try {
            long timediffer = new Date().getTime() - Long.parseLong(timestamp);
            long duration = securityProperties.getSignature().getDuration();
            if(timediffer < -duration * 1000L || timediffer > duration * 1000L) {
                return false;
            }
        }catch (NumberFormatException e) {
            return false;
        }

        //data
        TreeMap<String, Object> signData = new TreeMap<String, Object>();
        signData.put("data", new TreeMap<String, Object>());
        if(StringUtils.isNotEmpty(requestBody) && GatewayUtil.isApplicationJson(request)) {
            if(requestBody.startsWith("{") && requestBody.endsWith("}")) {
                signData.put("data", this.signDataWithObject(requestBody));
            }else if(requestBody.startsWith("[") && requestBody.endsWith("]")) {
                signData.put("data", this.signDataWithList(requestBody));
            }
        }

        //params
        MultiValueMap<String, String> multiValueMap = new MultiValueMapAdapter<String, String>(paramsMap);
        signData.put("params", this.signDataWithObject(multiValueMap.toSingleValueMap()));

        signData.put("secret", secret);
        signData.put("timestamp", Long.parseLong(timestamp));
        signData.put("url", request.getPath().toString());

        String encrypt = SM3Utils.encrypt(JacksonUtils.toJSONString(signData));
        return StringUtils.isNotEmpty(signature) && signature.equals(encrypt);
    }

    @SuppressWarnings("unchecked")
    private TreeMap<String, Object> signDataWithObject(Object data) {
        TreeMap<String, Object> sortMap = JacksonUtils.toJavaObject(data, TreeMap.class);
        for(Map.Entry<String, Object> entry: sortMap.entrySet()) {
            Object value = entry.getValue();
            if(value instanceof Map) {
                sortMap.put(entry.getKey(), this.signDataWithObject(value));
            }else if(value instanceof List) {
                sortMap.put(entry.getKey(), this.signDataWithList(value));
            }else {
                sortMap.put(entry.getKey(), value);
            }
        }
        return sortMap;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private List signDataWithList(Object data) {
        List list = JacksonUtils.toJavaObject(data, List.class);
        if(StringUtils.isNotEmpty(list)) {
            if(list.get(0) instanceof Comparable) {
                Collections.sort(list);
                return list;
            }else if(list.get(0) instanceof Map) {
                for (int i = 0; i < list.size(); i++) {
                    String jsonStr = JacksonUtils.toJSONString(this.signDataWithObject(list.get(i)));
                    list.set(i, jsonStr);
                }
                Collections.sort(list);
                return list;
            }
        }
        return list;
    }

}
