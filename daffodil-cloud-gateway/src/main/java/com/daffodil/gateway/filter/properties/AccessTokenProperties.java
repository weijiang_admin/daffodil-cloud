package com.daffodil.gateway.filter.properties;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
@Setter
@Getter
public class AccessTokenProperties {

    /**
     * 忽略Token认证拦截的URL
     */
    private List<String> ignoreUrls = new ArrayList<String>();

    /**
     * 是否开启AccessToken的登录IP过滤
     */
    private Boolean enableFilterLoginIp = true;
}
