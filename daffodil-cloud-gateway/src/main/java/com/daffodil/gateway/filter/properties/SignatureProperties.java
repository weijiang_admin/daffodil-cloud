package com.daffodil.gateway.filter.properties;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * -接口参数签名配置
 * @author yweijian
 * @date 2022年12月22日
 * @version 2.0.0
 * @description
 */
@Setter
@Getter
public class SignatureProperties {

    /** 签名验签是否开启 */
    private Boolean enable = true;

    /** 签名签值有效时间 单位秒 */
    private Long duration = 5L;

    /** 签名算法 SM3 */
    private String method = "SM3";

    /** 忽略签名验证的Url */
    private List<String> ignoreUrls = new ArrayList<String>();
}
