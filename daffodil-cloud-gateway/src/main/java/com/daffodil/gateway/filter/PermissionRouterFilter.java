package com.daffodil.gateway.filter;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.gateway.constant.GatewayConstant;
import com.daffodil.gateway.util.GatewayUtil;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.StringUtils;
import com.daffodil.util.WebFluxUtils;

import reactor.core.publisher.Mono;

/**
 * -系统路由权限过滤
 * @author yweijian
 * @date 2024年6月6日
 * @version 1.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
@Component
public class PermissionRouterFilter implements GlobalFilter, Ordered {

    public final static String COMN = "COMN";

    public final static String AUTH = "AUTH";

    public final static String OPEN = "OPEN";

    public final static String PERM = "perm";

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public int getOrder() {
        return 20;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String levelToken = request.getHeaders().getFirst(GatewayConstant.X_REQUEST_ACCESS_TOKEN_LEVEL_HEADER);
        if(String.valueOf(GatewayConstant.LEVEL_TOKEN_FIRST).equals(levelToken)) {
            if(this.checkPermission(exchange, chain, COMN)) {
                return chain.filter(exchange);
            }
            return this.checkPermission(exchange, chain, AUTH) ? chain.filter(exchange) : WebFluxUtils.responseWriter(response, HttpStatus.FORBIDDEN, "请求未授权访问");
        }else if (String.valueOf(GatewayConstant.LEVEL_TOKEN_SECOND).equals(levelToken)) {
            return this.checkPermission(exchange, chain, OPEN) ? chain.filter(exchange) : WebFluxUtils.responseWriter(response, HttpStatus.FORBIDDEN, "请求未授权访问");
        }else {
            return chain.filter(exchange);
        }
    }

    private Boolean checkPermission(ServerWebExchange exchange, GatewayFilterChain chain, String belong){
        ServerHttpRequest request = exchange.getRequest();
        String tenantId = GatewayUtil.getTenantId(request);
        String accessToken = GatewayUtil.getAccessToken(request);
        if(StringUtils.isEmpty(accessToken)) {
            return true;
        }
        String hashKey = belong + request.getURI().getPath();
        //从静态路由中匹配
        if(redisTemplate.opsForHash().hasKey(GatewayConstant.ROUTER_STATIC_CACHEKEY, hashKey)) {
            return this.matchPermission(tenantId, accessToken, GatewayConstant.ROUTER_STATIC_CACHEKEY, hashKey);
        }

        //从动态路由中匹配
        Set<Object> dynamicRouters = redisTemplate.opsForHash().keys(GatewayConstant.ROUTER_DYNAMIC_CACHEKEY);
        if(StringUtils.isNotEmpty(dynamicRouters)) {
            for(Object object : dynamicRouters) {
                String path = (String) object;
                if(antPathMatcher.match(path, hashKey)) {
                    return this.matchPermission(tenantId, accessToken, GatewayConstant.ROUTER_DYNAMIC_CACHEKEY, path);
                }
            }
        }

        return false;
    }

    /**
     * -匹配操作权限
     * @param accessToken
     * @param routerKey GatewayConstant.ROUTER_STATIC_CACHEKEY or GatewayConstant.ROUTER_DYNAMIC_CACHEKEY
     * @param path
     * @return
     */
    @SuppressWarnings("unchecked")
    private Boolean matchPermission(String tenantId, String accessToken, String routerKey, String path) {
        Object obj = redisTemplate.opsForHash().get(routerKey, path);
        Map<String, String> router = JacksonUtils.toJavaObject(obj, Map.class, () -> new HashMap<String, String>());
        String perm = router.get(PERM);
        if(StringUtils.isEmpty(perm)) {
            return true;
        }
        String cacheKey = StringUtils.format(GatewayConstant.PERMISSION_TOKEN_CACHEKEY, accessToken);
        cacheKey = StringUtils.isNotBlank(tenantId) ? tenantId + ":" + cacheKey : cacheKey;
        return redisTemplate.opsForSet().isMember(cacheKey, perm);
    }

}
