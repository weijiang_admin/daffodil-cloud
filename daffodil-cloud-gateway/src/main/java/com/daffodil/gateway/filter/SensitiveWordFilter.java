package com.daffodil.gateway.filter;

import java.net.URI;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.gateway.constant.GatewayConstant;
import com.daffodil.gateway.exception.SensitiveWordException;
import com.daffodil.gateway.filter.properties.SecurityProperties;
import com.daffodil.gateway.filter.properties.SensitiveProperties;
import com.daffodil.util.StringUtils;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * -网络敏感词过滤（不文明用语）
 * <p>必须在org.springframework.cloud.gateway.filter.AdaptCachedBodyGlobalFilter.java之后  即 getOrder() > Ordered.HIGHEST_PRECEDENCE + 1000;</p>
 * @author yweijian
 * @date 2022年9月13日
 * @version 2.0.0
 * @description
 */
@Slf4j
@Component
public class SensitiveWordFilter implements GlobalFilter, Ordered{

    @Autowired
    private WebClient webClient;

    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    private SensitiveProperties sensitiveProperties;

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    private static final Integer SUCCESS = 0;

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 3001;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        if(!securityProperties.getSensitiveWord().getEnable()) {
            return chain.filter(exchange);
        }

        ServerHttpRequest request = exchange.getRequest();
        List<String> urls = securityProperties.getSensitiveWord().getUrls();

        Boolean isMatched = urls.stream().anyMatch(path -> antPathMatcher.match(path, request.getURI().getPath()));
        if(!isMatched) {
            return chain.filter(exchange);
        }

        String requestBody = null;
        DataBuffer dataBuffer = exchange.getAttributeOrDefault(GatewayConstant.CACHED_REQUEST_BODY_ATTR_KEY, null);
        if(dataBuffer != null && dataBuffer.readableByteCount() > 0) {
            CharBuffer charBuffer = StandardCharsets.UTF_8.decode(dataBuffer.asByteBuffer());
            requestBody = charBuffer.toString();
        }
        if (StringUtils.isEmpty(requestBody)) {
            return chain.filter(exchange);
        }

        Map<String, String> filterWords = new HashMap<String, String>();
        filterWords.put("text", requestBody);
        URI uri = sensitiveProperties.getSensitiveWordsCheckUri();
        return webClient.post().uri(uri )
                .headers(httpHeaders -> {
                    HttpHeaders headers = HttpHeaders.writableHttpHeaders(request.getHeaders());
                    headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
                    httpHeaders.addAll(headers);
                })
                .bodyValue(filterWords).retrieve().bodyToMono(HashMap.class)
                .flatMap(jsonResult -> {
                    Integer code = (Integer) jsonResult.get("code");
                    if(SUCCESS.equals(code)) {
                        return chain.filter(exchange);
                    }
                    String data = (String) jsonResult.get("data");
                    throw new SensitiveWordException(HttpStatus.BAD_REQUEST.value(), "请求中包含不文明用语或敏感词词语【" + data + "】，已拦截");
                }).onErrorResume(e -> {
                    if(e instanceof SensitiveWordException) {
                        throw new SensitiveWordException(HttpStatus.BAD_REQUEST.value(), e.getMessage());
                    }else {
                        if(log.isWarnEnabled()) {
                            log.warn(e.getMessage(), e);
                        }
                    }
                    return chain.filter(exchange);
                });
    }
}
