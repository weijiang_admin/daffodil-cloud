package com.daffodil.gateway.filter;

import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.gateway.constant.GatewayConstant;
import com.daffodil.gateway.exception.SqlAtackException;
import com.daffodil.gateway.exception.XssAtackException;
import com.daffodil.gateway.filter.properties.SecurityProperties;
import com.daffodil.gateway.util.GatewayUtil;
import com.daffodil.util.StringUtils;
import com.daffodil.util.VerificateUtils;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * XSS渗透漏洞和SQL注入漏洞攻击拦截
 * <p>必须在org.springframework.cloud.gateway.filter.AdaptCachedBodyGlobalFilter.java之后  即 getOrder() > Ordered.HIGHEST_PRECEDENCE + 1000;</p>
 * @author yweijian
 * @date 2021年9月14日
 * @version 1.0
 * @description
 */
@Slf4j
@Component
public class XssAndSqlAttackFilter implements GlobalFilter, Ordered {

    @Autowired
    private SecurityProperties securityProperties;

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 3000;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        if(log.isDebugEnabled()) {
            log.debug("开始检查请求中包含xss渗透攻击或sql注入攻击的特殊字符...");
        }
        // 判断白名单的API
        ServerHttpRequest request = exchange.getRequest();
        List<String> ignoreUrls = new ArrayList<String>();
        ignoreUrls.addAll(securityProperties.getXssAttack().getIgnoreUrls());
        ignoreUrls.addAll(securityProperties.getSqlAttack().getIgnoreUrls());

        for (String path : ignoreUrls) {
            if (antPathMatcher.match(path, request.getURI().getPath())) {
                return chain.filter(exchange);
            }
        }

        this.checkHeaders(exchange);
        this.checkQueryParams(exchange);
        this.checkRequestBody(exchange);

        if(log.isDebugEnabled()) {
            log.debug("结束检查请求中包含xss渗透攻击或sql注入攻击的特殊字符...");
        }
        return chain.filter(exchange);
    }

    /**
     * 校验Headers头部
     * 
     * @param request
     * @return
     */
    private void checkHeaders(ServerWebExchange exchange) {
        Set<Entry<String, List<String>>> headers = exchange.getRequest().getHeaders().entrySet();
        this.specialCharsCheck(headers);
    }

    /**
     * 校验请求参数
     * 
     * @param request
     * @return
     */
    private void checkQueryParams(ServerWebExchange exchange) {
        Map<String, List<String>> paramsMap = exchange.getAttributeOrDefault(GatewayConstant.CACHED_REQUEST_PARAMS_ATTR_KEY, Collections.emptyMap());
        Set<Entry<String, List<String>>> params = paramsMap.entrySet();
        this.specialCharsCheck(params);
    }

    /**
     * 校验请求体参数
     * 
     * @param request
     * @return
     */
    private void checkRequestBody(ServerWebExchange exchange) {
        ServerHttpRequest request = exchange.getRequest();
        if(GatewayUtil.isMultipart(request)) {
            return;
        }

        String requestBody = null;
        DataBuffer dataBuffer = exchange.getAttributeOrDefault(GatewayConstant.CACHED_REQUEST_BODY_ATTR_KEY, null);
        if(dataBuffer != null && dataBuffer.readableByteCount() > 0) {
            CharBuffer charBuffer = StandardCharsets.UTF_8.decode(dataBuffer.asByteBuffer());
            requestBody = charBuffer.toString();
        }
        if (StringUtils.isEmpty(requestBody)) {
            return;
        }
        if (this.securityProperties.getXssAttack().getEnable()) {
            boolean checked = VerificateUtils.check(requestBody, this.securityProperties.getXssAttack().getRegexs());
            if (checked) {
                throw new XssAtackException(HttpStatus.BAD_REQUEST.value(), "请求中包含xss渗透攻击的特殊字符，已拦截");
            }
        }
        if (this.securityProperties.getSqlAttack().getEnable()) {
            boolean checked = VerificateUtils.check(requestBody, this.securityProperties.getSqlAttack().getRegexs());
            if (checked) {
                throw new SqlAtackException(HttpStatus.BAD_REQUEST.value(), "请求中包含sql注入攻击的特殊字符，已拦截");
            }
        }
    }

    /**
     * 校验集合参数
     * 
     * @param sets
     * @return
     */
    private void specialCharsCheck(Set<Entry<String, List<String>>> sets) {
        Iterator<Entry<String, List<String>>> iterator = sets.iterator();
        iterator.forEachRemaining(entry -> {
            List<String> values = entry.getValue();
            if(StringUtils.isNotEmpty(values)) {
                values.forEach(value -> {
                    if (this.securityProperties.getXssAttack().getEnable()) {
                        boolean checked = VerificateUtils.check(value, this.securityProperties.getXssAttack().getRegexs());
                        if (checked) {
                            throw new XssAtackException(HttpStatus.BAD_REQUEST.value(), "请求中包含xss渗透攻击的特殊字符，已拦截");
                        }
                    }
                    if (this.securityProperties.getSqlAttack().getEnable()) {
                        boolean checked = VerificateUtils.check(value, this.securityProperties.getSqlAttack().getRegexs());
                        if (checked) {
                            throw new SqlAtackException(HttpStatus.BAD_REQUEST.value(), "请求中包含sql注入攻击的特殊字符，已拦截");
                        }
                    }
                });
            }
        });
    }
}
