package com.daffodil.gateway.filter;

import java.io.IOException;
import java.io.InputStream;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.hutool.core.collection.CollectionUtil;
import org.apache.commons.fileupload.ParameterParser;
import org.apache.commons.io.IOUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.util.MultiValueMapAdapter;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.gateway.constant.GatewayConstant;
import com.daffodil.gateway.util.GatewayUtil;
import com.daffodil.util.StringUtils;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * -链路追踪过滤<br>
 * <p>必须在org.springframework.cloud.gateway.filter.AdaptCachedBodyGlobalFilter.java之后  即 getOrder() > Ordered.HIGHEST_PRECEDENCE + 1000;</p>
 * @author yweijian
 * @date 2021年10月28日
 * @version 1.0
 * @description
 */
@Slf4j
@Component
public class ServerHttpRequestTraceFilter implements GlobalFilter, Ordered {

    @Value("${spring.application.name:}")
    private String appName;

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 2000;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        Instant startTime = Instant.now();
        ServerHttpRequest request = exchange.getRequest();

        String requestTraceId = request.getHeaders().getFirst(GatewayConstant.X_REQUEST_TRACEID_HEADER);
        final String traceId = StringUtils.isEmpty(requestTraceId) ? UUID.randomUUID().toString() : requestTraceId;
        MDC.put(GatewayConstant.MDC_TRACE_APP, appName);
        MDC.put(GatewayConstant.MDC_TRACE_ID, traceId);

        MultiValueMap<String, String> paramsMap = new MultiValueMapAdapter<String, String>(new HashMap<String, List<String>>());
        paramsMap.addAll(request.getQueryParams());
        if(GatewayUtil.isMultipart(request)) {
            String body = this.getRequestBody(exchange);
            String content = request.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE);
            String boundary = this.getContentDispositionParser(content, "boundary");
            String charset = request.getHeaders().getFirst(HttpHeaders.ACCEPT_CHARSET);
            this.setQueryParamsMap(body, boundary, paramsMap, charset);
        }
        exchange.getAttributes().put(GatewayConstant.CACHED_REQUEST_PARAMS_ATTR_KEY, Collections.unmodifiableMap(paramsMap));

        ServerHttpRequest newRequest = exchange.getRequest().mutate().headers(header -> {
            HttpHeaders headers = request.getHeaders();
            headers.forEach((key, values) -> header.put(key, values));
            header.addIfAbsent(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.name());
            header.addIfAbsent(GatewayConstant.X_REQUEST_TRACEID_HEADER, traceId);
        }).build();

        ServerWebExchange newExchange = exchange.mutate().request(newRequest).build();

        if(StringUtils.isEmpty(newExchange.getResponse().getHeaders().getFirst(GatewayConstant.X_REQUEST_TRACEID_HEADER))) {
            newExchange.getResponse().getHeaders().add(GatewayConstant.X_REQUEST_TRACEID_HEADER, traceId);
        }

        if (log.isInfoEnabled()) {
            String body = GatewayUtil.isMultipart(newRequest) ? "" : this.getRequestBody(newExchange);
            log.info("request method={} uri={} params={} body={} headers={} cookies={}", 
                    newRequest.getMethodValue(), newRequest.getPath(), paramsMap.toSingleValueMap(), body, 
                    newRequest.getHeaders().toSingleValueMap(), newRequest.getCookies().toSingleValueMap());
        }

        return chain.filter(newExchange).doFinally(onFinally -> {
            if (log.isInfoEnabled()) {
                Long executeTime = Duration.between(startTime, Instant.now()).toMillis();
                log.info("request total execution time : {} ms", executeTime);
            }
            MDC.remove(GatewayConstant.MDC_TRACE_APP);
            MDC.remove(GatewayConstant.MDC_TRACE_ID);
        });
    }

    private String getRequestBody(ServerWebExchange exchange) {
        DataBuffer dataBuffer = exchange.getAttributeOrDefault(GatewayConstant.CACHED_REQUEST_BODY_ATTR_KEY, null);
        if(dataBuffer != null && dataBuffer.readableByteCount() > 0) {
            CharBuffer charBuffer = StandardCharsets.UTF_8.decode(dataBuffer.asByteBuffer());
            return charBuffer.toString();
        }
        return null;
    }

    private void setQueryParamsMap(String body, String boundary, Map<String, List<String>> paramsMap, String charset) {
        if(StringUtils.isEmpty(body)) {
            return;
        }
        charset = StringUtils.isNotEmpty(charset) ? charset : StandardCharsets.UTF_8.name();
        try (InputStream input = IOUtils.toInputStream(body, Charset.forName(charset))){
            List<String> lines = IOUtils.readLines(input, Charset.forName(charset));
            if(CollectionUtil.isEmpty(lines)){
                return;
            }
            String key = null;
            for(int i = 0; i < lines.size(); i++) {
                String line = lines.get(i);
                if(line.contains(boundary) || "".equals(line)) {
                    continue;
                }
                if(line.contains("Content-Disposition")) {
                    key = this.getContentDispositionParser(line, "name");
                    String filename = this.getContentDispositionParser(line, "filename");
                    if(StringUtils.isNotEmpty(filename)) {
                        paramsMap.computeIfAbsent(key, k -> new ArrayList<>()).add(filename);
                        key = null;
                    }
                    continue;
                }
                if(StringUtils.isNotEmpty(key)) {
                    paramsMap.computeIfAbsent(key, k -> new ArrayList<>()).add(line);
                    key = null;
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    private String getContentDispositionParser(String content, String key) {
        ParameterParser parser = new ParameterParser();
        parser.setLowerCaseNames(true);
        Map<String, String> params = parser.parse(content, new char[] {';', ','});
        return params.get(key);
    }
}
