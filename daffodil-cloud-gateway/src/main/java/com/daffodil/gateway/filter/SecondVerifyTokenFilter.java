package com.daffodil.gateway.filter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.gateway.constant.GatewayConstant;
import com.daffodil.gateway.filter.properties.SecurityProperties;
import com.daffodil.gateway.util.GatewayUtil;
import com.daffodil.util.StringUtils;
import com.daffodil.util.WebFluxUtils;

import reactor.core.publisher.Mono;

/**
 * -二次操作二次验证Token
 * @author yweijian
 * @date 2022年10月21日
 * @version 2.0.0
 * @description
 */
@Component
public class SecondVerifyTokenFilter implements GlobalFilter, Ordered{

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    
    @Autowired
    private SecurityProperties securityProperties;

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public int getOrder() {
        return 30;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        if(!securityProperties.getSecondVerifyToken().getEnable()) {
            return chain.filter(exchange);
        }

        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        List<String> urls = securityProperties.getSecondVerifyToken().getUrls();
        Boolean isMatched = urls.stream().anyMatch(path -> antPathMatcher.match(path, request.getURI().getPath()));
        if(isMatched) {
            String tenantId = GatewayUtil.getTenantId(request);
            String accessToken = GatewayUtil.getAccessToken(request);
            String cacheKey = StringUtils.format(GatewayConstant.USER_TOKEN_VERIFY_CACHEKEY, accessToken);
            cacheKey = StringUtils.isNotBlank(tenantId) ? tenantId + ":" + cacheKey : cacheKey;
            String cacheToken = (String) redisTemplate.opsForValue().get(cacheKey);
            String verifyToken = request.getHeaders().getFirst(GatewayConstant.X_REQUEST_SECOND_VERIFY_TOKEN_HEADER);
            if(verifyToken == null || !verifyToken.equals(cacheToken)) {
                return WebFluxUtils.responseWriter(response, HttpStatus.FORBIDDEN, "二次验证令牌不可信或已过期");
            }
            redisTemplate.delete(cacheKey);
        }
        
        return chain.filter(exchange);
    }
    
}
