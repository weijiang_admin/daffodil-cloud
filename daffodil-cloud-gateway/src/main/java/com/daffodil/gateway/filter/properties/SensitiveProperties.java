package com.daffodil.gateway.filter.properties;

import java.net.URI;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import com.daffodil.util.StringUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

/**
 * -基础平台应用daffodil-cloud-auth
 * @author yweijian
 * @date 2023年3月27日
 * @version 2.0.0
 * @description
 */
@Setter
@Getter
@Component
@RefreshScope
public class SensitiveProperties {

    @Value("${daffodil.service.sensitive.protocol:lb}")
    private String protocol;

    @Value("${daffodil.service.sensitive.service-id:daffodil-cloud-sensitive}")
    private String serviceId;

    @Value("${daffodil.service.sensitive.service-path:/api-sensitive}")
    private String servicePath;
    
    public String getServiceUrl() {
        return StringUtils.format("{}://{}{}", this.protocol, this.serviceId, this.servicePath);
    }

    @SneakyThrows
    public URI getSensitiveWordsCheckUri() {
        return new URI(StringUtils.format("{}/words/check", this.getServiceUrl()));
    }
}
