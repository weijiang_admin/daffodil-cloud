package com.daffodil.gateway.filter;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.gateway.filter.properties.SecurityProperties;
import com.daffodil.util.StringUtils;
import com.daffodil.util.WebFluxUtils;

import reactor.core.publisher.Mono;

/**
 * Http请求源拦截器
 * @author yweijian
 * @date 2022年5月11日
 * @version 1.0
 * @description
 */
@Component
public class ServerHttpRequestRefererFilter implements GlobalFilter, Ordered {

    @Autowired
    private SecurityProperties securityProperties;

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 2100;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 判断忽略访问来源白名单的API
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        List<String> origins = securityProperties.getReferer().getAllowedOrigin();
        if(origins.contains("*")) {
            return chain.filter(exchange);
        }

        List<String> ignoreUrls = securityProperties.getReferer().getIgnoreUrls();
        for (String path : ignoreUrls) {
            if (antPathMatcher.match(path, request.getURI().getPath())) {
                return chain.filter(exchange);
            }
        }

        String referer = request.getHeaders().getFirst("Referer");
        if(StringUtils.isEmpty(referer)) {
            return WebFluxUtils.responseWriter(response, HttpStatus.FORBIDDEN, "不信任的访问来源");
        }
        Pattern pattern = Pattern.compile("(http://|https://)?([^/]*)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(referer);
        String host = matcher.find() ? matcher.group(2) : referer;

        boolean match = origins.stream().anyMatch(origin -> origin.equals(host));
        if(match) {
            return chain.filter(exchange);
        }

        return WebFluxUtils.responseWriter(response, HttpStatus.FORBIDDEN, "不信任的访问来源");
    }

}
