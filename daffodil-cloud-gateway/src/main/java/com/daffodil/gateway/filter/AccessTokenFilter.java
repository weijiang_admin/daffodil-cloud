package com.daffodil.gateway.filter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;

import com.daffodil.gateway.constant.GatewayConstant;
import com.daffodil.gateway.filter.properties.SecurityProperties;
import com.daffodil.gateway.util.GatewayUtil;
import com.daffodil.util.IpUtils;
import com.daffodil.util.JacksonUtils;
import com.daffodil.util.JwtTokenUtils;
import com.daffodil.util.WebFluxUtils;
import com.daffodil.util.StringUtils;

import reactor.core.publisher.Mono;

/**
 * AccessToken拦截器
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
@Component
public class AccessTokenFilter implements GlobalFilter, Ordered {

    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 判断忽略token认证白名单的API
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        List<String> ignoreUrls = securityProperties.getAccessToken().getIgnoreUrls();
        for (String path : ignoreUrls) {
            if (antPathMatcher.match(path, request.getURI().getPath())) {
                String tenantId = GatewayUtil.getTenantId(request);
                String accessToken = GatewayUtil.getAccessToken(request);
                //如果请求参数中携带accessToken，也将authorToken放入请求到下游，但不校验accessToken和authorToken是否合法有效
                if(StringUtils.isNotEmpty(accessToken)) {
                    String cacheKey = StringUtils.format(GatewayConstant.ACCESS_TOKEN_CACHEKEY, accessToken);
                    cacheKey = StringUtils.isNotBlank(tenantId) ? tenantId + ":" + cacheKey : cacheKey;
                    String authorToken = (String) redisTemplate.opsForValue().get(cacheKey);
                    ServerHttpRequest newRequest = exchange.getRequest().mutate().header(GatewayConstant.X_REQUEST_AUTHOR_TOKEN_HEADER, authorToken).build();
                    ServerWebExchange newExchange = exchange.mutate().request(newRequest).build();
                    return chain.filter(newExchange);
                }
                return chain.filter(exchange);
            }
        }
        //校验Header头部的token或者参数中的access_token
        String tenantId = GatewayUtil.getTenantId(request);
        String accessToken = GatewayUtil.getAccessToken(request);
        if(StringUtils.isEmpty(accessToken)) {
            return WebFluxUtils.responseWriter(response, HttpStatus.UNAUTHORIZED, "授权认证令牌不可信或为空值");
        }
        //校验redis缓存中的token
        String cacheKey = StringUtils.format(GatewayConstant.ACCESS_TOKEN_CACHEKEY, accessToken);
        cacheKey = StringUtils.isNotBlank(tenantId) ? tenantId + ":" + cacheKey : cacheKey;
        String authorToken = (String) redisTemplate.opsForValue().get(cacheKey);
        if(StringUtils.isEmpty(authorToken)) {
            return WebFluxUtils.responseWriter(response, HttpStatus.UNAUTHORIZED, "授权认证令牌不可信或已过期");
        }
        //校验authorToken是否有效
        if(!JwtTokenUtils.isValid(authorToken)) {
            return WebFluxUtils.responseWriter(response, HttpStatus.UNAUTHORIZED, "授权认证令牌不可信或已过期");
        }
        //校验authorToken是否超期
        if(JwtTokenUtils.isExpireTime(authorToken)) {
            return WebFluxUtils.responseWriter(response, HttpStatus.UNAUTHORIZED, "授权认证令牌不可信或已过期");
        }

        //校验请求地址和授权令牌的登录地址是否一致
        Boolean enableFilterLoginIp = securityProperties.getAccessToken().getEnableFilterLoginIp();
        if(enableFilterLoginIp) {
            String data = JwtTokenUtils.getData(authorToken);
            Map<String, String> dataMap = JacksonUtils.toJavaObject(data, HashMap.class);
            String requestIp = IpUtils.getIpAddrByServerHttpRequest(request);
            if(null == dataMap || !requestIp.equals(dataMap.get("loginIp"))) {
                return WebFluxUtils.responseWriter(response, HttpStatus.UNAUTHORIZED, "授权认证令牌不可信或已过期");
            }
        }

        //校验levelToken是否过期
        String levelTokenCachekey = StringUtils.format(GatewayConstant.LEVEL_TOKEN_CACHEKEY, accessToken);
        levelTokenCachekey = StringUtils.isNotBlank(tenantId) ? tenantId + ":" + levelTokenCachekey : levelTokenCachekey;
        Integer levelToken = (Integer) redisTemplate.opsForValue().get(levelTokenCachekey);
        if(StringUtils.isNull(levelToken)) {
            return WebFluxUtils.responseWriter(response, HttpStatus.UNAUTHORIZED, "授权认证令牌不可信或已过期");
        }

        ServerHttpRequest newRequest = exchange.getRequest().mutate()
                .headers(header -> {
                    header.set(GatewayConstant.X_REQUEST_AUTHOR_TOKEN_HEADER, authorToken);
                    header.set(GatewayConstant.X_REQUEST_ACCESS_TOKEN_LEVEL_HEADER, String.valueOf(levelToken));
                }).build();
        ServerWebExchange newExchange = exchange.mutate().request(newRequest).build();

        return chain.filter(newExchange);
    }

}
