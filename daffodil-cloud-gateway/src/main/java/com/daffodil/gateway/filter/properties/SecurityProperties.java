package com.daffodil.gateway.filter.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 系统安全配置
 * @author yweijian
 * @date 2021年9月14日
 * @version 1.0
 * @description
 */
@Setter
@Getter
@Component
@RefreshScope
@ConfigurationProperties(prefix = "daffodil.security")
public class SecurityProperties {
    
    private AccessTokenProperties accessToken = new AccessTokenProperties();
    
    private XssAttackProperties xssAttack = new XssAttackProperties();
    
    private SqlAttackProperties sqlAttack = new SqlAttackProperties();
    
    private CrosProperties cros = new CrosProperties();
    
    private RefererProperties referer = new RefererProperties();
    
    private SensitiveWordProperties sensitiveWord = new SensitiveWordProperties();
    
    private SecondVerifyTokenProperties secondVerifyToken = new SecondVerifyTokenProperties();
    
    private SignatureProperties signature = new SignatureProperties();
    
}
