package com.daffodil.gateway.filter.properties;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
@Setter
@Getter
public class CrosProperties {

    /**
     * 允许跨域请求源
     */
    private List<String> allowedOrigin = new ArrayList<String>();
    
    /**
     * 允许跨域请求方法
     */
    private List<String> allowedMethod = new ArrayList<String>();
    
    /**
     * 允许跨域请求头
     */
    private List<String> allowedHeader = new ArrayList<String>();
    
    /**
     * 是否支持用户凭据cookie
     */
    Boolean allowCredentials = false;
    
    /**
     * 允许跨域请求接口
     */
    private List<String> allowedUrls = new ArrayList<String>();

}
