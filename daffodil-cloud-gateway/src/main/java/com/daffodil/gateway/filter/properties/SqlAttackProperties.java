package com.daffodil.gateway.filter.properties;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * SQL注入漏洞攻击配置
 * @author yweijian
 * @date 2021年9月17日
 * @version 1.0
 * @description
 */
@Setter
@Getter
public class SqlAttackProperties {

    /**
     * SQL注入漏洞是否开启
     */
    private Boolean enable = true;
    
    /**
     * SQL注入漏洞 自定义匹配正则表达式
     */
    private List<String> regexs = new ArrayList<String>();;
    
    /**
     * SQL注入漏洞  配置不进行检验的url
     */
    private List<String> ignoreUrls = new ArrayList<String>();
}
