package com.daffodil.gateway.exception;

/**
 * -敏感词拦截异常
 * @author yweijian
 * @date 2022年9月14日
 * @version 2.0.0
 * @description
 */
public class SensitiveWordException extends GatewayException {

    private static final long serialVersionUID = -238120652937325724L;

    public SensitiveWordException(int code, String defaultMessage) {
        super(code, defaultMessage);
    }

}
