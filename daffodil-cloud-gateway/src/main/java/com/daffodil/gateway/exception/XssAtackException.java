package com.daffodil.gateway.exception;

/**
 * XSS渗透漏洞攻击拦截异常
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
public class XssAtackException extends GatewayException{

    private static final long serialVersionUID = 593764114576496023L;

    public XssAtackException(int code, String defaultMessage) {
        super(code, defaultMessage);
    }

}
