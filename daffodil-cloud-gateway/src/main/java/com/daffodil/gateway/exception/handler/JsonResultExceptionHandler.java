package com.daffodil.gateway.exception.handler;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.WebProperties.Resources;
import org.springframework.boot.autoconfigure.web.reactive.error.DefaultErrorWebExceptionHandler;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.daffodil.gateway.exception.GatewayException;

import reactor.core.publisher.Mono;

/**
 * JsonResultException 自定义异常返回格式结果
 * 
 * @author yweijian
 * @date 2021年9月16日
 * @version 1.0
 * @description
 */
public class JsonResultExceptionHandler extends DefaultErrorWebExceptionHandler {

    public JsonResultExceptionHandler(ErrorAttributes errorAttributes, Resources resources,
            ErrorProperties errorProperties, ApplicationContext applicationContext) {
        super(errorAttributes, resources, errorProperties, applicationContext);
    }

    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(ErrorAttributes errorAttributes) {
        return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse);
    }

    @Override
    protected Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
        Map<String, Object> map = super.getErrorAttributes(request, options);
        Throwable throwable = super.getError(request);
        int status = (int) map.get("status");
        String message = (String) map.get("error");

        // 自定义处理拦截的相关错误返回提示
        if (throwable instanceof GatewayException) {
            status = ((GatewayException) throwable).getCode();
            message = throwable.getMessage();
        }

        Map<String, Object> jsonResult = new HashMap<String, Object>();
        jsonResult.put("code", status);
        jsonResult.put("msg", message);
        jsonResult.put("data", null);
        return jsonResult;
    }

    @Override
    protected int getHttpStatus(Map<String, Object> errorAttributes) {
        return (int) errorAttributes.get("code");
    }

    @Override
    protected Mono<ServerResponse> renderErrorView(ServerRequest request) {
        return super.renderErrorView(request);
    }

}
