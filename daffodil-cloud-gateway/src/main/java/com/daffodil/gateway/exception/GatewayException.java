package com.daffodil.gateway.exception;

/**
 * 微服务网关异常
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
public class GatewayException extends RuntimeException{

    private static final long serialVersionUID = 3085965837189479463L;

    /**
     * 错误状态码
     */
    private int code;
    
    /**
     * 异常消息
     */
    private String defaultMessage;
    
    public GatewayException(String message) {
        this.defaultMessage = message;
    }

    public GatewayException(int code, String defaultMessage) {
        super();
        this.code = code;
        this.defaultMessage = defaultMessage;
    }

    @Override
    public String getMessage() {
        return this.defaultMessage;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }

    public void setDefaultMessage(String defaultMessage) {
        this.defaultMessage = defaultMessage;
    }

}
