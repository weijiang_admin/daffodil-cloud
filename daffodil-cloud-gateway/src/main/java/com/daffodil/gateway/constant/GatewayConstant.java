package com.daffodil.gateway.constant;

public class GatewayConstant {

    /** api请求上下文 */
    public static final String API_CONTENT_PATH = "/api-gateway";

    /** token认证前缀 */
    public static final String BEARER = "Bearer ";

    /** 认证token关键字 */
    public static final String ACCESS_TOKEN1 = "access_token";

    /** 认证token关键字 */
    public static final String ACCESS_TOKEN2 = "accessToken";

    /** 授权token关键字 */
    public static final String X_REQUEST_AUTHOR_TOKEN_HEADER = "X-Request-Author-Token";

    /** 二次验证token关键字 */
    public static final String X_REQUEST_SECOND_VERIFY_TOKEN_HEADER = "X-Request-Second-Verify-Token";

    /** 请求头的追踪标识 */
    public static final String X_REQUEST_TRACEID_HEADER = "X-Request-Trace-Id";

    /** 请求头的租户标识 */
    public static final String X_REQUEST_TENANTID_HEADER = "X-Request-Tenant-Id";

    /** 请求头的Token等级标识 */
    public static final String X_REQUEST_ACCESS_TOKEN_LEVEL_HEADER = "X-Request-Access-Token-Level";

    /** 追踪应用标识 */
    public static final String MDC_TRACE_APP = "appName";

    /** 追踪标识 */
    public static final String MDC_TRACE_ID = "traceId";

    /** 签名验签值 */
    public static final String SIGN_SIGNATURE = "signature";

    /** 签名随机数 */
    public static final String SIGN_SECRET = "secret";

    /** 签名时间戳 */
    public static final String SIGN_TIMESTAMP = "timestamp";

    /** 登录令牌 */
    public static final String ACCESS_TOKEN_CACHEKEY = "system:token:access:{}";

    /** 令牌等级*/
    public static final String LEVEL_TOKEN_CACHEKEY = "system:token:level:{}";

    /** 令牌权限 */
    public static final String PERMISSION_TOKEN_CACHEKEY = "system:token:permission:{}";

    /** 动态路由 */
    public static final String ROUTER_DYNAMIC_CACHEKEY = "system:router:dynamic";

    /** 静态路由 */
    public static final String ROUTER_STATIC_CACHEKEY = "system:router:static";

    /** 不文明词语 */
    public static final String SENSITIVE_WORD_CACHEKEY = "system:sensitive:word";

    /** 用户操作二次token验证 */
    public static final String USER_TOKEN_VERIFY_CACHEKEY = "user:token:verify:{}";

    /** 请求body参数缓存 */
    public static final String CACHED_REQUEST_BODY_ATTR_KEY = "cachedRequestBody";

    /** 请求params参数缓存 */
    public static final String CACHED_REQUEST_PARAMS_ATTR_KEY = "cachedRequestParams";

    /** 一类登录令牌 */
    public static final Integer LEVEL_TOKEN_FIRST = 1;

    /** 二类登录令牌 */
    public static final Integer LEVEL_TOKEN_SECOND = 2;
}
