package com.daffodil.gateway.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.pattern.PathPatternParser;

import com.daffodil.gateway.filter.properties.SecurityProperties;
import com.daffodil.util.StringUtils;

/**
 * 允许跨域请求白名单配置
 * @author yweijian
 * @date 2022年1月22日
 * @description
 */
@Configuration
public class CorsWebFilterConfiguration {

    @Autowired
    private SecurityProperties securityProperties;

    @Bean
    public CorsWebFilter corsWebFilter() {
        CorsConfiguration config = new CorsConfiguration();
        List<String> allowedOrigins = securityProperties.getCros().getAllowedOrigin();
        //config.setAllowedOrigins(allowedOrigins);
        //SpringBoot2.4版本++使用setAllowedOriginPatterns(allowedOrigins)
        config.setAllowedOriginPatterns(allowedOrigins);

        List<String> allowedMethods = securityProperties.getCros().getAllowedMethod();
        config.setAllowedMethods(allowedMethods);

        List<String> allowedHeaders = securityProperties.getCros().getAllowedHeader();
        config.setAllowedHeaders(allowedHeaders);

        Boolean allowCredentials = securityProperties.getCros().getAllowCredentials();
        config.setAllowCredentials(allowCredentials);

        List<String> allowedUrls = securityProperties.getCros().getAllowedUrls();
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource(new PathPatternParser());
        if(StringUtils.isNotEmpty(allowedUrls)) {
            for(String path : allowedUrls) {
                source.registerCorsConfiguration(path, config);
            }
        }
        return new CorsWebFilter(source);
    }
}
