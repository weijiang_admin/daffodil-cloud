package com.daffodil.gateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.client.loadbalancer.reactive.LoadBalancedExchangeFilterFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * 
 * @author yweijian
 * @date 2022年9月19日
 * @version 2.0.0
 * @description
 */
@Configuration
public class WebClientConfiguration {

    @Autowired
    public WebClient.Builder webClientBuilder;

    @Autowired
    private LoadBalancedExchangeFilterFunction filter;

    @Bean
    @ConditionalOnMissingBean
    public WebClient webClient() {
        return this.webClientBuilder.filter(filter).build();
    }
}
