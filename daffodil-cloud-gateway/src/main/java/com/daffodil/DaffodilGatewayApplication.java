package com.daffodil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 达佛基础平台网关微服务
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
@EnableDiscoveryClient
@SpringBootApplication
public class DaffodilGatewayApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(DaffodilGatewayApplication.class);
        application.setWebApplicationType(WebApplicationType.REACTIVE);
        application.addListeners(new ApplicationPidFileWriter());
        application.run(args);
    }
}
