
## 用户
##-- `daffodil-cloud`.sys_user definition

CREATE TABLE `sys_user` (
  `user_id` varchar(32) NOT NULL,
  `avatar` varchar(256) DEFAULT NULL,
  `change_reason` varchar(1) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `dept_id` varchar(32) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `expire_time` datetime DEFAULT NULL,
  `is_admin` varchar(1) DEFAULT NULL,
  `login_ip` varchar(32) DEFAULT NULL,
  `login_name` varchar(32) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `remark` varchar(64) DEFAULT NULL,
  `salt` varchar(32) DEFAULT NULL,
  `sex` varchar(1) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `user_name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO sys_user
(user_id, avatar, change_reason, create_by, create_time, dept_id, email, expire_time, is_admin, login_ip, login_name, login_time, password, phone, remark, salt, sex, status, update_by, update_time, user_name)
VALUES('402855816f1e2a2e016f1e2a45af0001', '', '0', 'system', '2020-02-15 19:38:14', '', 'daffodil@daffodil-cloud.com.cn', '2023-05-23 10:54:24', 'Y', '127.0.0.1', 'admin', '2022-12-20 15:02:02', '60c204e8033a93ad37844c5a00a6fa873d7aab3b135fcef82c3b7ee6baf7dfdc', '18088888888', '系统保留用户', '4e990b', '0', '0', 'admin', '2022-11-23 10:54:24', '达佛');

## 角色
##-- `daffodil-cloud`.sys_role definition

CREATE TABLE `sys_role` (
  `role_id` varchar(32) NOT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `data_scope` varchar(1) DEFAULT NULL,
  `order_num` bigint(20) DEFAULT NULL,
  `remark` varchar(64) DEFAULT NULL,
  `role_key` varchar(64) DEFAULT NULL,
  `role_name` varchar(32) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO sys_role
(role_id, create_by, create_time, data_scope, order_num, remark, role_key, role_name, status, update_by, update_time)
VALUES('402855816f1e2a2e016f1e2a45510000', 'system', '2019-12-19 20:38:04', '1', 1, '系统保留角色', 'SysAdmin', '超级管理员', '0', 'admin', '2021-10-13 13:37:50');

## 字典
##-- `daffodil-cloud`.sys_dictionary definition

CREATE TABLE `sys_dictionary` (
  `dict_id` varchar(32) NOT NULL,
  `ancestors` varchar(256) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `dict_label` varchar(64) DEFAULT NULL,
  `dict_name` varchar(64) DEFAULT NULL,
  `dict_type` varchar(255) DEFAULT NULL,
  `dict_value` varchar(64) DEFAULT NULL,
  `is_default` varchar(1) DEFAULT NULL,
  `order_num` bigint(20) DEFAULT NULL,
  `parent_id` varchar(32) DEFAULT NULL,
  `remark` varchar(64) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`dict_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('04dd93d28f1540eb83186209ac721037', ',root,4028b26c75684644017568652476001b,ddfbc54298134a6e8e1c61100f5e6453', 'admin', '2020-03-18 15:56:47', '', 'PC端', 'dictionary', '1', 'Y', 1, 'ddfbc54298134a6e8e1c61100f5e6453', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('0682885ad8144601806f118278bf4b0b', ',root,4028b26c75684644017568652476001b,dca26407442443b1a7c49a38a8eac93f', 'admin', '2020-03-18 10:41:12', '', '正常', 'dictionary', '0', 'Y', 1, 'dca26407442443b1a7c49a38a8eac93f', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('0b0f4ecbca8c45e38210c71d6e8c2e9f', ',root,4028b26c75684644017568652476001b,ddfbc54298134a6e8e1c61100f5e6453', 'admin', '2020-03-18 15:56:23', '', '其它端', 'dictionary', '0', 'N', 0, 'ddfbc54298134a6e8e1c61100f5e6453', '', '0', 'admin', '2022-09-20 11:01:58');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('0e4ad1b471d0459b8de904ac487116d8', ',root,4028b26c75684644017568652476001b,5c3cd4b16d0b4dc28c2cea11629230c9', 'admin', '2020-03-18 10:30:10', '', '删除', 'dictionary', '3', 'N', 3, '5c3cd4b16d0b4dc28c2cea11629230c9', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('1f5255985c374d64920f2bda198ced58', ',root,4028b26c75684644017568652476001b,4ae041232b854064bbd574bfb2cc4bd0', 'admin', '2020-03-18 09:46:08', '', '成功', 'dictionary', '0', 'Y', 1, '4ae041232b854064bbd574bfb2cc4bd0', '', '0', 'admin', '2021-10-20 14:34:59');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('1f77b35a233f4819baea460b84cd3036', ',root,4028b26c75684644017568652476001b,5c3cd4b16d0b4dc28c2cea11629230c9', 'admin', '2020-03-18 09:55:32', '', '新增', 'dictionary', '1', 'N', 1, '5c3cd4b16d0b4dc28c2cea11629230c9', '', '0', 'admin', '2020-03-18 10:29:18');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('2da4d568038f482dba7753492d18f974', ',root,4028b26c75684644017568652476001b,dca26407442443b1a7c49a38a8eac93f', 'admin', '2020-03-18 10:41:39', '', '删除', 'dictionary', '2', 'N', 3, 'dca26407442443b1a7c49a38a8eac93f', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('320ad502da6746988a7d2d43f8a7b34d', ',root,4028b26c75684644017568652476001b,ec9fb496e40e4ae5bd422d65e30577e3', 'admin', '2020-03-18 10:51:54', '', '目录', 'dictionary', 'catalog', 'Y', 1, 'ec9fb496e40e4ae5bd422d65e30577e3', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('34b1eab26685480a8d5a93aeaf79affd', ',root,4028b26c75684644017568652476001b,5c3cd4b16d0b4dc28c2cea11629230c9', 'admin', '2020-03-18 10:30:42', '', '导入', 'dictionary', '6', 'N', 6, '5c3cd4b16d0b4dc28c2cea11629230c9', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('39bcc5e71b154d79bf8c91ef4e7f5ff4', ',root,4028b26c75684644017568652476001b,4ae041232b854064bbd574bfb2cc4bd0', 'admin', '2020-03-18 09:46:21', '', '失败', 'dictionary', '1', 'N', 2, '4ae041232b854064bbd574bfb2cc4bd0', '', '0', 'admin', '2020-03-18 10:29:07');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('3efbf0f7037f4d05ace7c07a58864acd', ',root,4028b26c75684644017568652476001b,4b332d745d7c4555a988676fb627138f', 'admin', '2020-03-18 10:43:00', '', '隐藏', 'dictionary', '1', 'N', 2, '4b332d745d7c4555a988676fb627138f', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('40280981835865c5018358d6ee640000', ',root,4028b26c75684644017568652476001b', 'admin', '2022-09-20 11:00:24', 'sys_tag_type', '标签类型', 'catalog', NULL, 'N', 12, '4028b26c75684644017568652476001b', NULL, '0', 'admin', '2022-09-20 11:00:32');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('40280981835865c5018358d776e70001', ',root,4028b26c75684644017568652476001b,40280981835865c5018358d6ee640000', 'admin', '2022-09-20 11:00:59', '', '目录', 'dictionary', 'catalog', 'N', 1, '40280981835865c5018358d6ee640000', NULL, '0', 'admin', '2022-09-20 11:00:59');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('40280981835865c5018358d7b8f80002', ',root,4028b26c75684644017568652476001b,40280981835865c5018358d6ee640000', 'admin', '2022-09-20 11:01:16', '', '标签', 'dictionary', 'tag', 'Y', 2, '40280981835865c5018358d6ee640000', NULL, '0', 'admin', '2022-09-20 11:01:16');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('40280981835865c5018358db966c0003', ',root,4028b26c75684644017568652476001b', 'admin', '2022-09-20 11:05:29', 'sys_word_type', '词语类型', 'catalog', NULL, NULL, 13, '4028b26c75684644017568652476001b', NULL, '0', 'admin', '2022-09-20 11:05:29');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('40280981835865c5018358dbf7900004', ',root,4028b26c75684644017568652476001b,40280981835865c5018358db966c0003', 'admin', '2022-09-20 11:05:54', '', '目录', 'dictionary', 'catalog', 'N', 1, '40280981835865c5018358db966c0003', NULL, '0', 'admin', '2022-09-20 11:05:54');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('40280981835865c5018358dc3f6a0005', ',root,4028b26c75684644017568652476001b,40280981835865c5018358db966c0003', 'admin', '2022-09-20 11:06:13', '', '单词', 'dictionary', 'word', 'Y', 2, '40280981835865c5018358db966c0003', NULL, '0', 'admin', '2022-09-20 11:06:13');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('4028b26c75684644017568652476001b', ',root', 'admin', '2020-10-27 12:51:00', 'sys_dictionary', '基础管理系统', 'catalog', '', 'N', 1, 'root', '基础管理系统字典', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('41fe59dd732a45fc85868f243fa08278', ',root,4028b26c75684644017568652476001b,6a794ac896e64e9daf58e0501d75587c', 'admin', '2020-03-18 10:34:48', '', '新稿', 'dictionary', '0', 'Y', 1, '6a794ac896e64e9daf58e0501d75587c', '', '0', 'admin', '2020-03-18 10:35:31');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('42c45e85e62f47f4a86d4e1a0c96a2f9', ',root,4028b26c75684644017568652476001b,72cf3e936c0e44fa8dc45b4defb2d614', 'admin', '2020-03-18 10:45:44', '', '目录', 'dictionary', 'catalog', 'Y', 1, '72cf3e936c0e44fa8dc45b4defb2d614', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('4982bb49add744ccbfae5117c886e15e', ',root,4028b26c75684644017568652476001b,e824488f1d4643529b046e5c6ca23cc9', 'admin', '2020-03-18 10:44:36', '', '未知', 'dictionary', '2', 'N', 3, 'e824488f1d4643529b046e5c6ca23cc9', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('4ae041232b854064bbd574bfb2cc4bd0', ',root,4028b26c75684644017568652476001b', 'admin', '2020-03-18 09:37:12', 'sys_success_status', '操作状态', 'catalog', '', 'N', 1, '4028b26c75684644017568652476001b', '', '0', 'admin', '2022-12-07 15:55:09');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('4b332d745d7c4555a988676fb627138f', ',root,4028b26c75684644017568652476001b', 'admin', '2020-03-18 10:42:05', 'sys_show_hide', '菜单状态', 'catalog', '', 'N', 7, '4028b26c75684644017568652476001b', '', '0', 'admin', '2020-10-27 12:56:25');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('4d5ed6e3511a46a2a8e75cef381e37ef', ',root,4028b26c75684644017568652476001b,5c3cd4b16d0b4dc28c2cea11629230c9', 'admin', '2020-03-18 10:31:05', '', '清空', 'dictionary', '9', 'N', 9, '5c3cd4b16d0b4dc28c2cea11629230c9', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('52bacdd6a0624e57a6a09c4098dd355b', ',root,4028b26c75684644017568652476001b,e824488f1d4643529b046e5c6ca23cc9', 'admin', '2020-03-18 10:44:08', '', '男', 'dictionary', '0', 'Y', 1, 'e824488f1d4643529b046e5c6ca23cc9', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('5a6faf23a96948ac9a5877b9cbd52b50', ',root,4028b26c75684644017568652476001b,5c3cd4b16d0b4dc28c2cea11629230c9', 'admin', '2020-03-18 10:29:56', '', '修改', 'dictionary', '2', 'N', 2, '5c3cd4b16d0b4dc28c2cea11629230c9', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('5c3cd4b16d0b4dc28c2cea11629230c9', ',root,4028b26c75684644017568652476001b', 'admin', '2020-03-18 09:43:13', 'sys_business_type', '操作类型', 'catalog', '', 'N', 2, '4028b26c75684644017568652476001b', '', '0', 'admin', '2020-10-27 12:55:40');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('64fdac8643d54d45b32e5016955a9ca8', ',root,4028b26c75684644017568652476001b,6a794ac896e64e9daf58e0501d75587c', 'admin', '2020-03-18 10:35:56', '', '发布', 'dictionary', '1', 'N', 2, '6a794ac896e64e9daf58e0501d75587c', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('6a794ac896e64e9daf58e0501d75587c', ',root,4028b26c75684644017568652476001b', 'admin', '2020-03-18 10:32:02', 'sys_notice_status', '通知状态', 'catalog', '', 'N', 3, '4028b26c75684644017568652476001b', '', '0', 'admin', '2020-10-27 12:55:54');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('71dcbce5601f4d6da76611af40ed34ef', ',root,4028b26c75684644017568652476001b,9d77ae358e4b4f98b5fd26e92b326840', 'admin', '2020-03-18 10:40:25', '', '否', 'dictionary', 'N', 'N', 2, '9d77ae358e4b4f98b5fd26e92b326840', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('72cf3e936c0e44fa8dc45b4defb2d614', ',root,4028b26c75684644017568652476001b', 'admin', '2020-03-18 10:44:57', 'sys_menu_type', '菜单类型', 'catalog', '', 'N', 8, '4028b26c75684644017568652476001b', '', '0', 'admin', '2020-10-27 12:56:32');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('7b76d93f8cdb4bd89602af52318c873c', ',root,4028b26c75684644017568652476001b,72cf3e936c0e44fa8dc45b4defb2d614', 'admin', '2020-03-18 10:45:58', '', '菜单', 'dictionary', 'menu', 'N', 2, '72cf3e936c0e44fa8dc45b4defb2d614', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('826445d6cf4441bb99f18ac661c4e436', ',root,4028b26c75684644017568652476001b,9d77ae358e4b4f98b5fd26e92b326840', 'admin', '2020-03-18 10:40:09', '', '是', 'dictionary', 'Y', 'Y', 1, '9d77ae358e4b4f98b5fd26e92b326840', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('868b2d9ab6eb4c27a21c50ce728634c9', ',root,4028b26c75684644017568652476001b,ec9fb496e40e4ae5bd422d65e30577e3', 'admin', '2020-03-18 10:52:24', '', '字典', 'dictionary', 'dictionary', 'N', 2, 'ec9fb496e40e4ae5bd422d65e30577e3', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('8abb25c2138d4e58ad785e9c266042d7', ',root,4028b26c75684644017568652476001b,e824488f1d4643529b046e5c6ca23cc9', 'admin', '2020-03-18 10:44:20', '', '女', 'dictionary', '1', 'N', 2, 'e824488f1d4643529b046e5c6ca23cc9', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('9d77ae358e4b4f98b5fd26e92b326840', ',root,4028b26c75684644017568652476001b', 'admin', '2020-03-18 10:39:40', 'sys_yes_no', '系统是否', 'catalog', '', 'N', 5, '4028b26c75684644017568652476001b', '', '0', 'admin', '2020-10-27 12:56:15');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('a160ed2405fa482784a985078310b21d', ',root,4028b26c75684644017568652476001b,ddd7ef5169d54afd962662d05984c4eb', 'admin', '2020-03-18 10:38:07', '', '通知公告', 'dictionary', '1', 'Y', 1, 'ddd7ef5169d54afd962662d05984c4eb', '', '0', 'admin', '2020-03-18 10:39:03');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('b0c9bc34ebf342eca6b1db948462d6a9', ',root,4028b26c75684644017568652476001b,4b332d745d7c4555a988676fb627138f', 'admin', '2020-03-18 10:42:46', '', '显示', 'dictionary', '0', 'Y', 1, '4b332d745d7c4555a988676fb627138f', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('c5e3c2d24a324f0fa7ed0465c9112d5f', ',root,4028b26c75684644017568652476001b,ddfbc54298134a6e8e1c61100f5e6453', 'admin', '2020-03-18 15:57:21', '', 'APP端', 'dictionary', '3', 'N', 3, 'ddfbc54298134a6e8e1c61100f5e6453', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('d2b0c204de5d4723af26ed0e0b9a4e2b', ',root,4028b26c75684644017568652476001b,5c3cd4b16d0b4dc28c2cea11629230c9', 'admin', '2020-03-18 10:30:49', '', '强退', 'dictionary', '7', 'N', 7, '5c3cd4b16d0b4dc28c2cea11629230c9', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('dca26407442443b1a7c49a38a8eac93f', ',root,4028b26c75684644017568652476001b', 'admin', '2020-03-18 10:40:45', 'sys_data_status', '数据状态', 'catalog', '', 'N', 6, '4028b26c75684644017568652476001b', '', '0', 'admin', '2020-10-27 12:52:49');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('ddd7ef5169d54afd962662d05984c4eb', ',root,4028b26c75684644017568652476001b', 'admin', '2020-03-18 10:37:48', 'sys_notice_type', '通知类型', 'catalog', '', 'N', 4, '4028b26c75684644017568652476001b', '', '0', 'admin', '2020-10-27 12:56:07');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('ddfbc54298134a6e8e1c61100f5e6453', ',root,4028b26c75684644017568652476001b', 'admin', '2020-03-18 15:55:51', 'sys_client_type', '平台类型', 'catalog', '', 'N', 11, '4028b26c75684644017568652476001b', '', '0', 'admin', '2022-09-20 11:01:43');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('e0f1bdc8bb394055a0d856651fc3a58a', ',root,4028b26c75684644017568652476001b,72cf3e936c0e44fa8dc45b4defb2d614', 'admin', '2020-03-18 10:46:10', '', '按钮', 'dictionary', 'button', 'N', 3, '72cf3e936c0e44fa8dc45b4defb2d614', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('e4a82e16d6a442e79a6fef81cade175a', ',root,4028b26c75684644017568652476001b,5c3cd4b16d0b4dc28c2cea11629230c9', 'admin', '2020-03-18 15:53:32', '', '其他', 'dictionary', '0', 'N', 0, '5c3cd4b16d0b4dc28c2cea11629230c9', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('e4e8be8843c448fb8069f6df05baa56b', ',root,4028b26c75684644017568652476001b,5c3cd4b16d0b4dc28c2cea11629230c9', 'admin', '2020-03-18 10:30:57', '', '生成', 'dictionary', '8', 'N', 8, '5c3cd4b16d0b4dc28c2cea11629230c9', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('e824488f1d4643529b046e5c6ca23cc9', ',root,4028b26c75684644017568652476001b', 'admin', '2020-03-18 10:43:43', 'sys_user_sex', '用户性别', 'catalog', '', 'N', 9, '4028b26c75684644017568652476001b', '', '0', 'admin', '2020-10-27 12:56:39');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('e8a1fbfce6044f78a2d69707bb3849fb', ',root,4028b26c75684644017568652476001b,ddd7ef5169d54afd962662d05984c4eb', 'admin', '2020-03-18 10:38:19', '', '新闻消息', 'dictionary', '2', 'N', 2, 'ddd7ef5169d54afd962662d05984c4eb', '', '0', 'admin', '2020-03-18 10:39:14');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('ec9fb496e40e4ae5bd422d65e30577e3', ',root,4028b26c75684644017568652476001b', 'admin', '2020-03-18 10:51:37', 'sys_dict_type', '字典类型', 'catalog', '', 'N', 10, '4028b26c75684644017568652476001b', '', '0', 'admin', '2020-10-27 12:56:46');
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('eed0134ee8c34bf2b016a5ed7954a440', ',root,4028b26c75684644017568652476001b,5c3cd4b16d0b4dc28c2cea11629230c9', 'admin', '2020-03-18 10:30:32', '', '导出', 'dictionary', '5', 'N', 5, '5c3cd4b16d0b4dc28c2cea11629230c9', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('efa82a97bc0747f69365833115e65dfa', ',root,4028b26c75684644017568652476001b,5c3cd4b16d0b4dc28c2cea11629230c9', 'admin', '2020-03-18 10:30:22', '', '授权', 'dictionary', '4', 'N', 4, '5c3cd4b16d0b4dc28c2cea11629230c9', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('efd3cb2ce5dc407aa9dd6721323f66fc', ',root,4028b26c75684644017568652476001b,ddfbc54298134a6e8e1c61100f5e6453', 'admin', '2020-03-18 15:57:00', '', '微信端', 'dictionary', '2', 'N', 2, 'ddfbc54298134a6e8e1c61100f5e6453', '', '0', NULL, NULL);
INSERT INTO sys_dictionary
(dict_id, ancestors, create_by, create_time, dict_label, dict_name, dict_type, dict_value, is_default, order_num, parent_id, remark, status, update_by, update_time)
VALUES('f701324f2b5a4ac6a7fd0d01418669f5', ',root,4028b26c75684644017568652476001b,dca26407442443b1a7c49a38a8eac93f', 'admin', '2020-03-18 10:41:27', '', '停用', 'dictionary', '1', 'N', 2, 'dca26407442443b1a7c49a38a8eac93f', '', '0', NULL, NULL);

## 菜单
##-- `daffodil-cloud`.sys_menu definition

CREATE TABLE `sys_menu` (
  `menu_id` varchar(32) NOT NULL,
  `ancestors` varchar(256) DEFAULT NULL,
  `component` varchar(256) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `icon` varchar(64) DEFAULT NULL,
  `is_affix` varchar(1) DEFAULT NULL,
  `is_common` varchar(1) DEFAULT NULL,
  `is_hide` varchar(1) DEFAULT NULL,
  `is_iframe` varchar(1) DEFAULT NULL,
  `is_keep_alive` varchar(1) DEFAULT NULL,
  `is_link` varchar(1) DEFAULT NULL,
  `link_url` varchar(256) DEFAULT NULL,
  `menu_name` varchar(32) DEFAULT NULL,
  `menu_type` varchar(32) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `order_num` bigint(20) DEFAULT NULL,
  `parent_id` varchar(32) DEFAULT NULL,
  `perms` varchar(128) DEFAULT NULL,
  `remark` varchar(64) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7c356114017c35669de10000', ',root', '/home/index', 'admin', '2020-10-29 17:39:21', 'fa fa-home', 'Y', 'Y', 'N', 'N', 'Y', 'N', NULL, '首页', 'menu', 'home', 0, 'root', 'system:home:view', '', 'admin', '2022-03-01 09:27:20');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('82808381818f7fe901818f8abeb70002', ',root', NULL, 'admin', '2022-06-23 15:50:44', 'fa fa-universal-access', 'N', 'Y', 'Y', 'N', 'N', 'N', NULL, '用户中心', 'catalog', 'center', 1, 'root', 'system:center:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f6580029', ',root,40280981816630d00181663c70e9001d', '/system/menu/index', 'system', '2019-12-23 15:59:49', 'fa fa-map-signs', 'N', 'N', 'N', 'N', 'Y', 'N', NULL, '菜单管理', 'menu', 'menu', 1, '40280981816630d00181663c70e9001d', 'system:menu:view', '', 'admin', '2022-06-15 15:21:08');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816603d70181661b68430027', ',root,40280981816630d00181663c70e9001d,40280981816603d70181661ab6aa0023', NULL, 'admin', '2022-06-15 14:44:41', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '标签查询', 'button', NULL, 1, '40280981816603d70181661ab6aa0023', 'system:tag:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816630d00181663ac22d0015', ',root,40280981816630d00181663c70e9001d,40280981816630d00181663996e50010', NULL, 'admin', '2022-06-15 15:18:56', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '区划查询', 'button', NULL, 1, '40280981816630d00181663996e50010', 'system:area:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981834455dc0183446505e90010', ',root,40280981816630d00181663c70e9001d,40280981834455dc0183446505e9000f', NULL, 'system', '2022-09-16 11:45:52', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '词语查询', 'button', NULL, 1, '40280981834455dc0183446505e9000f', 'sensitive:words:list', NULL, 'admin', '2022-09-19 17:02:32');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098185186273018518944fb70004', ',root,40280981816630d00181663c70e9001d,4028098185186273018518931fbb0000', NULL, 'admin', '2022-12-16 09:37:31', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '对象查询', 'button', NULL, 1, '4028098185186273018518931fbb0000', 'storage:object:list', NULL, 'admin', '2022-12-16 09:37:31');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7340043', ',root,40280981816630d00181663c70e9001d,402855816f31c4de016f31c4f6580029', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '菜单查询', 'button', NULL, 1, '402855816f31c4de016f31c4f6580029', 'system:menu:list', '', 'admin', '2019-12-25 15:47:59');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7900050', ',root,40280981816630d00181663c70e9001d,402855816f31c4de016f31c4f67a002c', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '字典查询', 'button', NULL, 1, '402855816f31c4de016f31c4f67a002c', 'system:dictionary:list', '', 'admin', '2021-10-13 15:25:16');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('8280840183bb863a0183bb8e24b40005', ',root,40280981816630d00181663c70e9001d,8280840183bb863a0183bb8c23670001', NULL, 'admin', '2022-10-09 15:03:18', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '键值查询', 'button', NULL, 1, '8280840183bb863a0183bb8c23670001', 'system:cache:keys', NULL, 'admin', '2022-10-13 14:29:13');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981838722d701838759c7450000', ',root,40280981831a8f1f01831bec26830029', '/config/login/index', 'admin', '2022-09-29 11:45:51', 'fa fa-sign-in', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '登录设置', 'menu', 'config/login', 1, '40280981831a8f1f01831bec26830029', 'config:login:view', NULL, 'admin', '2022-09-29 11:45:51');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f6470027', ',root,402855816f31c4de016f31c4f6260024', '/system/user/index', 'system', '2019-12-23 15:59:49', 'fa fa-user-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '用户管理', 'menu', 'user', 1, '402855816f31c4de016f31c4f6260024', 'system:user:view', '', 'admin', '2021-11-04 15:36:56');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816603d70181660dcb1f0002', ',root,402855816f31c4de016f31c4f6260024,40280981816603d70181660d53090000', NULL, 'admin', '2022-06-15 14:29:49', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '职级查询', 'button', NULL, 1, '40280981816603d70181660d53090000', 'system:rank:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981818463d0018184e1621f0004', ',root,402855816f31c4de016f31c4f6260024,40280981818463d0018184e0e4900001', NULL, 'admin', '2022-06-21 14:09:32', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '群组查询', 'button', NULL, 1, '40280981818463d0018184e0e4900001', 'system:group:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981818564d901818569bbcd0003', ',root,402855816f31c4de016f31c4f6260024,40280981818463d0018184e0e4900001,40280981818463d0018184e1621f0004', NULL, 'admin', '2022-06-21 16:38:28', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '群组用户', 'button', NULL, 1, '40280981818463d0018184e1621f0004', 'group:user:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f6ec0037', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027', NULL, 'system', '2019-12-23 15:59:49', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '用户查询', 'button', NULL, 1, '402855816f31c4de016f31c4f6470027', 'system:user:list', '', 'admin', '2021-11-04 15:36:58');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('828083817fd49301017fd497cb550016', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027,402855816f31c4de016f31c4f6ec0037', NULL, 'admin', '2022-03-29 15:33:12', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '部门查询', 'button', '', 1, '402855816f31c4de016f31c4f6ec0037', 'system:dept:list', NULL, 'admin', '2022-03-29 15:33:24');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f716003e', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6570028', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '角色查询', 'button', NULL, 1, '402855816f31c4de016f31c4f6570028', 'system:role:list', '', 'admin', '2019-12-25 15:47:37');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402809818170cb21018170d6280b0013', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6570028,402855816f31c4de016f31c4f716003e', NULL, 'admin', '2022-06-17 16:44:52', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '菜单查询', 'button', NULL, 1, '402855816f31c4de016f31c4f716003e', 'system:menu:list', NULL, 'admin', '2022-06-20 11:12:11');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7530047', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f668002a', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '部门查询', 'button', NULL, 1, '402855816f31c4de016f31c4f668002a', 'system:dept:list', '', 'admin', '2019-12-25 15:50:01');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f76c004b', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002b', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '岗位查询', 'button', NULL, 1, '402855816f31c4de016f31c4f67a002b', 'system:post:list', '', 'admin', '2019-12-25 15:48:30');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7cd005a', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '消息查询', 'button', NULL, 1, '402855816f31c4de016f31c4f69b002e', 'system:notice:list', '', 'admin', '2021-11-04 16:08:24');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098184a295fb0184a332bdea0013', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e,40280981818986d8018189fdd38d0003', NULL, 'admin', '2022-11-23 14:35:22', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '用户查询', 'button', NULL, 1, '40280981818986d8018189fdd38d0003', 'system:user:list', NULL, 'admin', '2022-11-23 14:35:22');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f6e20035', ',root,402855816f31c4de016f31c4f6350025', '/system/operlog/index', 'system', '2019-12-23 15:59:49', 'fa fa-laptop', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '操作日志', 'menu', 'operlog', 1, '402855816f31c4de016f31c4f6350025', 'system:operlog:view', '操作日志菜单', 'admin', '2022-10-13 14:22:12');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7ed005e', ',root,402855816f31c4de016f31c4f6350025,402855816f31c4de016f31c4f6e20035', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '操作查询', 'button', NULL, 1, '402855816f31c4de016f31c4f6e20035', 'system:operlog:list', '', 'admin', '2022-10-13 14:22:35');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f8060062', ',root,402855816f31c4de016f31c4f6350025,402855816f31c4de016f31c4f6e70036', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '登录查询', 'button', NULL, 1, '402855816f31c4de016f31c4f6e70036', 'system:logininfo:list', '', 'admin', '2022-10-13 14:23:02');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddab793017ddae21e650005', ',root,402885887ddab793017ddadf51910004', NULL, NULL, NULL, 'fa fa-calendar-plus-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '我的任务', 'catalog', 'flowable/task/myself', 1, '402885887ddab793017ddadf51910004', 'task:myself:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddab793017ddaea71460009', ',root,402885887ddab793017ddadf51910004,402885887ddab793017ddae21e650005', '/flowable/task/myself/unfinished', NULL, NULL, 'fa fa-calendar-minus-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '待办任务', 'menu', 'flowable/task/myself/unfinished', 1, '402885887ddab793017ddae21e650005', 'myself:unfinished:view', NULL, 'admin', '2022-03-11 09:55:09');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddbc0b1017ddbc6c7fb0001', ',root,402885887ddbc0b1017ddbc30f0b0000', '/demo/expense/index', NULL, NULL, 'fa fa-file-word-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '费用报销', 'menu', 'demo/expense', 1, '402885887ddbc0b1017ddbc30f0b0000', 'demo:expense:view', NULL, 'admin', '2022-03-11 10:03:09');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddfa962017ddfb5f2a10000', ',root,402885887ddbc0b1017ddbc30f0b0000,402885887ddbc0b1017ddbc6c7fb0001', '/demo/expense/view', 'admin', '2021-12-22 09:16:23', 'fa fa-file-word-o', 'N', 'Y', 'Y', 'N', 'N', 'N', NULL, '费用报销单', 'menu', 'flowable/demo/expense/view', 1, '402885887ddbc0b1017ddbc6c7fb0001', 'expense:add:view', NULL, 'admin', '2022-03-04 09:57:01');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('828083817f624881017f62ff07b700a1', ',root,402885887ddbc0b1017ddbc30f0b0000,828083817f624881017f62ff07b700a0', NULL, 'system', '2022-03-07 14:13:34', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '学生信息查询', 'button', NULL, 1, '828083817f624881017f62ff07b700a0', 'school:student:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f431659870003', ',root,402885887f43108e017f431438640000', '/devtool/metadata/index', 'admin', '2022-03-01 09:26:53', 'fa fa-meetup', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '元数据管理', 'menu', 'devtool/metadata', 1, '402885887f43108e017f431438640000', 'devtool:metadata:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f431828380007', ',root,402885887f43108e017f431438640000,402885887f43108e017f431659870003', NULL, 'admin', '2022-03-01 09:28:51', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '元数据查询', 'button', NULL, 1, '402885887f43108e017f431659870003', 'devtool:metadata:list', NULL, 'admin', '2022-03-01 15:34:12');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f431b94260011', ',root,402885887f43108e017f431438640000,402885887f43108e017f431ad0cf000f', NULL, 'admin', '2022-03-01 09:32:35', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '库表查询', 'button', NULL, 1, '402885887f43108e017f431ad0cf000f', 'devtool:gentable:list', NULL, 'admin', '2022-03-01 09:42:17');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f4328d1dd002f', ',root,402885887f43108e017f431438640000,402885887f43108e017f43285c86002d', NULL, 'admin', '2022-03-01 09:47:03', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '字段查询', 'button', NULL, 1, '402885887f43108e017f43285c86002d', 'devtool:gencolumn:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea2904bb0020', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/tagsView/index', 'admin', '2021-11-04 16:55:29', 'fa fa-tags', 'N', 'N', 'N', 'N', 'N', 'N', NULL, 'tagsView 操作', 'menu', 'tagsView', 1, '4028b26c7ce9c55e017cea27d324001e', 'fun:tagsView:view', NULL, 'admin', '2021-11-04 16:55:55');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402881977d4a52fe017d4b9fb13400a5', ',root,4028b26c7ce9c55e017cea27d324001e,402881977d4a52fe017d4b9e306200a3', '/params/common/index', 'admin', '2021-11-23 15:08:17', 'fa fa-lemon-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '普通路由', 'menu', 'params/common', 1, '402881977d4a52fe017d4b9e306200a3', 'params:common:index', NULL, 'admin', '2021-11-23 15:12:14');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea3717ce003e', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/filtering/index', 'admin', '2021-11-04 17:10:52', 'fa fa-filter', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '过滤筛选', 'menu', 'filtering', 1, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:filtering:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea3866680040', ',root,4028b26c7ce9c55e017cea35fcf7003c,4028b26c7ce9c55e017cea3717ce003e', '/pages/filtering/details', 'admin', '2021-11-04 17:12:17', 'fa fa-filter', 'N', 'N', 'Y', 'N', 'N', 'N', NULL, '过滤筛选详情', 'menu', 'filtering/details', 1, '4028b26c7ce9c55e017cea3717ce003e', 'pages:filtering:details', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea4b45260064', ',root,4028b26c7ce9c55e017cea35fcf7003c,4028b26c7ce9c55e017cea494a910062', '/visualizing/demo1', 'admin', '2021-11-04 17:32:54', 'fa fa-desktop', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '大屏演示1', 'menu', 'visualizing/demo1', 1, '4028b26c7ce9c55e017cea494a910062', 'pages:visualizing:demo1', NULL, 'admin', '2021-11-04 17:36:17');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ceeb8ce017ceec2c7150002', ',root,4028b26c7ceeb8ce017ceec0c0280000', '/flowable/modeler/index', 'admin', '2021-11-05 14:21:55', 'fa fa-puzzle-piece', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '绘制模板', 'menu', 'flowable/modeler', 1, '4028b26c7ceeb8ce017ceec0c0280000', 'flowable:modeler:view', NULL, 'admin', '2021-12-16 17:57:51');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028819f7dc132e8017dc1c91ca60010', ',root,4028b26c7ceeb8ce017ceec0c0280000,4028819f7d93e1cc017d940aaffd0001', NULL, 'admin', '2021-12-16 13:48:43', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '模板查询', 'button', NULL, 1, '4028819f7d93e1cc017d940aaffd0001', 'flowable:modeler:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('828083827dc25f84017dc26de1fe0005', ',root,4028b26c7ceeb8ce017ceec0c0280000,4028819f7d93e1cc017d940c98a40003', NULL, 'admin', '2021-12-16 16:48:41', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '流程查询', 'button', NULL, 1, '4028819f7d93e1cc017d940c98a40003', 'flowable:modeler:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017ce9dce8ca0000', ',root,82808381818f7fe901818f8abeb70002', '/personal/index', 'admin', '2021-11-04 15:32:21', 'fa fa-user-o', 'N', 'Y', 'Y', 'N', 'Y', 'N', NULL, '个人中心', 'menu', 'personal', 1, '82808381818f7fe901818f8abeb70002', 'system:personal:view', '', 'admin', '2022-06-23 15:50:53');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('8280840383c5fb4c0183c60efa320005', ',root,8280840383c5fb4c0183c60d0d660003', '/monitor/rediscache/index', 'admin', '2022-10-11 16:00:14', 'fa fa-server', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '缓存监控', 'menu', 'rediscache', 1, '8280840383c5fb4c0183c60d0d660003', 'monitor:rediscache:view', NULL, 'admin', '2022-10-13 14:26:30');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098183d408c80183d42dc7eb0004', ',root,8280840383c5fb4c0183c60d0d660003,4028098183d009810183d00f364d0004', '/monitor/service/dashboard', 'admin', '2022-10-14 09:48:34', 'fa fa-dashcube', 'N', 'N', 'Y', 'N', 'N', 'N', NULL, '仪表盘', 'menu', 'service/dashboard', 1, '4028098183d009810183d00f364d0004', 'monitor:service:dashboard', NULL, 'admin', '2022-10-14 09:49:37');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098183d408c80183d4304b930009', ',root,8280840383c5fb4c0183c60d0d660003,4028098183d009810183d00f364d0004,4028098183d408c80183d42dc7eb0004', NULL, 'admin', '2022-10-14 09:51:18', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '磁盘空间', 'button', NULL, 1, '4028098183d408c80183d42dc7eb0004', 'monitor:service:disk', NULL, 'admin', '2022-10-14 09:51:18');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('8280840383c5fb4c0183c6131b01000a', ',root,8280840383c5fb4c0183c60d0d660003,8280840383c5fb4c0183c60efa320005', NULL, 'admin', '2022-10-11 16:04:44', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '健康查询', 'button', NULL, 1, '8280840383c5fb4c0183c60efa320005', 'monitor:rediscache:health', NULL, 'admin', '2022-10-13 14:24:10');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f6260024', ',root', NULL, 'system', '2019-12-23 15:59:49', 'fa fa-gear', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '系统管理', 'catalog', 'system', 2, 'root', 'system:index:view', '', 'admin', '2021-11-04 15:47:23');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f67a002c', ',root,40280981816630d00181663c70e9001d', '/system/dictionary/index', 'system', '2019-12-23 15:59:49', 'fa fa-wikipedia-w', 'N', 'N', 'N', 'N', 'Y', 'N', NULL, '字典管理', 'menu', 'dictionary', 2, '40280981816630d00181663c70e9001d', 'system:dictionary:view', '', 'admin', '2022-06-15 15:21:18');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816603d70181661ba9490029', ',root,40280981816630d00181663c70e9001d,40280981816603d70181661ab6aa0023', NULL, 'admin', '2022-06-15 14:44:58', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '标签新增', 'button', NULL, 2, '40280981816603d70181661ab6aa0023', 'system:tag:add', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816630d00181663afe350017', ',root,40280981816630d00181663c70e9001d,40280981816630d00181663996e50010', NULL, 'admin', '2022-06-15 15:19:11', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '区划新增', 'button', NULL, 2, '40280981816630d00181663996e50010', 'system:area:add', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981834455dc0183446505e90011', ',root,40280981816630d00181663c70e9001d,40280981834455dc0183446505e9000f', NULL, 'system', '2022-09-16 11:45:52', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '词语新增', 'button', NULL, 2, '40280981834455dc0183446505e9000f', 'sensitive:words:add', NULL, 'admin', '2022-09-19 17:02:37');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402809818518627301851894f09f0007', ',root,40280981816630d00181663c70e9001d,4028098185186273018518931fbb0000', NULL, 'admin', '2022-12-16 09:38:12', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '对象上传', 'button', NULL, 2, '4028098185186273018518931fbb0000', 'storage:object:upload', NULL, 'admin', '2022-12-16 09:39:10');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7340044', ',root,40280981816630d00181663c70e9001d,402855816f31c4de016f31c4f6580029', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '菜单新增', 'button', NULL, 2, '402855816f31c4de016f31c4f6580029', 'system:menu:add', '', 'admin', '2019-12-25 15:48:06');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7980051', ',root,40280981816630d00181663c70e9001d,402855816f31c4de016f31c4f67a002c', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '字典新增', 'button', NULL, 2, '402855816f31c4de016f31c4f67a002c', 'system:dictionary:add', '', 'admin', '2021-10-13 15:25:22');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('8280840183bb863a0183bb8ecb040007', ',root,40280981816630d00181663c70e9001d,8280840183bb863a0183bb8c23670001', NULL, 'admin', '2022-10-09 15:04:01', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '数值查询', 'button', NULL, 2, '8280840183bb863a0183bb8c23670001', 'system:cache:values', NULL, 'admin', '2022-10-13 14:29:18');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f668002a', ',root,402855816f31c4de016f31c4f6260024', '/system/dept/index', 'system', '2019-12-23 15:59:49', 'fa fa-address-book-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '部门管理', 'menu', 'dept', 2, '402855816f31c4de016f31c4f6260024', 'system:dept:view', '', 'admin', '2022-03-23 09:52:34');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816603d70181660e17880004', ',root,402855816f31c4de016f31c4f6260024,40280981816603d70181660d53090000', NULL, 'admin', '2022-06-15 14:30:09', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '职级新增', 'button', NULL, 2, '40280981816603d70181660d53090000', 'system:rank:add', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981818463d0018184e19cb30006', ',root,402855816f31c4de016f31c4f6260024,40280981818463d0018184e0e4900001', NULL, 'admin', '2022-06-21 14:09:47', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '群组新增', 'button', NULL, 2, '40280981818463d0018184e0e4900001', 'system:group:add', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f6f30038', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027', NULL, 'system', '2019-12-23 15:59:49', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '用户新增', 'button', NULL, 2, '402855816f31c4de016f31c4f6470027', 'system:user:add', '', 'admin', '2021-11-04 15:37:00');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('828083817fd49301017fd4986f730019', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027,402855816f31c4de016f31c4f6ec0037', NULL, 'admin', '2022-03-29 15:33:54', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '角色查询', 'button', NULL, 2, '402855816f31c4de016f31c4f6ec0037', 'system:role:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f71d003f', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6570028', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '角色新增', 'button', NULL, 2, '402855816f31c4de016f31c4f6570028', 'system:role:add', '', 'admin', '2019-12-25 15:49:21');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402809818170e2d0018170e5749c0018', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6570028,402855816f31c4de016f31c4f716003e', NULL, 'admin', '2022-06-17 17:01:35', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '部门查询', 'button', NULL, 2, '402855816f31c4de016f31c4f716003e', 'system:dept:list', NULL, 'admin', '2022-06-20 11:12:21');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7590048', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f668002a', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '部门新增', 'button', NULL, 2, '402855816f31c4de016f31c4f668002a', 'system:dept:add', '', 'admin', '2019-12-25 15:50:07');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f773004c', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002b', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '岗位新增', 'button', NULL, 2, '402855816f31c4de016f31c4f67a002b', 'system:post:add', '', 'admin', '2019-12-25 15:48:37');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7cd005b', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '消息新增', 'button', NULL, 2, '402855816f31c4de016f31c4f69b002e', 'system:notice:add', '', 'admin', '2021-11-04 16:08:31');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098184a295fb0184a3330f240015', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e,40280981818986d8018189fdd38d0003', NULL, 'admin', '2022-11-23 14:35:43', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '部门查询', 'button', NULL, 2, '40280981818986d8018189fdd38d0003', 'system:dept:list', NULL, 'admin', '2022-11-23 14:35:43');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f6e70036', ',root,402855816f31c4de016f31c4f6350025', '/system/logininfo/index', 'system', '2019-12-23 15:59:49', 'fa fa-sign-in', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '登录日志', 'menu', 'logininfo', 2, '402855816f31c4de016f31c4f6350025', 'system:logininfo:view', '登录日志菜单', 'admin', '2022-10-13 14:22:27');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7f2005f', ',root,402855816f31c4de016f31c4f6350025,402855816f31c4de016f31c4f6e20035', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '操作删除', 'button', NULL, 2, '402855816f31c4de016f31c4f6e20035', 'system:operlog:remove', '', 'admin', '2022-10-13 14:22:40');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f80b0063', ',root,402855816f31c4de016f31c4f6350025,402855816f31c4de016f31c4f6e70036', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '登录删除', 'button', NULL, 2, '402855816f31c4de016f31c4f6e70036', 'system:logininfo:remove', '', 'admin', '2022-10-13 14:23:08');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddab793017ddae4c8b00006', ',root,402885887ddab793017ddadf51910004', '/flowable/task/unfinished', NULL, NULL, 'fa fa-calendar-minus-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '待办任务', 'menu', 'flowable/task/unfinished', 2, '402885887ddab793017ddadf51910004', 'task:unfinished:view', NULL, 'admin', '2022-03-11 09:57:11');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddab793017ddaebea47000a', ',root,402885887ddab793017ddadf51910004,402885887ddab793017ddae21e650005', '/flowable/task/myself/finished', NULL, NULL, 'fa fa-calendar-check-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '已办任务', 'menu', 'flowable/task/myself/finished', 2, '402885887ddab793017ddae21e650005', 'myself:finished:view', NULL, 'admin', '2022-03-11 09:56:45');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('828083817f624881017f62ff07b700a0', ',root,402885887ddbc0b1017ddbc30f0b0000', '/school/student/index', 'system', '2022-03-07 14:13:34', 'fa fa-folder-open', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '学生信息管理', 'menu', 'school/student', 2, '402885887ddbc0b1017ddbc30f0b0000', 'school:student:view', NULL, 'admin', '2022-03-11 10:00:48');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddbc0b1017ddbc8c74f0002', ',root,402885887ddbc0b1017ddbc30f0b0000,402885887ddbc0b1017ddbc6c7fb0001', NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '报销查询', 'button', NULL, 2, '402885887ddbc0b1017ddbc6c7fb0001', 'demo:expense:list', NULL, 'admin', '2022-03-04 09:57:06');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('828083817f624881017f62ff07b700a2', ',root,402885887ddbc0b1017ddbc30f0b0000,828083817f624881017f62ff07b700a0', NULL, 'system', '2022-03-07 14:13:34', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '学生信息新增', 'button', NULL, 2, '828083817f624881017f62ff07b700a0', 'school:student:add', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f431ad0cf000f', ',root,402885887f43108e017f431438640000', '/devtool/gentable/index', 'admin', '2022-03-01 09:31:45', 'fa fa-database', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '库表管理', 'menu', 'devtool/gentable', 2, '402885887f43108e017f431438640000', 'devtool:gentable:view', NULL, 'admin', '2022-03-01 09:42:11');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f431875860009', ',root,402885887f43108e017f431438640000,402885887f43108e017f431659870003', NULL, 'admin', '2022-03-01 09:29:11', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '元数据新增', 'button', NULL, 2, '402885887f43108e017f431659870003', 'devtool:metadata:add', NULL, 'admin', '2022-03-01 15:34:20');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f431c25e70013', ',root,402885887f43108e017f431438640000,402885887f43108e017f431ad0cf000f', NULL, 'admin', '2022-03-01 09:33:13', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '库表新增', 'button', NULL, 2, '402885887f43108e017f431ad0cf000f', 'devtool:gentable:add', NULL, 'admin', '2022-03-01 09:42:22');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f4329264b0031', ',root,402885887f43108e017f431438640000,402885887f43108e017f43285c86002d', NULL, 'admin', '2022-03-01 09:47:25', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '字段新增', 'button', NULL, 2, '402885887f43108e017f43285c86002d', 'devtool:gencolumn:add', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea2a365d0023', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/countup/index', 'admin', '2021-11-04 16:56:47', 'fa fa-sort-numeric-asc', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '数字滚动', 'menu', 'countup', 2, '4028b26c7ce9c55e017cea27d324001e', 'fun:countup:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402881977d4a52fe017d4ba1334300a8', ',root,4028b26c7ce9c55e017cea27d324001e,402881977d4a52fe017d4b9e306200a3', '/params/common/details', 'admin', '2021-11-23 15:09:55', 'fa fa-lemon-o', 'N', 'N', 'Y', 'N', 'N', 'N', NULL, '普通路由详情', 'menu', 'params/common/details', 2, '402881977d4a52fe017d4b9e306200a3', 'params:common:details', NULL, 'admin', '2021-11-23 15:12:19');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea4bee300066', ',root,4028b26c7ce9c55e017cea35fcf7003c,4028b26c7ce9c55e017cea494a910062', '/visualizing/demo2', 'admin', '2021-11-04 17:33:37', 'fa fa-desktop', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '大屏演示2', 'menu', 'visualizing/demo2', 2, '4028b26c7ce9c55e017cea494a910062', 'pages:visualizing:demo2', NULL, 'admin', '2021-11-04 17:36:25');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028819f7d93e1cc017d940aaffd0001', ',root,4028b26c7ceeb8ce017ceec0c0280000', '/flowable/modeler/undeploy', 'admin', '2021-12-07 16:37:48', 'fa fa-unlock', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '部署模板', 'menu', 'flowable/modeler/undeploy', 2, '4028b26c7ceeb8ce017ceec0c0280000', 'flowable:undeploy:view', NULL, 'admin', '2021-12-16 17:55:03');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028819f7dc132e8017dc1c98ef70012', ',root,4028b26c7ceeb8ce017ceec0c0280000,4028819f7d93e1cc017d940aaffd0001', NULL, 'admin', '2021-12-16 13:49:12', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '模板部署', 'button', NULL, 2, '4028819f7d93e1cc017d940aaffd0001', 'flowable:modeler:deploy', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('828083827dc25f84017dc26e692d0007', ',root,4028b26c7ceeb8ce017ceec0c0280000,4028819f7d93e1cc017d940c98a40003', NULL, 'admin', '2021-12-16 16:49:16', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '撤销部署', 'button', NULL, 2, '4028819f7d93e1cc017d940c98a40003', 'flowable:modeler:undeploy', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('82808381818f7fe901818f8978b50000', ',root,82808381818f7fe901818f8abeb70002', '/personal/notice/index', 'admin', '2022-06-23 15:49:20', 'fa fa-bell-o', 'N', 'Y', 'Y', 'N', 'N', 'N', NULL, '通知中心', 'menu', 'personal/notice', 2, '82808381818f7fe901818f8abeb70002', 'personal:notice:view', NULL, 'admin', '2022-06-23 15:51:01');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098183d009810183d00f364d0004', ',root,8280840383c5fb4c0183c60d0d660003', '/monitor/service/index', 'admin', '2022-10-13 14:36:41', 'fa fa-desktop', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '服务监控', 'menu', 'service', 2, '8280840383c5fb4c0183c60d0d660003', 'monitor:service:view', NULL, 'admin', '2022-10-13 14:36:41');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098183d009810183d012bd360006', ',root,8280840383c5fb4c0183c60d0d660003,4028098183d009810183d00f364d0004', NULL, 'admin', '2022-10-13 14:40:33', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '服务查询', 'button', NULL, 2, '4028098183d009810183d00f364d0004', 'monitor:service:list', NULL, 'admin', '2022-10-14 09:47:15');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098183d408c80183d49c30010016', ',root,8280840383c5fb4c0183c60d0d660003,4028098183d009810183d00f364d0004,4028098183d408c80183d42dc7eb0004', NULL, 'admin', '2022-10-14 11:49:09', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, 'CPU信息', 'button', NULL, 2, '4028098183d408c80183d42dc7eb0004', 'monitor:service:cpu', NULL, 'admin', '2022-10-20 17:28:50');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816630d00181663c70e9001d', ',root', NULL, 'admin', '2022-06-15 15:20:46', 'fa fa-database', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '数据管理', 'catalog', 'database', 3, 'root', 'system:database:view', NULL, 'admin', '2022-06-15 15:23:19');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816603d70181661ab6aa0023', ',root,40280981816630d00181663c70e9001d', '/system/tag/index', 'admin', '2022-06-15 14:43:56', 'fa fa-tags', 'N', 'N', 'N', 'N', 'Y', 'N', NULL, '标签管理', 'menu', 'tag', 3, '40280981816630d00181663c70e9001d', 'system:tag:view', NULL, 'admin', '2022-06-15 15:21:27');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816603d70181661bfdec002b', ',root,40280981816630d00181663c70e9001d,40280981816603d70181661ab6aa0023', NULL, 'admin', '2022-06-15 14:45:19', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '标签修改', 'button', NULL, 3, '40280981816603d70181661ab6aa0023', 'system:tag:edit', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816630d00181663b39d30019', ',root,40280981816630d00181663c70e9001d,40280981816630d00181663996e50010', NULL, 'admin', '2022-06-15 15:19:26', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '区划修改', 'button', NULL, 3, '40280981816630d00181663996e50010', 'system:area:edit', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981834455dc0183446505e90012', ',root,40280981816630d00181663c70e9001d,40280981834455dc0183446505e9000f', NULL, 'system', '2022-09-16 11:45:52', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '词语修改', 'button', NULL, 3, '40280981834455dc0183446505e9000f', 'sensitive:words:edit', NULL, 'admin', '2022-09-19 17:02:43');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402809818518627301851895eead000a', ',root,40280981816630d00181663c70e9001d,4028098185186273018518931fbb0000', NULL, 'admin', '2022-12-16 09:39:17', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '对象下载', 'button', NULL, 3, '4028098185186273018518931fbb0000', 'storage:object:download', NULL, 'admin', '2022-12-16 09:39:17');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7450045', ',root,40280981816630d00181663c70e9001d,402855816f31c4de016f31c4f6580029', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '菜单修改', 'button', NULL, 3, '402855816f31c4de016f31c4f6580029', 'system:menu:edit', '', 'admin', '2019-12-25 15:47:48');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f79c0052', ',root,40280981816630d00181663c70e9001d,402855816f31c4de016f31c4f67a002c', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '字典修改', 'button', NULL, 3, '402855816f31c4de016f31c4f67a002c', 'system:dictionary:edit', '', 'admin', '2021-10-13 15:25:27');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981831fc17201831ff94b89000c', ',root,40280981831a8f1f01831bec26830029', NULL, 'admin', '2022-09-09 09:59:35', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '设置查询', 'button', NULL, 3, '40280981831a8f1f01831bec26830029', 'system:config:info', NULL, 'admin', '2022-09-29 11:48:01');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f6570028', ',root,402855816f31c4de016f31c4f6260024', '/system/role/index', 'system', '2019-12-23 15:59:49', 'fa fa-user-circle', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '角色管理', 'menu', 'role', 3, '402855816f31c4de016f31c4f6260024', 'system:role:view', '', 'admin', '2022-03-23 09:52:40');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816603d70181660e52930006', ',root,402855816f31c4de016f31c4f6260024,40280981816603d70181660d53090000', NULL, 'admin', '2022-06-15 14:30:24', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '职级修改', 'button', NULL, 3, '40280981816603d70181660d53090000', 'system:rank:edit', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981818463d0018184e1eaa10008', ',root,402855816f31c4de016f31c4f6260024,40280981818463d0018184e0e4900001', NULL, 'admin', '2022-06-21 14:10:07', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '群组修改', 'button', NULL, 3, '40280981818463d0018184e0e4900001', 'system:group:edit', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f6f90039', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027', NULL, 'system', '2019-12-23 15:59:49', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '用户修改', 'button', NULL, 3, '402855816f31c4de016f31c4f6470027', 'system:user:edit', '', 'admin', '2021-11-04 15:37:02');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('828083817fd49301017fd498b1fe001b', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027,402855816f31c4de016f31c4f6ec0037', NULL, 'admin', '2022-03-29 15:34:11', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '岗位查询', 'button', NULL, 3, '402855816f31c4de016f31c4f6ec0037', 'system:post:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7240040', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6570028', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '角色修改', 'button', NULL, 3, '402855816f31c4de016f31c4f6570028', 'system:role:edit', '', 'admin', '2019-12-25 15:49:26');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f75f0049', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f668002a', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '部门修改', 'button', NULL, 3, '402855816f31c4de016f31c4f668002a', 'system:dept:edit', '', 'admin', '2019-12-25 15:50:12');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f775004d', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002b', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '岗位修改', 'button', NULL, 3, '402855816f31c4de016f31c4f67a002b', 'system:post:edit', '', 'admin', '2019-12-25 15:48:48');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7e2005c', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '消息修改', 'button', NULL, 3, '402855816f31c4de016f31c4f69b002e', 'system:notice:edit', '', 'admin', '2021-11-04 16:08:37');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098184a295fb0184a3334c160017', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e,40280981818986d8018189fdd38d0003', NULL, 'admin', '2022-11-23 14:35:59', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '角色查询', 'button', NULL, 3, '40280981818986d8018189fdd38d0003', 'system:role:list', NULL, 'admin', '2022-11-23 14:35:59');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea0c40850016', ',root,402855816f31c4de016f31c4f6350025,402855816f31c4de016f31c4f6e20035', NULL, 'admin', '2021-11-04 16:24:04', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '操作清空', 'button', NULL, 3, '402855816f31c4de016f31c4f6e20035', 'system:operlog:clean', NULL, 'admin', '2022-10-13 14:22:46');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea0e77840019', ',root,402855816f31c4de016f31c4f6350025,402855816f31c4de016f31c4f6e70036', NULL, 'admin', '2021-11-04 16:26:29', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '登录清空', 'button', NULL, 3, '402855816f31c4de016f31c4f6e70036', 'system:logininfo:clean', NULL, 'admin', '2022-10-13 14:23:14');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddab793017ddae7e48d0007', ',root,402885887ddab793017ddadf51910004', '/flowable/task/finished', NULL, NULL, 'fa fa-calendar-check-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '经办任务', 'menu', 'flowable/task/finished', 3, '402885887ddab793017ddadf51910004', 'task:finished:view', NULL, 'admin', '2022-03-11 09:57:32');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddab793017ddaecc2a4000b', ',root,402885887ddab793017ddadf51910004,402885887ddab793017ddae21e650005', '/flowable/task/myself/completed', NULL, NULL, 'fa fa-calendar-times-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '办结任务', 'menu', 'flowable/task/myself/completed', 3, '402885887ddab793017ddae21e650005', 'myself:completed:view', NULL, 'admin', '2022-03-11 09:56:53');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddbc0b1017ddbc91dd70003', ',root,402885887ddbc0b1017ddbc30f0b0000,402885887ddbc0b1017ddbc6c7fb0001', NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '报销新增', 'button', NULL, 3, '402885887ddbc0b1017ddbc6c7fb0001', 'demo:expense:add', NULL, 'admin', '2022-03-04 09:57:10');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('828083817f624881017f62ff07b700a3', ',root,402885887ddbc0b1017ddbc30f0b0000,828083817f624881017f62ff07b700a0', NULL, 'system', '2022-03-07 14:13:34', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '学生信息修改', 'button', NULL, 3, '828083817f624881017f62ff07b700a0', 'school:student:edit', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f43285c86002d', ',root,402885887f43108e017f431438640000', 'devtool/gencolumn/index', 'admin', '2022-03-01 09:46:33', 'fa fa-hashtag', 'N', 'N', 'Y', 'N', 'N', 'N', NULL, '库表字段管理', 'menu', 'devtool/gencolumn', 3, '402885887f43108e017f431438640000', 'devtool:gencolumn:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f4318bced000b', ',root,402885887f43108e017f431438640000,402885887f43108e017f431659870003', NULL, 'admin', '2022-03-01 09:29:29', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '元数据修改', 'button', NULL, 3, '402885887f43108e017f431659870003', 'devtool:metadata:edit', NULL, 'admin', '2022-03-01 15:34:27');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f431c74680015', ',root,402885887f43108e017f431438640000,402885887f43108e017f431ad0cf000f', NULL, 'admin', '2022-03-01 09:33:33', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '库表修改', 'button', NULL, 3, '402885887f43108e017f431ad0cf000f', 'devtool:gentable:edit', NULL, 'admin', '2022-03-01 09:42:27');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f432963d00033', ',root,402885887f43108e017f431438640000,402885887f43108e017f43285c86002d', NULL, 'admin', '2022-03-01 09:47:40', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '字段修改', 'button', NULL, 3, '402885887f43108e017f43285c86002d', 'devtool:gencolumn:edit', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea2bcc810025', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/selector/index', 'admin', '2021-11-04 16:58:31', 'fa fa-flickr', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '图标选择器', 'menu', 'iconselector', 3, '4028b26c7ce9c55e017cea27d324001e', 'fun:iconselector:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402881977d4a52fe017d4ba2192200ab', ',root,4028b26c7ce9c55e017cea27d324001e,402881977d4a52fe017d4b9e306200a3', '/params/dynamic/index', 'admin', '2021-11-23 15:10:54', 'fa fa-lemon-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '动态路由', 'menu', 'params/dynamic', 3, '402881977d4a52fe017d4b9e306200a3', 'params:dynamic:index', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028819f7d93e1cc017d940c98a40003', ',root,4028b26c7ceeb8ce017ceec0c0280000', '/flowable/modeler/deploy', 'admin', '2021-12-07 16:39:53', 'fa fa-lock', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '流程模板', 'menu', 'flowable/modeler/deploy', 3, '4028b26c7ceeb8ce017ceec0c0280000', 'flowable:deploy:view', NULL, 'admin', '2021-12-16 17:55:36');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098183d583f10183d5c66510000c', ',root,8280840383c5fb4c0183c60d0d660003,4028098183d009810183d00f364d0004,4028098183d408c80183d42dc7eb0004', NULL, 'admin', '2022-10-14 17:14:53', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '内存空间', 'button', NULL, 3, '4028098183d408c80183d42dc7eb0004', 'monitor:service:memory', NULL, 'admin', '2022-10-20 17:28:40');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f6350025', ',root', NULL, 'system', '2019-12-23 15:59:49', 'fa fa-video-camera', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '审计管理', 'catalog', 'audit', 4, 'root', 'system:audit:view', '系统监控目录', 'admin', '2022-10-11 15:56:01');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816630d00181663996e50010', ',root,40280981816630d00181663c70e9001d', '/system/area/index', 'admin', '2022-06-15 15:17:39', 'fa fa-map-marker', 'N', 'N', 'N', 'N', 'Y', 'N', NULL, '区划管理', 'menu', 'area', 4, '40280981816630d00181663c70e9001d', 'system:area:view', '中华人民共和国行政区划范围为全国31个省（自治区、直辖市），不包括我国台湾省、香港特别行政区和澳门特别行政区。', 'admin', '2022-06-15 15:25:06');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816603d70181661c6f6c002d', ',root,40280981816630d00181663c70e9001d,40280981816603d70181661ab6aa0023', NULL, 'admin', '2022-06-15 14:45:49', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '标签删除', 'button', NULL, 4, '40280981816603d70181661ab6aa0023', 'system:tag:remove', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816630d00181663b7397001b', ',root,40280981816630d00181663c70e9001d,40280981816630d00181663996e50010', NULL, 'admin', '2022-06-15 15:19:41', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '区划删除', 'button', NULL, 4, '40280981816630d00181663996e50010', 'system:area:remove', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981834455dc0183446505e90013', ',root,40280981816630d00181663c70e9001d,40280981834455dc0183446505e9000f', NULL, 'system', '2022-09-16 11:45:52', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '词语删除', 'button', NULL, 4, '40280981834455dc0183446505e9000f', 'sensitive:words:remove', NULL, 'admin', '2022-09-19 17:02:48');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098185186273018518963418000c', ',root,40280981816630d00181663c70e9001d,4028098185186273018518931fbb0000', NULL, 'admin', '2022-12-16 09:39:35', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '对象删除', 'button', NULL, 4, '4028098185186273018518931fbb0000', 'storage:object:remove', NULL, 'admin', '2022-12-16 09:39:35');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f74b0046', ',root,40280981816630d00181663c70e9001d,402855816f31c4de016f31c4f6580029', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '菜单删除', 'button', NULL, 4, '402855816f31c4de016f31c4f6580029', 'system:menu:remove', '', 'admin', '2019-12-25 15:48:19');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f79c0053', ',root,40280981816630d00181663c70e9001d,402855816f31c4de016f31c4f67a002c', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '字典删除', 'button', NULL, 4, '402855816f31c4de016f31c4f67a002c', 'system:dictionary:remove', '', 'admin', '2021-10-13 15:25:32');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981831fc17201831ffa2a940010', ',root,40280981831a8f1f01831bec26830029', NULL, 'admin', '2022-09-09 10:00:32', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '设置修改', 'button', NULL, 4, '40280981831a8f1f01831bec26830029', 'system:config:edit', NULL, 'admin', '2022-09-29 11:48:07');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f67a002b', ',root,402855816f31c4de016f31c4f6260024', '/system/post/index', 'system', '2019-12-23 15:59:49', 'fa fa-user-circle-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '岗位管理', 'menu', 'post', 4, '402855816f31c4de016f31c4f6260024', 'system:post:view', '', 'admin', '2019-12-25 15:56:23');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816603d70181660eae780008', ',root,402855816f31c4de016f31c4f6260024,40280981816603d70181660d53090000', NULL, 'admin', '2022-06-15 14:30:47', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '职级删除', 'button', NULL, 4, '40280981816603d70181660d53090000', 'system:rank:remove', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981818463d0018184e23841000a', ',root,402855816f31c4de016f31c4f6260024,40280981818463d0018184e0e4900001', NULL, 'admin', '2022-06-21 14:10:27', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '群组删除', 'button', NULL, 4, '40280981818463d0018184e0e4900001', 'system:group:remove', NULL, 'admin', '2022-06-21 14:10:37');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f6fe003a', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '用户删除', 'button', NULL, 4, '402855816f31c4de016f31c4f6470027', 'system:user:remove', '', 'admin', '2021-11-04 15:37:12');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981817037a3018170b6c4c1003a', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027,402855816f31c4de016f31c4f6ec0037', NULL, 'admin', '2022-06-17 16:10:35', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '职级查询', 'button', NULL, 4, '402855816f31c4de016f31c4f6ec0037', 'system:rank:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7290041', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6570028', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '角色删除', 'button', NULL, 4, '402855816f31c4de016f31c4f6570028', 'system:role:remove', '', 'admin', '2019-12-25 15:49:32');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f765004a', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f668002a', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '部门删除', 'button', NULL, 4, '402855816f31c4de016f31c4f668002a', 'system:dept:remove', '', 'admin', '2019-12-25 15:49:04');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f775004e', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002b', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '岗位删除', 'button', NULL, 4, '402855816f31c4de016f31c4f67a002b', 'system:post:remove', '', 'admin', '2019-12-25 15:48:58');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7e7005d', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '消息删除', 'button', NULL, 4, '402855816f31c4de016f31c4f69b002e', 'system:notice:remove', '', 'admin', '2021-11-04 16:08:41');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098184a295fb0184a33391220019', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e,40280981818986d8018189fdd38d0003', NULL, 'admin', '2022-11-23 14:36:17', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '岗位查询', 'button', NULL, 4, '40280981818986d8018189fdd38d0003', 'system:post:list', NULL, 'admin', '2022-11-23 14:36:17');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f7f80060', ',root,402855816f31c4de016f31c4f6350025,402855816f31c4de016f31c4f6e20035', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '详细信息', 'button', NULL, 4, '402855816f31c4de016f31c4f6e20035', 'system:operlog:detail', '', 'admin', '2022-10-13 14:22:52');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddab793017ddae948be0008', ',root,402885887ddab793017ddadf51910004', '/flowable/task/completed', NULL, NULL, 'fa fa-calendar-times-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '办结任务', 'menu', 'flowable/task/completed', 4, '402885887ddab793017ddadf51910004', 'task:completed:view', NULL, 'admin', '2022-03-11 09:57:45');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddbc0b1017ddbc96e270004', ',root,402885887ddbc0b1017ddbc30f0b0000,402885887ddbc0b1017ddbc6c7fb0001', NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '报销修改', 'button', NULL, 4, '402885887ddbc0b1017ddbc6c7fb0001', 'demo:expense:edit', NULL, 'admin', '2022-03-04 09:57:14');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('828083817f624881017f62ff07b700a4', ',root,402885887ddbc0b1017ddbc30f0b0000,828083817f624881017f62ff07b700a0', NULL, 'system', '2022-03-07 14:13:34', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '学生信息删除', 'button', NULL, 4, '828083817f624881017f62ff07b700a0', 'school:student:remove', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f43192f9b000d', ',root,402885887f43108e017f431438640000,402885887f43108e017f431659870003', NULL, 'admin', '2022-03-01 09:29:58', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '元数据删除', 'button', NULL, 4, '402885887f43108e017f431659870003', 'devtool:metadata:remove', NULL, 'admin', '2022-03-01 15:34:35');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f431cfab10017', ',root,402885887f43108e017f431438640000,402885887f43108e017f431ad0cf000f', NULL, 'admin', '2022-03-01 09:34:07', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '库表删除', 'button', NULL, 4, '402885887f43108e017f431ad0cf000f', 'devtool:gentable:remove', NULL, 'admin', '2022-03-01 09:42:32');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f4329a09e0035', ',root,402885887f43108e017f431438640000,402885887f43108e017f43285c86002d', NULL, 'admin', '2022-03-01 09:47:56', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '字段删除', 'button', NULL, 4, '402885887f43108e017f43285c86002d', 'devtool:gencolumn:remove', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea2c6c810027', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/noticeBar/index', 'admin', '2021-11-04 16:59:12', 'fa fa-bell-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '滚动消息', 'menu', 'noticeBar', 4, '4028b26c7ce9c55e017cea27d324001e', 'fun:noticeBar:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402881977d4a52fe017d4ba308cc00ad', ',root,4028b26c7ce9c55e017cea27d324001e,402881977d4a52fe017d4b9e306200a3', '/params/dynamic/details', 'admin', '2021-11-23 15:11:56', 'fa fa-lemon-o', 'N', 'N', 'Y', 'N', 'N', 'N', NULL, '动态路由详情', 'menu', 'params/dynamic/details', 4, '402881977d4a52fe017d4b9e306200a3', 'params:dynamic:details', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098183ef425c0183ef6fa7d70000', ',root,8280840383c5fb4c0183c60d0d660003,4028098183d009810183d00f364d0004,4028098183d408c80183d42dc7eb0004', NULL, 'admin', '2022-10-19 16:50:16', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '应用信息', 'button', NULL, 4, '4028098183d408c80183d42dc7eb0004', 'monitor:service:appinfo', NULL, 'admin', '2022-10-19 16:50:16');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981831a8f1f01831bec26830029', ',root', '', 'admin', '2022-09-08 15:06:45', 'fa fa-wrench', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '系统设置', 'catalog', 'config', 5, 'root', 'system:config:view', NULL, 'admin', '2022-09-29 11:44:40');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('8280840183bb863a0183bb8c23670001', ',root,40280981816630d00181663c70e9001d', '/system/cache/index', 'admin', '2022-10-09 15:01:07', 'fa fa-server', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '缓存管理', 'menu', 'cache', 5, '40280981816630d00181663c70e9001d', 'system:cache:view', NULL, 'admin', '2022-10-13 14:29:07');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981816603d70181660d53090000', ',root,402855816f31c4de016f31c4f6260024', '/system/rank/index', 'admin', '2022-06-15 14:29:18', 'fa fa-user-secret', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '职级管理', 'menu', 'rank', 5, '402855816f31c4de016f31c4f6260024', 'system:rank:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981818589cd0181858b4fe00000', ',root,402855816f31c4de016f31c4f6260024,40280981818463d0018184e0e4900001', NULL, 'admin', '2022-06-21 17:15:09', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '用户添加', 'button', NULL, 5, '40280981818463d0018184e0e4900001', 'group:user:add', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f710003d', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027', NULL, 'system', '2019-12-23 15:59:50', '', NULL, 'N', NULL, NULL, NULL, NULL, NULL, '重置密码', 'button', NULL, 5, '402855816f31c4de016f31c4f6470027', 'system:user:resetPwd', '', 'admin', '2021-11-04 15:37:29');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981818463d0018184e2f398000d', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027,402855816f31c4de016f31c4f6ec0037', NULL, 'admin', '2022-06-21 14:11:15', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '群组查询', 'button', NULL, 5, '402855816f31c4de016f31c4f6ec0037', 'system:group:list', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981818986d8018189fdd38d0003', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e', NULL, 'admin', '2022-06-22 13:58:42', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '消息发布', 'button', NULL, 5, '402855816f31c4de016f31c4f69b002e', 'system:notice:pub', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098184a295fb0184a333d15a001b', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e,40280981818986d8018189fdd38d0003', NULL, 'admin', '2022-11-23 14:36:33', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '职级查询', 'button', NULL, 5, '40280981818986d8018189fdd38d0003', 'system:rank:list', NULL, 'admin', '2022-11-23 14:36:33');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddbc0b1017ddbc9c6310005', ',root,402885887ddbc0b1017ddbc30f0b0000,402885887ddbc0b1017ddbc6c7fb0001', NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '报销删除', 'button', NULL, 5, '402885887ddbc0b1017ddbc6c7fb0001', 'demo:expense:remove', NULL, 'admin', '2022-03-04 09:57:19');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f431fa24c0019', ',root,402885887f43108e017f431438640000,402885887f43108e017f431ad0cf000f', NULL, 'admin', '2022-03-01 09:37:01', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '预览代码', 'button', NULL, 5, '402885887f43108e017f431ad0cf000f', 'devtool:gencode:preview', NULL, 'admin', '2022-03-01 09:39:20');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea2d9ac2002b', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/cropper/index', 'admin', '2021-11-04 17:00:30', 'fa fa-crop', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '图片剪裁', 'menu', 'cropper', 5, '4028b26c7ce9c55e017cea27d324001e', 'fun:cropper:view', NULL, 'admin', '2021-11-04 17:00:34');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea3bb4760042', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/cityLinkage/index', 'admin', '2021-11-04 17:15:54', 'fa fa-columns', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '城市级联', 'menu', 'cityLinkage', 5, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:cityLinkage:view', NULL, 'admin', '2021-11-04 17:16:15');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098183f357740183f44e57dc0002', ',root,8280840383c5fb4c0183c60d0d660003,4028098183d009810183d00f364d0004,4028098183d408c80183d42dc7eb0004', NULL, 'admin', '2022-10-20 15:31:59', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '进程信息', 'button', NULL, 5, '4028098183d408c80183d42dc7eb0004', 'monitor:service:thread', NULL, 'admin', '2022-10-20 15:31:59');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('8280840383c5fb4c0183c60d0d660003', ',root', NULL, 'admin', '2022-10-11 15:58:08', 'fa fa-line-chart', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '监控管理', 'catalog', 'monitor', 6, 'root', 'system:monitor:view', NULL, 'admin', '2022-10-11 15:58:08');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981834455dc0183446505e9000f', ',root,40280981816630d00181663c70e9001d', '/system/words/index', 'system', '2022-09-16 11:45:52', 'fa fa-wikipedia-w', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '词语管理', 'menu', 'words', 6, '40280981816630d00181663c70e9001d', 'sensitive:words:view', NULL, 'admin', '2022-12-20 15:12:23');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981818463d0018184e0e4900001', ',root,402855816f31c4de016f31c4f6260024', '/system/group/index', 'admin', '2022-06-21 14:09:00', 'fa fa-user-md', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '群组管理', 'menu', 'group', 6, '402855816f31c4de016f31c4f6260024', 'system:group:view', NULL, 'admin', '2022-06-21 14:09:06');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981818589cd0181858ba9130002', ',root,402855816f31c4de016f31c4f6260024,40280981818463d0018184e0e4900001', NULL, 'admin', '2022-06-21 17:15:31', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '用户修改', 'button', NULL, 6, '40280981818463d0018184e0e4900001', 'group:user:edit', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098184a295fb0184a33422fc001d', ',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e,40280981818986d8018189fdd38d0003', NULL, 'admin', '2022-11-23 14:36:54', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '群组查询', 'button', NULL, 6, '40280981818986d8018189fdd38d0003', 'system:group:list', NULL, 'admin', '2022-11-23 14:36:54');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f43237dc70026', ',root,402885887f43108e017f431438640000,402885887f43108e017f431ad0cf000f', NULL, 'admin', '2022-03-01 09:41:14', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '生成代码', 'button', NULL, 6, '402885887f43108e017f431ad0cf000f', 'devtool:gencode:download', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea2e2c95002e', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/qrcode/index', 'admin', '2021-11-04 17:01:07', 'fa fa-qrcode', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '二维码生成', 'menu', 'qrcode', 6, '4028b26c7ce9c55e017cea27d324001e', 'fun:qrcode:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea3c7d940045', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/formAdapt/index', 'admin', '2021-11-04 17:16:45', 'fa fa-wpforms', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '表单自适应', 'menu', 'formAdapt', 6, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:formAdapt:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098183f357740183f4b9ddbf0007', ',root,8280840383c5fb4c0183c60d0d660003,4028098183d009810183d00f364d0004,4028098183d408c80183d42dc7eb0004', NULL, 'admin', '2022-10-20 17:29:25', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, 'JVM信息', 'button', NULL, 6, '4028098183d408c80183d42dc7eb0004', 'monitor:service:jvminfo', NULL, 'admin', '2022-10-20 17:29:25');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098185186273018518931fbb0000', ',root,40280981816630d00181663c70e9001d', '/storage/index', 'admin', '2022-12-16 09:36:13', 'fa fa-file-excel-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '文件管理', 'menu', 'storage', 7, '40280981816630d00181663c70e9001d', 'storage:object:view', NULL, 'admin', '2022-12-16 11:27:11');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402855816f31c4de016f31c4f69b002e', ',root,402855816f31c4de016f31c4f6260024', '/system/notice/index', 'system', '2019-12-23 15:59:49', 'fa fa-bell-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '消息管理', 'menu', 'notice', 7, '402855816f31c4de016f31c4f6260024', 'system:notice:view', '', 'admin', '2022-10-21 19:58:23');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('40280981818589cd0181858c17570004', ',root,402855816f31c4de016f31c4f6260024,40280981818463d0018184e0e4900001', NULL, 'admin', '2022-06-21 17:16:00', NULL, 'N', 'N', 'N', 'N', 'N', 'N', NULL, '用户退群', 'button', NULL, 7, '40280981818463d0018184e0e4900001', 'group:user:remove', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea2f3dd90030', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/printJs/index', 'admin', '2021-11-04 17:02:17', 'fa fa-print', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '页面打印', 'menu', 'printJs', 7, '4028b26c7ce9c55e017cea27d324001e', 'fun:printJs:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea3d2ea60047', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/formI18n/index', 'admin', '2021-11-04 17:17:31', 'fa fa-internet-explorer', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '表单国际化', 'menu', 'formI18n', 7, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:formI18n:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea2fe3bb0032', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/clipboard/index', 'admin', '2021-11-04 17:03:00', 'fa fa-copyright', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '复制剪切', 'menu', 'clipboard', 8, '4028b26c7ce9c55e017cea27d324001e', 'fun:clipboard:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea3ea2660049', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/formRules/index', 'admin', '2021-11-04 17:19:06', 'fa fa-contao', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '多表单验证', 'menu', 'formRules', 8, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:formRules:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea3087310034', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/screenShort/index', 'admin', '2021-11-04 17:03:41', 'fa fa-clone', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '页面截屏', 'menu', 'screenShort', 9, '4028b26c7ce9c55e017cea27d324001e', 'fun:screenShort:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea3f48b2004b', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/listAdapt/index', 'admin', '2021-11-04 17:19:48', 'fa fa-list', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '列表自适应', 'menu', 'listAdapt', 9, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:listAdapt:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea3173180036', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/gridLayout/index', 'admin', '2021-11-04 17:04:42', 'fa fa-hand-pointer-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '拖拽布局', 'menu', 'gridLayout', 10, '4028b26c7ce9c55e017cea27d324001e', 'fun:gridLayout:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea402021004d', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/waterfall/index', 'admin', '2021-11-04 17:20:44', 'fa fa-trello', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '瀑布屏效果', 'menu', 'waterfall', 10, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:waterfall:view', NULL, 'admin', '2021-11-04 17:22:36');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea27d324001e', ',root', NULL, 'admin', '2021-11-04 16:54:11', 'fa fa-cube', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '功能组件', 'catalog', 'fun', 11, 'root', 'fun:index:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea327e000038', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/splitpanes/index', 'admin', '2021-11-04 17:05:50', 'fa fa-trello', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '布局拆分器', 'menu', 'splitpanes', 11, '4028b26c7ce9c55e017cea27d324001e', 'fun:splitpanes:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea40aa7c004f', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/steps/index', 'admin', '2021-11-04 17:21:19', 'fa fa-list-ol', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '步骤条', 'menu', 'steps', 11, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:steps:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea35fcf7003c', ',root', NULL, 'admin', '2021-11-04 17:09:39', 'fa fa-map-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '页面组件', 'catalog', 'pages', 12, 'root', 'pages:index:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea33dcac003a', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/dragVerify/index', 'admin', '2021-11-04 17:07:20', 'fa fa-adn', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '拖拽验证', 'menu', 'dragVerify', 12, '4028b26c7ce9c55e017cea27d324001e', 'fun:dragVerify:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea4179660051', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/preview/index', 'admin', '2021-11-04 17:22:12', 'fa fa-window-maximize', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '图片预览', 'menu', 'imgpreview', 12, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:imgpreview:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402881977d4a52fe017d4b9e306200a3', ',root,4028b26c7ce9c55e017cea27d324001e', NULL, 'admin', '2021-11-23 15:06:38', 'fa fa-paypal', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '路由参数', 'catalog', 'params', 13, '4028b26c7ce9c55e017cea27d324001e', 'fun:params:view', NULL, 'admin', '2021-11-23 15:08:25');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea4250390054', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/waves/index', 'admin', '2021-11-04 17:23:07', 'fa fa-google-wallet', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '波浪效果', 'menu', 'waves', 13, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:waves:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098181c823da0181c834fcc50003', ',root,4028b26c7ce9c55e017cea27d324001e', '/fun/echartsMap/index', 'admin', '2022-07-04 15:55:25', 'fa fa-map-marker', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '百度地图', 'menu', 'fun/echartsMap', 14, '4028b26c7ce9c55e017cea27d324001e', 'fun:echartsMap:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea4324d10056', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/tree/index', 'admin', '2021-11-04 17:24:01', 'fa fa-table', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '树形表格', 'menu', 'tabletree', 14, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:tabletree:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028098183ea0c1e0183ea821a0d0000', ',root,4028b26c7ce9c55e017cea27d324001e', '/tools/index', 'admin', '2022-10-18 17:52:18', 'fa fa-wpforms', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '表单校验', 'menu', 'tools', 15, '4028b26c7ce9c55e017cea27d324001e', 'pages:tools:view', NULL, 'admin', '2022-10-18 17:52:18');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea4423d90058', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/drag/index', 'admin', '2021-11-04 17:25:07', 'fa fa-hand-pointer-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '组件拖拽', 'menu', 'drag', 15, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:drag:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea44c880005a', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/lazyImg/index', 'admin', '2021-11-04 17:25:49', 'fa fa-flag-checkered', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '图片懒加载', 'menu', 'lazyImg', 16, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:lazyImg:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea4556d9005c', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/dynamicForm/index', 'admin', '2021-11-04 17:26:25', 'fa fa-wpforms', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '动态表单', 'menu', 'dynamicForm', 17, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:dynamicForm:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea45e6a2005e', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/pages/workflow/index', 'admin', '2021-11-04 17:27:02', 'fa fa-stack-overflow', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '工作流', 'menu', 'workflow', 18, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:workflow:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea47bcc20060', ',root,4028b26c7ce9c55e017cea35fcf7003c', NULL, 'admin', '2021-11-04 17:29:02', 'fa fa-file-word-o', 'N', 'N', 'N', 'Y', 'N', 'Y', 'https://wdd.js.org/jsplumb-chinese-tutorial/#/', 'jsplumb 中文教程', 'menu', 'jsplumb', 19, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:jsplumb:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea494a910062', ',root,4028b26c7ce9c55e017cea35fcf7003c', NULL, 'admin', '2021-11-04 17:30:44', 'fa fa-desktop', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '数据大屏', 'catalog', 'visualizing', 20, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:visualizing:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ce9c55e017cea4d1dbb0068', ',root,4028b26c7ce9c55e017cea35fcf7003c', '/chart/index', 'admin', '2021-11-04 17:34:55', 'fa fa-area-chart', 'N', 'N', 'N', 'N', 'N', 'N', NULL, 'echarts 图表', 'menu', 'echarts', 21, '4028b26c7ce9c55e017cea35fcf7003c', 'pages:echarts:view', NULL, NULL, NULL);
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddab793017ddadf51910004', ',root', NULL, NULL, NULL, 'fa fa-tasks', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '任务管理', 'catalog', 'flowable/task', 50, 'root', 'flowable:task:view', NULL, 'admin', '2022-09-08 14:56:31');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887ddbc0b1017ddbc30f0b0000', ',root', NULL, NULL, NULL, 'fa fa-slideshare', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '演示示例', 'catalog', 'demo', 51, 'root', 'demo:example:view', NULL, 'admin', '2022-09-08 14:56:40');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('4028b26c7ceeb8ce017ceec0c0280000', ',root', NULL, 'admin', '2021-11-05 14:19:42', 'fa fa-sliders', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '流程管理', 'catalog', 'flowable', 52, 'root', 'flowable:index:view', NULL, 'admin', '2022-09-08 14:56:48');
INSERT INTO sys_menu
(menu_id, ancestors, component, create_by, create_time, icon, is_affix, is_common, is_hide, is_iframe, is_keep_alive, is_link, link_url, menu_name, menu_type, name, order_num, parent_id, perms, remark, update_by, update_time)
VALUES('402885887f43108e017f431438640000', ',root', NULL, 'admin', '2022-03-01 09:24:33', 'fa fa-file-code-o', 'N', 'N', 'N', 'N', 'N', 'N', NULL, '开发管理', 'catalog', 'devtool', 100, 'root', 'devtool:index:view', NULL, 'admin', '2022-06-17 10:46:21');

