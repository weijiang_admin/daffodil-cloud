##部署说明
1. 复制run-app文件部署包到服务器上，并重命名。
2. 修改部署包里面的conf文件夹下的bootstrap.yml文件的nacos配置属性信息。
3. 拷贝工程***-boot.jar包到run-app目录下即可。
4. 赋予bin目录的sh脚本执行权限，例：`chmod -R 755 ./bin/*.sh`
5. 启动直接执行`bin/startup.sh` 即可。可携带参数 `bin/startup.sh -l` 表示打印查看日志
6. 关闭直接执行`bin/shutdown.sh` 并输入正确的进程号，为了防止不小心错误执行，再次输入相应的进程号确认关闭。可携带参数 `bin/shutdown.sh -f`表示强制关闭

##敏感信息加密处理
1. 在你的windows电脑上有java运行环境下，打开cmd输入以下命令 `java -jar daffodil-jasypt-boot.jar`就会打开javaUI的弹窗输入明文和密文加解密即可。
2. 在yml文件中必须以ENC(*******)包括起来。
