#!/bin/bash

EXTRA_JVM_ARGUMENTS="-server -Xms512M -Xmx512M -Xss512k -XX:MetaspaceSize=256M -XX:MaxMetaspaceSize=256M -XX:+UseG1GC"

## resolve links
PRG="$0"
APP_RUN_MODE="$1"

# need this for relative symlinks
while [ -h "$PRG" ] ; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG="`dirname "$PRG"`/$link"
  fi
done

APP_HOME=`dirname "$PRG"`/..

# make it fully qualified
cd $APP_HOME
APP_HOME=`pwd`
CURRENT_FOLDER=${APP_HOME##*/}

SEARCH_JAR_NAME="*.jar"
APP_JAR_NAME=`echo ${SEARCH_JAR_NAME}`

if [ "$APP_JAR_NAME" == "$SEARCH_JAR_NAME" ] ; then
  echo "cannot match the jar [${SEARCH_JAR_NAME}] in the folder [${APP_HOME}]"
  exit 1
fi

APP_JAR_FULL_NAME=${APP_HOME}/${APP_JAR_NAME}
echo "find the jar [$APP_JAR_FULL_NAME]"

JDK_LOCATION="${APP_HOME}/jdk"
if [ -d "${JDK_LOCATION}" ] ; then
  JAVA_HOME="${JDK_LOCATION}"
fi

if [ -z "$JAVA_HOME" ] ; then
  JAVACMD=`which java`
else
  JAVACMD="$JAVA_HOME/bin/java"
fi

if [ ! -x "$JAVACMD" ] ; then
  echo "cannot find java command in the directory of [$JAVA_HOME/bin]"
  echo "The JAVA_HOME environment variable is not defined correctly" >&2
  echo "This environment variable is needed to run this program" >&2
  echo "NB: JAVA_HOME should point to a JDK not a JRE" >&2
  exit 1
fi

APP_PID=`ps -ef|grep "\-Dbasedir=$APP_HOME/"|grep -v grep|awk '{print $2}'`
if [ -n "$APP_PID" ] ; then
  echo "the project [${CURRENT_FOLDER}] is already running in process [$APP_PID]."
  exit 1
fi

APP_BIN_DIR=${APP_HOME}/bin
APP_CONF_DIR=${APP_HOME}/conf
APP_LOG_DIR=${APP_HOME}/logs
APP_LIB_DIR=${APP_HOME}/lib
APP_CLASSES_DIR=${APP_HOME}/classes
APP_TMP_DIR=${APP_HOME}/tmp
APP_LOG_FILENAME=${APP_HOME##*/}-`date '+%Y%m%d%H%M'`.log

if [ -n "$APP_JAVA_OPTS" ]; then
  EXTRA_JVM_ARGUMENTS="$APP_JAVA_OPTS"
fi
JAVA_OPTS="${JAVA_OPTS} ${EXTRA_JVM_ARGUMENTS}"

JAVA_MAJOR_VERSION=$($JAVACMD -version 2>&1 | sed -E -n 's/.* version "([0-9]*).*$/\1/p')
if [ "$JAVA_MAJOR_VERSION" -lt "9" ] ; then
  JAVA_OPTS="${JAVA_OPTS} -Xloggc:${APP_LOG_DIR}/gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M"
else
  JAVA_OPTS="${JAVA_OPTS} -Xlog:gc*,gc+ref=debug,gc+heap=debug,gc+age=trace,gc+ergo*=trace:file=${APP_LOG_DIR}/gc.log:tags,uptime,time,level:filecount=10,filesize=100m"
fi

JAVA_OPTS="${JAVA_OPTS} -Dfile.encoding=UTF-8"
JAVA_OPTS="${JAVA_OPTS} -Dcsp.logging.file.path=${APP_LOG_DIR} -Dcsp.logging.file.encoding=UTF-8 -Dcsp.logging.console.encoding=UTF-8"
JAVA_OPTS="${JAVA_OPTS} -Dspring.pid.file=${APP_BIN_DIR}/application.pid"

echo "APP_HOME:${APP_HOME}"
echo "APP_BIN_DIR:${APP_BIN_DIR}"
echo "APP_CLASSES_DIR:${APP_CLASSES_DIR}"
echo "APP_CONF_DIR:${APP_CONF_DIR}"
echo "APP_LIB_DIR:${APP_LIB_DIR}"
echo "APP_LOG_DIR:${APP_LOG_DIR}"
echo "APP_TMP_DIR:${APP_TMP_DIR}"

result=$(echo $APP_JAR_NAME | grep "\-boot.jar$")
if [ -n "$result" ]; then
  SPRING_BOOT_OPTS=""
  BOOT_LOADER_PATH=""
  if [ -d "${APP_CONF_DIR}" ] ; then
    SPRING_BOOT_OPTS="${SPRING_BOOT_OPTS} -Dspring.config.additional-location=${APP_CONF_DIR}/"
    BOOT_LOADER_PATH="${BOOT_LOADER_PATH},${APP_CONF_DIR}"
  fi
  if [ -d "${APP_CLASSES_DIR}" ] ; then
    BOOT_LOADER_PATH="${BOOT_LOADER_PATH},${APP_CLASSES_DIR}"
  fi
  if [ -d "${APP_LIB_DIR}" ] ; then
    BOOT_LOADER_PATH="${BOOT_LOADER_PATH},${APP_LIB_DIR}"
  fi
  if [ -n "$BOOT_LOADER_PATH" ]; then
    BOOT_LOADER_PATH=${BOOT_LOADER_PATH:1}
  fi
  JAVA_OPTS="${JAVA_OPTS} ${SPRING_BOOT_OPTS} -Dloader.path=${BOOT_LOADER_PATH} -Dbasedir=${APP_HOME}/ -jar ${APP_JAR_FULL_NAME}"
fi

if [ 0"$APP_RUNNING_MODE" = "0docker" ]; then
  set -x
  exec "$JAVACMD" ${JAVA_OPTS} "$@"
  set +x

  sleep 2s
  echo "The project ${CURRENT_FOLDER} is running in the docker container."
else
  set -x
  nohup "$JAVACMD" ${JAVA_OPTS} "$@" >${APP_LOG_DIR}/${APP_LOG_FILENAME} 2>&1 &
  set +x

  sleep 2s
  APP_PID=`ps -ef|grep "\-Dbasedir=$APP_HOME/"|grep -v grep|awk '{print $2}'`
  echo "The project ${CURRENT_FOLDER} is starting with pid ${APP_PID}"
  
  if [ "$APP_RUN_MODE" == "-l" ] ; then
    if [ -f "${APP_LOG_DIR}/${APP_LOG_FILENAME}" ] ; then
      tail -100f ${APP_LOG_DIR}/${APP_LOG_FILENAME}
    fi
  fi
fi
