#!/bin/bash

## resolve links
PRG="$0"
APP_RUN_MODE="$1"

# need this for relative symlinks
while [ -h "$PRG" ] ; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG="`dirname "$PRG"`/$link"
  fi
done

APP_HOME=`dirname "$PRG"`/..

# make it fully qualified
cd $APP_HOME
APP_HOME=`pwd`
CURRENT_FOLDER=${APP_HOME##*/}

SEARCH_JAR_NAME="*.jar"
APP_JAR_NAME=`echo ${SEARCH_JAR_NAME}`

if [ "$APP_JAR_NAME" == "$SEARCH_JAR_NAME" ] ; then
  echo "cannot match the jar [${SEARCH_JAR_NAME}] in the folder [${APP_HOME}]"
  exit 1
fi

APP_JAR_FULL_NAME=${APP_HOME}/${APP_JAR_NAME}
echo "find the jar [$APP_JAR_FULL_NAME]"

APP_PID=`ps -ef|grep "\-Dbasedir=$APP_HOME/"|grep -v grep|awk '{print $2}'`

if [ -n "$APP_PID" ] ; then
  if [ "$APP_RUN_MODE" == "-f" ] ; then
    echo "Try shutdown using kill"
    kill -9 $APP_PID
    echo "shutdown process [$APP_PID] of the project [$CURRENT_FOLDER]"
    if [ -f "${APP_HOME}/bin/application.pid" ] ; then
      rm -fr ${APP_HOME}/bin/application.pid
    fi
  else
    INPUT_APP_PID=appid
    read -p "please input process id [$APP_PID] to shutdown this program:" INPUT_APP_PID
    if [ "$APP_PID" == "$INPUT_APP_PID" ];then
      echo "Try shutdown using kill"
      kill -9 $APP_PID
      echo "shutdown process [$APP_PID] of the project [$CURRENT_FOLDER]"
      if [ -f "${APP_HOME}/bin/application.pid" ] ; then
        rm -fr ${APP_HOME}/bin/application.pid
      fi
    else
      echo "[$INPUT_APP_PID] is error, the correct process id is $APP_PID"
    fi
  fi
else
  echo "the project [$CURRENT_FOLDER] is not running"
fi
