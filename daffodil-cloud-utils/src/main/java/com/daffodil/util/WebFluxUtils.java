package com.daffodil.util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;

import reactor.core.publisher.Mono;

/**
 * WebFlux响应工具集合
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
public class WebFluxUtils {
    
    /** 状态码 */
    public static final String CODE_TAG = "code";

    /** 返回内容 */
    public static final String MSG_TAG = "msg";

    /** 数据对象 */
    public static final String DATA_TAG = "data";
    
    /**
     * 
     * @param response
     * @param status Http 状态码
     * @param data 内容
     * @return
     */
    public static Mono<Void> responseWriter(ServerHttpResponse response, HttpStatus status, Object data){
        return responseWriter(response, MediaType.APPLICATION_JSON_VALUE, status, "", data);
    }
    
    /**
     * 
     * @param response
     * @param status Http 状态码
     * @param msg 消息
     * @return
     */
    public static Mono<Void> responseWriter(ServerHttpResponse response, HttpStatus status, String msg){
        return responseWriter(response, MediaType.APPLICATION_JSON_VALUE, status, msg, null);
    }
    
    /**
     * 
     * @param response
     * @param contentType
     * @param status Http 状态码
     * @param msg 消息
     * @return
     */
    public static Mono<Void> responseWriter(ServerHttpResponse response, String contentType, HttpStatus status, String msg){
        return responseWriter(response, contentType, status, msg, null);
    }
    
    /**
     * 
     * @param response
     * @param contentType
     * @param status Http 状态码
     * @param data 内容
     * @return
     */
    public static Mono<Void> responseWriter(ServerHttpResponse response, String contentType, HttpStatus status, Object data){
        return responseWriter(response, contentType, status, "", data);
    }
    
    /**
     * 
     * @param response
     * @param contentType
     * @param status Http 状态码
     * @param msg 消息
     * @param data 内容
     * @return
     */
    public static Mono<Void> responseWriter(ServerHttpResponse response, String contentType, HttpStatus status, String msg, Object data){
        int code = status.equals(HttpStatus.OK) ? 0 : status.value();//响应码200 -> 0
        Map<String, Object> jsonResult = new HashMap<String, Object>();
        jsonResult.put(CODE_TAG, code);
        jsonResult.put(MSG_TAG, msg);
        jsonResult.put(DATA_TAG, data);
        return responseWriter(response, contentType, code, JacksonUtils.toJSONString(jsonResult), StandardCharsets.UTF_8);
    }
    
    /**
     * 
     * @param response
     * @param code 自定义状态码
     * @param data 内容
     * @return
     */
    public static Mono<Void> responseWriter(ServerHttpResponse response, int code, Object data){
        return responseWriter(response, MediaType.APPLICATION_JSON_VALUE, code, "", data);
    }
    
    /**
     * 
     * @param response
     * @param code 自定义状态码
     * @param msg 消息
     * @return
     */
    public static Mono<Void> responseWriter(ServerHttpResponse response, int code, String msg){
        return responseWriter(response, MediaType.APPLICATION_JSON_VALUE, code, msg, null);
    }
    
    /**
     * 
     * @param response
     * @param contentType
     * @param code 自定义状态码
     * @param msg 消息
     * @return
     */
    public static Mono<Void> responseWriter(ServerHttpResponse response, String contentType, int code, String msg){
        return responseWriter(response, contentType, code, msg, null);
    }
    
    /**
     * 
     * @param response
     * @param contentType
     * @param code 自定义状态码
     * @param data 内容
     * @return
     */
    public static Mono<Void> responseWriter(ServerHttpResponse response, String contentType, int code, Object data){
        return responseWriter(response, contentType, code, "", data);
    }
    
    /**
     * 
     * @param response
     * @param contentType
     * @param code 自定义状态码
     * @param msg 消息
     * @param data 内容
     * @return
     */
    public static Mono<Void> responseWriter(ServerHttpResponse response, String contentType, int code, String msg, Object data){
        Map<String, Object> jsonResult = new HashMap<String, Object>();
        jsonResult.put(CODE_TAG, code);
        jsonResult.put(MSG_TAG, msg);
        jsonResult.put(DATA_TAG, data);
        return responseWriter(response, contentType, code, JacksonUtils.toJSONString(jsonResult), StandardCharsets.UTF_8);
    }
    
    /**
     * 
     * @param response
     * @param contentType
     * @param code
     * @param data
     * @param charset
     * @return
     */
    public static Mono<Void> responseWriter(ServerHttpResponse response, String contentType, int code, String data, Charset charset){
        response.setRawStatusCode(code);
        response.getHeaders().add(HttpHeaders.CONTENT_TYPE, contentType);
        DataBuffer dataBuffer = response.bufferFactory().wrap(data.getBytes(charset));
        return response.writeWith(Mono.just(dataBuffer));
    }
    
}
