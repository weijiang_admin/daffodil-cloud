package com.daffodil.util.text;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串格式化
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
public class StrFormatter {

    public static final Pattern NAMES_PATTERN = Pattern.compile("\\{([^/]+?)\\}");
    public static final String EMPTY_JSON = "{}";
    public static final char C_BACKSLASH = '\\';
    public static final char C_DELIM_START = '{';
    public static final char C_DELIM_END = '}';

    /**
     * 格式化字符串<br>
     * 此方法只是简单将占位符 {} 按照顺序替换为参数<br>
     * 如果想输出 {} 使用 \\转义 { 即可，如果想输出 {} 之前的 \ 使用双转义符 \\\\ 即可<br>
     * 例：<br>
     * 通常使用：format("this is {} for {}", "a", "b") -> this is a for b<br>
     * 转义{}： format("this is \\{} for {}", "a", "b") -> this is \{} for a<br>
     * 转义\： format("this is \\\\{} for {}", "a", "b") -> this is \a for b<br>
     * 
     * @param source 字符串模板
     * @param variables 参数列表
     * @return
     */
    public static String format(String source, Object... variables) {
        if (null == source || "".equals(source.trim()) || null == variables || variables.length == 0) {
            return source;
        }
        final int strPatternLength = source.length();

        // 初始化定义好的长度以获得更好的性能
        StringBuilder sbuf = new StringBuilder(strPatternLength + 50);

        int handledPosition = 0;
        int delimIndex;// 占位符所在位置
        for (int argIndex = 0; argIndex < variables.length; argIndex++) {
            delimIndex = source.indexOf(EMPTY_JSON, handledPosition);
            if (delimIndex == -1) {
                if (handledPosition == 0) {
                    return source;
                } else { // 字符串模板剩余部分不再包含占位符，加入剩余部分后返回结果
                    sbuf.append(source, handledPosition, strPatternLength);
                    return sbuf.toString();
                }
            } else {
                if (delimIndex > 0 && source.charAt(delimIndex - 1) == C_BACKSLASH) {
                    if (delimIndex > 1 && source.charAt(delimIndex - 2) == C_BACKSLASH) {
                        // 转义符之前还有一个转义符，占位符依旧有效
                        sbuf.append(source, handledPosition, delimIndex - 1);
                        String variable = Convert.utf8Str(variables[argIndex]);
                        sbuf.append(getVariableValueAsString(variable));
                        handledPosition = delimIndex + 2;
                    } else {
                        // 占位符被转义
                        argIndex--;
                        sbuf.append(source, handledPosition, delimIndex - 1);
                        sbuf.append(C_DELIM_START);
                        handledPosition = delimIndex + 1;
                    }
                } else {
                    // 正常占位符
                    sbuf.append(source, handledPosition, delimIndex);
                    String variable = Convert.utf8Str(variables[argIndex]);
                    sbuf.append(getVariableValueAsString(variable));
                    handledPosition = delimIndex + 2;
                }
            }
        }
        // 加入最后一个占位符后所有的字符
        sbuf.append(source, handledPosition, source.length());

        return sbuf.toString();
    }

    /**
     * 格式化字符串<br>
     * 此方法按key将占位符 {key} 按照参数顺序替换为参数<br>
     * 例：<br>
     * 通常使用：expand("this is {a} for {b}", "a", "b") -> this is a for b<br>
     * @param source 字符串模板
     * @param variables 参数列表
     * @return
     */
    public static String expand(String source, Object... variables) {
        if (source == null) {
            return null;
        }
        if (source.indexOf('{') == -1) {
            return source;
        }
        if (source.indexOf(':') != -1) {
            source = sanitizeSource(source);
        }
        Matcher matcher = NAMES_PATTERN.matcher(source);
        StringBuffer sb = new StringBuffer();
        Iterator<Object> valueIterator = Arrays.asList(variables).iterator();
        while (matcher.find()) {
            String match = matcher.group(1);
            String varName = getVariableName(match);
            Object varValue = valueIterator.hasNext() ? valueIterator.next() : varName;
            String formatted = getVariableValueAsString(varValue);
            formatted = Matcher.quoteReplacement(formatted);
            matcher.appendReplacement(sb, formatted);
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 格式化字符串<br>
     * 此方法按key将占位符 {key} 按照参数Map.key值替换为参数<br>
     * 例：<br>
     * 通常使用：expand("this is {a} for {b}", {"a" : "a", "b" : "b"}) -> this is a for b<br>
     * @param source 字符串模板
     * @param variables 参数列表
     * @return
     */
    public static String expand(String source, Map<String, ?> variables) {
        if (source == null) {
            return null;
        }
        if (source.indexOf('{') == -1) {
            return source;
        }
        if (source.indexOf(':') != -1) {
            source = sanitizeSource(source);
        }
        Matcher matcher = NAMES_PATTERN.matcher(source);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            String match = matcher.group(1);
            String varName = getVariableName(match);
            Object varValue = variables.get(varName);
            String formatted = getVariableValueAsString(varValue);
            formatted = Matcher.quoteReplacement(formatted);
            matcher.appendReplacement(sb, formatted);
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * -移除嵌套的"{}"
     * @param source
     * @return
     */
    private static String sanitizeSource(String source) {
        int level = 0;
        int lastCharIndex = 0;
        char[] chars = new char[source.length()];
        for (int i = 0; i < source.length(); i++) {
            char c = source.charAt(i);
            if (c == '{') {
                level++;
            }
            if (c == '}') {
                level--;
            }
            if (level > 1 || (level == 1 && c == '}')) {
                continue;
            }
            chars[lastCharIndex++] = c;
        }
        return new String(chars, 0, lastCharIndex);
    }

    private static String getVariableName(String match) {
        int colonIdx = match.indexOf(':');
        return (colonIdx != -1 ? match.substring(0, colonIdx) : match);
    }

    private static String getVariableValueAsString(Object variableValue) {
        return (variableValue != null ? variableValue.toString() : "");
    }
}
