package com.daffodil.util;

import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.Assert;

import cn.hutool.captcha.generator.RandomGenerator;
import cn.hutool.crypto.digest.DigestAlgorithm;
import cn.hutool.crypto.digest.Digester;

/**
 * 
 * @author yweijian
 * @date 2022年11月21日
 * @version 2.0.0
 * @description
 */
public class PasswordUtils {

    /**
     * 弱密码校验模式
     */
    public static final String PASSWORD_REGX = "^[a-zA-Z0-9][a-zA-Z0-9!@#$%^&\\.*_]{5,15}$";

    /**
     * 强密码校验模式
     */
    public static final String PASSWORD_STRONG_REGX = "^(?![a-zA-z]+$)(?!\\d+$)(?![!@#$%^&\\.*_]+$)(?![a-zA-Z\\d]+$)(?![a-zA-Z!@#$%^&\\.*_]+$)(?![\\d!@#$%^&\\.*_]+$)[a-zA-Z\\d!@#$%^&\\.*_]{6,16}$";

    /**
     * SHA256-用户密码加密
     * @param loginName 用户名
     * @param password 密码
     * @param salt 加密盐
     * @return
     */
    public static String encryptPassword(String loginName, String password, String salt) {
        Digester digester = new Digester(DigestAlgorithm.SHA256);
        digester.setSalt(salt.getBytes(StandardCharsets.UTF_8));
        return digester.digestHex(loginName + password);
    }

    /**
     * 生成字符串长度为6的随机盐
     */
    public static String randomSalt() {
        return new RandomGenerator(6).generate();
    }

    /**
     * 校验密码是否合法（弱密码校验模式）
     * @param value
     * @return
     */
    public static boolean verifyPassword(String value) {
        return verifyPassword(value, PASSWORD_REGX);
    }

    /**
     * 校验密码是否合法（强密码校验模式）
     * @param value
     * @return
     */
    public static boolean verifyStrongPassword(String value) {
        return verifyPassword(value, PASSWORD_STRONG_REGX);
    }

    /**
     * 校验密码是否合法
     * @param value
     * @param regex
     * @return
     */
    public static boolean verifyPassword(String value, String regex) {
        Assert.hasText(regex, "regex must not be empty");
        if (StringUtils.isNotEmpty(value)) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(value);
            if (matcher.find()) {
                return true;
            }
        }
        return false;
    }
}
