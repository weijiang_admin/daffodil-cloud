package com.daffodil.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.extern.slf4j.Slf4j;

/**
 * 校验辅助工具类
 * 
 * @author yweijian
 * @date 2021年9月14日
 * @version 1.0
 * @description
 */
@Slf4j
public class VerificateUtils {

    /**
     * 根据正则表达式过滤
     * @param value
     * @param regex
     * @return
     */
    public static boolean check(String value, String regex) {
        if (StringUtils.isNotEmpty(value) && StringUtils.isNotEmpty(regex)) {
            Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
            Matcher matcher = pattern.matcher(value.toLowerCase());
            if (matcher.find()) {
                log.info("Contains special characters：" + matcher.group());
                return true;
            }
        }
        return false;
    }

    /**
     * XSS渗透漏洞攻击校验
     * @param value
     * @param list
     * @return
     */
    public static boolean check(String value, List<String> list) {
        if (StringUtils.isNotEmpty(value)) {
            List<Pattern> patterns = getPatterns(list);
            for (Pattern pattern : patterns) {
                Matcher matcher = pattern.matcher(value.toLowerCase());
                if (matcher.find()) {
                    log.info("Contains special characters：" + matcher.group());
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 获取正则表达式
     * @param list
     * @return
     */
    private static List<Pattern> getPatterns(List<String> list){
        List<Pattern> patterns = new ArrayList<Pattern>();
        if(StringUtils.isNotEmpty(list)) {
            list.forEach(pattern -> {
                if(StringUtils.isNotEmpty(pattern)) {
                    patterns.add(Pattern.compile(pattern, Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL));
                }
            });
        }
        return patterns;
    }
}
