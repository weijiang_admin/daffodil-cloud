package com.daffodil.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.server.reactive.ServerHttpRequest;

import cn.hutool.http.HttpRequest;
import lombok.extern.slf4j.Slf4j;

/**
 * 获取IP方法
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
@Slf4j
public class IpUtils {

    public static final String UNKNOWN = "unknown";

    public static final String IPV4_LOCAL_ADDRESS = "127.0.0.1";

    public static final String IPV6_LOCAL_ADDRESS = "0:0:0:0:0:0:0:1";

    /** 太平洋IP地址查询 */
    public static final String IP_URL_PACIFIC = "http://whois.pconline.com.cn/ipJson.jsp";

    /**
     * 获取IP的真实地理位置名称
     * @param ip
     * @param json
     * @return
     */
    public static String getRealIpAddressName(String ip, boolean json) {
        if (IpUtils.internalIp(ip)) {
            return "局域网IP " + ip;
        }
        Map<String, String> jsonObject = getRealIpAddress(ip, json);
        if (StringUtils.isEmpty(jsonObject)) {
            log.error("获取地理位置异常 {}", ip);
            return "未知IP " + ip;
        }
        return jsonObject.get("addr");
    }
    
    /**
     * 获取IP的真实地理位置
     * <br>
     * {"ip":"220.250.58.18","pro":"福建省","proCode":"350000","city":"福州市","cityCode":"350100","region":"","regionCode":"0","addr":"福建省福州市 联通","regionNames":"","err":""}
     * @param ip
     * @param json
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Map<String, String> getRealIpAddress(String ip, boolean json) {
        String url = IP_URL_PACIFIC + "?ip=" + ip + "&json=" + json;
        String body = HttpRequest.get(url ).charset(Charset.forName("GBK")).timeout(5000).execute().body();
        return JacksonUtils.toJavaObject(body, Map.class);
    }
    
    /**
     * 获取请求真实IP
     * @param request
     * @return
     */
    public static String getIpAddrByHttpServletRequest(HttpServletRequest request) {
        if (request == null) {
            return UNKNOWN;
        }
        String ip = request.getHeader("X-Forwarded-For");
        if (ip != null && ip.length() != 0 && !UNKNOWN.equalsIgnoreCase(ip)) {
            if (ip.indexOf(',') != -1) {
                ip = ip.split(",")[0];
            }
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }

        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        return IPV6_LOCAL_ADDRESS.equals(ip) ? IPV4_LOCAL_ADDRESS : ip;
    }

    /**
     * 获取请求真实IP
     * @param request
     * @return
     */
    public static String getIpAddrByServerHttpRequest(ServerHttpRequest request) {
        if (request == null) {
            return UNKNOWN;
        }
        String ip = request.getHeaders().getFirst("X-Forwarded-For");
        if (ip != null && ip.length() != 0 && !UNKNOWN.equalsIgnoreCase(ip)) {
            if (ip.indexOf(',') != -1) {
                ip = ip.split(",")[0];
            }
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeaders().getFirst("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeaders().getFirst("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeaders().getFirst("X-Real-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddress().getAddress().getHostAddress();
        }

        return IPV6_LOCAL_ADDRESS.equals(ip) ? IPV4_LOCAL_ADDRESS : ip;
    }

    public static boolean internalIp(String ip) {
        byte[] addr = textToNumericFormatV4(ip);
        return internalIp(addr) || IPV4_LOCAL_ADDRESS.equals(ip);
    }

    private static boolean internalIp(byte[] addr) {
        if (StringUtils.isNull(addr) || addr.length < 2) {
            return true;
        }
        final byte b0 = addr[0];
        final byte b1 = addr[1];
        // 10.x.x.x/8
        final byte SECTION_1 = 0x0A;
        // 172.16.x.x/12
        final byte SECTION_2 = (byte) 0xAC;
        final byte SECTION_3 = (byte) 0x10;
        final byte SECTION_4 = (byte) 0x1F;
        // 192.168.x.x/16
        final byte SECTION_5 = (byte) 0xC0;
        final byte SECTION_6 = (byte) 0xA8;
        switch (b0) {
        case SECTION_1:
            return true;
        case SECTION_2:
            if (b1 >= SECTION_3 && b1 <= SECTION_4) {
                return true;
            }
        case SECTION_5:
            if(b1 == SECTION_6) {
                return true;
            }
        default:
            return false;
        }
    }

    /**
     * 将IPv4地址转换成字节
     * 
     * @param text
     *            IPv4地址
     * @return byte 字节
     */
    public static byte[] textToNumericFormatV4(String text) {
        byte[] bytes = new byte[4];
        if (text.length() == 0) {
            return bytes;
        }
        String[] elements = text.split("\\.", -1);
        try {
            long l;
            int i;
            switch (elements.length) {
            case 1:
                l = Long.parseLong(elements[0]);
                if ((l < 0L) || (l > 4294967295L)) {
                    return null;
                }
                bytes[0] = (byte) (int) (l >> 24 & 0xFF);
                bytes[1] = (byte) (int) ((l & 0xFFFFFF) >> 16 & 0xFF);
                bytes[2] = (byte) (int) ((l & 0xFFFF) >> 8 & 0xFF);
                bytes[3] = (byte) (int) (l & 0xFF);
                break;
            case 2:
                l = Integer.parseInt(elements[0]);
                if ((l < 0L) || (l > 255L)) {
                    return null;
                }
                bytes[0] = (byte) (int) (l & 0xFF);
                l = Integer.parseInt(elements[1]);
                if ((l < 0L) || (l > 16777215L)) {
                    return null;
                }
                bytes[1] = (byte) (int) (l >> 16 & 0xFF);
                bytes[2] = (byte) (int) ((l & 0xFFFF) >> 8 & 0xFF);
                bytes[3] = (byte) (int) (l & 0xFF);
                break;
            case 3:
                for (i = 0; i < 2; ++i) {
                    l = Integer.parseInt(elements[i]);
                    if ((l < 0L) || (l > 255L)) {
                        return null;
                    }
                    bytes[i] = (byte) (int) (l & 0xFF);
                }
                l = Integer.parseInt(elements[2]);
                if ((l < 0L) || (l > 65535L)) {
                    return null;
                }
                bytes[2] = (byte) (int) (l >> 8 & 0xFF);
                bytes[3] = (byte) (int) (l & 0xFF);
                break;
            case 4:
                for (i = 0; i < 4; ++i) {
                    l = Integer.parseInt(elements[i]);
                    if ((l < 0L) || (l > 255L)) {
                        return null;
                    }
                    bytes[i] = (byte) (int) (l & 0xFF);
                }
                break;
            default:
                break;
            }
        } catch (NumberFormatException e) {

        }
        return bytes;
    }

    public static String getHostIp() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
        }
        return IPV4_LOCAL_ADDRESS;
    }

    public static String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
        }
        return UNKNOWN;
    }
}