package com.daffodil.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpHeaders;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.MultiValueMapAdapter;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.daffodil.util.text.Convert;

import lombok.extern.slf4j.Slf4j;

/**
 * 客户端工具类
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
@Slf4j
public class ServletUtils {
    /**
     * 获取String参数
     */
    public static String getParameter(String name) {
        return getRequest().getParameter(name);
    }

    /**
     * 获取String参数
     */
    public static String getParameter(String name, String defaultValue) {
        return Convert.toStr(getRequest().getParameter(name), defaultValue);
    }

    /**
     * 获取Integer参数
     */
    public static Integer getParameterToInt(String name) {
        return Convert.toInt(getRequest().getParameter(name));
    }

    /**
     * 获取Integer参数
     */
    public static Integer getParameterToInt(String name, Integer defaultValue) {
        return Convert.toInt(getRequest().getParameter(name), defaultValue);
    }

    /**
     * 获取request
     */
    public static HttpServletRequest getRequest() {
        return getRequestAttributes() != null ? getRequestAttributes().getRequest() : null;
    }

    /**
     * 获取response
     */
    public static HttpServletResponse getResponse() {
        return getRequestAttributes() != null ? getRequestAttributes().getResponse() : null;
    }

    /**
     * 获取session
     */
    public static HttpSession getSession() {
        return getRequest().getSession();
    }

    public static ServletRequestAttributes getRequestAttributes() {
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes) attributes;
    }

    /**
     * 将字符串渲染到客户端
     * 
     * @param response
     *            渲染对象
     * @param string
     *            待渲染的字符串
     * @return null
     */
    public static String renderString(HttpServletResponse response, String string) {
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            response.getWriter().print(string);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * 是否是Ajax异步请求
     * 
     * @param request
     */
    public static boolean isAjaxRequest(HttpServletRequest request) {
        String accept = request.getHeader("accept");
        if (accept != null && accept.indexOf("application/json") != -1) {
            return true;
        }

        String xRequestedWith = request.getHeader("X-Requested-With");
        if (xRequestedWith != null && xRequestedWith.indexOf("XMLHttpRequest") != -1) {
            return true;
        }

        String uri = request.getRequestURI();
        if (StringUtils.inStringIgnoreCase(uri, ".json", ".xml")) {
            return true;
        }

        String ajax = request.getParameter("__ajax");
        if (StringUtils.inStringIgnoreCase(ajax, "json", "xml")) {
            return true;
        }
        return false;
    }

    /**
     * 获取完整的请求路径，包括：域名，端口，上下文访问路径
     * @return
     */
    public static String getServletUrl() {
        HttpServletRequest request = ServletUtils.getRequest();
        StringBuffer url = request.getRequestURL();
        String contextPath = request.getContextPath();
        return url.delete(url.length() - request.getRequestURI().length(), url.length()).append(contextPath).toString();
    }

    /**
     * 获取相对上下文访问路径
     * @return
     */
    public static String getContextPath() {
        HttpServletRequest request = ServletUtils.getRequest();
        return request.getContextPath();
    }

    /**
     * 获取相对上下文绝对路径
     * @return
     */
    public static String getRealPath() {
        HttpServletRequest request = ServletUtils.getRequest();
        return request.getServletContext().getRealPath("");
    }

    /**
     * -将headers转成MultiValueMap
     * @param request
     * @return
     */
    public static HttpHeaders getHeaders(HttpServletRequest request){
        MultiValueMap<String, String> map = CollectionUtils.toMultiValueMap(new LinkedCaseInsensitiveMap<>(8, Locale.ENGLISH));
        Enumeration<String> headers = request.getHeaderNames();
        if(null != headers) {
            while (headers.hasMoreElements()) { 
                String key = headers.nextElement();
                Enumeration<String> values = request.getHeaders(key);
                List<String> list = new ArrayList<String>();
                if(null != values) {
                    while (values.hasMoreElements()) {
                        String value = values.nextElement();
                        list.add(value);
                    }
                }
                map.put(key, list);
            }
        }
        return new HttpHeaders(new MultiValueMapAdapter<String, String>(map));
    }

    /**
     * -将参数parameter转成MultiValueMap
     * @param request
     * @return
     */
    public static MultiValueMap<String, String> getParameterMap(HttpServletRequest request){
        Enumeration<String> params = request.getParameterNames();
        MultiValueMap<String, String> map = CollectionUtils.toMultiValueMap(new LinkedCaseInsensitiveMap<>(8, Locale.ENGLISH));
        if(null != params) {
            while (params.hasMoreElements()) {
                String key = params.nextElement();
                String[] value = request.getParameterValues(key);
                map.put(key, Arrays.asList(value));
            }
        }
        return new MultiValueMapAdapter<String, String>(map);
    }

    /**
     * -将cookies转成MultiValueMap
     * @param request
     * @return
     */
    public static MultiValueMap<String, String> getCookieMap(HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        MultiValueMap<String, String> map = CollectionUtils.toMultiValueMap(new LinkedCaseInsensitiveMap<>(8, Locale.ENGLISH));
        if(null != cookies && cookies.length > 0) {
            Stream.of(cookies).forEach(cookie -> {
                List<String> values = map.get(cookie.getName());
                if(StringUtils.isNotEmpty(values)) {
                    values.add(cookie.getValue());
                }else {
                    values = Arrays.asList(cookie.getValue());
                }
                map.put(cookie.getName(), values);
            });
        }
        return new MultiValueMapAdapter<String, String>(map);
    }

    /**
     * -将Map<String, String[]>转成MultiValueMap
     * @param map
     * @return
     */
    public static MultiValueMap<String, String> toMultiValueMap(Map<String, String[]> map){
        MultiValueMap<String, String> result = CollectionUtils.toMultiValueMap(new LinkedCaseInsensitiveMap<>(8, Locale.ENGLISH));
        if(StringUtils.isNotEmpty(map)) {
            map.forEach((key, values) -> {
                result.put(key, Arrays.asList(values));
            });
        }
        return new MultiValueMapAdapter<String, String>(result);
    }
}
