package com.daffodil.util;

import cn.hutool.http.HttpRequest;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

/**
 * 
 * @author yweijian
 * @date 2025年3月12日
 * @version 2.0.0
 * @copyright Copyright 2020-2024 www.daffodilcloud.com.cn
 * @description
 */
public class BingImageUtils {

    public enum HPSIZE {
        HP_3840x2160("UHD"),
        HP_1920x1080("1920x1080"),
        HP_1366x768("1366x768"),
        HP_1024x768("1024x768"),
        HP_800x600("800x600");

        private final String size;

        HPSIZE(String size) {
            this.size = size;
        }

        public String size() {
            return size;
        }

        public static HPSIZE test(String size) {
            for (HPSIZE hp : HPSIZE.values()) {
                if (hp.size().equals(size)) {
                    return hp;
                }
            }
            return null;
        }
    }

    /**
     * -获取必应图片列表
     * @param dix 表示第几天，0表示当天，1表示明天，2表示后天，以此类推。
     * @param n 表示返回几张图片，1表示返回1张图片，2表示返回2张图片，以此类推。
     * @param hp 默认 HPSIZE.HP_1920x1080
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List<String> getImageUrls(int dix, int n, HPSIZE hp) {
        List<String> imageUrls = new ArrayList<>();
        dix = dix < 0 ? 0 : Math.min(dix, 7);
        n = n < 1 ? 1 : Math.min(n, 7);
        hp = hp == null ? HPSIZE.HP_1920x1080 : hp;
        String url = "https://cn.bing.com/HPImageArchive.aspx?format=js&idx=" + dix + "&n=" + n + "&mkt=zh-CN";
        String body = HttpRequest.get(url).charset(StandardCharsets.UTF_8).timeout(5000).execute().body();
        Map<String, Object> responses = JacksonUtils.toJavaObject(body, Map.class);
        List<?> images = JacksonUtils.getAsList(responses, "images");
        if (images != null && images.size() > 0) {
            for (Object image : images) {
                Map<String, Object> imageMap = (Map<String, Object>) image;
                String imageUrl = "https://cn.bing.com" + JacksonUtils.getAsString(imageMap, "urlbase") + "_" + hp.size() + ".jpg";
                imageUrls.add(imageUrl);
            }
        }
        return imageUrls;
    }

    /**
     * -获取随机必应图片地址
     * @param size
     * @return
     */
    public static String getRandomImageUrl(HPSIZE hp) {
        int dix = (int) (Math.random() * 7);
        int num = (int) (Math.random() * 7);
        List<String> imageUrls = BingImageUtils.getImageUrls(dix, num, hp);
        if(CollectionUtils.isEmpty(imageUrls)) {
            return null;
        }
        int inx = (int) (Math.random() * imageUrls.size());
        return imageUrls.get(inx);
    }

    /**
     * -获取今日必应图片地址
     * @param size
     * @return
     */
    public static String getTodayImageUrl(HPSIZE hp) {
        List<String> imageUrls = BingImageUtils.getImageUrls(0, 1, hp);
        if(CollectionUtils.isEmpty(imageUrls)) {
            return null;
        }
        return imageUrls.get(0);
    }

}
