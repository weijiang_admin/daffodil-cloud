package com.daffodil.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.UUID;

/**
 * 
 * @author yweijian
 * @date 2021年9月18日
 * @version 1.0
 * @description
 */
@Slf4j
public class JwtTokenUtils {
    
    /**
     * 私钥密匙
     */
    private static final String SECRET_KEY = "secret";
    
    /**
     * 公共信息
     */
    private static final String DATA_KEY = "data";
    
    /**
     * 当前时间
     */
    private static final String CURRENT_TIME_MILLIS_KEY = "currentTimeMillis";
    
    /**
     * token有效期30分钟=30 * 60 * 1000
     */
    public static final long EXPIRE_TIME = 30L * 60L * 1000L;
    
    /**
     * 私钥密匙
     * @param token
     * @return
     */
    public static String getSecret(String token) {
        if(StringUtils.isEmpty(token)) {
            return null;
        }
        Claim claim = getClaim(token, SECRET_KEY);
        return StringUtils.isNotNull(claim) ? claim.asString() : null;
    }
    
    /**
     * 获取公共信息
     * @param token
     * @return
     */
    public static String getData(String token){
        if(StringUtils.isEmpty(token)) {
            return null;
        }
        Claim claim = getClaim(token, DATA_KEY);
        return StringUtils.isNotNull(claim) ? claim.asString() : null;
    }
    
    /**
     * 获取token生成时间
     * @param token
     * @return
     */
    public static long getCurrentTimeMillis(String token) {
        if(StringUtils.isEmpty(token)) {
            return 0L;
        }
        Claim claim = getClaim(token, CURRENT_TIME_MILLIS_KEY);
        return StringUtils.isNotNull(claim) ? claim.asLong() : 0L;
    }
    
    /**
     * 获取Claim
     * @param token
     * @param name claim name
     * @return
     */
    public static Claim getClaim(String token, String name) {
        if(StringUtils.isEmpty(token)) {
            return null;
        }
        try {
            DecodedJWT jwt = JWT.decode(token);
            Claim claim = jwt.getClaim(name);
            return claim;
        }catch (JWTDecodeException e) {
            log.warn("解密token令牌异常：{}", e.getMessage());
        }
        return null;
    }

    /**
     * 验证token是否有效
     * @param token
     * @return
     */
    public static boolean isValid(String token) {
        if(StringUtils.isEmpty(token)) {
            return false;
        }
        try {
            DecodedJWT jwt = JWT.decode(token);
            String secret = jwt.getClaim(SECRET_KEY).asString();
            return isValid(secret, token);
        }catch (JWTDecodeException e) {
            log.warn("解密token令牌异常：{}", e.getMessage());
        }
        return false;
    }
    
    /**
     * 验证token是否有效
     * @param secret token私钥
     * @param token 加密的token
     * @return
     */
    public static boolean isValid(String secret, String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm).build();
            verifier.verify(token);
            return true;
        }catch (JWTVerificationException e) {
            log.warn(e.getMessage(), e);
        }
        return false;
    }
    
    /**
     * 判断token是否过期
     * @param token 加密的token
     * @return
     */
    public static boolean isExpireTime(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            long lastTimeMillis = jwt.getClaim(CURRENT_TIME_MILLIS_KEY).asLong();
            long currentTimeMillis = System.currentTimeMillis();
            return currentTimeMillis - lastTimeMillis >= EXPIRE_TIME;
        }catch (JWTDecodeException e) {
            log.warn("解密token令牌异常：{}", e.getMessage());
        }
        return true;
    }
    
    /**
     * 生成签名
     * @param secret 私钥密匙
     * @param data 公共信息
     * @return
     */
    public static String sign(String secret, String data) {
        long currentTimeMillis = System.currentTimeMillis();
        Date date = new Date(currentTimeMillis + EXPIRE_TIME);
        Algorithm algorithm = Algorithm.HMAC256(secret);
        String token = JWT.create()
                .withClaim(SECRET_KEY, secret)
                .withClaim(DATA_KEY, data)
                .withClaim(CURRENT_TIME_MILLIS_KEY, currentTimeMillis)
                .withExpiresAt(date)
                .sign(algorithm);
        return token;
    }
    
    /**
     * 随机UUID
     * @return
     */
    public static String uuid() {
        return UUID.randomUUID().toString();
    }
    
}
